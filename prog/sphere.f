! FDMNES subroutines
! Resolution de l'equation de Schrodinger dans la partie spherique
! des atomes. Calcul de ces fonctions sur les points en bordures.

      subroutine sphere(Absorbeur,Axe_Atom_Clui,Cal_xanes,Density,
     &            Dipmag,Ecinetic,eimag,energ,
     &            Enervide,Eseuil,Final_tddft,Full_atom,Green,
     &            Green_plus,Hubb,
     &            Hubbard,iaabsi,iapr,iaprotoi,ibord,icheck,ie,
     &            ip_max,ip0,is_t,ipr,konde,lmax,lmaxabs_t,
     &            lmax_probe,m_hubb,n_atom_0,n_atom_0_self,
     &            n_atom_ind,n_atom_ind_self,natome,nbord,nbseuil,nbtm,
     &            ninitls,nllm0,nllmm,nlmam,nlmam_t,nlmagm,
     &            nphiato1,nphiato7,npoint,npsom,nrato,
     &            nrm,nrm_self,nrm_tddft,nspin,nspin_t,nspino,nspino_t,
     &            numat,occ_diag_t,Octupole,phiato,posi,psii,rato,
     &            Relativiste,rhov_self,rmtg,rmtsd,rof,
     &            rofsd,rofsd_hd,sing_self,singul,singulsd,
     &            Solsing,Solsing_bess,Spinorbite,state_all,
     &            tau_ato,Tddft,V_hubbard,Vr,vrato,V0bd,xyz,zet)
 
      use declarations
      implicit real(kind=db) (a-h,o-z)

      complex(kind=db):: fnormc, integr_sing, sing, z
      complex(kind=db), dimension(0:lmax):: bess, neum
      complex(kind=db), dimension(nspin):: konde
      complex(kind=db), dimension(nspin_t):: e1, e2, s1, s2
      complex(kind=db), dimension(nspin_t,nspin_t):: ampl, taug, uu
      complex(kind=db), dimension(nlmam,nspin_t,nspin_t):: amplitg
      complex(kind=db), dimension(2,0:lmax,nspin):: bs, bssing,
     &                                              neuing, nm
      complex(kind=db), dimension(nlmagm,nspin,nlmagm,nspin,
     &                                   n_atom_0:n_atom_ind):: tau_ato
      complex(kind=db), dimension(nlmam_t,nspin_t,nspino_t,0:ip_max,
     &                                                   ninitls):: rof
      complex(kind=db), dimension(0:nrm_self,nllm0:nllmm,nspin,
     &    nspino,nspino,n_atom_0_self:n_atom_ind_self):: rhov_self
      complex(kind=db), dimension(nlmagm,nspin,nspino,nspino,
     &                                     n_atom_0:n_atom_ind):: rofsd

      complex(kind=db), dimension(:,:,:), allocatable :: us
      complex(kind=db), dimension(:), allocatable:: f_reg, f_irg 
      complex(kind=db), dimension(-m_hubb:m_hubb,nspino,-m_hubb:m_hubb,
     &        nspino,nspin,n_atom_0_self:n_atom_ind_self):: rofsd_hd
      complex(kind=db), dimension(nlmam,nspin,nlmam,nspin,ip0:ip_max,
     &                            nbseuil):: Singul

      integer icheck
      integer, dimension(2):: mm, mmh
      integer, dimension(nspino_t):: iso
      integer, dimension(natome):: iaprotoi, nbord
      integer, dimension(nbtm,natome):: ibord
      
      logical Absorbeur, Cal_xanes, Density, Dipmag, Ecomp, Ecompe, 
     &        Ecompr, Final_tddft, Full_atom,
     &        Green, Green_plus, Hubb, Hubb_m, Hubb_pot, Hubb_self, 
     &        Hubbard, Octupole, Relativiste, Self,     
     &        Solsing, Spherical_tensor, Spinorbite, Solsing_bess,
     &        State_all, Tddft

      real(kind=db):: konder
      real(kind=db), dimension(nbseuil):: Eseuil
      real(kind=db), dimension(3):: ps, w, x
      real(kind=db), dimension(0:lmax):: bessr, neumr
      real(kind=db), dimension(nspin):: Ecinetic, Vmax, V0bd
      real(kind=db), dimension(nbseuil):: vecond
      real(kind=db), dimension(nrm,nbseuil):: psii
      real(kind=db), dimension(npoint,nspin):: Vr
      real(kind=db), dimension(3,natome):: Axe_Atom_Clui, posi
      real(kind=db), dimension(4,npsom):: xyz
      real(kind=db), dimension(0:nrm):: rato
      real(kind=db), dimension(-m_hubb:m_hubb,nspin_t):: V_hubb
      real(kind=db), dimension(-m_hubb:m_hubb,nspin):: occ_diag_t
      real(kind=db), dimension(0:nrm,nspin):: vrato 
      real(kind=db), dimension(nlmagm,nspin,n_atom_0:n_atom_ind)::
     &                                                        singulsd
      real(kind=db), dimension(0:nrm_self,nllm0:nllmm,nspin, 
     &                      n_atom_0_self:n_atom_ind_self):: sing_self
      real(kind=db), dimension(nphiato1,nllm0:nllmm,
     &                 nspin,nspino,natome,nphiato7):: phiato
      real(kind=db), dimension(nrm_tddft,nlmam_t,nspino_t,nspin_t,
     &                                                  ninitls):: zet
      real(kind=db), dimension(:,:), allocatable :: g0, gc, gm, gp, gso
      real(kind=db), dimension(:), allocatable ::
     &      cgradm, cgradp, cgrad0, claplm, claplp, clapl0, fct, f2,
     &      phi, r, rr, t, tt
      real(kind=db), dimension(:,:), allocatable:: ui, ur, V
      real(kind=db), dimension(:,:,:)  , allocatable:: uis, urs
      real(kind=db), dimension(:,:,:,:), allocatable:: uiss, urss

      common/ad/ ad
      common/iopsymp/ iopsymp(nopsm)
      common/spheric/ Spherical_tensor 
      common/v_intmax/ v_intmax

      if( icheck > 0 ) write(3,120) iapr, numat, lmax

      Solsing_bess = .false.
!      Solsing_bess = .true.
      
! Hubb_pot indique si l'atome a ete calcule Hubbard auparavant (c.a.d. il est de 
! la bonne espece chimique et il se trouve dans le petit agregat)
      Hubb_pot = .false.
      if( Hubb ) then
        if( Full_atom ) then
          if( iapr <= n_atom_ind_self ) then
            Hubb_pot = .true.
          else
            Hubb_pot = .false.
          end if
        else
          do ia1 = 1, natome
            ipr1 = iaprotoi(ia1)
            if( ipr1 == iapr ) exit
          end do
          if( ia1 == natome + 1 ) then
            Hubb_pot = .false.
          else
            Hubb_pot = .true.
          end if
        end if
      end if

      Self = .not. Cal_xanes
      Hubb_self = Hubb_pot .and. Self 

      if( abs(eimag) > eps10 .or. Ecinetic(1) < eps10
     &     .or. Ecinetic(nspin) < eps10 ) then
        Ecomp = .true.
      else
        Ecomp = .false.
      endif
      Ecompe = Ecomp
      Ecompr = Ecomp

      iso(1) = 2
      iso(nspino_t) = 1

! alfa_sf = constante de structure fine.
      a2s4 = 0.25_db * alfa_sf**2

      if( Final_tddft .and. nbseuil /= ninitls ) then
        ninit1 = ( ninitls - 2 ) / 2
        if( is_t <= ninit1 ) then
          iseuil_t = 1
        else
          iseuil_t = 2
        endif
      else
        iseuil_t = is_t
      endif
      if( Final_tddft ) then
        ns1 = is_t
        ns2 = is_t
      else
        ns1 = 1
        ns2 = nbseuil
      endif

      if( Full_atom ) then
        ia = iapr
      else
        do ia = 1,natome
          if( iaprotoi(ia) == ipr ) exit
        end do
        if( ia == natome+1 ) then
          do ia = 1,natome
            if( iaprotoi(ia) == 0 ) exit
          end do
        endif
      endif

      if( Hubb_pot ) then

        V_hubb(:,:) = 0._db
        lh = l_hubbard( numat )
        pop_0 = sum( occ_diag_t(-lh:lh,1:nspin) ) / (( 2*lh + 1 )*nspin)
        do m = - lh,lh 
          do isp = 1,nspin
            V_hubb(m,isp) = - 0.5_db * V_hubbard 
     &                    * ( nspin * occ_diag_t(m,isp) - pop_0 )
          end do
        end do

        if( icheck > 0 .and. ie == 1 ) then
          if( nspin == 1 ) then
            write(3,130) lh
          else
            write(3,135) lh
          endif
          do m = - lh,lh 
            write(3,140) m, ( V_hubb(m,isp)*rydb, occ_diag_t(m,isp),
     &                        isp = 1,nspin )
          end do
        endif

      end if

! Maillage du rayon et potentiel

      nr = nrato

! Il faut eventuellement extrapoler jusqu'au point le plus loin
      rmax = max( rmtsd, rmtg ) + eps10

      if( .not. Green ) then
        do ia = 1,natome
          if( iaprotoi(ia) == ipr ) then
            ps(1:3) = posi(1:3,ia)
            do ib = 1,nbord(ia)
              i = ibord(ib,ia)
              x(1:3) = xyz(1:3,i)
              call posrel(x,ps,w,rrel,isym)
              if( rrel > rmax ) then
                rmax = rrel
                iii = i
              endif
            end do
          endif
        end do
      else
        iii = nr
      endif

      if( rmax > rato(nr) ) then
        rextra = rmax - rato(nr)
        Vmax(:) = Vr(iii,:)
        n = int( rextra / ( rato(nr) - rato(nr-1) ) ) + 1
        nr = nr + n
      endif

      allocate( r(0:nr) )
      allocate( g0(nr,nspin_t) ); allocate( gc(nr,nspin_t) )
      allocate( gm(nr,nspin_t) ); allocate( gp(nr,nspin_t) )
      allocate( cgradm(nr) ); allocate( cgradp(nr) )
      allocate( cgrad0(nr) ); allocate( claplm(nr) )
      allocate( claplp(nr) ); allocate( clapl0(nr) )
      allocate( f2(nr) )
      allocate( V(nr,nspin) )
      if( Spinorbite )  allocate( gso(nr,nspino_t) )

      r(1:nrato) = rato(1:nrato)
      r(0) = r(1)**2 / r(2)
      V(1:nrato,:) = vrato(1:nrato,:)

      if( rmax > rato(nrato) ) then
        dh = rextra / n
        do ir = nr-n+1, nr
          r(ir) = r(ir-1) + dh
          p2 = ( r(ir) - rato(nrato) ) / rextra
          p1 = 1._db - p2
          V(ir,:) = p1 * V(nrato,:) + p2 * Vmax(:)
        end do
      endif

      if( v_intmax < 1000._db ) then
        do ispin = 1,nspin
          do ir = 1,nr
            V(ir,ispin) = min( V(ir,ispin), v_intmax )
          end do
        end do 
      endif

      do ir = 1,nr
        if( r(ir) > rmtsd + eps10 ) exit
      end do
      nrmtsd = ir

      rmtgg = rmtg
      do ir = 1,nr
        if( r(ir) > rmtgg-eps10 ) exit
      end do
      nrmtgg = ir
! n'a pas d'importance en FDM
      do ispin = 1,nspin
        V(nrmtgg:nr,ispin) = V0bd(ispin)
      end do

      if( icheck > 1 ) then
        if( Final_tddft ) write(3,145) is_t, iseuil_t
        write(3,146) Energ*rydb, Enervide*rydb
        write(3,147) Ecinetic(:)*rydb
        write(3,148) V0bd(:)*rydb
        write(3,149) konde(:)
      endif

      if( icheck > 2 ) then
        if( nspin == 2 ) then
          write(3,'(/A)') '    Radius         V(up)         V(dn)'
        else
          write(3,'(/A)') '    Radius           V'
        endif
        do ir = 1,nr
          write(3,150) r(ir)*bohr, V(ir,:)*rydb
        end do
      endif

      do ir = 1,nr-1
        rp = r(ir+1)
        rm = r(ir-1)
        r0 = r(ir)
        dr = 0.5 * ( rp - rm )
        claplm(ir) = 1 / ( ( r0 - rm ) * dr )
        claplp(ir) = 1 / ( ( rp - r0 ) * dr )
        clapl0(ir) = - claplm(ir) - claplp(ir)
        if( Spinorbite .or. Relativiste ) then
          cgradm(ir) = ( rp - r0 ) / ( ( rm - r0 ) * ( rp - rm ) )
          cgradp(ir) = ( rm - r0 ) / ( ( rp - r0 ) * ( rm - rp ) )
          cgrad0(ir) = - cgradm(ir) - cgradp(ir)
        endif
        f2(ir) = 1 / r(ir)**2
      end do

      do ir = 1,nr-1
        do is = 1,nspin_t
          js = min(is,nspin)

          vme = V(ir,js) - enervide
          g0(ir,is) = - clapl0(ir) + vme
          gm(ir,is) = - claplm(ir)
          gp(ir,is) = - claplp(ir)
          if( ir == nr ) cycle

          if( Relativiste .or. Spinorbite ) then

            bder = 1 / ( 1 - a2s4 * vme )
            if( ir == 1 ) then
              dvr = 2 * numat / r(1)**2
            else
              dvr = cgradm(ir) * V(ir-1,js)
     &            + cgrad0(ir) * V(ir,js)
     &            + cgradp(ir) * V(ir+1,js)
            endif
            fac = a2s4 * bder * dvr

            if( Relativiste ) then
              g0(ir,is) = g0(ir,is) - a2s4 * vme**2
     &                   - fac * ( cgrad0(ir) - 1 / r(ir) )
              gm(ir,is) = gm(ir,is) - fac * cgradm(ir)
              gp(ir,is) = gp(ir,is) - fac * cgradp(ir)
            endif
            if( Spinorbite ) gso(ir,is) = fac / r(ir)

          endif

        end do
      end do

      if( nspin_t == 2 .and. nspino_t == 1 ) then
        nspinorb = 2
      else
        nspinorb = 1
      endif

      do iseuil = 1,nbseuil
        ephoton = energ + Eseuil(iseuil)
! Terme multiplicatif pour les transitions quadrupolaires
! En S.I. vecond = k = E*alfa_sf*4*pi*epsilon0 / (e*e)
! En ua et rydb : k = 0.5 * alfa_sf * E
        vecond(iseuil) = 0.5 * alfa_sf * ephoton
      end do

      amplitg(:,:,:) = (0._db,0._db)

      allocate( urs(0:nr,nspin_t,nspin_t) )
      allocate( uis(0:nr,nspin_t,nspin_t) )
      urs(:,:,:) = 0._db
      uis(:,:,:) = 0._db

      if( Hubb_self ) then
        allocate( urss(nr,-m_hubb-1:m_hubb,nspin,nspin) ) 
        allocate( uiss(nr,-m_hubb-1:m_hubb,nspin,nspin) )
        urss(:,:,:,:) = 0._db 
        if( Ecomp) uiss(:,:,:,:) = 0._db
      endif

! Bessel et Neuman aux rayons Rmtg et Rmtsd

      do isp = 1,nspin

        if( Ecompe ) then
          konder = real( abs(konde(isp)),db )
        else
          konder = sqrt( Ecinetic(isp) )
        endif
        fnorm = sqrt( konder / pi )
        fnormc = sqrt( konde(isp) / pi )

        j = 0
        dh = 0.0001_db
        do i = -1,1,2
          j = j + 1
          if( Ecomp ) then
            z = konde(isp) * ( rmtgg + i * 0.5_db * dh )
            call cbessneu(fnormc,z,lmax,lmax,bess,neum)
            bs(j,0:lmax,isp) = bess(0:lmax)
            nm(j,0:lmax,isp) = neum(0:lmax)
          else
            zr = konder * ( rmtgg + i * 0.5_db * dh )
            call cbessneur(fnorm,zr,lmax,lmax,bessr,neumr)
            bs(j,0:lmax,isp) = cmplx( bessr(0:lmax), 0._db,db )
            nm(j,0:lmax,isp) = cmplx( neumr(0:lmax), 0._db,db )
          endif
        end do
        do l = 0,lmax
          bs(2,l,isp) = ( bs(2,l,isp) - bs(1,l,isp) ) / dh
          nm(2,l,isp) = ( nm(2,l,isp) - nm(1,l,isp) ) / dh
        end do

        if( Ecomp ) then
          z = konde(isp) * rmtgg
          call cbessneu(fnormc,z,lmax,lmax,bess,neum)
          bs(1,0:lmax,isp) = bess(0:lmax)
          nm(1,0:lmax,isp) = neum(0:lmax)
        else
          zr = konder * rmtgg
          call cbessneur(fnorm,zr,lmax,lmax,bessr,neumr)
          bs(1,0:lmax,isp) = cmplx( bessr(0:lmax), 0._db,db )
          nm(1,0:lmax,isp) = cmplx( neumr(0:lmax), 0._db,db )
        endif

        if( Solsing ) then

          n = max(nrmtsd,nrmtgg)
          do jr = 1,2
            if( Ecomp ) then
              z = konde(isp) * r(n-1+jr)
              call cbessneu(fnormc,z,lmax,lmax,bess,neum)
              bssing(jr,0:lmax,isp) = bess(0:lmax)
              neuing(jr,0:lmax,isp) = neum(0:lmax)
            else
              zr = konder * r(n-1+jr)
              call cbessneur(fnorm,zr,lmax,lmax,bessr,neumr)
              bssing(jr,0:lmax,isp) = cmplx( bessr(0:lmax), 0._db,db )
              neuing(jr,0:lmax,isp) = cmplx( neumr(0:lmax), 0._db,db )
            endif
          end do

        endif

      end do

      Er = sum( Ecinetic(:) ) / nspin

      do l = 0,lmax

        if( Hubb_pot .and. l == l_hubbard( numat ) )  then
          Hubb_m = .true.
        else
          Hubb_m = .false.
        endif  

        l2 = l * ( l + 1 )
        lm0 = l2 + 1
        do isp = 1,nspin_t
          gc(1:nr,isp) = g0(1:nr,isp) + l2 * f2(1:nr)
        end do

! La continuite est : amp * ur(r) = bessel(r) - i * tau * hankel_sortant
! Donc tau = k * tau_habituel
        do isp = 1,nspin_t
          jsp = min( isp, nspin )
          e1(isp) = bs(1,l,jsp)
          e2(isp) = bs(2,l,jsp)
          s1(isp) = - img * bs(1,l,jsp) + nm(1,l,jsp)
          s2(isp) = - img * bs(2,l,jsp) + nm(2,l,jsp)
        end do

        if( Spinorbite ) then
          nm1 = - l - 1
          nm2 = l
        elseif( Hubb_m ) then
          nm1 = - l
          nm2 = l
        else
          nm1 = 0
          nm2 = 0
        endif
               
        do m = nm1,nm2

          if( Hubb_m ) then
            lm1 = lm0 + m
            lm2 = lm0 + m
          else
            lm1 = lm0 - l
            lm2 = lm0 + l
          endif

          mm(1) = m
          mmh(1) = m
          if( Spinorbite ) then
            mm(2) = - ( m + 1 )
            mmh(2) = m + 1
          else
            mm(2) = m
            mmh(2) = m
          endif

          if( m == nm1 .or. m == nm2 .or. .not. Spinorbite ) then
            nsol = 1
          else
            nsol = 2
          endif

          do isol = 1,nsol   ! boucle sur les 2 solutions a l'origine

            allocate( ur(0:nr,nspin_t) )
            allocate( ui(0:nr,nspin_t) )
            ui(:,:) = 0._db

            call sch_radial(Ecomp,Ecompr,Eimag,Er,gc,gm,gp,gso,
     &              Hubb_m,isol,l,m,m_hubb,mm,mmh,nm1,nr,nr,nsol,
     &              nspin_t,nspino_t,numat,r,Relativiste,Spinorbite,
     &              ui,ur,V_hubb)

            if( nsol == 1 ) then
              do isp = 1,nspin_t
                urs(:,isp,isp) = ur(:,isp)
                uis(:,isp,isp) = ui(:,isp)
              end do
            else
              urs(:,:,isol) = ur(:,:)
              uis(:,:,isol) = ui(:,:)
            endif

            deallocate( ur )
            deallocate( ui )

          end do

          if( icheck > 2 ) then
            ipas = 10
            if( icheck > 3 ) ipas = 1
            if( Spinorbite .or. Hubb_m ) then
              write(3,180) l, m, numat
            else
              write(3,190) l, numat
            endif
            if( Spinorbite ) then
              if( Ecomp ) then
                write(3,200)
              else
                write(3,210)
              endif
            else
              if( Ecomp ) then
                write(3,'(A)') '     Radius          ur          ui'
              else
                write(3,'(A)') '     Radius          ur'
              endif
            endif
            do ir = 1,nr,ipas
              if( Spinorbite ) then
                if( Ecomp ) then
                  write(3,220) r(ir)*bohr, ( ( urs(ir,isp,i),
     &                  uis(ir,isp,i), isp = 1,nspin_t ), i = 1,nspin_t)
                else
                  write(3,220) r(ir)*bohr,
     &                        ( urs(ir,1:nspin_t,i), i = 1,nspin_t )
                endif
              else
                if( Ecomp ) then
                  write(3,220) r(ir)*bohr, ( urs(ir,isp,isp),
     &                    uis(ir,isp,isp), isp = 1,nspin_t )
                else
                  write(3,220) r(ir)*bohr,
     &                        ( urs(ir,isp,isp), isp = 1,nspin_t )
                endif
              endif
            end do
          endif

! ui et us sont les solutions de l eq Schrodinger radiale, soit r*fct
! urs et uis sont fct vraie

          if( Spinorbite ) then
            do isol = 1,nspin_t  ! boucle sur les 2 solutions a l'origine
              do isp = 1,nspin_t
                if( ( ( m == nm1 .and. isp == 1 ) .or.
     &               ( m == nm2 .and. isp == 2 ) ) ) cycle
                urs(1:nr,isp,isol) = urs(1:nr,isp,isol) / r(1:nr)
                if(Ecomp) uis(1:nr,isp,isol) =uis(1:nr,isp,isol)/r(1:nr)
              end do
            end do   ! fin de la boucle sur les solutions
          else
            do isp = 1,nspin_t
              urs(1:nr,isp,isp) = urs(1:nr,isp,isp) / r(1:nr)
              if( Ecomp ) uis(1:nr,isp,isp) = uis(1:nr,isp,isp) /r(1:nr)
            end do
          endif

          if( Ecomp ) then
            call renormalc(ampl,taug,m,nm1,nm2,nspin_t,uis,urs,r,
     &                    nrmtgg,nr,rmtgg,s1,s2,e1,e2,Spinorbite,icheck)
          else
            call renormal(ampl,taug,m,nm1,nm2,nspin_t,urs,r,
     &                    nrmtgg,nr,rmtgg,s1,s2,e1,e2,Spinorbite,icheck)
          endif

! Renormalisation des fonctions d'onde :
! Le deuxieme indice va correspondre a la solution correspondant au spin
! d'attaque.
          if( Spinorbite ) then
            do ir = 1,nr
              if( Ecomp ) then
                uu(:,:) = cmplx( urs(ir,:,:), uis(ir,:,:),db )
              else
                uu(:,:) = cmplx( urs(ir,:,:), 0._db,db )
              endif
              do isp = 1,nspin_t  ! spin de la fonction d'onde
                do isq = 1,nspin_t ! spin d'attaque (du bessel attaquant)
                  z = sum( ampl(isq,:) * uu(isp,:) )
                  urs(ir,isp,isq) = real( z,db )
                  if( Ecomp ) uis(ir,isp,isq) = aimag( z )
                end do
              end do
            end do
          else
            do ir = 1,nr
              do isp = 1,nspin_t
                if( Ecomp ) then
                  z = cmplx( urs(ir,isp,isp), uis(ir,isp,isp),db )
     &              * ampl(isp,isp) 
                  urs(ir,isp,isp) = real( z,db )  
                  uis(ir,isp,isp) = aimag( z )
                else
                  urs(ir,isp,isp) = urs(ir,isp,isp)
     &                            * real( ampl(isp,isp),db )  
                endif
              end do
            end do
          endif

          if( Final_tddft .and. l <= lmaxabs_t ) then

            do isp = 1,nspin_t  ! spin de la fonction d'onde

              if( Spinorbite ) then

                mv = m + isp - 1   ! vrai
                if( mv < -l .or. mv > l ) cycle
                lmv = lm0 + mv 
                do isq = 1,nspino_t 
                  zet(1:nr,lmv,isq,isp,is_t) = urs(1:nr,isp,isq)
                end do

              else

                do lm = lm1,lm2
                  zet(1:nr,lm,1,isp,is_t) = urs(1:nr,isp,isp)
                end do

              endif

            end do

          end if
      
! Integrale radiale pour la regle d'or de Fermi
          if( Absorbeur .and. ( ( Tddft .and. l <= lmaxabs_t )
     &                          .or. l <= lmax_probe ) )
     &      call radial_matrix(Ecomp,Final_tddft,Green_plus,
     &         ip_max,iseuil_t,lm0,lm1,lm2,m,nbseuil,ninitls,
     &         nlmam_t,nm1,nm2,nr,nrm,nrmtsd,ns1,ns2,nspin_t,nspino_t,
     &         psii,r,rmtsd,rof,Spinorbite,uis,urs,vecond)

          if( Final_tddft ) cycle

! Integrale radiale pour la densite d'etat
          if( ( Density .and. Absorbeur ) .or. State_all .or. Self )
     &      call radial_stdens(Ecomp,Hubbard,iapr,l,
     &           lm,lm0,lm1,lm2,m,n_atom_0,n_atom_0_self,n_atom_ind,
     &           n_atom_ind_self,nllm0,nllmm,nlmagm,nm1,nm2,nr,
     &           nrm_self,nrmtsd,nspin,nspino,r,rhov_self,rmtsd,rofsd,
     &           Self,Spinorbite,uis,urs)

          if( Spinorbite ) then
            do isp1 = 1,nspino
              m1 = m + isp1 - 1  ! vrai
              if( m1 < -l .or. m1 > l ) cycle
              lmv1 = lm0 + m1
              do isp2 = 1,nspino
                m2 = m + isp2 - 1
                if( m2 < -l .or. m2 > l ) cycle
                lmv2 = lm0 + m2
                tau_ato(lmv1,isp1,lmv2,isp2,iapr) = taug(isp1,isp2)
                if( Absorbeur .and. Green 
     &                        .and. ( Tddft .or. lmax <= lmax_probe ) )
     &              amplitg(lmv1,isp1,isp2) = ampl(isp1,isp2)
              end do
            end do
          else
            do lm = lm1,lm2
              do isp = 1,nspin
                tau_ato(lm,isp,lm,isp,iapr) = taug(isp,isp)
                if( Absorbeur .and. Green
     &                        .and. ( Tddft .or. lmax <= lmax_probe ) )
     &            amplitg(lm,isp,isp) = ampl(isp,isp)
              end do
            end do
          endif

! Recopies pour Hubbard:
          if( Hubb_m .and. Hubb_self ) then
            urss(1:nr,m,:,:) = urs(1:nr,:,:)
            if( Ecomp ) uiss(1:nr,m,:,:) = uis(1:nr,:,:)
          end if

! Calcul de la solution singuliere
          if( Solsing .and. (Absorbeur .or. Self .or. state_all) ) then
!          if( ( Solsing .and. Absorbeur ) .or. 
!     &        ( Ecomp .and. ( Self .or. ( Density .and. Absorbeur)
!     &            .or. state_all ) ) ) then

            n = max(nrmtsd,nrmtgg) 
            allocate( f_reg(n) )
            allocate( f_irg(n) )
            allocate( rr(n) )
            allocate( phi(n) )
            allocate( us(n+1,nspin,nspin) )

            rr(1:n) = r(1:n) 

            us(:,:,:) = (0._db,0._db)

            if( nsol == 1 ) then

              do ispinorb = 1,nspinorb

                if( Spinorbite .and. m == nm1 ) then
                  isp = nspino
                else
                  isp = ispinorb
                endif

                if( Solsing_bess ) then
                  us(n,isp,isp) = - bssing(1,l,isp) * r(n) 
                  us(n+1,isp,isp) = - bssing(2,l,isp) * r(n+1)
                else
                  us(n,isp,isp) = taug(isp,isp) * ( neuing(1,l,isp)
     &                          - img * bssing(1,l,isp) ) * r(n) 
                  us(n+1,isp,isp) = taug(isp,isp) * ( neuing(2,l,isp)
     &                            - img * bssing(2,l,isp) ) * r(n+1)
                endif

                do ir = n,2,-1
                  im = ir - 1
                  ip = ir + 1
                  td = gc(ir,isp)
                  if( Hubb_m .and. ir < n )
     &              td = td + V_hubb(mmh(isp),isp)
                  if( Ecomp .and. .not. Ecompr ) then  
                    us(im,isp,isp) = - ( td * us(ir,isp,isp) 
     &                      + gp(ir,isp) * us(ip,isp,isp) ) / gm(ir,isp)
                  elseif( Ecomp ) then  
                    us(im,isp,isp) = - ( (td - img*eimag)*us(ir,isp,isp) 
     &                      + gp(ir,isp) * us(ip,isp,isp) ) / gm(ir,isp)
                  else
                    us(im,isp,isp) = - ( td * us(ir,isp,isp) 
     &                      + gp(ir,isp) * us(ip,isp,isp) ) / gm(ir,isp)
                  endif
                end do

              end do

            else
              
              fac = sqrt( ( l - m ) * ( l + m + 1._db ) )

              do isp = 1,nspino  ! spin d'attaque

                if( Solsing_bess ) then
                  us(n,isp,isp) = - bssing(1,l,isp) * r(n)
                  us(n+1,isp,isp) = - bssing(2,l,isp) * r(n+1)
                  ispp = 3 - isp
                  us(n,ispp,isp) = - bssing(1,l,ispp) * r(n)
                  us(n+1,ispp,isp) = - bssing(2,l,ispp) * r(n+1)
                else
! Attention que la solution singuliere est deja multipliee par tau !
                  us(n,isp,isp) = taug(isp,isp) * ( neuing(1,l,isp)
     &                          - img * bssing(1,l,isp) ) * r(n)
                  us(n+1,isp,isp) = taug(isp,isp) * ( neuing(2,l,isp)
     &                          - img * bssing(2,l,isp) ) * r(n+1)
                  ispp = 3 - isp
                  us(n,ispp,isp) = taug(isp,ispp)  * ( neuing(1,l,ispp)
     &                           - img * bssing(1,l,ispp) ) * r(n)
                  us(n+1,ispp,isp) = taug(isp,ispp) * ( neuing(2,l,ispp)
     &                           - img * bssing(2,l,ispp) ) * r(n+1)
                endif

                do ir = n,2,-1
                  im = ir - 1
                  ip = ir + 1
 
                  do isq = 1,2   ! spin interne (spin reel)

                    td = gc(ir,isq)
                    if( mm(isq) /= 0 ) td = td  + mm(isq) * gso(ir,isq) 

                    if( Hubb_m .and. ir < n )
     &                td = td + V_hubb(mmh(isq),isq)

                    if( Ecomp .and. .not. Ecompr ) then
                      us(im,isq,isp) =
     &                     - ( td * us(ir,isq,isp) 
     &                           + gp(ir,isq) * us(ip,isq,isp) 
     &              + fac * gso(ir,isq) * us(ir,iso(isq),isp) )
     &                               / gm(ir,isq)

                    elseif( Ecomp ) then
                      us(im,isq,isp) =
     &                     - ( ( td - img * eimag ) * us(ir,isq,isp) 
     &                           + gp(ir,isq) * us(ip,isq,isp) 
     &              + fac * gso(ir,isq) * us(ir,iso(isq),isp) )
     &                               / gm(ir,isq)

                    else
                      us(im,isq,isp) = - ( td * us(ir,isq,isp) 
     &                         + gp(ir,isq) * us(ip,isq,isp) 
     &              + fac * gso(ir,isq) * us(ir,iso(isq),isp) )
     &                               / gm(ir,isq)
                    endif

                  end do

                end do
              end do

            endif

            if( Solsing .and. Absorbeur .and.
     &                         ( ( Tddft .and. l <= lmaxabs_t )
     &              .or. ( .not. Tddft .and. l <= lmax_probe) ) ) then
! Le cas du dipole-Octupole, c'est dipole bra - Octupole ket.
              do ip = ip0,ip_max  ! boucle sur dipole, quadrupole, Octupole

                do iseuil = 1,nbseuil

                  select case(ip)
                    case(0)
! correspond a (k/Eph)**2 = (alfa_sf/2)**2
                      f = a2s4 
                    case(1)
                      f = 1._db
                    case(2)
                      f = ( 0.5_db * vecond(iseuil) )**2
                    case(3)
! correspond en fait a la normalisation du terme dipole-Octupole
                      f = - ( 1._db / 6 ) * vecond(iseuil)**2 
                  end select

                  if( ip == 0 ) then
                    phi(1:n) = psii(1:n,iseuil)
                  else
                    phi(1:n) = psii(1:n,iseuil) * r(1:n)**ip
                  endif

                  do isp = 1,nspin
                    if( Spinorbite ) then
                      mv = m + isp - 1
                      if( mv < - l .or. mv > l ) cycle
                      lmp = lm0 + mv  ! correspond au lm vrai
                    endif
! us est en fait r*us, mais urs n'est pas multiplie par r.
                    do isq = 1,nspin
                      if( Spinorbite .and. ( m == nm1 .or. m == nm2 )
     &                      .and. isq /= isp ) cycle

! Solutions regulieres et irregulieres multipliees par r.
                      if( Ecomp ) then
                        f_reg(1:n) = cmplx( urs(1:n,isp,isq),
     &                                    uis(1:n,isp,isq),db ) * r(1:n)
                      else
                        f_reg(1:n) = cmplx( urs(:,isp,isq), 0._db,db )
     &                               * r(1:n)
                      endif
! La solution irreguliere est multipliee par tau.
                      f_irg(1:n) = us(1:n,isp,isq)  
                      sing = 
     &                   integr_sing(n,phi,f_reg,f_irg,rmtsd,rr,icheck)
    
                      if( Spinorbite ) then
                        lmq = lm0 + m + isq - 1  ! correspond au lm vrai
                        singul(lmp,isp,lmq,isq,ip,iseuil) = - f * sing
                      else
                        do lm = lm1,lm2
                          singul(lm,isp,lm,isq,ip,iseuil) = - f * sing
                        end do
                      endif
                    end do
                  end do
                
                end do ! fin boucle seuil

              end do ! fin boucle dipole, quadrupole

            endif

            deallocate( phi )
            deallocate( f_reg )
            deallocate( f_irg )

! Calcul de la partie singuliere de la densite d'etat
            if( Ecomp .and. ( Self .or. ( Density .and. Absorbeur)
     &            .or. state_all ) .and. .not. final_tddft ) then

              allocate( fct(n) )
              allocate( t(n) ) ; allocate( tt(n) )
              do isp = 1,nspin
                if( Spinorbite ) then
                  mv = m + isp - 1
                  if( mv < - l .or. mv > l ) cycle
                  lm = lm0 + mv  ! correspond au lm vrai
                endif
! us est en fait r*us, mais urs n'est pas multiplie par r.
                do isq = 1,nspin
                  if( Spinorbite .and. ( m == nm1 .or. m == nm2 )
     &                  .and. isq /= isp ) cycle
! La solution singuliere contient la multiplication par tau !
! Ici on prend r = r', car on calcule la trace de G(r,r')
! On ne calcule que la partie imaginaire.
                  t(1:n) = ( urs(1:n,isp,isq) * aimag(us(1:n,isp,isq))
     &                  + uis(1:n,isp,isq) * real(us(1:n,isp,isq),db) )
                  fct(1:n) = t(1:n) * r(1:n)
                  tt(1:n) = t(1:n) / ( quatre_pi * r(1:n) )
                  
                  radl = f_integr3(rr,fct,n,1,n,rmtsd)

                  if( Spinorbite ) then
                    singulsd(lm,isp,iapr) = singulsd(lm,isp,iapr) - radl
                    if( Self ) sing_self(1:n,lm,isp,iapr) =
     &                       sing_self(1:n,lm,isp,iapr) - tt(1:n)
                  else
                    singulsd(lm1:lm2,isp,iapr) = 
     &                             singulsd(lm1:lm2,isp,iapr) - radl
                    if( Self ) then
                      do ir = 1, n
                        if ( Hubbard ) then
                          sing_self(ir,lm1:lm2,isp,iapr) =
     &                    sing_self(ir,lm1:lm2,isp,iapr) - tt(ir)
                        else
                          sing_self(ir,l,isp,iapr) =
     &                      sing_self(ir,l,isp,iapr) - tt(ir)
                       end if
                      end do
                    endif
                  endif  

                end do
              end do
              deallocate( fct )
              deallocate( t ); deallocate( tt )

            endif
           
            if( icheck > 3 ) then
              if( Spinorbite ) then
                write(3,250) l, m, numat
              else
                write(3,260) l, numat
              endif
              do ir = 1,n
                if( Spinorbite ) then
                  write(3,220) r(ir)*bohr, (us(ir,:,isp), isp =1,nspino)
                else
                  write(3,220) r(ir)*bohr, (us(ir,isp,isp), isp=1,nspin)
                endif
              end do
            endif
            if( icheck > 2 .and. Self ) then
              if( Spinorbite ) then
                write(3,180) l, m, numat
              else
                write(3,190) l, numat
              endif
              if( nspin == 2 ) then
                write(3,263)
              else
                write(3,264)
              endif
              if( Spinorbite ) then
                lms = lm
              elseif( Hubbard ) then
                lms = l**2 + l + 1 + m
              else
                lms = l
              endif
              do ir = 1,n
                f = quatre_pi* r(ir)**2 
                write(3,220) r(ir)*bohr, 
     &            ( f * sing_self(ir,lms,isp,iapr),
     &              isp = 1,nspin ),
     &            ( f * rhov_self(ir,lms,isp,isp,isp,iapr),
     &              isp = 1,nspin )
              end do
            endif

            deallocate( us )
            deallocate( rr )

          endif

! Interpolation pour avoir les fonctions radiales phiato.
          if( .not. Green ) then
            if( m == nm2 .and. l == lmax ) then
              iwrite = 1
            else
              iwrite = 0
            endif
            do ib = 1,natome
              if( Full_atom ) then
                if( ib > 1 ) exit
                ia = iapr
              else
                ia = ib
                if( iaprotoi(ia) /= ipr ) cycle
              endif
              if( nspin == 2 ) then
                cosang =sum(Axe_Atom_Clui(:,ia)*Axe_Atom_Clui(:,iaabsi)) 
                if( abs(cosang - 1) < eps4 ) then
                  iang = 1
                elseif( abs(cosang + 1) < eps4 ) then
                  iang = -1
                else
                  iang = 0
                endif
              else
                iang = 1
              endif
              call cal_phiato(Hubb_m,Hubbard,ia,iang,ibord,icheck,
     &              iwrite,l,lm0,lm1,lm2,lmax,m,natome,nbord(ia),
     &              nbtm,nllm0,nllmm,nm1,nm2,nphiato1,nphiato7,
     &              npsom,nr,nspin,nspino,phiato,posi,r,
     &              Spinorbite,uis,urs,xyz)
            end do
          endif
        end do   ! fin de la boucle sur m

      end do   ! fin de la boucle sur l

      if( Hubb_self ) then

        l = l_hubbard(numat) 
! Integrale radiale non diagonale pour la densite d'etat en cas de
! Hubbard
        call radial_stdens_hubb(Ecomp,iapr,l,m_hubb,n_atom_0_self,
     &               n_atom_ind_self,nr,nrmtsd,nspin,nspino,r,Rmtsd,
     &               rofsd_hd,uiss,urss,Spinorbite)

        deallocate( urss )
        deallocate( uiss )

      endif

      if( ( ( icheck > 1 .and. Cal_xanes ) .or.  
     &      ( icheck > 2 .and. Self ) ) .and. Absorbeur  ) then

        do is = ns1,ns2
          if( Ecomp ) then
            if( Octupole ) then
              write(3,270) is
            else
              write(3,275) is
            endif
          else
            if( Octupole ) then
              write(3,280) is
            else
              write(3,285) is
            endif
          endif
          do l = 0,lmax
            if( ( Tddft .and. l > lmaxabs_t ) .or. 
     &             ( .not. Tddft .and. l > lmax_probe ) ) exit
            if( Hubb_pot .and. l == l_hubbard( numat ) )
     &        then
              Hubb_m = .true.
            else
              Hubb_m = .false.
            endif
            lm0 = l**2 + l + 1
            do m = -l,l
              if( .not. ( Spinorbite .or. Hubb_m ) .and. m /= 0 ) cycle                                           
              lm = lm0 + m
              do isp = 1,nspin_t
                do isol = 1,nspino_t
                  if( Ecomp ) then
                    write(3,290) l, m, isp, isol,
     &                           rof(lm,isp,isol,0:ip_max,is)
                  else
                    write(3,300) l, m, isp, isol,
     &                           real(rof(lm,isp,isol,0:ip_max,is),db)
                  endif
                end do
              end do
            end do
          end do
        end do
      endif

      if( Final_tddft ) return

      if( icheck > 1 .and. Green .and. Absorbeur ) then
        if( Spinorbite ) then
          write(3,310) 
        elseif( nspin == 1 ) then 
          write(3,312)
        else
          write(3,315)
        endif
        do l = 0,lmax
          if( .not. Tddft .and. lmax > lmax_probe ) exit
          if( Hubb_pot .and. l == l_hubbard( numat ) ) then
            Hubb_m = .true.
          else
            Hubb_m = .false.
          endif
          lm0 = l**2 + l + 1
          do m = -l,l
            if( .not. ( Spinorbite .or. Hubb_m ) .and. m /= 0 ) cycle
            lm = lm0 + m
            if( Spinorbite ) then
              if( l == 0 ) then
                write(3,320) l, m, ( amplitg(lm,isp,isp), isp = 1,nspin)
              elseif( m == -l ) then
                write(3,322) l, m, amplitg(lm,1,1),
     &             amplitg(lm,1,2), amplitg(lm,2,2) 
              elseif( m == l ) then
                write(3,324) l, m, amplitg(lm,1,1),
     &             amplitg(lm,2,2), amplitg(lm,2,1) 
              else
                write(3,326) l, m, amplitg(lm,1,1),
     &             amplitg(lm,1,2), amplitg(lm,2,2), amplitg(lm,2,1) 
              endif 
            else
              write(3,322) l, m, ( amplitg(lm,isp,isp), isp = 1,nspin )
            endif
          end do
        end do
      endif

      if( icheck > 1 ) then
        if( Spinorbite ) then
          write(3,330) 
        elseif( nspin == 1 ) then 
          write(3,332)
        else
          write(3,335)
        endif
        do l = 0,lmax
          if( Hubb_pot .and. l == l_hubbard( numat ) ) then
            Hubb_m = .true.
          else
            Hubb_m = .false.
          endif
          lm0 = l**2 + l + 1
          do m = -l,l
            if( .not. ( Spinorbite .or. Hubb_m ) .and. m /= 0 ) cycle
            lm = lm0 + m
            if( Spinorbite ) then
              if( l == 0 ) then
                write(3,320) l, m, ( tau_ato(lm,isp,lm,isp,iapr),
     &                               isp = 1,nspin )
              elseif( m == -l ) then
                write(3,322) l, m, tau_ato(lm,1,lm,1,iapr),
     &             tau_ato(lm,1,lm+1,2,iapr), tau_ato(lm,2,lm,2,iapr) 
              elseif( m == l ) then
                write(3,324) l, m, tau_ato(lm,1,lm,1,iapr),
     &             tau_ato(lm,2,lm,2,iapr), tau_ato(lm,2,lm-1,1,iapr) 
              else
                write(3,326) l, m, tau_ato(lm,1,lm,1,iapr),
     &             tau_ato(lm,1,lm+1,2,iapr),
     &             tau_ato(lm,2,lm,2,iapr), tau_ato(lm,2,lm-1,1,iapr) 
              endif 
            else
              write(3,322) l, m, ( tau_ato(lm,isp,lm,isp,iapr),
     &                             isp = 1,nspin )
            endif
          end do
        end do
      endif

      if( icheck > 1 .and. Solsing .and. Absorbeur ) then
        do iseuil = 1,nbseuil
          if( Dipmag ) then
            if( Octupole ) then
              write(3,340) iseuil
            else
              write(3,345) iseuil
            endif
          else
            if( Octupole ) then
              write(3,346) iseuil
            else
              write(3,347) iseuil
            endif
          endif
          do l = 0,lmax
            if( ( Tddft .and. l > lmaxabs_t )
     &              .or. ( .not. Tddft .and. l > lmax_probe ) ) exit
            if( Hubb_pot .and. l == l_hubbard( numat ) ) then
              Hubb_m = .true.
            else
              Hubb_m = .false.
            endif
            lm0 = l**2 + l + 1
            do m = -l,l
              if( .not. ( Spinorbite .or. Hubb_m ) .and. m /= 0 ) cycle            
              lm = lm0 + m
              do isp = 1,nspin
                do mp = max(m-1,-l),min(m+1,l)
                  if( .not. spinorbite .and. mp /= m ) cycle
                  lmp = lm0 + mp
                  do isq = 1,nspin
                    if( .not. spinorbite .and. isp /= isq ) cycle
                    write(3,348) l, m, isp, mp, isq,
     &                         singul(lm,isp,lmp,isq,ip0:ip_max,iseuil)
                  end do
                end do
              end do
            end do
          end do
        end do
      endif

      if( icheck > 1 .and. ( ( Density .and. Absorbeur )
     &        .or. State_all .or. Self ) ) then
        if( Solsing ) then
          if( nspin == 2 ) then
            write(3,350)
          else 
            write(3,352)
          endif 
          do l = 0,lmax
            if( Hubb_pot .and. l == l_hubbard( numat ) ) then
              Hubb_m = .true.
            else
              Hubb_m = .false.
            endif
            lm0 = l**2 + l + 1
            do m = -l,l
              if( .not. ( Spinorbite .or. Hubb_m ) .and. m /= 0 ) cycle                 
              lm = lm0 + m
              write(3,355) l, m, singulsd(lm,1:nspin,iapr)
            end do
          end do
        endif
        if( Spinorbite ) then
          write(3,360) 
        else
          write(3,365) 
        endif
        do l = 0,lmax
          if( Hubb_pot .and. l == l_hubbard( numat ) ) then
            Hubb_m = .true.
          else
            Hubb_m = .false.
          endif
          lm0 = l**2 + l + 1
          do m = -l,l
            if( .not. ( Spinorbite .or. Hubb_m ) .and. m /= 0 )  cycle                             
            lm = lm0 + m
            do isp = 1,nspin
              write(3,370) l, m, isp, rofsd(lm,isp,:,:,iapr)
            end do
          end do
        end do
      endif

      if( icheck > 1 .and. Hubb_self ) then
        write(3,705) iapr
        if( Spinorbite ) then
          write(3,706)
        else
          write(3,707)
        end if
        lh = l_hubbard( numat )
        do isp = 1, nspin
          do m1 = -lh,lh
            do i1 = 1, nspino
              do i2 = 1, nspino
                if( Spinorbite ) then
                  write(3,710) m1, i1, i2, isp,
     &                        rofsd_hd(m1,i1,-lh:lh,i2,isp,iapr)
                else
                  write(3,711) m1, isp,
     &                        rofsd_hd(m1,1,-lh:lh,1,isp,iapr)
                end if
              end do
            end do
          end do
        end do
      end if

      deallocate( r )
      deallocate( g0 );     deallocate( gc )
      deallocate( gm );     deallocate( gp )
      if( Spinorbite ) deallocate( gso )
      deallocate( cgradm ); deallocate( cgradp )
      deallocate( cgrad0 ); deallocate( claplm )
      deallocate( claplp ); deallocate( clapl0 )
      deallocate( f2 )
      deallocate( urs )
      deallocate( uis )
      deallocate( V )

      return
  120 format(/' iapr =',i3,', Z =',i3,', lmax =',i2)
  130 format(/' Hubbard shifts (eV) for l =',i2/
     &        '   m       Shift     occ_mat')
  135 format(/' Hubbard shifts (eV) for l =',i2/
     &        '   m     Shift up  occ_mat up    Shift dn  occ_mat dn')
  140 format(i4,2(f12.3,f12.5)) 
  145 format(/' is_t =',i2,' iseuil =',i2)
  146 format(' Energ =',f10.3,' eV,  Enervide =',f10.3,' eV')
  147 format(' Ecinetic =',2f10.3)
  148 format(' V0bd  =',2f10.3)
  149 format(' konde =',2f12.5)
  150 format(1p,3e14.6)
  180 format(/'  l =',i2,', m =',i2,', Z =',i3)
  190 format(/'  l =',i2,', Z =',i3)
  200 format('     Radius   ',
     &  '    ur(spup,1)    ui(spup,1)    ur(spdn,1)    ui(spdn,1)',
     &  '    ur(spup,2)    ui(spup,2)    ur(spdn,2)    ui(spdn,2)')
  210 format(/5x,'Radius         ur(spup,1)    ur(spdn,1)',
     &           '    ur(spup,2)    ur(spdn,2)')
  220 format(1p,9e14.6)
  250 format(/'  l =',i2,', m =',i2,/
     &  '   Radius',9x,'u_sing(up,up)',11x,'u_sing(dn,up)',11x,
     &  'u_sing(dn,up)',11x,'u_sing(dn,dn)')
  260 format(/'  l =',i2,', Z =',i3,/
     &        '   Radius        u_irreg_r      u_irreg_i')
  263 format(/5x,'Radius',2x,'4pi*r2*sing_self(up)',2x,
     &        '4pi*r2*sing_self(dn)',3x,'4pi*r2*rhov_self(up)',8x,
     &        '4pi*r2*rhov_self(dn)')
  264 format(/5x,'Radius',2x,'4pi*r2*sing_self',1x,'4pi*r2*rhov_self_r',
     &          1x,'4pi*r2*rhov_self_i')
  270 format(/' Radial integral'/,'  l  m  isp isol',10x,'monopole',
     &    17x,'dipole',15x,'quadrupole',15x,'Octupole',9x,'is =',i2)
  275 format(/' Radial integral'/,'  l  m  isp isol',10x,'monopole',
     &    17x,'dipole',15x,'quadrupole',9x,'is =',i2)
  280 format(/' Radial integral'/,'  l  m  isp isol',4x,'monopole',
     &    4x,'dipole',4x,'quadrupole',4x,'Octupole',5x,'is =',i2)
  285 format(/' Radial integral'/,'  l  m  isp isol',4x,'monopole',
     &    4x,'dipole',4x,'quadrupole',5x,'is =',i2)
  290 format(2i3,2x,2i3,2x,1p,4(2x,2e11.3))
  300 format(2i3,2x,2i3,2x,1p,4e12.3)
  310 format(/' Atomic amplitude:',/
     &    '  l  m    Amplitg(m,up,m,up)    Amplitg(m,up,m+1,dn)',
     &    '    Amplitg(m,dn,m,dn)    Amplitg(m,dn,m-1,up)')
  312 format(/' Atomic scattering amplitude:',/
     &        '  l  m',9x,'Amplitg')
  315 format(/' Atomic scattering amplitude:',/
     &        '  l  m',9x,'Amplitg(up)    Amplitg(dn)')
  320 format(2i3,1p,1x,2e11.3,1x,22x,1x,2e11.3)
  322 format(2i3,1p,3(1x,2e11.3))
  324 format(2i3,1p,1x,2e11.3,1x,22x,2(1x,2e11.3))
  326 format(2i3,1p,4(1x,2e11.3))
  330 format(/' Atomic scattering amplitude:',/
     &    '  l  m    tau_ato(m,up,m,up)    tau_ato(m,up,m+1,dn)',
     &    '    tau_ato(m,dn,m,dn)    tau_ato(m,dn,m-1,up)')
  332 format(/' Atomic scattering amplitude:',/
     &        '  l  m',9x,'tau_ato')
  335 format(/' Atomic scattering amplitude:',/
     &        '  l  m',9x,'tau_ato(up)    tau_ato(dn)')
  340 format(/' Singular solution :',/'  l  m isp mp isq',5x,
     &         ' monopole-monopole   ',
     &         ' dipole-dipole       quadrupole-quadrupole       ',
     &         'dipole-Octupole       iseuil =',i2)
  345 format(/' Singular solution :',/'  l  m isp mp isq',5x,
     &         ' monopole-monopole   ',
     &         ' dipole-dipole       quadrupole-quadrupole       ',
     &         'iseuil =',i2)
  346 format(/' Singular solution :',/'  l  m isp mp isq',5x,
     &         ' dipole-dipole       quadrupole-quadrupole       ',
     &         'dipole-Octupole       iseuil =',i2)
  347 format(/' Singular solution :',/'  l  m isp mp isq',5x,
     &         ' dipole-dipole       quadrupole-quadrupole       ',
     &         'iseuil =',i2)
  348 format(5i3,1x,1p,4(1x,2e11.3))
  350 format(/' Singular solution for the density of states:',
     &        /'  l  m   Singulsd(up)  Singulsd(dn)')
  352 format(/' Singular solution for the density of states:',
     &        /'  l  m      Singulsd')
  355 format(2i3,1p,2e14.3)
  360 format(/' Radial integral for the density of states:',
     &        /'  l  m isp     Rofsd(isp,1,1)',10x,'Rofsd(isp,1,2)',
     &         10x,'Rofsd(isp,2,1)',10x,'Rofsd(isp,2,2)')
  365 format(/' Radial integral for the density of states:',
     &        /'  l  m isp',9x,'Rofsd')
  370 format(3i3,1x,1p,4(2e11.3,2x))
  705 format(/' Matrix radial integral for the density of states,',
     &       ' Hubbard case, for atom iapr = ',i3)
  706 format('   m  iso iso isp     Rofsd_hd  ')
  707 format('   m  isp   Rofsd_hd  ')
  710 format(4i4,1x,1p,7(1x,2e11.3))
  711 format(2i4,1x,1p,7(1x,2e11.3))
      end

!***********************************************************************

      subroutine sch_radial(Ecomp,Ecompr,Eimag,Er,gc,gm,gp,gso,
     &              Hubb_m,isol,l,m,m_hubb,mm,mmh,nm1,nr,nrm,
     &              nsol,nspin,nspino,numat,r,Relativiste,spinorbite,
     &              ui,ur,V_hubb)

      use declarations
      implicit none

      integer:: im, ip, ir, isol, isp, isq, l, m, m_hubb, nm1,
     &          nr, nrm, nsol, nspin, nspino, numat
      integer, dimension(2):: mm, mmh

      logical:: Ecomp, Ecompr, Hubb_m, Relativiste, spinorbite

      real(kind=db):: br, cr, ci, Eimag, Er, fac, p, td, ur1 
      real(kind=db), dimension(2):: faa 
      real(kind=db), dimension(0:nrm):: r 
      real(kind=db), dimension(nrm,nspin):: gc, gm, gp, gso 
      real(kind=db), dimension(0:nrm,nspin):: ui, ur 
      real(kind=db), dimension(-m_hubb:m_hubb,nspin):: V_hubb

      ur(:,:) = 0._db
      if( Ecomp ) ui(:,:) = 0._db

      ur1 = r(1)**(l+1)

      if( Relativiste ) then
        if( .not. spinorbite ) then
          p = sqrt( l**2 + l + 1 - ( alfa_sf * numat )**2 )
        elseif( isol == 1 .or. nsol == 1 ) then
          p = sqrt( ( l + 1 )**2 - ( alfa_sf * numat )**2 )
        else
          p = sqrt( l**2 - ( alfa_sf * numat )**2 )
        endif
      else
        if( nsol == 1 ) then
          p = l + 1._db
        elseif( isol == 1 ) then
          p = 0.5 + 0.5 * sqrt( 1. + 4*(l**2) + 8*l )
        else
          if( l == 1 ) then
! En fait, il n'y a pas de solution pour l=1 avec spin-orbite non
! Relativiste !
!                p = 1._db * l
            p = l + 1._db
          else
            p = 0.5 + 0.5 * sqrt( -5._db + 4*(l**2) )
          endif
        endif
      endif

      br = - numat / ( l + 1._db )
      cr = - ( 2 * numat * br + Er ) / ( 4 * l + 6 )  
      do isp = 1,nspin
        ur(0,isp) = ur1 * ( 1._db + br * r(0) + cr * r(0)**2 )
     &                  * ( r(0) / r(1) )**p
        ur(1,isp) = ur1 * ( 1._db + br * r(1) + cr * r(1)**2  )
        if( Ecomp ) then
          ci = - Eimag / ( 4 * l + 6 )  
          ui(0,isp) = ur1 * ci * r(0)**2 * ( r(0) / r(1) )**p
          ui(1,isp) = ur1 * ci * r(1)**2
        endif 
      end do

      if( spinorbite ) then

        if( nsol == 1 ) then

          if( m == nm1 ) then
            isp = nspino
          else
            isp = 1
          endif

          do ir = 1,nr-1
            im = ir - 1
            ip = ir + 1
            if( mm(isp) /= 0 ) then
              td = gc(ir,isp) + mm(isp) * gso(ir,isp)
            else
              td = gc(ir,isp)
            endif
            if( Hubb_m ) td = td + V_hubb(mmh(isp),isp)

            if( Ecomp .and. .not. Ecompr ) then
              ur(ip,isp) = - ( td * ur(ir,isp)  
     &                   + gm(ir,isp) * ur(im,isp) ) / gp(ir,isp)
              ui(ip,isp) = - ( td * ui(ir,isp)  
     &                   + gm(ir,isp) * ui(im,isp) ) / gp(ir,isp)

            elseif( Ecomp ) then
              ur(ip,isp) = - ( td * ur(ir,isp) + eimag *ui(ir,isp) 
     &                   + gm(ir,isp) * ur(im,isp) ) / gp(ir,isp)
              ui(ip,isp) = - ( td * ui(ir,isp) - eimag *ur(ir,isp) 
     &                   + gm(ir,isp) * ui(im,isp) ) / gp(ir,isp)

            else
              ur(ip,isp) = - ( td * ur(ir,isp)
     &                   + gm(ir,isp) * ur(im,isp) ) / gp(ir,isp)
            endif

          end do

        else

          if( isol == 1 ) then
            faa(1) = sqrt( ( l - m ) / ( 2*l + 1._db ) )
            faa(2) =  - sqrt( ( l + m + 1 ) / ( 2*l + 1._db ) )
          else
            faa(1) = sqrt( ( l + m + 1 ) / ( 2*l + 1._db ) )
            faa(2) = sqrt( ( l - m ) / ( 2*l + 1._db ) )
          endif

          do isp = 1,2
            ur(0:1,isp) = faa(isp) * ur(0:1,isp)
            if( Ecomp ) ui(0:1,isp) = faa(isp) * ui(0:1,isp)
          end do

          fac = sqrt( ( l - m ) * ( l + m + 1._db ) )

          do ir = 1,nr-1
            im = ir - 1
            ip = ir + 1
            do isp = 1,2
              isq = 3 - isp
              if( mm(isp) == 0 ) then
                td = gc(ir,isp)
              else
                td = gc(ir,isp) + mm(isp) * gso(ir,isp)
              endif
              if( Hubb_m ) td = td + V_hubb(mmh(isp),isp)

              if( Ecomp .and. .not. Ecompr ) then
                ur(ip,isp) = - ( td * ur(ir,isp)  
     &                     + gm(ir,isp) * ur(im,isp) 
     &                     + fac * gso(ir,isp) * ur(ir,isq) )
     &                     / gp(ir,isp)
                ui(ip,isp) = - ( td * ui(ir,isp)  
     &                     + gm(ir,isp) * ui(im,isp) 
     &                     + fac * gso(ir,isp) * ui(ir,isq) )
     &                     / gp(ir,isp)

              elseif( Ecomp ) then
                ur(ip,isp) = - ( td * ur(ir,isp) + eimag *ui(ir,isp) 
     &                     + gm(ir,isp) * ur(im,isp) 
     &                     + fac * gso(ir,isp) * ur(ir,isq) )
     &                     / gp(ir,isp)
                ui(ip,isp) = - ( td * ui(ir,isp) - eimag *ur(ir,isp) 
     &                     + gm(ir,isp) * ui(im,isp) 
     &                     + fac * gso(ir,isp) * ui(ir,isq) )
     &                     / gp(ir,isp)

              else
                ur(ip,isp) = - ( td * ur(ir,isp)
     &                     + gm(ir,isp) * ur(im,isp)
     &                     + fac * gso(ir,isp) * ur(ir,isq) )
     &                     / gp(ir,isp)
              endif

            end do
          end do

        endif

      else

        do isp = 1,nspin

          do ir = 1,nr-1
            im = ir - 1
            ip = ir + 1
            td = gc(ir,isp)
            if( Hubb_m ) td = td + V_hubb(mmh(isp),isp)

            if( Ecomp .and. .not. Ecompr ) then
              ur(ip,isp) = - ( td * ur(ir,isp)  
     &                   + gm(ir,isp) * ur(im,isp) ) / gp(ir,isp)
              ui(ip,isp) = - ( td * ui(ir,isp)  
     &                   + gm(ir,isp) * ui(im,isp) ) / gp(ir,isp)

            elseif( Ecomp ) then
              ur(ip,isp) = - ( td * ur(ir,isp) + eimag *ui(ir,isp) 
     &                   + gm(ir,isp) * ur(im,isp) ) / gp(ir,isp)
              ui(ip,isp) = - ( td * ui(ir,isp) - eimag *ur(ir,isp) 
     &                   + gm(ir,isp) * ui(im,isp) ) / gp(ir,isp)

            else
              ur(ip,isp) = - ( td * ur(ir,isp)
     &                   + gm(ir,isp) * ur(im,isp) ) / gp(ir,isp)
            endif

          end do

        end do

      endif

      return
      end

!***********************************************************************

      subroutine renormal(ampl,taug,m,nm1,nm2,nspin,urs,r,nrmtg,
     &                    nr,rmtgg,s1,s2,e1,e2,Spinorbite,icheck)

      use declarations
      implicit real(kind=db) (a-h,o-z)

      complex(kind=db):: den
      complex(kind=db), dimension(nspin):: e1, e2, s1, s2
      complex(kind=db), dimension(nspin,nspin):: ampl, amplo, taug,
     &                                            wronsk

      integer iso(nspin)
      integer icheck
      logical Spinorbite

      real(kind=db), dimension(0:nr) :: r
      real(kind=db), dimension(nspin,nspin):: u1, u2
      real(kind=db), dimension(0:nr,nspin,nspin) :: urs

      iso(1) = 2
      iso(nspin) = 1

      inr = min( nrmtg+1, nr )
      d12 = r(inr-1) - r(inr-2)
      d01 = r(inr) - r(inr-1)
      d02 = r(inr) - r(inr-2)

      u1(:,:) = 0._db
      u2(:,:) = 0._db

      do isp = 1,nspin  ! spin d'attaque
        do isol = 1,nspin  ! spin d'attaque
          if( .not. Spinorbite .and. isp /= isol ) cycle

          if( Spinorbite .and. ( ( m == nm1 .and. isp == 1 ) .or.
     &                          ( m == nm2 .and. isp == 2 ) )  ) cycle

          a = ( urs(inr-2,isp,isol) * d01 - urs(inr-1,isp,isol) * d02
     &        + urs(inr,isp,isol) * d12 ) / ( d12 * d01 * d02 )
          b = ( urs(inr-1,isp,isol) - urs(inr-2,isp,isol) ) / d12
     &      - a * ( r(inr-1) + r(inr-2) )
          c = urs(inr,isp,isol) - a * r(inr)**2 - b * r(inr)

          u1(isp,isol) = a * rmtgg**2 + b * rmtgg + c
          u2(isp,isol) = 2 * a * rmtgg + b

        end do
      end do

      ampl(:,:) = (0._db,0._db)
      taug(:,:) = (0._db,0._db)
      wronsk(:,:) = (0._db,0._db)
      wronskout = 1 / ( pi * rmtgg**2 )

! wronskout = 1 / ( pi * rmtg**2 ) a cause de la normalisation en
! rac(k/pi) des fonctions de bessel et hankel

      if( m > nm1 .and. m < nm2 .and. Spinorbite ) then

        do isp = 1,nspin
          wronsk(isp,:) = u1(isp,:)*s2(isp) - u2(isp,:)*s1(isp)
        end do

        do isp = 1,nspin   ! spin d'attaque
          is = iso(isp)

          den =  wronsk(is,1) / wronsk(is,nspin) 

          ampl(isp,1) = wronskout / ( wronsk(isp,1)
     &                               - wronsk(isp,nspin) * den )

          ampl(isp,nspin) = wronskout / ( wronsk(isp,nspin)
     &                                  - wronsk(isp,1) / den )

          taug(isp,isp) = ( sum( ampl(isp,:) * u1(isp,:) )
     &                                       - e1(isp)  ) / s1(isp)
          taug(isp,is) = sum( ampl(isp,:) * u1(is,:) ) / s1(is)
        end do

        if( icheck > 2 ) then
          write(3,110) m, den
          write(3,120) ( ampl(:,isol), isol = 1, nspin )
          write(3,130) ( wronskout / wronsk(isp,isp), isp=1,nspin)
        endif

! On recommence en normalisation neuman bessel

         amplo(:,:) = ampl(:,:)
         den =  1 / ( taug(1,1) * taug(2,2) - taug(1,2) * taug(2,1) ) 
         do isp = 1,nspin
           is = iso(is)
           ampl(isp,isp) = ( taug(is,is) * amplo(isp,isp) -
     &                       taug(isp,is) * amplo(is,isp) ) * den
           ampl(isp,is) = ( taug(is,is) * amplo(isp,is) -
     &                      taug(isp,is) * amplo(is,is) ) * den
         end do

      else

        do isp = 1,nspin

          if( Spinorbite ) then
            if( m == nm2 ) then
              if( isp == 2 ) cycle
            else
              if( isp == 1 ) cycle
            endif
          endif

          wronsk(isp,isp) = u1(isp,isp) * s2(isp) - u2(isp,isp) *s1(isp)

          den = u2(isp,isp) * e1(isp) - u1(isp,isp) * e2(isp) 
          taug(isp,isp) = den / wronsk(isp,isp)
! En fait, il faut l'amplitude ampp correspondant a la normalisation
! neuman-bessel
          ampl(isp,isp) = wronskout / den
          amplo(isp,isp) = wronskout / wronsk(isp,isp)
        end do

      endif

      if( icheck > 2 ) then
        write(3,140) s1(:)
        write(3,150) s2(:)
        write(3,160) e1(:)
        write(3,170) e2(:)
        write(3,180) wronskout
        write(3,190) den
        if( Spinorbite ) then
          write(3,200) ( wronsk(:,isol), isol = 1,nspin )
          write(3,210) ( u1(:,isol), isol = 1,nspin )
          write(3,220) ( u2(:,isol), isol = 1,nspin )
          write(3,120) ( ampl(:,isol), isol = 1,nspin )
          write(3,230) ( taug(:,isol), isol = 1,nspin )
        else
          write(3,200) ( wronsk(isp,isp), isp = 1,nspin )
          write(3,210) ( u1(isp,isp), isp = 1,nspin )
          write(3,220) ( u2(isp,isp), isp = 1,nspin )
          write(3,120) ( ampl(isp,isp), isp = 1,nspin )
          write(3,230) ( taug(isp,isp), isp = 1,nspin )
        endif
      endif

      return
  110 format(//' m = ',i3,'  den =',1p,8e11.3) 
  120 format(' ampl      =',1p,8e11.3) 
  130 format(' rap       =',1p,8e11.3) 
  140 format(/' s1        =',1p,8e11.3) 
  150 format(' s2        =',1p,8e11.3) 
  160 format(' e1        =',1p,8e11.3) 
  170 format(' e2        =',1p,8e11.3) 
  180 format(' wronskout =',1p,8e11.3) 
  190 format(' den       =',1p,8e11.3) 
  200 format(' wronsk    =',1p,8e11.3) 
  210 format(' u1        =',1p,8e11.3) 
  220 format(' u2        =',1p,8e11.3) 
  230 format(' taug      =',1p,8e11.3) 

      end

!**********************************************************************

      subroutine renormalc(ampl,taug,m,nm1,nm2,nspin,uis,urs,r,
     &                     nrmtg,nr,rmtgg,s1,s2,e1,e2,Spinorbite,icheck)

      use declarations
      implicit real(kind=db) (a-h,o-z)

      complex(kind=db):: den
      complex(kind=db), dimension(nspin):: e1, e2, s1, s2                                     
      complex(kind=db), dimension(nspin,nspin):: ampl, amplo, taug, u1,
     &                                        u2, wronsk

      integer iso(nspin)
      integer icheck
      logical Spinorbite

      real(kind=db), dimension(0:nr) :: r
      real(kind=db), dimension(0:nr,nspin,nspin) :: uis, urs

      iso(1) = 2
      iso(nspin) = 1

      inr = min( nrmtg+1, nr )
      d12 = r(inr-1) - r(inr-2)
      d01 = r(inr) - r(inr-1)
      d02 = r(inr) - r(inr-2)

      u1(:,:) = (0._db,0._db)
      u2(:,:) = (0._db,0._db)

      do isp = 1,nspin  ! spin 
        do isol = 1,nspin  ! solution
          if( .not. Spinorbite .and. isp /= isol ) cycle

          if( Spinorbite .and. ( ( m == nm1 .and. isp == 1 ) .or.
     &                          ( m == nm2 .and. isp == 2 ) )  ) cycle

          ar = ( urs(inr-2,isp,isol) * d01 - urs(inr-1,isp,isol) * d02
     &        + urs(inr,isp,isol) * d12 ) / ( d12 * d01 * d02 )
          br = ( urs(inr-1,isp,isol) - urs(inr-2,isp,isol) ) / d12
     &       - ar * ( r(inr-1) + r(inr-2) )
          cr = urs(inr,isp,isol) - ar * r(inr)**2 - br * r(inr)

          ai = ( uis(inr-2,isp,isol) * d01 - uis(inr-1,isp,isol) * d02
     &        + uis(inr,isp,isol) * d12 ) / ( d12 * d01 * d02 )
          bi = ( uis(inr-1,isp,isol) - uis(inr-2,isp,isol) ) / d12
     &      - ai * ( r(inr-1) + r(inr-2) )
          ci = uis(inr,isp,isol) - ai * r(inr)**2 - bi * r(inr)

          u1r = ar * rmtgg**2 + br * rmtgg + cr
          u2r = 2 * ar * rmtgg + br
          u1i = ai * rmtgg**2 + bi * rmtgg + ci
          u2i = 2 * ai * rmtgg + bi
          u1(isp,isol) = cmplx(u1r,u1i,db)
          u2(isp,isol) = cmplx(u2r,u2i,db)

        end do
      end do

      ampl(:,:) = (0._db,0._db)
      taug(:,:) = (0._db,0._db)
      wronsk(:,:) = (0._db,0._db)
      wronskout = 1 / ( pi * rmtgg**2 )

! wronskout = 1 / ( pi * rmtg**2 ) a cause de la normalisation en
! rac(k/pi) des fonctions de bessel et hankel

      if( m > nm1 .and. m < nm2 .and. Spinorbite ) then

        do isp = 1,nspin
          wronsk(isp,:) = u1(isp,:)*s2(isp) - u2(isp,:)*s1(isp)
        end do

        do isp = 1,nspin   ! spin d'attaque
          is = iso(isp)

          den =  wronsk(is,1) / wronsk(is,nspin) 

          ampl(isp,1) = wronskout / ( wronsk(isp,1)
     &                               - wronsk(isp,nspin) * den )

          ampl(isp,nspin) = wronskout / ( wronsk(isp,nspin)
     &                                  - wronsk(isp,1) / den )

          taug(isp,isp) = ( sum( ampl(isp,:) * u1(isp,:) )
     &                                       - e1(isp)  ) / s1(isp)
          taug(isp,is) = sum( ampl(isp,:) * u1(is,:) ) / s1(is)
        end do

        if( icheck > 2 ) then
          write(3,110) m, den
          write(3,120) ( ampl(:,isol), isol = 1, nspin )
          write(3,130) ( wronskout / wronsk(isp,isp), isp=1,nspin)
        endif

        amplo(:,:) = ampl(:,:)
        den =  1 / ( taug(1,1) * taug(2,2) - taug(1,2) * taug(2,1) ) 
        do isp = 1,nspin
          is = iso(is)
          ampl(isp,isp) = ( taug(is,is) * amplo(isp,isp) -
     &                     taug(isp,is) * amplo(is,isp) ) * den
          ampl(isp,is) = ( taug(is,is) * amplo(isp,is) -
     &                    taug(isp,is) * amplo(is,is) ) * den
        end do

      else

        do isp = 1,nspin

          if( Spinorbite ) then
            if( m == nm2 ) then
              if( isp == 2 ) cycle
            else
              if( isp == 1 ) cycle
            endif
          endif

          wronsk(isp,isp) = u1(isp,isp) * s2(isp) - u2(isp,isp) *s1(isp)

          den = u2(isp,isp) * e1(isp) - u1(isp,isp) * e2(isp) 
          taug(isp,isp) = den / wronsk(isp,isp)

! En fait, il faut l'amplitude ampp correspondant a la normalisation
! neuman-bessel
          ampl(isp,isp) = wronskout / den 

        end do

      endif

      if( icheck > 2 ) then
        write(3,140) s1(:)
        write(3,150) s2(:)
        write(3,160) e1(:)
        write(3,170) e2(:)
        write(3,180) wronskout
        write(3,190) den
        if( Spinorbite ) then
          write(3,200) ( wronsk(:,isol), isol = 1,nspin )
          write(3,210) ( u1(:,isol), isol = 1,nspin )
          write(3,220) ( u2(:,isol), isol = 1,nspin )
          write(3,120) ( ampl(:,isol), isol = 1,nspin )
          write(3,230) ( taug(:,isol), isol = 1,nspin )
        else
          write(3,200) ( wronsk(isp,isp), isp = 1,nspin )
          write(3,210) ( u1(isp,isp), isp = 1,nspin )
          write(3,220) ( u2(isp,isp), isp = 1,nspin )
          write(3,120) ( ampl(isp,isp), isp = 1,nspin )
          write(3,230) ( taug(isp,isp), isp = 1,nspin )
        endif
      endif

      return
  110 format(//' m = ',i3,'  den =',1p,8e11.3) 
  120 format(' ampl      =',1p,8e11.3) 
  130 format(' rap       =',1p,8e11.3) 
  140 format(/' s1        =',1p,8e11.3) 
  150 format(' s2        =',1p,8e11.3) 
  160 format(' e1        =',1p,8e11.3) 
  170 format(' e2        =',1p,8e11.3) 
  180 format(' wronskout =',1p,8e11.3) 
  190 format(' den       =',1p,8e11.3) 
  200 format(' wronsk    =',1p,8e11.3) 
  210 format(' u1        =',1p,8e11.3) 
  220 format(' u2        =',1p,8e11.3) 
  230 format(' taug      =',1p,8e11.3) 
      end

!**********************************************************************
! Calcul de l'integrale radiale de la regle d'or de Fermi.
! psii, urs et uis sont les fonctions d'onde fois r.

      subroutine radial_matrix(Ecomp,Final_tddft,Green_plus,
     &         ip_max,iseuil_t,lm0,lm1,lm2,m,nbseuil,ninitls,
     &         nlmam_t,nm1,nm2,nr,nrm,nrmtsd,ns1,ns2,nspin,nspino,
     &         psii,r,rmtsd,rof,Spinorbite,uis,urs,vecond)

      use declarations
      implicit none

      integer ip, ip_max, ipp, iseuil, iseuil_t, is, isol, isolp,
     &        isp, lm, lm0, lm1, lm2, m, mp, nbseuil, n, ninitls,
     &        nlmam_t, nm1, nm2, nr, nrm, nrmtsd, ns1, ns2, nspin,
     &        nspino

      complex(kind=db), dimension(nlmam_t,nspin,nspino,0:ip_max,
     &                                                  ninitls):: rof

      logical Ecomp, Final_tddft, Green_plus, Spinorbite

      real(kind=db):: f_integr3, fac, radlr, radlrr, radli, radlii,rmtsd
      real(kind=db), dimension(nbseuil):: vecond
      real(kind=db), dimension(0:nr):: r
      real(kind=db), dimension(nrmtsd):: fct, rr
      real(kind=db), dimension(nrm,nbseuil):: psii
      real(kind=db), dimension(0:nr,nspin,nspin):: uis, urs

      n = nrmtsd
      rr(1:n) = r(1:n)

      do isp = 1,nspin   ! boucle sur les 2 solutions a l'origine

        do isol = 1,nspino
          if( Spinorbite ) then
            if( ( m == nm1 .and. isp == 1 ) .or.
     &          ( m == nm2 .and. isp == 2 ) ) cycle
            isolp = isol
          else
            isolp = isp
          endif

          do ip = 0,ip_max

            ipp = ip + 1
  
            do is = ns1,ns2

              if( Final_tddft ) then
                iseuil = iseuil_t
              else
                iseuil = is
              endif

              fct(1:n) = psii(1:n,iseuil) * urs(1:n,isp,isolp)
     &                 * r(1:n)**ipp
              radlr = f_integr3(rr,fct,n,1,n,rmtsd)
              if( Ecomp ) then
                fct(1:n) = psii(1:n,iseuil) * uis(1:n,isp,isolp)
     &                   * r(1:n)**ipp
                radli = f_integr3(rr,fct,n,1,n,rmtsd)
              else
                radli = 0._db
              endif

              select case(ip)
                case(0)
                  fac = - 0.5_db * alfa_sf 
                case(1)
                  fac = 1._db
                case(2)
                  fac = 0.5_db * vecond(iseuil)
                  if( .not. Green_plus ) fac = - fac
                case(3)
                fac = - ( 1._db / 6 ) * vecond(iseuil)**2 
              end select
              radlrr = fac * radlr
              radlii = fac * radli
              if( Spinorbite ) then
                mp = m + isp - 1
                lm = lm0 + mp   ! correspond au lm vrai
                rof(lm,isp,isol,ip,is) = cmplx(radlrr,radlii,db)
              else
                rof(lm1:lm2,isp,isol,ip,is) = cmplx(radlrr,radlii,db)
              endif

            end do

          end do
        end do

      end do

      return
      end

!***********************************************************************

      subroutine radial_stdens(Ecomp,Hubbard,iapr,l,
     &           lm,lm0,lm1,lm2,m,n_atom_0,n_atom_0_self,n_atom_ind,
     &           n_atom_ind_self,nllm0,nllmm,nlmagm,nm1,nm2,nr,
     &           nrm_self,nrmtsd,nspin,nspino,r,rhov_self,rmtsd,rofsd,
     &           Self,Spinorbite,uis,urs)

      use declarations

      implicit none

      integer iapr, iop, iopp, ioq, ioqq, ir, isp, l, lm, lm0,
     &        lm1, lm2, m, n, n_atom_0, n_atom_0_self, n_atom_ind,
     &        n_atom_ind_self, nllm0, nllmm, nlmagm,
     &        nm1, nm2, nr, nrm_self, nrmtsd, nspin, nspino

      complex(kind=db), dimension(0:nrm_self,nllm0:nllmm,nspin,
     &    nspino,nspino,n_atom_0_self:n_atom_ind_self):: rhov_self
      complex(kind=db), dimension(nlmagm,nspin,nspino,nspino,
     &                                     n_atom_0:n_atom_ind):: rofsd

      logical Ecomp, Hubbard, Self, Spinorbite

      real(kind=db):: f_integr3, radl, radli, rmtsd
      real(kind=db), dimension(0:nr):: r
      real(kind=db), dimension(nrmtsd):: a, b, fct, rr, u12s, u21s, 
     &                                   uis2, urs2
      real(kind=db), dimension(0:nr,nspin,nspin) :: uis, urs

      n = nrmtsd
      rr(1:n) = r(1:n)

      boucle_spin: do isp = 1,nspin
        if( Spinorbite ) then
          lm = lm0 + m + isp - 1  ! correspond au lm vrai
          if( ( m == nm1 .and. isp == 1 ) .or.
     &        ( m == nm2 .and. isp == 2 ) ) cycle
        endif

        do iopp = 1,nspino
          if( Spinorbite ) then
            iop = iopp
          else
            iop = isp
          endif

          do ioqq = 1,nspino
            if( Spinorbite ) then
              ioq = ioqq
            else
              ioq = isp
            endif

            urs2(1:n) = urs(1:n,isp,iop) * urs(1:n,isp,ioq)
            if( Ecomp ) then
              uis2(1:n) = uis(1:n,isp,iop) * uis(1:n,isp,ioq)
              u12s(1:n) = urs(1:n,isp,iop) * uis(1:n,isp,ioq)
              u21s(1:n) = uis(1:n,isp,iop) * urs(1:n,isp,ioq)
            endif
       
            a(1:n) = urs2(1:n)
            fct(1:n) = urs2(:) * r(1:n)**2
   
            if( Ecomp ) then
              fct(1:n) = fct(1:n) - uis2(1:n) * r(1:n)**2    
              a(1:n) = a(1:n) - uis2(1:n)
            end if 
            radl = f_integr3(rr,fct,n,1,n,rmtsd)
            if( Ecomp ) then
              b(1:n) = u12s(1:n) + u21s(1:n)
              fct(1:n) = b(1:n) * r(1:n)**2
              radli = f_integr3(rr,fct,n,1,n,rmtsd)
            else
              b(:) = 0._db
              radli = 0._db
            endif

! Pour le calcul auto coherent on travaille toujours avec un potentiel
! complexe
            if( Spinorbite ) then
              rofsd(lm,isp,iop,ioq,iapr) = cmplx(radl,radli,db)
              if( Self ) rhov_self(1:n,lm,isp,iop,ioq,iapr) 
     &                          = cmplx(a(1:n),b(1:n),db) / quatre_pi
            else
              rofsd(lm1:lm2,isp,1,1,iapr) = cmplx(radl,radli,db)
              if( Self ) then
                do ir = 1,n
                  if( Hubbard ) then 
                    rhov_self(ir,lm1:lm2,isp,1,1,iapr) = 
     &                             cmplx(a(ir),b(ir),db) / quatre_pi
                  else
                    rhov_self(ir,l,isp,1,1,iapr) = 
     &                             cmplx(a(ir),b(ir),db) / quatre_pi
                  end if
                end do
              endif
            endif

          end do

        end do

      end do boucle_spin

      return
      end

!***********************************************************************

! Calcul des integrales radiales, y compris non diagonales en (m,m'),
! pour la densite d'etat des termes de Hubbard.

      subroutine radial_stdens_hubb(Ecomp,iapr,l,m_hubb,n_atom_0_self,
     &               n_atom_ind_self,nr,nrmtsd,nspin,nspino,r,Rmtsd,
     &               rofsd_hd,uiss,urss,Spinorbite)

      use declarations
      implicit none

      integer:: iapr, ioq, ior, isp, l, m_hubb, m1, m2, mv1, mv2, n, 
     &      n_atom_0_self, n_atom_ind_self,nr, nrmtsd, nspin, nspino

      complex(kind=db), dimension(-m_hubb:m_hubb,nspino,-m_hubb:m_hubb,
     &        nspino,nspin,n_atom_0_self:n_atom_ind_self):: rofsd_hd

      logical Ecomp, Spinorbite

      real(kind=db):: f_integr3, radl, radli, Rmtsd
      real(kind=db), dimension(0:nr):: r
      real(kind=db), dimension(nrmtsd):: fct, rr
      real(kind=db), dimension(nr,-m_hubb-1:m_hubb,nspin,nspin):: uiss,
     &                                                            urss
 
      n = nrmtsd
      rr(1:n) = r(1:n)

      if( Spinorbite ) then
!  c'est le vrai m qui intervient dans les recopies urss et uiss
        do isp = 1,nspin
          do m1 = -l-1,l   ! le faux
            mv1 = m1 + isp - 1 ! le vrai
            if( mv1 < -l .or. mv1 > l ) cycle 
            do m2 = -l-1,l
              mv2 = m2 + isp - 1
              if( mv2 < -l .or. mv2 > l ) cycle          
              do ioq = 1,nspino    
                do ior = 1,nspino
                  fct(1:n) = urss(1:n,m1,isp,ioq) * urss(1:n,m2,isp,ior) 
                  if( Ecomp ) fct(1:n) = fct(1:n) 
     &                    - uiss(1:n,m1,isp,ioq) * uiss(1:n,m2,isp,ior)
                  fct(1:n) = fct(1:n) * r(1:n)**2
                  radl = f_integr3(rr,fct,n,1,n,rmtsd)
                  if( Ecomp) then
                    fct(1:n) = urss(1:n,m1,isp,ioq)*uiss(1:n,m2,isp,ior) 
     &                       + urss(1:n,m2,isp,ior)*uiss(1:n,m1,isp,ioq)
                    fct(1:n) = fct(1:n) * r(1:n)**2
                    radli = f_integr3(rr,fct,n,1,n,rmtsd)
                  else
                    radli = 0._db
                  end if
                  rofsd_hd(mv1,ioq,mv2,ior,isp,iapr)
     &                                      = cmplx(radl, radli, db)
                end do
              end do
            end do                            
          end do
        end do
      else
        do isp = 1,nspin
          do m1 = -l,l 
            do m2 = -l,l
              fct(1:n) = urss(1:n,m1,isp,isp) * urss(1:n,m2,isp,isp)
              if( Ecomp ) fct(1:n) = fct(1:n) 
     &                   - uiss(1:n,m1,isp,isp) * uiss(1:n,m2,isp,isp)
              fct(1:n) = fct(1:n) * r(1:n)**2
              radl = f_integr3(rr,fct,n,1,n,rmtsd)
              if( Ecomp) then
                fct(1:n) = urss(1:n,m1,isp,isp) * uiss(1:n,m2,isp,isp)
     &                   + urss(1:n,m2,isp,isp) * uiss(1:n,m1,isp,isp)
                fct(1:n) = fct(1:n) * r(1:n)**2
                radli = f_integr3(rr,fct,n,1,n,rmtsd)
              else
                radli = 0._db
              end if
              rofsd_hd(m1,1,m2,1,isp,iapr)= cmplx(radl,radli,db)                                
            end do                            
          end do
        end do
      end if

      return
      end

!***********************************************************************

      subroutine cal_phiato(Hubb,Hubbard,ia,iang,ibord,icheck,
     &              iwrite,l,lm0,lm1,lm2,lmax,m,natome,nbord,
     &              nbtm,nllm0,nllmm,nm1,nm2,nphiato1,nphiato7,
     &              npsom,nr,nspin,nspino,phiato,posi,r,
     &              Spinorbite,uis,urs,xyz)

      use declarations
      implicit real(kind=db) (a-h,o-z)

      integer, dimension(nbtm,natome):: ibord
      integer icheck
      logical Hubb, Hubbard, Spinorbite

      real(kind=db), dimension(3):: ps, x, w
      real(kind=db), dimension(0:nr):: r
      real(kind=db), dimension(3,natome):: posi
      real(kind=db), dimension(0:nr,nspin,nspin) :: uis, urs
      real(kind=db), dimension(4,npsom):: xyz
      real(kind=db), dimension(nphiato1,nllm0:nllmm,
     &                 nspin,nspino,natome,nphiato7):: phiato

      ps(1:3) = posi(1:3,ia)

      if( Spinorbite ) then
        nspin2 = 2
      else
        nspin2 = 1
      endif

      do ib = 1,nbord
        i = ibord(ib,ia)
        x(1:3) = xyz(1:3,i)
        call posrel(x,ps,w,rrel,isym)

        do i = 2,nr
          if( r(i) > rrel ) exit
        end do
        i = min(i,nr)

        rm = r(i-2)
        r0 = r(i-1)
        rp = r(i)

        do isp = 1,nspin
          if( Spinorbite ) then
            if( ( m == nm1 .and. isp == 1 ) .or.
     &          ( m == nm2 .and. isp == 2 ) ) cycle
            if( iang == 1 ) then
              mp = m + isp - 1
            elseif( iang == - 1 ) then
              mp = - m - isp - 1
            endif
            lm = lm0 + mp
            lm1 = lm
            lm2 = lm
            nspin2 = nspin
          endif
          if( iang == 1 ) then
            ispp = isp
          elseif( iang == - 1 ) then
            ispp = 3 - isp
          else
            ispp = isp
          endif

          do isol = 1,nspin

            if( Spinorbite ) then
              if( iang == 1 ) then
                isolp = isol
              elseif( iang == - 1 ) then
                isolp = 3 - isol
              endif
            else
              isolp = isol
            endif

            if( isolp /= ispp .and. (.not. Spinorbite .or. iang == 0 ) )
     &                                            cycle

            do icp = 1,nphiato7
              if( icp == 1 ) then
                um = urs(i-2,isp,isol)
                u0 = urs(i-1,isp,isol)
                up = urs(i,isp,isol)
              else
                um = uis(i-2,isp,isol)
                u0 = uis(i-1,isp,isol)
                up = uis(i,isp,isol)
              endif
              phi = f_interp2(rrel,rm,r0,rp,um,u0,up)

              do lm = lm1, lm2
                if( Spinorbite ) then
                  phiato(ib,lm,ispp,isolp,ia,icp) = phi
                elseif( Hubbard ) then
                  phiato(ib,lm,ispp,1,ia,icp) = phi
                else
                  phiato(ib,l,ispp,1,ia,icp) = phi
                endif
              end do

              if( iang == 0 .and. isol == 2 ) then
                if( Spinorbite .or. Hubbard ) then
                  do lm = lm1,lm2
                    phi = 0.5 * ( phiato(ib,lm,1,1,ia,icp)
     &                          + phiato(ib,lm,nspin,nspin2,ia,icp) )
                    phiato(ib,lm,1,1,ia,icp) = phi
                    phiato(ib,lm,nspin,nspin2,ia,icp) = phi
                  end do
                else
                  phi = 0.5 * ( phiato(ib,l,1,1,ia,icp)
     &                        + phiato(ib,l,nspin,nspin2,ia,icp) )
                  phiato(ib,l,1,1,ia,icp) = phi
                  phiato(ib,l,nspin,nspin2,ia,icp) = phi
                endif
              endif

            end do

         end do

        end do
      end do

      if( icheck > 2 .and. iwrite == 1 ) then
        write(3,110) ia
        do ll = 0,lmax
          if( Spinorbite .or. Hubb ) then
            lm0 = ll**2 + ll + 1
            do mm = -ll,ll
              if( Spinorbite ) then
                write(3,120) ll, mm
              else
                write(3,130) ll, mm
              endif
              lm = lm0 + mm
              do ib = 1,nbord
                write(3,140) ibord(ib,ia),
     &          ( phiato(ib,lm,1:nspin,isol,ia,:), isol = 1,nspin2 )
              end do
            end do
          else
            write(3,150) ll
            do ib = 1,nbord
              write(3,140) ibord(ib,ia),
     &                     phiato(ib,ll,1:nspin,1,ia,:)
            end do
          endif
        end do
      endif

      return
  110 format(/' ia =',i3)
  120 format(/' ibord phiato(u,1)  phiato(d,1)  phiato(u,2)  ',
     &        'phiato(d,2)   l =',i3,', m =',i3)
  130 format(/' ibord  phiato(u)    phiato(d)   l =',i3,', m =',i3)
  140 format(i5,8e13.4)
  150 format(/' ibord    phiato   l =',i3)
      end

!***********************************************************************

! Calcul de Integrale_sur_r_et_r' de conj(phi(r) * f_reg( min(r,r') ) * f_irg( max(r,r') ) * phi(r') * dr * dr' ) 
! = Integrale_sur_r ( phi(r) * f_irg(r) * Integrale_sur_r'_de_0_a_r ( f_reg(r') * phi(r') * dr' )
!                   + phi(r) * f_reg(r) * Integrale_sur_r'_de_r_a_Rmax ( f_irg(r') * phi(r') * dr' ) * dr 
!
! f_reg = r * solution reguliere
! f_irg = r * solution irreguliere
! phi = r * fonction initiale * r^p    ou   r * solution reguliere

      function integr_sing(n,phi,f_reg,f_irg,rmtsd,r,icheck) 

      use declarations
      implicit real(kind=db) (a-h,o-z)

      complex(kind=db):: integr_sing

      complex(kind=db), dimension(n):: f_irg, f_reg, fct, phi_irg,
     &                                phi_reg, s_phi_irg, s_phi_reg 
      integer icheck
      real(kind=db), dimension(n):: fct_r, phi, r


      phi_reg(:) = phi(:) * f_reg(:)
      call ffintegr2(s_phi_reg,phi_reg,r,n,1,rmtsd)

      phi_irg(:) = phi(:) * f_irg(:)
      call ffintegr2(s_phi_irg,phi_irg,r,n,-1,rmtsd)

      fct(:) = phi_irg(:) * s_phi_reg(:) + phi_reg(:) * s_phi_irg(:)

      fct_r(:) = real( fct(:),db )
      fr = f_integr3(r,fct_r,n,1,n,rmtsd)

      fct_r(:) = aimag( fct(:) )
      fi = f_integr3(r,fct_r,n,1,n,rmtsd)

      integr_sing = - cmplx(fr,fi,db)

      if( icheck > 4 ) then
        write(3,110)
        do ir = 1,n
          write(3,120) r(ir)*bohr, f_reg(ir), phi_reg(ir),s_phi_reg(ir),  
     &                 phi_irg(ir), s_phi_irg(ir), fct(ir)
        end do
        write(3,130) integr_sing 
      endif

      return
  110 format(/5x,'Radius',15x,'f_reg',23x,'phi_reg',19x,'s_phi_reg',19x,
     &          'phi_irg',21x,'s_phi_irg',22x,'fct')
  120 format(1p,13e14.6)
  130 format(/' Integr_sing =',1p,2e14.6)
      end

!***********************************************************************

! Calcul l'integrale de 0 a r (is=1) ou r a rmtsd (is=-1) de fct
! Cas complexe

      subroutine ffintegr2(fint,fct,r,n,is,rmtsd)

      use declarations
      implicit real(kind=db) (a-h,o-z)

      complex(kind=db):: a, a_tiers, b, b_demi, c, f0, fm, fp
      complex(kind=db), dimension(2):: dintegr
      complex(kind=db), dimension(n):: fct, fint

      real(kind=db), dimension(n):: r

      tiers = 1._db / 3._db

      if( is == 1 ) then
        i1 = 1
        i2 = n - 1
      else
        i1 = n - 1
        i2 = 1
        fint(n) = (0._db, 0._db)
      endif

      do i = i1,i2,is
        if( i == 1 ) then
          rm = r(i)
          r0 = r(i+1)
          rp = r(i+2)
          fm = fct(i)
          f0 = fct(i+1)
          fp = fct(i+2)
          xm = 0._db
          x0 = rm
          xp = 0.5 * ( rm + r0 )
        else
          rm = r(i-1)
          r0 = r(i)
          rp = r(i+1)
          fm = fct(i-1)
          f0 = fct(i)
          fp = fct(i+1)
          xm = 0.5 * ( rm + r0 )
          x0 = r0
          xp = 0.5 * ( r0 + rp )
        endif

        if( is == 1 .and. r0 > rmtsd ) then
          if( r(i-1) > rmtsd ) then
            fint(i) = fint(i-1)
            cycle
          elseif( rm > rmtsd ) then
            fint(i) = fint(i-1) + dintegr(2)
            cycle
          else
            x0 = rmtsd
          endif
        endif
        if( is == - 1 .and. r0 > rmtsd ) then
          fint(i) = (0._db, 0._db)
          cycle
        endif
        if( xp > rmtsd ) xp = rmtsd

        a = ( fm * ( rp - r0 ) - f0 * ( rp - rm ) + fp * ( r0 - rm ) )
     &    / ( ( r0 - rm ) * ( rp - r0 ) * ( rp - rm ) )
        b = ( f0 - fm ) / ( r0 - rm ) - a * ( r0 + rm )
        c = f0 - a * r0**2 - b * r0

        a_tiers = a * tiers
        b_demi = b * 0.5_db

        if( is == 1 ) then
          dintegr(1) = ( a_tiers * ( xm**2 + xm * x0  + x0**2 )
     &               + b_demi * ( xm + x0 ) + c ) * ( x0 - xm )
        else
          dintegr(1) = ( a_tiers * ( x0**2 + x0 * xp  + xp**2 )
     &               + b_demi * ( x0 + xp ) + c ) * ( xp - x0 )
        endif

        if( i == i1 ) then
          fint(i) = dintegr(1)
        else
          fint(i) = fint(i-is) + sum( dintegr(:) )
        endif

        if( is == 1 ) then
          dintegr(2) = ( a_tiers * ( x0**2 + x0 * xp  + xp**2 )
     &               + b_demi * ( x0 + xp ) + c ) * ( xp - x0 )
        else
          dintegr(2) = ( a_tiers * ( xm**2 + xm * x0  + x0**2 )
     &               + b_demi * ( xm + x0 ) + c ) * ( x0 - xm )
        endif

      end do

      if( is == 1 ) fint(n) = fint(n-1)

      return
      end

!***********************************************************************

! Calcul l'integrale de 0 a r (is=1) ou r a rmtsd (is=-1) de fct
! Cas complexe

      subroutine ffintegr2_r(fint,fct,r,n,is,rmtsd)

      use declarations
      implicit real(kind=db) (a-h,o-z)

      real(kind=db):: a, a_tiers, b, b_demi, c, f0, fm, fp
      real(kind=db), dimension(2):: dintegr
      real(kind=db), dimension(n):: fct, fint

      real(kind=db), dimension(n):: r

      tiers = 1._db / 3._db

      if( is == 1 ) then
        i1 = 1
        i2 = n - 1
      else
        i1 = n - 1
        i2 = 1
        fint(n) = 0._db
      endif

      do i = i1,i2,is
        if( i == 1 ) then
          rm = r(i)
          r0 = r(i+1)
          rp = r(i+2)
          fm = fct(i)
          f0 = fct(i+1)
          fp = fct(i+2)
          xm = 0._db
          x0 = rm
          xp = 0.5 * ( rm + r0 )
        else
          rm = r(i-1)
          r0 = r(i)
          rp = r(i+1)
          fm = fct(i-1)
          f0 = fct(i)
          fp = fct(i+1)
          xm = 0.5 * ( rm + r0 )
          x0 = r0
          xp = 0.5 * ( r0 + rp )
        endif

        if( is == 1 .and. r0 > rmtsd ) then
          if( r(i-1) > rmtsd ) then
            fint(i) = fint(i-1)
            cycle
          elseif( rm > rmtsd ) then
            fint(i) = fint(i-1) + dintegr(2)
            cycle
          else
            x0 = rmtsd
          endif
        endif
        if( is == - 1 .and. r0 > rmtsd ) then
          fint(i) = 0._db
          cycle
        endif
        if( xp > rmtsd ) xp = rmtsd

        a = ( fm * ( rp - r0 ) - f0 * ( rp - rm ) + fp * ( r0 - rm ) )
     &    / ( ( r0 - rm ) * ( rp - r0 ) * ( rp - rm ) )
        b = ( f0 - fm ) / ( r0 - rm ) - a * ( r0 + rm )
        c = f0 - a * r0**2 - b * r0

        a_tiers = a * tiers
        b_demi = b * 0.5_db

        if( is == 1 ) then
          dintegr(1) = ( a_tiers * ( xm**2 + xm * x0  + x0**2 )
     &               + b_demi * ( xm + x0 ) + c ) * ( x0 - xm )
        else
          dintegr(1) = ( a_tiers * ( x0**2 + x0 * xp  + xp**2 )
     &               + b_demi * ( x0 + xp ) + c ) * ( xp - x0 )
        endif

        if( i == i1 ) then
          fint(i) = dintegr(1)
        else
          fint(i) = fint(i-is) + sum( dintegr(:) )
        endif

        if( is == 1 ) then
          dintegr(2) = ( a_tiers * ( x0**2 + x0 * xp  + xp**2 )
     &               + b_demi * ( x0 + xp ) + c ) * ( xp - x0 )
        else
          dintegr(2) = ( a_tiers * ( xm**2 + xm * x0  + x0**2 )
     &               + b_demi * ( xm + x0 ) + c ) * ( x0 - xm )
        endif

      end do

      if( is == 1 ) fint(n) = fint(n-1)

      return
      end
