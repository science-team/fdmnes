!***********************************************************************
!   Petit formulaire Hubbard:
!
!    <f'|M_d|f'> = <f U |U_t * M * U| U_t f>
!
! f' est la base ou la matrice est diagonale (M_d)
! les vecteurs propres de M se trouvent sur les colonnes de U
!
!   Les changements de base:
!      la base locale = celle ou occ_hubb est diagonale
!      la base du cristal (ou cluster) = occ_hubb n'est pas diagonale
!
!   locale --> cristal:    U   * T * U_t
!   cristal --> locale:    U_t * T * U

!***********************************************************************
!   La logique du calcul Hubbard:
!   
! i_self = 1: - on calcule tout dans la base du cristal
!             - on obtient un occ_hubb(m,m') qu'on diagonalise pour 
!               trouver la base locale (subroutine diag)
!
! i_self > 1: - on reboucle avec un occ_hubb(m,m) diagonale en m 
!               (c.a.d. on se place dans la base locale)     
!             - subroutine sphere: on calcule rofsd_hd, tau_ato,
!               singul, singulsd dans la base locale
!             - on applique la correction de Hubbard
!               dU_mm = (n_mm - 0.5) * V_hubb
!               dans l eq de Schrodinger radiale
!             - subroutine rotation_hubb: on fait la rotation inverse des
!               tau_ato et phiato vers la base du cristal
!             - Apres calcul des taull en base cluster, on repasse en
!               basse locales avec routine rot_taull
!             - subroutine cal_data_hub: on calcule statedens_hd 
!               directement dans la base locale 
!             -  on recalcule DOS, occ_hubb etc.; si on continue les 
!               iterations, on re-appelle diag et on recommence du debut

!***********************************************************************

! Calculs de la densite d'etat pour Hubbard

      subroutine cal_data_hub(Full_atom,hubb,iaprotoi,icheck,
     &            iprabs,itypei,m_hubb,mpinodes,mpirank,n_atom_0,
     &            n_atom_0_self,n_atom_ind,n_atom_ind_self,natome,
     &            natome_self,nlmagm,nspin,nspino,ntype,
     &            numat,rofsd_hd,self_nonexc,singulsd,solsing,
     &            statedens_hd,spinorbite,taull)

      use declarations  
      implicit none

      integer ia, iapr, icheck, ior, ioq, ipr, iprabs,
     &        isp, it, l_hubbard, lhubb, lm, lm1, lm2, lmq,
     &        lmr, m, m_hubb, m1, m1f, m2, m2f, mpinodes, mpirank, mq,
     &        mr, n_atom_0, n_atom_0_self, n_atom_ind, n_atom_ind_self, 
     &        natome, natome_self, nlmagm, nspin, nspino, ntype, Z

      complex(kind=db), dimension(nlmagm,nspin,nlmagm,nspin,natome)::
     &                                                         taull
      complex(kind=db), dimension(-m_hubb:m_hubb,nspino,-m_hubb:m_hubb,
     &        nspino,nspin,n_atom_0_self:n_atom_ind_self):: rofsd_hd

      integer, dimension(0:ntype):: numat
      integer, dimension(natome):: iaprotoi, itypei

      logical:: Full_atom, self_nonexc, solsing, spinorbite
      logical, dimension(0:ntype):: hubb
      logical, dimension(n_atom_0_self:n_atom_ind_self):: iapr_done
 
      complex(kind=db), dimension(-m_hubb:m_hubb,-m_hubb:m_hubb,nspin, 
     &       n_atom_0_self:n_atom_ind_self,0:mpinodes-1):: statedens_hd

      real(kind=db), dimension(nlmagm,nspin,n_atom_0:n_atom_ind)::
     &                                                     singulsd
     
      if( icheck > 1 ) write(3,110)

      iapr_done(:) = .false.

      boucle_ia: do ia = 1,natome  
       
        ipr = iaprotoi(ia)
        it = itypei(ia)

        Z = numat( it )

        if( Full_atom ) then
          iapr = ia
        else if( self_nonexc .and. ipr == 0) then
          iapr = iprabs
        else
          iapr = ipr
        endif

        if( .not. hubb(it) .or. ia > natome_self .or. iapr_done(iapr) )
     &                                                       cycle                                            

        lhubb = l_hubbard( Z ) 
 
!  il n'existe pas d'etat croise en spin pour la matrice d'occupation,
!              du moins, on croit...

        statedens_hd(:,:,:,iapr,mpirank) = (0._db, 0._db)

        if( spinorbite ) then

          do isp = 1, nspin
            do m1 = -lhubb-1, lhubb
              m1f = m1 + isp - 1
              if( m1f > lhubb .or. m1f < -lhubb ) cycle
              do m2 = -lhubb-1, lhubb
                m2f = m2 + isp - 1
                if( m2f > lhubb .or. m2f < -lhubb ) cycle
                do ior = 1,2
                  mr = m1 + ior - 1
                  if( mr > lhubb .or. mr < -lhubb ) cycle
                  lmr = lhubb**2 + lhubb + 1 + mr
                  do ioq = 1,2
                    mq = m2 + ioq - 1
                    if( mq > lhubb .or. mq < -lhubb ) cycle
                    lmq = lhubb**2 + lhubb + 1 + mq

                    statedens_hd(m1f,m2f,isp,iapr,mpirank)
     &                = statedens_hd(m1f,m2f,isp,iapr,mpirank)
     &                + img * rofsd_hd(m1f,ior,m2f,ioq,isp,iapr)
     &                      * taull(lmr,ior,lmq,ioq,ia) 


                  end do
                end do
              end do
            end do 
          end do

        else

          do isp = 1, nspin
            do m1 = -lhubb, lhubb
              lm1 = lhubb**2 + lhubb + 1 + m1
              do m2 = -lhubb, lhubb
                lm2 = lhubb**2 + lhubb + 1 + m2
                statedens_hd(m1,m2,isp,iapr,mpirank)
     &               = img * rofsd_hd(m1,1,m2,1,isp,iapr)
     &                     * taull(lm1,isp,lm2,isp,ia) 
              end do
            end do
          end do

        end if

! On ajoute la solution singuliere
        if( solsing ) then
          do m = -lhubb, lhubb
            lm = lhubb**2 + lhubb + m + 1
            do isp = 1, nspin
              statedens_hd(m,m,isp,iapr,mpirank)
     &               = statedens_hd(m,m,isp,iapr,mpirank)
     &               + singulsd(lm,isp,iapr)
            end do
          end do
        end if

        if( icheck > 1 ) then
           do isp = 1, nspin
             write(3,120) iapr, isp
             do m = -lhubb, lhubb
               write(3,130) statedens_hd(m,-lhubb:lhubb,isp,iapr,
     &                                   mpirank)
             end do     
           end do
        end if

        iapr_done(iapr) = .true.

      end do boucle_ia
         
      return
  110 format(/' ---- Cal_data_hub --------',100('-'))
  120 format(/' Statedens_hd for ia = ',i2,'  isp = ', i1)
  130 format(1p,14(1x,2e13.5))
      end

!***********************************************************************

! Sous - programme qui diagonalise la matrice d'occupation et calcule
! la rotation qui correspond
! rot_hubb : matrice locale reelle vers cluster (complexe) 

      subroutine diag(Atom_comp,Full_atom,hubb,i_self,iaprotoi,
     &              icheck,iprabs,itypei,itypepr,m_hubb,
     &              n_atom_0_self,n_atom_ind_self,n_atom_proto,
     &              natome,natome_self,nspin,ntype,numat,
     &              occ_diag,occ_hubb,rot_hubb,self_nonexc,
     &              tau_nondiag)

      use declarations
      implicit none

      integer:: i, i_self, ia, iapr, info, ipr, irep, it, isp, j, l,
     &          l_hubbard, lwork, n, icheck, iprabs,
     &          m, m_hubb, m1, m2, n_atom_0_self, n_atom_ind_self,
     &          n_atom_proto, natome, natome_self, nrep, nspin, ntype 
      integer, dimension(natome):: iaprotoi, itypei
      integer, dimension(0:ntype):: numat
      integer, dimension(0:n_atom_proto):: itypepr
      integer, dimension(2*m_hubb+1):: n_elem
      integer, dimension(2*m_hubb+1,2*m_hubb+1):: m_elem

      logical:: Full_atom, tau_nondiag, self_nonexc
      logical, dimension(0:ntype):: hubb
      logical, dimension(0:natome):: Atom_comp
      logical, dimension(-m_hubb:m_hubb):: m_ok

      complex(kind=db), dimension(:,:), allocatable:: Mat, Rot, Trans
      complex(kind=db), dimension(-m_hubb:m_hubb,-m_hubb:m_hubb,nspin, 
     &                  n_atom_0_self:n_atom_ind_self):: rot_hubb, rott
      complex(kind=db), dimension(-m_hubb:m_hubb,-m_hubb:m_hubb,nspin,
     &                    n_atom_0_self:n_atom_ind_self):: occ_hubb

      real(kind=db):: Popm, tr
      real(kind=db), dimension(-m_hubb:m_hubb,nspin,
     &                    n_atom_0_self:n_atom_ind_self):: occ_diag

      real(kind=db), dimension(:), allocatable:: Wr, Work
      real(kind=db), dimension(:,:), allocatable:: Nondiag

      if( icheck > 0 ) then
        write(3,110)
        write(3,120) i_self
      end if

      rott(:,:,:,:) = rot_hubb(:,:,:,:)

      rot_hubb(:,:,:,:) = (0._db, 0._db)
      do m = -m_hubb,m_hubb 
        rot_hubb(m,m,:,:) = (1._db, 0._db) 
      end do      

      where( abs(occ_hubb) < eps10 ) occ_hubb = (0._db, 0._db)

      do iapr = n_atom_0_self,n_atom_ind_self
        if( Full_atom ) then
          ipr = iaprotoi(iapr)
          ia = iapr
          it = itypei(ia)
        else
          ipr = iapr
          if( self_nonexc .and. ipr == 0 ) ipr = iprabs ! car l'absorbeur n'etait pas indexe 0 a l'iteration precedente 
          it = itypepr(ipr)
          do ia = 1,natome
            if( iaprotoi(ia) == ipr ) exit
          end do
          if( ia > natome_self ) cycle
        endif 
        if( .not. hubb(it) ) cycle
        l = l_hubbard( numat(it) )

        boucle_spin: do isp = 1, nspin

          if( icheck > 0 ) then
            write(3,130) iapr, isp
            do m = -l,l
              write(3,140) m, occ_hubb(m,-l:l,isp,iapr)
            end do
          endif

! on passe en base cluster et harmo reelle.
! A la premier iteration, on est en base cluster (et complexe)
! Aux iterations suivantes, on est en base locale et reelle
          if( ( i_self > 1 .and. tau_nondiag ) .or. Atom_comp(ia) ) then

            allocate( Rot(-l:l,-l:l) )
            allocate( Mat(-l:l,-l:l) )

            if( Atom_comp(ia) ) then
! Trans : de comp vers reelle
              allocate( Trans(-l:l,-l:l) )
              call Cal_Trans_lh(l,Trans)
            endif

            if( i_self > 1 .and. tau_nondiag  ) then
              Rot(-l:l,-l:l) = Rott(-l:l,-l:l,isp,iapr)
              if( Atom_comp(ia) ) 
     &          Rot = Matmul( Trans, Rot )
            else
              Rot = Trans
            endif

            Mat(-l:l,-l:l) = occ_hubb(-l:l,-l:l,isp,iapr)
            
            Mat = matmul( Rot, matmul( Mat, transpose( conjg(Rot) ) ) )

            if( icheck > 0 ) then
              write(3,145) iapr, isp
              do m1 = -l,l
                write(3,140) m1, Mat(m1,:)
              end do
            end if

            do m1 = -l,l
              do m2 = -l,l
                occ_hubb(m1,m2,isp,iapr) = Mat(m1,m2)
              end do
            end do

            Deallocate( Mat ); Deallocate( Rot )
          endif

          do m = -l,l
            Occ_diag(m,isp,iapr) = real( occ_hubb(m,m,isp,iapr), db )
          end do 

! Analyse de la matrice
          irep = 0
          n_elem(:) = 0
          m_ok( : ) = .false.
          do m1 = -l,l
            if( m_ok(m1) ) cycle
            irep = irep + 1
            i = 1
            m_ok( m1 ) = .true.
            m_elem( i, irep ) = m1
            do m2 = -l,l
              if( m_ok(m2) ) cycle
              if( abs( occ_hubb(m1,m2,isp,iapr) ) < eps10 ) cycle
              i = i + 1
              m_elem( i, irep ) = m2
              m_ok( m2 ) = .true.
! on verifie les elements couplees a m2 sans etre couple a m1
              do m = -l,l
                if( m_ok(m) ) cycle
                if( abs( occ_hubb(m2,m,isp,iapr) ) < eps10 ) cycle
                i = i + 1
                m_elem( i, irep ) = m
                m_ok( m ) = .true.
              end do
            end do
            n_elem(irep) = i
          end do
          nrep = irep
          
          boucle_repr: do irep = 1, nrep

! Les m appartenant aux representations differentes ne peuvent pas se
! melanger
            n = n_elem(irep) 
            if( icheck > 1 ) then
              write(3,150) ia, isp, irep
              write(3,160) n
            endif
            if( n == 1 ) cycle boucle_repr

            allocate( nondiag(n,n) )
            nondiag(:,:) = (0._db, 0._db)

! A l'entree de la sousroutine LAPACK il faut une matrice indexee a partir de 1

            do i = 1,n
              m1 = m_elem(i,irep)
              do j = 1,n
                m2 = m_elem(j,irep)
                nondiag(i,j) = real( occ_hubb(m1,m2,isp,iapr), db )
              end do
            end do 

            if( icheck > 1 ) then
              write(3,170)
              do i = 1, n
                write(3,185) Nondiag(i,1:n)
              end do
            end if

! Si Hubbard est non diagonal, les tau_ato seront non diagonaux
            tau_nondiag = .true.

! Faire attention aux problemes numeriques dus a l'incompatibilite double precision / kind=db 
! Dimensionement du workspace a l'interieur des routines LAPACK
            lwork = 5 * n

! En realite la matrice est toujours reele et symetrique
!            call diagmat(nondiag,VecPr,n,lwork,Atom_comp(ia))

! Il y a un bug dans Lapack --> donne Math erreur sur log 
! Cas symetrique 
! Les vecteurs et valeurs propres sont forcement reels dans ce cas
            allocate( Wr(n) )
            allocate( Work(lwork) )

            call dsyev('V','U',n,Nondiag,n,wr,work,lwork,info)

            if( icheck > 1 ) then
              write(3,190)
              write(3,185) Wr(1:n)
            end if

! Matrice diagonalisee:
            do i = 1,n
              m1 = m_elem(i,irep)
              do j = 1,n
                m2 = m_elem(j,irep)
                if( m1 == m2 ) Occ_diag(m1,isp,iapr) = wr(i)
                rot_hubb(m1,m2,isp,iapr) = cmplx(Nondiag(i,j), 0._db,db)
              end do
            end do 

            deallocate ( Nondiag, Wr, Work )

          end do boucle_repr

          if( Atom_comp(ia) .and. tau_nondiag ) then
            Allocate( Rot(-l:l,-l:l) )
            Rot(-l:l,-l:l) = rot_hubb(-l:l,-l:l,isp,iapr)
            Rot = Matmul( Conjg(Transpose(Trans)), Rot )            
            rot_hubb(-l:l,-l:l,isp,iapr) = Rot(-l:l,-l:l)
            Deallocate( Rot )
          endif
          if( Atom_comp(ia) ) Deallocate( Trans )

          Popm = 3._db - nspin
          do m = -l,l
            occ_diag(m,isp,iapr) = max( occ_diag(m,isp,iapr), 0._db )    
            occ_diag(m,isp,iapr) = min( occ_diag(m,isp,iapr), Popm )
          end do    
          tr = 0._db
          do m = -l, l
            tr = tr + occ_diag(m,isp,iapr)
          end do
          if( icheck > 0 ) write(3,240) tr

! Ecritures:
          if( icheck > 0 ) then
            write(3,200) iapr, isp
            do m = -l, l
              write(3,220) m, rot_hubb(m,-l:l,isp,iapr)
            end do
            write(3,230) iapr, isp
            do m = -l, l
              write(3,140) m, occ_diag(m,isp,iapr)
            end do
          end if

        end do boucle_spin

      end do

      return
  110 format(/' ---- Diag ------',100('-')) 
  120 format(/' Cycle ',i2)
  130 format(/' Hubbard occupation matrix for ia =',i3,', isp =',i2)
  140 format(i3,1p,14(1x,2e13.5))
  145 format(/' Hubbard occupation matrix for ia =',i3,', isp =',i2,
     & ', in cluster basis')
  150 format(/' Atom =',i2,', isp =',i2,', representation number',i2)
  160 format(/' Number of harmonics in the representation =',i2)
  170 format(/' Sub-matrix of occ_hubb before diagonalization')
  185 format(1p,28e13.5)
  190 format(/' Sub-matrix of occ_hubb after diagonalization')
  200 format(/' Hubbard rotation matrix for ia =',i3,', isp = ',i3)
  220 format(i3,1p,28(1x,2e13.5))
  230 format(/' Diagonalized occupation matrix for ia =',i3,', isp =',
     &         i2)
  240 format(/' Number of electron:',f9.5) 
      end

!***********************************************************************

! Selection de la matrice de changement de base de l'atome absorbeur

      subroutine sel_rot_hub_abs(Full_atom,hub_nondiag_abs,hubb,
     &            iaabsi,iaprotoi,iprabs,itabs,lh_abs,m_hubb,
     &            n_atom_0_self,n_atom_ind_self,natome,nspin,ntype,
     &            numat,rot_hubb,rot_hubb_abs)

      use declarations  
      implicit none

      integer:: iaabsi, iapr_s, ipr, iprabs, isp,
     &          itabs, l_hubbard, lh_abs, m, m_hubb,
     &          n_atom_0_self, n_atom_ind_self, natome, nspin, ntype

      integer, dimension(0:ntype):: numat
      integer, dimension(natome):: iaprotoi

      logical Full_atom, hub_nondiag_abs 
      logical, dimension(0:ntype), intent(in):: hubb

      complex(kind=db), dimension(-m_hubb:m_hubb,-m_hubb:m_hubb,nspin, 
     &                       n_atom_0_self:n_atom_ind_self):: rot_hubb
      complex(kind=db), dimension(-m_hubb:m_hubb,-m_hubb:m_hubb,nspin):: 
     &                                                    rot_hubb_abs

      if( .not. hubb(itabs) ) return 

      if( Full_atom ) then
        iapr_s = iaabsi
      else
        ipr = iaprotoi(iaabsi)
        if( ipr == 0 .and. n_atom_0_self == 1 ) ipr = iprabs
        iapr_s = ipr
      endif
      lh_abs = l_hubbard( numat(itabs) )

      do isp = 1,nspin
        do m = -lh_abs,lh_abs
          if( abs( rot_hubb(m,m,isp,iapr_s) - 1._db ) < eps10 ) cycle
          hub_nondiag_abs = .true.
          exit
        end do
      end do
      
      rot_hubb_abs(:,:,:) = rot_hubb(:,:,:,iapr_s) 

      return
      end

!***********************************************************************

! Sousprogramme qui transforme les grandeurs calculees en base locale
! vers la base du cluster en Green et l'inverse en MDF

      subroutine rotation_hubb(Cal_xanes,Full_atom,hubb,
     &          iaprotoi,icheck,iprabs,itypei,green,  
     &          m_hubb,n_atom_0,n_atom_0_self,n_atom_ind,
     &          n_atom_ind_self,natome,natome_self,nbord,nbtm, 
     &          nlmagm,nlmmax,nspin,nspino,ntype,numat,
     &          rot_hubb,spinorbite,tau_ato,Ylmato,Ysens)

      use declarations
      implicit none
 
      integer, intent(in):: icheck, iprabs, m_hubb,  
     &   n_atom_0, n_atom_0_self, n_atom_ind, n_atom_ind_self, 
     &   natome, natome_self, nbtm, nlmagm, nlmmax,  
     &   nspin, nspino, ntype

      logical, intent(in):: Cal_xanes, Full_atom, green, spinorbite,
     &                      Ysens

      integer, dimension(natome), intent(in):: iaprotoi, itypei, nbord
      integer, dimension(0:ntype), intent(in):: numat

      logical, dimension(0:ntype), intent(in):: hubb
      logical, dimension(n_atom_0:n_atom_ind):: iapr_done

      complex(kind=db), dimension(-m_hubb:m_hubb,-m_hubb:m_hubb,nspin, 
     &                     n_atom_0_self:n_atom_ind_self):: rot_hubb
      complex(kind=db), dimension(nlmagm,nspin,nlmagm,nspin,
     &                     n_atom_0:n_atom_ind), intent(inout):: tau_ato

      real(kind=db), dimension(nbtm,nlmmax,natome), intent(inout)::
     &                     Ylmato

      integer i, ia, ia1, iapr, iapr_s, ipr, ipr1, 
     &        iso, isp, isp1, isp2, it, l_hubbard, lh, lm, lm0, lm1,  
     &        lm2, m, m1, m2
      logical smart

      complex(kind=db), dimension(:,:), allocatable:: Mat, rotex, rotex2
      real(kind=db), dimension(:), allocatable:: Vec

      if( icheck > 1 ) write(3,110)
 
      iapr_done(:) = .false.

      do ia = 1,natome 

        it = itypei(ia)
        if( .not. hubb(it) ) cycle 

        if( Full_atom ) then
          iapr = ia
          iapr_s = ia
          if( ia > natome_self ) cycle 
        else
          ipr = iaprotoi(ia)
          iapr = ipr
          if( iapr_done(iapr) .and. green ) cycle
          if( ipr == 0 .and. n_atom_0_self == 1 ) ipr = iprabs
          iapr_s = ipr
          if( Cal_xanes .and. natome_self < natome ) then
! Test si iapr calcule car l'agregat de depart etait trop petit
            do ia1 = 1,natome_self
              ipr1 = iaprotoi(ia1)
              if( ipr1 == 0 .and. n_atom_0_self == 1 ) ipr1 = iprabs
              if( ipr1 == ipr ) exit
            end do 
            if( ia1 == natome_self + 1 ) cycle    
          endif
        end if

        lh = l_hubbard( numat(it) )

! test: si rot_hubb est diagonale ce n'est pas la peine de continuer:
        smart = .false.
        do isp = 1, nspin
          do m = -lh,lh
            if( abs( rot_hubb(m,m,isp,iapr_s) - 1._db ) > eps10 )
     &                                           smart = .true.
          end do
        end do
        if( .not. smart ) cycle

        allocate( rotex(-lh:lh,-lh:lh) )
        if( spinorbite ) allocate( rotex2(-lh:lh,-lh:lh) )
        allocate( mat(-lh:lh,-lh:lh) )

        lm0 = lh**2 + lh + 1

        do isp1 = 1,nspin
          isp = isp1
          do isp2 = 1,nspin
            if( .not. spinorbite .and. isp1 /= isp2 ) cycle                    

! Recopies pour la matrice de rotation 
            do m1 = -lh,lh
              do m2 = -lh,lh
                rotex(m1,m2) = rot_hubb(m1,m2,isp1,iapr_s)
              end do
            end do

            if( isp1 /= isp2 ) then
              do m1 = -lh,lh
                do m2 = -lh,lh
                  rotex2(m1,m2) = rot_hubb(m1,m2,isp2,iapr_s)
                end do
              end do
            endif

            if( icheck > 1 ) then
              write(3,120) ia
              do m1 = -lh,lh
                if( isp1 /= isp2 ) then
                  write(3,130)  rotex(m1,:), rotex2(m1,:)
                else
                  write(3,130)  rotex(m1,:)
                endif
              end do
            end if

            if( Green ) then

              if( .not. iapr_done(iapr) ) then

! Recopies pour tau_ato 
                do m1 = -lh,lh
                  lm1 = lm0 + m1
                  do m2 = -lh,lh
                    lm2 = lm0 + m2
                    Mat(m1,m2) = tau_ato(lm1,isp1,lm2,isp2,iapr)
                  end do
                end do

                if( icheck > 1 ) then
                  write(3,140) iapr
                  do m1 = -lh,lh
                    write(3,130) Mat(m1,:)
                  end do
                end if

                if( isp1 /= isp2 ) then
                  Mat = matmul( rotex,
     &                      matmul( Mat, transpose(conjg(rotex2)) ) )
                else
                  Mat = matmul( rotex,
     &                      matmul( Mat, transpose(conjg(rotex)) ) )
                endif
          
                if( icheck > 1 ) then
                  write(3,150) iapr
                  do m1 = -lh,lh
                    write(3,130) Mat(m1,:)
                  end do
                end if

                do m1 = -lh,lh
                  lm1 = lm0 + m1
                  do m2 = -lh,lh
                    lm2 = lm0 + m2
                    tau_ato(lm1,isp1,lm2,isp2,iapr) = Mat(m1,m2)
                  end do
                end do

              endif

            else

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! modifier si magnetique
              if( isp2 == 2 .or. isp1 == 2 ) cycle

              allocate( Vec(-lh:lh) )

              if( icheck > 1 ) then
                if( Ysens ) then
                  write(3,160) ia, (m, m = -lh,lh)
                else
                  write(3,165) ia, (m, m = -lh,lh)
                endif
              endif
                
              do i = 1,nbord(ia)
 
                Vec(-lh:lh) = Ylmato(i,lm0-lh:lm0+lh,ia)
      
                if( ( icheck > 1 .and. i == 1 ) .or. icheck > 2 )
     &            write(3,170) Vec(-lh:lh)

! Transpose quand on tourne vers base locale
                if( Ysens ) then 
                  Vec = real( matmul( rotex, Vec ), db )
                else
                  Vec = real( matmul( Transpose(rotex), Vec ), db )
                endif
          
                Ylmato(i,lm0-lh:lm0+lh,ia) = Vec(-lh:lh)

              end do

              if( icheck > 1 ) then
                if( Ysens ) then
                  write(3,180) (m, m = -lh,lh)
                else
                  write(3,185) (m, m = -lh,lh)
                endif
                do i = 1,nbord(ia) 
                  if( icheck == 2 .and. i > 1 ) exit
                  write(3,170) Ylmato(i,lm0-lh:lm0+lh,ia)
                end do
              endif

              deallocate( Vec )

            endif

          end do  ! fin boucle isp2

        end do ! fin boucle isp1

        iapr_done(iapr) = .true.

        deallocate( mat )
        deallocate( rotex )
        if( spinorbite ) deallocate( rotex2 ) 

      end do

      return

  110 format(/' ---- Rotation_hub ---',100('-')) 
  120 format(/' Rotation matrix for ia = ',i3) 
  130 format(1p,14(1x,2e13.5))
  140 format(/' Local base: tau_ato before rotation iapr = ',i3)
  150 format(/' Cluster base: tau_ato after rotation iapr = ',i3)
  160 format(/' Cluster base: Ylmato before rotation ia = ',i3,/
     &        ' m =',i4,13i13)
  165 format(/' Hubbard base: Ylmato before rotation ia = ',i3,/
     &        ' m =',i4,13i13)
  170 format(1p,14e13.5)
  180 format(/' Hubbard base: Ylmato after rotation',/
     &        ' m =',i4,13i13)
  185 format(/' Cluster base: Ylmato after rotation',/
     &        ' m =',i4,13i13)
      end

!***********************************************************************

! Rotation des taull de la base cluster vers la base locale 

      subroutine rot_taull(Cal_xanes,Full_atom,hubb,iaabsi,
     &            iaprotoi,icheck,iprabs,itypei,m_hubb,n_atom_0,
     &            n_atom_0_self,n_atom_ind,n_atom_ind_self,natome,
     &            natome_self,nlmagm,nspin,ntype,numat,rot_hubb,
     &            Spinorbite,State_all,tau_ato,taull)

      use declarations
      implicit none
 
      integer, intent(in):: iaabsi, icheck, iprabs, m_hubb,  
     &   n_atom_0, n_atom_ind, n_atom_0_self, n_atom_ind_self, 
     &   natome, natome_self, nlmagm, nspin, ntype

      logical, intent(in):: Cal_xanes, Full_atom, spinorbite, State_all

      integer, dimension(natome), intent(in):: iaprotoi, itypei
      integer, dimension(0:ntype), intent(in):: numat

      logical, dimension(0:ntype), intent(in):: hubb

      complex(kind=db), dimension(-m_hubb:m_hubb,-m_hubb:m_hubb,nspin, 
     &                      n_atom_0_self:n_atom_ind_self):: rot_hubb
      complex(kind=db), dimension(nlmagm,nspin,nlmagm,nspin,natome),
     &                                intent(inout):: taull
      complex(kind=db), dimension(nlmagm,nspin,nlmagm,nspin,
     &                     n_atom_0:n_atom_ind), intent(inout):: tau_ato

      integer ia, ia1, iapr, iapr_s, ib, ipr, ipr1, isp, isp1, isp2,  
     &        it, l_hubbard, lh, lm0, lm1, lm2, m, m1, m2, nb
      logical smart

      complex(kind=db), dimension(:,:), allocatable:: mat, rotex, rotex2

      if( icheck > 1 ) write(3,110)
 
      do ia = 1,natome 

        if( ia /= iaabsi .and. .not. state_all .and. cal_xanes ) cycle

        it = itypei(ia)
        if( .not. hubb(it) ) cycle 

        if( Full_atom ) then
          iapr = ia
          iapr_s = ia
          if( ia > natome_self ) cycle 
        else
          ipr = iaprotoi(ia)
          iapr = ipr
          if( ipr == 0 .and. n_atom_0_self == 1 ) ipr = iprabs
          iapr_s = ipr
          if( natome_self < natome ) then
            do ia1 = 1,natome_self
              ipr1 = iaprotoi(ia1)
              if( ipr1 == 0 .and. n_atom_0_self == 1 ) ipr1 = iprabs
              if( ipr1 == ipr ) exit
            end do 
            if( ia1 == natome_self + 1 ) cycle    ! le iapr en cause n'etait pas calcule en hubbard, car l'agregat de depart etait trop petit
          end if
        end if

        lh = l_hubbard( numat(it) )

! test: si rot_hubb est diagonale ce n'est pas la peine de continuer:
        smart = .false.
        do isp = 1, nspin
          do m = -lh,lh
            if( abs( rot_hubb(m,m,isp,iapr_s) - 1._db ) > eps10 )
     &                                           smart = .true.
          end do
        end do
        if( .not. smart ) cycle

        allocate( rotex(-lh:lh,-lh:lh) )
        if( spinorbite ) allocate( rotex2(-lh:lh,-lh:lh) ) 
        allocate( mat(-lh:lh,-lh:lh) )

        lm0 = lh**2 + lh + 1

        do isp1 = 1, nspin                    
          do isp2 = 1, nspin
            if( .not. spinorbite .and. isp1 /= isp2 ) cycle                    

! Recopies pour la matrice de rotation 
            do m1 = -lh,lh
              do m2 = -lh,lh
                rotex(m1,m2) = rot_hubb(m1,m2,isp1,iapr_s)
              end do
            end do

            if( isp1 /= isp2 ) then
              do m1 = -lh,lh
                do m2 = -lh,lh
                  rotex2(m1,m2) = rot_hubb(m1,m2,isp2,iapr_s)
                end do
              end do
            endif

            if( ia == iaabsi ) then
              nb = 2
            else
              nb = 1
            endif

            do ib = 1,nb
! Recopies pour taull 
              do m1 = -lh,lh
                lm1 = lm0 + m1
                do m2 = -lh,lh
                  lm2 = lm0 + m2
                  if( ib == 1 ) then
                    mat(m1,m2) = taull(lm1,isp1,lm2,isp2,ia)
                  else
                    mat(m1,m2) = tau_ato(lm1,isp1,lm2,isp2,iapr)
                  endif
                end do
              end do

              if( icheck > 1 ) then
                if( ib == 1 ) then
                  write(3,140) iapr
                else
                  write(3,'(/A)') ' Cluster base: tau_ato absorb. atom'
                endif
                do m1 = -lh,lh
                  write(3,130) mat(m1,:)
                end do
              end if

! Rotation inverse par rapport a la routine "rotations"
              if( isp1 /= isp2 ) then
                mat = matmul( transpose(conjg(rotex)),
     &                                   matmul( mat, rotex2 ) )
              else
                mat = matmul( transpose(conjg(rotex)),
     &                                   matmul( mat, rotex ) )
              endif
          
              if( icheck > 1 ) then
                if( ib == 1 ) then
                  write(3,150) iapr
                else
                  write(3,'(/A)') ' Local base: tau_ato absorb. atom'
                endif
                do m1 = -lh,lh
                  write(3,130) mat(m1,:)
                end do
              end if

              do m1 = -lh,lh
                lm1 = lm0 + m1
                do m2 = -lh,lh
                  lm2 = lm0 + m2
                  if( ib == 1 ) then
                    taull(lm1,isp1,lm2,isp2,ia) = mat(m1,m2)
                  else
                    tau_ato(lm1,isp1,lm2,isp2,iapr) = mat(m1,m2)
                  endif
                end do
              end do

            end do ! fin boucle nb

          end do
        end do

        deallocate( mat )
        deallocate( rotex )
        if( spinorbite ) deallocate( rotex2 )
 
      end do

      return

  110 format(/' ---- Rot_taull ------',100('-')) 
  130 format(1p,28e13.5)
  140 format(/' Cluster base: taull before rotation ia = ',i3)
  150 format(/' Local base: taull after rotation ia = ',i3)
      end

!***********************************************************************

! Transformation harmo comp vers Harmo reel pour un lh donne.
! La transformation inverse est le conjugue de la transpose

      subroutine Cal_Trans_lh(lh,Trans)

      use declarations
      implicit none

      integer:: is, lh, m1, m2
 
      complex(kind=db):: r2_r, r2_i
      complex(kind=db),dimension(-lh:lh,-lh:lh):: Trans

      real(kind=db):: r2

      Trans(:,:) = (0._db, 0._db)

      r2 = 1 / sqrt(2._db) 
      r2_r = cmplx( r2,    0._db, db)
      r2_i = cmplx( 0._db, r2,    db)

      do m1 = -lh,lh
        is = (-1)**m1 
        do m2 = -lh,lh
                     
          if( m1 == m2 ) then

            if( m1 == 0 ) then
              Trans(m1,m2) = (1._db,0._db)
            elseif( m1 > 0 ) then
              Trans(m1,m2) = r2_r
            else
              Trans(m1,m2) = is * r2_i
            endif

          elseif( m1 == - m2 ) then

            if( m1 > 0 ) then
              Trans(m1,m2) = is * r2_r
            else
              Trans(m1,m2) = - r2_i
            endif

          endif

        end do
      end do 
   
      return
      end
