! Routines of the FDMNES package

!***********************************************************************

! Sousprogramme elaborant la grille en energie pour le calcul du niveau
! de Fermi

      subroutine grille_coh(eimag_coh,energ_coh,E_start,Green,
     &                           icheck,nenerg_coh,Pas_SCF)
     
      use declarations
      implicit real(kind=db) (a-h,o-z)

      integer nenerg_coh

      logical Green

      real(kind=db), dimension(nenerg_coh) :: energ_coh, eimag_coh
        
      energ_coh(1) = E_start 
           
      do ie = 2,nenerg_coh
        energ_coh(ie) = energ_coh(ie-1) + Pas_SCF
      end do

      if( Green ) then
        eimag_coh(1:nenerg_coh) = 2 * Pas_SCF
      else
        eimag_coh(1:nenerg_coh) = 0._db
      endif
 
      if( icheck > 1 ) then
        write(3,110)
        write(3,120)
        do ie = 1,nenerg_coh
          write(3,130) energ_coh(ie)*rydb, eimag_coh(ie)*rydb
        end do
      endif

      return
  110 format(/' ---- Grille_coh -',100('-'))
  120 format(/'   energie    eimag      en eV')
  130 format(4f9.3)
      end

!***********************************************************************

! Calcul de la charge du petit agregat

      subroutine chg_agr(chargat,chargat_init,ch_c,chg_cluster,
     &               chg_open_val,Full_atom,iaprotoi,iprabs,
     &               ispin_maj,itabs,icheck,itypepr,lcoeur,mpirank,
     &               natome,n_atom_0_self,n_atom_ind_self,
     &               n_atom_proto,nb_eq,ncoeur,ngreq,nrato,nrm,
     &               nrm_self,nspin,ntype,numat,pop_open_val,psi_coeur,
     &               psi_open_val,rato,rho_chg,rho_coeur,rho_cor,
     &               rhoato_init,rmtsd,SCF_mag_fix,SCF_mag_free,sgn)             

      use declarations
      implicit real(kind=db) (a-h,o-z)
 
      integer:: itabs, Sum_Z
      integer, dimension(0:ntype):: nrato, numat 
      integer, dimension(2,0:ntype):: lcoeur, ncoeur
      integer, dimension(0:n_atom_proto):: itypepr, ngreq
      integer, dimension(natome):: iaprotoi, nb_eq
      integer, dimension(n_atom_0_self:n_atom_ind_self):: ispin_maj 

      logical:: Full_atom, SCF_mag_fix, SCF_mag_free
      logical, dimension(n_atom_0_self:n_atom_ind_self):: sgn
     
      real(kind=db), dimension(nspin):: chg_cluster 
      real(kind=db), dimension(0:n_atom_proto):: chargat 
      real(kind=db), dimension(0:nrm,0:ntype):: rato, rho_coeur,
     &                                            rho_cor
      real(kind=db), dimension(0:nrm_self,nspin, 
     &           n_atom_0_self:n_atom_ind_self):: rho_chg, rhoato_init
      real(kind=db), dimension(0:n_atom_proto):: rmtsd
      real(kind=db), dimension(0:nrm)::  r, rh          
      real(kind=db), dimension(n_atom_0_self:n_atom_ind_self):: ch_v, 
     &                              ch_c, chargat_sup        
      real(kind=db), dimension(n_atom_0_self:n_atom_ind_self,nspin)::
     &                                                    chargat_init 
      real(kind=db), dimension(0:nrm,2,0:ntype):: psi_coeur 
      real(kind=db), dimension(nrm,2):: psi_open_val
      real(kind=db), dimension(2):: chg_open_val, pop_open_val

      charge_init = 0._db
      chg_cluster(:) = 0._db
      chg_sup = 0._db
      chg_coeur = 0._db         
      chargat_init(:,:) = 0._db
      chargat_sup(:) = 0._db
      
      if( icheck > 0 ) write(3,110)       

      sum_Z = 0
      SCF_mag_fix = .false.

      do iapr = n_atom_0_self,n_atom_ind_self
        if( Full_atom ) then
          ipr = iaprotoi(iapr)
          n = nb_eq(iapr) 
        else
          ipr = iapr
          n = ngreq(ipr) 
        endif
        it = itypepr(ipr)
        nr = nrato(it)
        rayint = rmtsd(ipr)
        r(0:nrm) = rato(0:nrm,it)
        
! Calcul de la charge totale de l'agregat: rhoato est la vraie densite
        do ispin = 1,nspin
          rh(0:nr) = rhoato_init(0:nr,ispin,iapr) * r(0:nr)**2
          res = quatre_pi * f_integr3(r,rh,nr,0,nrm,rayint)
          chargat_init(iapr,ispin) = chargat_init(iapr,ispin) + res
        end do
        if( nspin == 2 .and.
     &    chargat_init(iapr,nspin) > chargat_init(iapr,1) + eps10 ) then
          ispin_maj(iapr) = 2
        else
          ispin_maj(iapr) = 1
        endif
        if( nspin == 2 .and. .not. SCF_mag_free .and.
     &  abs( chargat_init(iapr,nspin) - chargat_init(iapr,1) ) > eps10 )
     &    SCF_mag_fix = .true.

        isp = ispin_maj(iapr)             
        chg_cluster(1) = chg_cluster(1) + chargat_init(iapr,isp) * n
        if( nspin == 2 ) then
          isp = 3 - ispin_maj(iapr)             
          chg_cluster(2) = chg_cluster(2) + chargat_init(iapr,isp) * n
        endif
         
! Calcul de la charge totale de l'agregat: rho_chg est la densite
! venant des atomes exterieurs au petit agregat.
        do ispin = 1, nspin
          rh(0:nr) = rho_chg(0:nr,ispin,iapr) * r(0:nr)**2
          res = quatre_pi * f_integr3(r,rh,nr,0,nrm,rayint)
          chargat_sup(iapr) = chargat_sup(iapr) + res
        end do
        chg_sup = chg_sup + chargat_sup(iapr) * n
         
! Calcul de la charge des orbitales de coeur: rho_coeur est la vraie
! densite
        rh(0:nr) = rho_coeur(0:nr,it) * r(0:nr)**2
        ch_c(iapr) = quatre_pi * f_integr3(r,rh,nr,0,nrm,rayint)
! Si les orbitales de coeur sont trop approchees de celles de valence,
! on fait une correction:
        if( sgn(iapr) ) then
          rho_cor(0,it) = 0._db
          fac = ( 4 * lcoeur(1,it) + 2 ) / quatre_pi 
          rho_cor(1:nr,it) = fac * ( psi_coeur(1:nr,1,it) / r(1:nr) )**2
          rh(0:nr) = rho_cor(0:nr,it) * r(0:nr)**2
          corr = quatre_pi * f_integr3(r,rh,nr,0,nrm,rayint)
! psi_coeur sont les fct d'onde *r*(4pi)**0.5
! psi_coeur sont normees, on multiplie par la populations
          ch_c(iapr) = ch_c(iapr) - corr
        else
          rho_cor(:,it) = 0._db
        end if
 
        chg_coeur = chg_coeur + ch_c(iapr) * n
        Sum_Z = Sum_Z + n * numat(it) 
        charge_init = charge_init + n * chargat(ipr) 
      end do 

! Calcul de la charge de valence:
      chg = sum( chg_cluster(:) ) - chg_coeur

      if( icheck > 0 ) then
         write(3,120) chg
         write(3,130) chg_coeur
         if( .not. SCF_Mag_fix ) then
           write(3,140) sum( chg_cluster(:) )
         else
           write(3,142) sum( chg_cluster(:) ), chg_cluster(:)
         endif
         write(3,145) chg_sup
         write(3,150) Sum_Z
         write(3,160) charge_init
         write(3,170) Sum_Z - charge_init - sum( chg_cluster(:) )
      end if

      do iprint = 3,6,3
        if( iprint == 3 .and. icheck == 0 ) cycle 
        if( iprint == 6 .and. mpirank /= 0 ) cycle
        if( nspin == 1 ) then 
          write(iprint,300)
        else
          write(iprint,305)
        endif
        do iapr = n_atom_0_self,n_atom_ind_self
          ch_v(iapr) = sum(chargat_init(iapr,:)) - ch_c(iapr)
          if( Full_atom ) then
            ipr = iaprotoi(iapr)
          else
            ipr = iapr
          endif
          it = itypepr(ipr)
          if( nspin == 1 ) then 
            write(iprint,310) iapr, numat(it), ch_v(iapr), ch_c(iapr),
     &                 sum(chargat_init(iapr,:)), chargat_sup(iapr), 
     &                 numat(it)- sum(chargat_init(iapr,:))
          else
            write(iprint,310) iapr, numat(it), ch_v(iapr), ch_c(iapr),
     &                 sum(chargat_init(iapr,:)),
     &                 chargat_init(iapr,1) - chargat_init(iapr,2),
     &                 chargat_sup(iapr), 
     &                 numat(it)- sum(chargat_init(iapr,:))
          endif
        end do
      end do

      Sum_Z = Sum_Z - charge_init
 
      if( icheck > 1 ) then
        write(3,350)
        write(3,351) sum(ch_v(:))
        write(3,352) sum(ch_c(:))
        write(3,353) sum(chargat_init(:,:))

        do iapr = n_atom_0_self,n_atom_ind_self
          if( Full_atom ) then
            ipr = iaprotoi(iapr)
          else
            ipr = iapr
          endif
          it = itypepr(ipr)
          write(3,403) iapr
          if( nspin == 1 ) then
            write(3,401)
          else
            write(3,402)
          end if
          do ir = 1, nrato(it)
            write(3,405) rato(ir,it)*bohr, 
     &             quatre_pi * rato(ir,it)**2 * rho_chg(ir,1:nspin,iapr)
          end do
          if(sgn(iapr)) write(3,410) numat(it),ncoeur(1,it),lcoeur(1,it)
        end do
      end if

      do iapr = n_atom_0_self,n_atom_ind_self
        if( Full_atom ) then
          ipr = iaprotoi(iapr)
        else
          ipr = iapr
        endif
        it = itypepr(ipr)
        chargat_init(iapr,:) = real(numat(it),db) / nspin
     &                       - chargat_init(iapr,:)
      end do
 
! coupure ad hoc au niveau de Fermi: indice 1 <=> nonexcited absorber
!                                    indice 2 <=>    excited absorber

      do i = 1,2
        if( i == 1 ) then
          it = itabs
          rayint = rmtsd(iprabs) 
        else
          it = 0
          rayint = rmtsd(0)
        endif
        r(:) = rato(:,it)
        rh(0) = 0._db
        rh(1:nrm) = psi_open_val(1:nrm,i)**2
        nr = nrato(it)
        chg_open_val(i) = f_integr3(r,rh,nr,0,nrm,rayint)
! psi_level_val etait normalise a l'unite:
        chg_open_val(i) = chg_open_val(i) * pop_open_val(i)
      end do

      if( icheck > 0 ) then
        write(3,415) chg_open_val(1), pop_open_val(1)
        write(3,416) chg_open_val(2), pop_open_val(2)
      end if

      if( icheck > 2 ) then
       write(3,500)
       do ir = 1,nrato(itabs)
         f = quatre_pi * rato(ir,itabs)**2 
         write(3,405) rato(ir,itabs)*bohr, f * psi_open_val(ir,:)**2
       end do
      end if

      return
  110 format(/' ---- Chg_agr --------',100('-'))
  120 format(/' Number of valence electrons =',f9.3)  
  130 format(' Number of core electrons    =',f9.3)   
  140 format(' Total                       =',f9.3)   
  142 format(' Total, spin maj, spin min   =',3f9.3)   
  145 format(' Charge from outer sphere    =',f9.3)   
  150 format(' Sum of atomic number        =',i5)   
  160 format(' Initial charge              =',f9.3)   
  170 format(' Cluster charge              =',f9.3)   
  300 format(/' ia   Z     ch_val    ch_core   ch_total     ch_out  ',
     &        ' Atom charge')
  305 format(/' ia   Z     ch_val    ch_core   ch_total   ch_up-dn',
     &        '     ch_out     Charge')
  310 format(i3,i4,6f11.3) 
  350 format(/' Reference charges in the symmetrised cluster ')
  351 format(10x,'valence electrons: ',f9.3)
  352 format(10x,'core electrons: ',f9.3)
  353 format(10x,'total number of electrons: ',f9.3)
  401 format(/' Density coming from atoms outside the cluster',/
     &        7x,'rato     4pi*r2*rho_chg') 
  402 format(/' Density coming from atoms outside the cluster',/
     &        7x,'rato    4pi*r2*rho_chg(u)    4pi*r2*rho_chg(d)')  
  403 format(/7x,'ia =  ',i4) 
  405 format(1p,e13.5,2e17.5)
  410 format(/'For Z =',i4,2x,'orbital n =',i4,2x,'and l =',i4,
     &       2x,'is in the valence band')
  415 format(/' Number of electron in the valence orbital of the',
     &        ' absorbing atom:',/ 20x,'Integrated  Mulliken',
     &       /'   non excited atom =',f7.3,f10.3)
  416 format('   excited atom     =',f7.3,f10.3)
  500 format(/6x,'rato   4pi*r2*psi_open_val_nonexc**2', 
     &           '  4pi*r2*psi_open_val_exc**2') 
      end

!***********************************************************************

! Calcul du point du depart en energie, pour la determination du niveau
! de Fermi

      subroutine En_dep(E_start,Full_atom,Green,iaprotoi,
     &              icheck,itypepr,lcoeur,n_atom_0,n_atom_0_self,
     &              n_atom_ind,n_atom_ind_self,n_atom_proto,natome,
     &              ncoeur,nenerg_coh,nrato,nrm,numat,nspin,ntype,
     &              Pas_SCF,psi_coeur,relativiste,rato,sgn,
     &              Vcato,Vxcato,workf)
                  
      use declarations
      implicit real(kind=db) (a-h,o-z)
      implicit integer (i-n)

      integer, dimension(2,0:ntype):: lcoeur, ncoeur
      integer, dimension(0:ntype):: nrato, numat
      integer, dimension(natome):: iaprotoi
      integer, dimension(0:n_atom_proto):: itypepr

      logical:: Full_atom, Green, Relativiste
      logical, dimension(n_atom_0_self:n_atom_ind_self):: sgn

      real(kind=db), dimension(2):: E_psi_all
      real(kind=db), dimension(0:nrm,0:ntype):: rato  
      real(kind=db), dimension(0:nrm):: r, pot, psi    
      real(kind=db), dimension(0:nrm,n_atom_0:n_atom_ind):: Vcato
      real(kind=db), dimension(0:nrm,nspin,n_atom_0:n_atom_ind):: Vxcato
      real(kind=db), dimension(0:nrm,2,0:ntype):: psi_coeur       
      real(kind=db), dimension(2,n_atom_0_self:n_atom_ind_self):: E_psi
      
! On selectione l'orbitale de coeur de plus grande energie et celle de
! valence de plus basse energie (valable pour les niveaux atomiques
! ordones selon la regle d'occupation. Il est possible qu'ils soient
! inverses dans le cas d'un agregat)
! ces energies sont stoquees en E_psi

! l'indice 1 correspond a la derniere orbitale de coeur
! l'indice 2 correspond a la premiere orbitale de valence 

      if( icheck > 0 ) write (3,110)

      boucle_agr: do iapr = n_atom_0_self,n_atom_ind_self
        if( Full_atom ) then
          ipr = iaprotoi(iapr)
        else
          ipr = iapr
        endif 
        it = itypepr(ipr)
        r(:) = rato(:,it)
        nr = nrato(it)

        do i = 1,2
          psi(:) = psi_coeur(:,i,it) 
    
 ! On fait une moyenne sur les deux spins, si besoin :
         do ir = 1, nrm
           Vxc = sum( Vxcato(ir,:,iapr) ) / nspin
           pot(ir) = Vcato(ir,iapr) + Vxc
         end do
         E_psi(i,iapr) =  psiHpsi(.false.,0,nrm,1,lcoeur(i,it),0,
     &                               0,0,nr,nrm,1,1,0,0._db,pot,
     &                               psi,r,Relativiste,.false.)
        end do 
               
      end do boucle_agr
 
! Recherche de E_psi_all(2): orbitale de valence de plus basse Energie
      E_psi_all(2) = E_psi(2,n_atom_0_self)   
      do iapr = n_atom_0_self+1,n_atom_ind_self
        E_psi_all(2) = min( E_psi_all(2), E_psi(2,iapr) )
      end do
      E_psi_all(1) = E_psi(1,n_atom_0_self)   
      do iapr = n_atom_0_self+1,n_atom_ind_self
        E_psi_all(1) = max( E_psi_all(1), E_psi(1,iapr) )
      end do

      E_marge = 10._db / rydb

      sgn(:) = .false.
      if( E_psi_all(2) - E_psi_all(1) > 2*E_marge ) then 
        E_start = E_psi_all(2) - E_marge 
      elseif( E_psi_all(2) - E_psi_all(1) > E_marge ) then 
        E_start = 0.5_db * ( E_psi_all(1) + E_psi_all(2) )
      else 
! Cas des orbitales de coeur trop proches de celles de valence
        E_start1 = E_psi_all(2) - E_marge 
        do japr = n_atom_0_self,n_atom_ind_self
          E_start = E_start1 
          do iapr = n_atom_0_self,n_atom_ind_self
            if( sgn(iapr) ) cycle
            if( E_psi(1,iapr) < E_start ) cycle
            sgn(iapr) = .true.
            E_start1 = E_psi(1,iapr) - E_marge 
          end do 
        end do 
        E_start = E_start1
      endif 

! les potentiels sont references en fonction du niveau du vide alors que 
! la gamme d'energie commence dessous (workf)
      E_start = E_start + Workf

! Pas en energie
      if( Green ) then
        Pas_SCF = 0.1_db / rydb
      else
        Pas_SCF = 0.01_db / rydb
      endif
      E_max_Fermi = 30._db / rydb

! Evaluation du nombre de points pour la grille en energie
      nenerg_coh = nint( ( E_max_Fermi - E_start ) / Pas_SCF ) + 1      
      
      if( icheck > 0 ) then
        write(3,120)
        do iapr = n_atom_0_self,n_atom_ind_self
          if( Full_atom ) then
            ipr = iaprotoi(iapr)
          else
            ipr = iapr
          endif 
          it = itypepr(ipr)
          write(3,130) iapr, numat(it), 
     &      ( ncoeur(i,it), lcoeur(i,it), E_psi(i,iapr)*rydb, i = 1,2 )
        end do
      end if

      if( icheck > 2 ) then
        do it = 0,ntype
          write(3,135) numat(it), (ncoeur(i,it), lcoeur(i,it), i = 1,2)
          do ir = 1,nrato(it)
            write(3,137) ( psi_coeur(ir,i,it), i = 1,2 )
          end do
        end do
      end if
      
      if( icheck > 0 ) write (3,150) E_start * rydb, - Workf *rydb
      
      return
  110 format(/' ---- En_dep --------',100('-'))
  120 format(/'  ia     Z    n  l    E_core   n  l     E_val',
     &'   zero at infinity')   
  130 format(i3,4x,i3,2x,2(i3,i3,f11.3))   
  135 format(/'  Z = ',i3,3x,'n_coeur = ',i1,3x,'l_coeur = ',i1,3x,
     &         'n_val = ',i1,3x,'l_val = ',i1//
     &          4x,'psi_coeur    ','psi_val    '/)
  137 format(1p,2e14.6)
  150 format(/' Starting energy = ',f8.3,' eV,    zero at',f7.2,' eV')    
      end

!***********************************************************************

! Sousroutine qui corrige l'energie sortie du calcul DFT
! les energies sont en rydb
      
      subroutine En_DFT(En_cluster,Energ_self,en_coeur,excato,
     &           Full_atom,Hubb,iaprotoi,icheck,itypepr,m_hubb,
     &           n_atom_0,n_atom_0_self,n_atom_ind,n_atom_ind_self,
     &           n_atom_proto,natome,nb_eq,ngreq,nrm,nrm_self,nrato,
     &           nspin,ntype,numat,occ_diag,rato,rho_self,rmtsd,
     &           V_hubbard,Vcato,Vxcato)
   
      use declarations
      implicit real(kind=db) (a-h,o-z)
      include 'mpif.h'

      integer, dimension(0:ntype):: nrato, numat
      integer, dimension(natome):: iaprotoi, nb_eq
      integer, dimension(0:n_atom_proto):: itypepr, ngreq

      logical Full_atom
      logical, dimension(0:ntype):: Hubb

      real(kind=db), dimension(0:ntype):: V_hubbard
      real(kind=db), dimension(0:nrm_self,nspin,
     &                   n_atom_0_self:n_atom_ind_self):: rho_self
      real(kind=db), dimension(nrm,n_atom_0:n_atom_ind):: excato
      real(kind=db), dimension(n_atom_0_self:n_atom_ind_self)::
     &                   En_coeur, Energ_self, Energ_self_KS, 
     &                   E_exc, E_coul, E_hubbard, E_Vxc
      real(kind=db), dimension(0:nrm,0:ntype):: rato
      real(kind=db), dimension(0:nrm,nspin,n_atom_0:n_atom_ind):: Vxcato
      real(kind=db), dimension(0:nrm,n_atom_0:n_atom_ind):: Vcato, 
     &                                                      Vhartree                                                       
      real(kind=db), dimension(0:nrm):: fct1, fct2, fct3, r, r2
      real(kind=db), dimension(0:n_atom_proto):: rmtsd
      real(kind=db), dimension(-m_hubb:m_hubb,nspin,
     &                    n_atom_0_self:n_atom_ind_self):: occ_diag

! Enragr: la somme des valeurs propres pour les orbitales Kohn Sham
! occupees

      if( nrm /= nrm_self ) return  ! car rho_self n'est pas calcule

      Energ_self_KS(:) = Energ_self(:)

      boucle_ia: do iapr = n_atom_0_self,n_atom_ind_self
        if( Full_atom ) then
          ipr = iaprotoi(iapr)
        else
          ipr = iapr
        endif 
        it = itypepr(ipr)
        nr = nrato(it)   
        r(:) = rato(:,it)  
        r2(1:nr) = r(1:nr)**2
        r2(0) = 0._db  

        fct1(:) = 0._db; fct2(:) = 0._db; fct3(:) = 0._db

! le potentiel dont on a besoin pour la correction est le vrai Hartree,
!  c'est a dire il faut enlever la contribution des noyaux
       
        Vhartree(1:nr,iapr) = Vcato(1:nr,iapr) + 2 * numat(it) / r(1:nr)

        do ir = 1,nr 
          fac = sum( rho_self(ir,1:nspin,iapr) ) * r2(ir) 
          fct1(ir) = excato(ir,iapr) * fac
          fct2(ir) = Vhartree(ir,iapr) * fac
        end do 

        E_exc(iapr) = quatre_pi * f_integr3(r,fct1,nr,0,nrm,Rmtsd(ipr))
        E_coul(iapr) = quatre_pi * f_integr3(r,fct2,nr,0,nrm,Rmtsd(ipr))
        E_coul(iapr) = 0.5_db * E_coul(iapr)        

        E_Vxc(iapr) = 0._db
        do ispin = 1, nspin
          do ir = 1,nr
            fct3(ir) = Vxcato(ir,ispin,iapr) * rho_self(ir,ispin,iapr)
     &               * r2(ir)
          end do
          E_Vxc(iapr) = E_Vxc(iapr)
     &               + quatre_pi * f_integr3(r,fct3,nr,0,nrm,Rmtsd(ipr))
        end do

        E_hubbard(iapr) = 0._db
        fac = 1._db / ( 3 - nspin )  
        if( Hubb(it) ) then
          do isp = 1,nspin
            do m = -m_hubb,m_hubb
              E_hubbard(iapr) = E_hubbard(iapr) + 0.5_db * V_hubbard(it)
     &                   * occ_diag(m,isp,iapr)
     &                   * ( 1 - fac * occ_diag(m,isp,iapr) )
            end do
          end do
        endif         

! En_coeur: energie des orbitales KS correspondant aux etats de coeur,
! pour chaque atome;

! ATTENTION aux signes
        Energ_self(iapr) = Energ_self_KS(iapr) + En_coeur(iapr)  
     &                   + E_exc(iapr) - E_coul(iapr) - E_Vxc(iapr) 
     &                   + E_hubbard(iapr)
      end do boucle_ia

      Energ_self_KS_agr = 0._db
      Delta_En_coeur_agr = 0._db
      E_exc_agr = 0._db
      E_coul_agr = 0._db
      E_Vxc_agr = 0._db
      En_cluster = 0._db
      E_hubbard_agr = 0._db
       
      do iapr = n_atom_0_self,n_atom_ind_self
        if( Full_atom ) then
          n = nb_eq(iapr)
        else
          n = ngreq(iapr)
        endif
        Energ_self_KS_agr = Energ_self_KS_agr + n * Energ_self_KS(iapr)
        Delta_En_coeur_agr = Delta_En_coeur_agr + n * En_coeur(iapr)
        E_exc_agr = E_exc_agr + n * E_exc(iapr)
        E_coul_agr = E_coul_agr + n * E_coul(iapr)
        E_Vxc_agr = E_Vxc_agr + n * E_Vxc(iapr)
        E_hubbard_agr = E_hubbard_agr + n * E_hubbard(iapr)
        En_cluster = En_cluster + n * Energ_self(iapr)
      end do

      if( icheck > 0 ) then
        write(3,100)
        if( Full_atom ) then
          write(3,110) En_cluster * rydb
        else
          write(3,120) En_cluster * rydb
        endif
        write(3,500)
        do iapr = n_atom_0_self,n_atom_ind_self
          if( Full_atom ) then
            ipr = iaprotoi(iapr)
          else
            ipr = iapr
          endif
          it = itypepr( ipr ) 
          write(3,510) iapr, numat(it), Energ_self(iapr) * rydb, 
     &       Energ_self_KS(iapr) * rydb, En_coeur(iapr) * rydb,  
     &       E_exc(iapr) * rydb, E_coul(iapr) * rydb,
     &       E_Vxc(iapr) * rydb, E_hubbard(iapr) * rydb
        end do 
        write(3,520) En_cluster * rydb, 
     &       Energ_self_KS_agr * rydb, Delta_En_coeur_agr * rydb, 
     &       E_exc_agr * rydb, E_coul_agr * rydb,  
     &       E_Vxc_agr * rydb, E_hubbard_agr * rydb 
      end if

      return 

  100 format(/' ---- En_DFT ------',100('-'))
  110 format(/' Cluster energy: ', f12.3,' eV')
  120 format(/' Unit cell energy: ', f12.3,' eV')
  500 format(/'  ia   Z    Energ_atom      Energ_KS', 
     &  '   Delta_En_coeur     E_exc        E_coul        E_Vxc',
     &  '        E_hubbard')
  510 format(2i4,7f14.3)
  520 format(/'  Total:',7f14.3)
      end

!********************************************************************************

! Sousprogramme qui calcule l'energie de la derniere orbitales de coeur Kohn Sham;
! On considere que pendant les iterations la fonction d'onde de cette orbitale ne
! se modifie pas, par contre le potentiel et donc son energie bougent. On fait 
! l'approximation que ce deplacement est le meme pour tous les niveaux de coeur 

! le resultat sera utilise pour le calcul de l'energie de l'agregat

      subroutine eps_coeur(ch_coeur,En_coeur,Full_atom,iaprotoi,
     &            itypepr,lcoeur,n_atom_0,n_atom_0_self,
     &            n_atom_ind,n_atom_ind_self,n_atom_proto,natome,
     &            nrato,nrm,nspin,ntype,psi_coeur,rato,Relativiste,
     &            Vcato,Vxcato)

      use declarations
      implicit none

      integer iapr, ipr, ir, it, natome,
     &        n_atom_0, n_atom_0_self, n_atom_ind, n_atom_ind_self,
     &        n_atom_proto, nr, nrm, nspin, ntype

      logical Full_atom, Relativiste

      integer,dimension(2,0:ntype):: lcoeur
      integer,dimension(natome):: iaprotoi
      integer,dimension(0:n_atom_proto):: itypepr
      integer,dimension(0:ntype):: nrato

      real(kind=db):: psiHpsi, Vxc, res

      real(kind=db),dimension(n_atom_0_self:n_atom_ind_self):: ch_coeur
      real(kind=db),dimension(n_atom_0_self:n_atom_ind_self):: En_coeur
      real(kind=db),dimension(0:nrm,0:ntype):: rato
      real(kind=db),dimension(0:nrm):: r, psi, pot
      real(kind=db),dimension(0:nrm,n_atom_0:n_atom_ind):: Vcato
      real(kind=db),dimension(0:nrm,nspin,n_atom_0:n_atom_ind):: Vxcato
      real(kind=db),dimension(0:nrm,2,0:ntype):: psi_coeur

      En_coeur(:) = 0._db
      
      do iapr = n_atom_0_self,n_atom_ind_self
        if( Full_atom ) then
          ipr = iaprotoi(iapr)
        else
          ipr = iapr
        endif
        it = itypepr(ipr)
        nr = nrato(it)
        r(:) = rato(:,it)
 ! On fait une moyenne sur les deux spins, si besoin :
        do ir = 1, nrm
          Vxc = sum( Vxcato(ir,1:nspin,iapr) ) / nspin
          pot(ir) = Vcato(ir,iapr) + Vxc
          psi(ir) = psi_coeur(ir,1,it)
        end do
        res =  psiHpsi(.false.,0,nrm,1,lcoeur(1,it),0,0,
     &                 0,nr,nrm,1,1,0,0._db,pot,
     &                 psi,r,Relativiste,.false.)
        En_coeur(iapr) = res * ch_coeur(iapr)                         
      end do

      return
      end

!***********************************************************************

! Preparation de l'iteration suivante

      subroutine prep_next_iter(chargat_self,chargat_self_s,
     &             Convergence,Delta_En_conv,Delta_energ,Delta_energ_s,
     &             Delta_energ_t,En_cluster,En_cluster_s,En_cluster_t,
     &             Energ_self,Energ_self_s,Fermi,Full_atom,hubbard,
     &             i_self,icheck,m_hubb,
     &             mpirank,n_atom_0_self,n_atom_ind_self,n_atom_proto,
     &             natome,natomeq,nb_eq,ngreq,nrm_self,nself,nspin,
     &             occ_diag,occ_diag_s,p_self,p_self_s,
     &             p_self_t,p_self0,rho_self,rho_self_s)

      use declarations
      implicit none

      integer:: i_self, iapr, icheck, ipr, isp, m_hubb, mpirank,
     &          n, n_atom_0_self, n_atom_ind_self, n_atom_proto,
     &          natome, natomeq, nrm_self, nself, nspin
      integer, dimension(0:n_atom_proto):: ngreq
      integer, dimension(natome):: nb_eq

      logical:: Convergence, Fermi, Full_atom, hubbard 

      real(kind=db):: Delta_En_conv, Delta_energ, Delta_energ_s,
     &          Delta_energ_t, Delta_lim, En_cluster, En_cluster_s,
     &          En_cluster_t, p_self, p_self_s, p_self_t, p_self0, 
     &          pop, pop_s, rap, raq, rar 
      real(kind=db), dimension(n_atom_0_self:n_atom_ind_self)::
     &                                       Energ_self, Energ_self_s 
      real(kind=db), dimension(0:nrm_self,nspin,
     &          n_atom_0_self:n_atom_ind_self):: rho_self, rho_self_s
      real(kind=db), dimension(n_atom_0_self:n_atom_ind_self,nspin)::
     &                                   chargat_self, chargat_self_s
      real(kind=db), dimension(-m_hubb:m_hubb,nspin,
     &           n_atom_0_self:n_atom_ind_self):: occ_diag, occ_diag_s
 

      if( .not. Fermi .and. mpirank == 0 ) then
        call write_error
        do ipr = 3,9,3
          if( icheck > 0 .or. ipr > 3 ) write(ipr,110)
        end do
        stop
      endif

      if( mpirank == 0 ) write(6,120) En_cluster * rydb

! Interpolation d'une iteration de la boucle coherente a l'autre
      if( i_self == 1 ) then
 
        Delta_energ = 1000000._db
        p_self = p_self0
        p_self_s = p_self
        p_self_t = p_self_s
        En_cluster_s = En_cluster
        En_cluster_t = En_cluster

      else

        if( icheck > 0 ) write(3,100)

! Test convergence: sur l'energie et sur la charge de l'atome central
! a faire avant l'interpolation
        Delta_energ = 0._db
        if( Full_atom ) then
          Delta_lim = Delta_En_conv * natomeq
        else
          Delta_lim = Delta_En_conv * sum( ngreq(1:n_atom_proto) )
        endif 

        do iapr = n_atom_0_self,n_atom_ind_self
          if( Full_atom ) then
            n = nb_eq(iapr)
          else
            n = ngreq(iapr)
          endif
          Delta_energ = Delta_energ
     &                + n * abs( Energ_self(iapr) - Energ_self_s(iapr) )
        end do

        if( Delta_energ < Delta_lim ) Convergence = .true.

        if( Convergence ) then

          if( icheck > 0 ) then
            write(3,*)
            write(3,130) Delta_energ*rydb, Delta_lim*rydb,
     &                   p_self
          endif   
          if( mpirank == 0 ) write(6,130) Delta_energ*rydb,
     &                                    Delta_lim*rydb, p_self

        else
          if( icheck > 0 ) then
            write(3,*) 
            write(3,140) Delta_energ*rydb, Delta_lim*rydb, p_self
          endif   
          if( mpirank == 0 ) write(6,140) Delta_energ*rydb,
     &                                  Delta_lim*rydb, p_self

          if( i_self == nself .and. mpirank == 0 ) then
            if( icheck > 0 ) write(3,'(/A)')
     &               ' Calculation has not converged !'
            write(6,'(/A)') ' Calculation has not converged !'
          else
            rap = 1.2 * max( p_self / p_self_s, 1._db )
            raq = 0.8 * min( p_self / p_self_s, 1._db )
            rar = 0.8 * min( p_self_s / p_self_t, 1._db )
            p_self_t = p_self_s
            p_self_s = p_self
! Si calcul divergent, on diminue le poids.
            if( ( ( En_cluster - En_cluster_s )
     &          * ( En_cluster_s - En_cluster_t ) < - eps10 
     &         .and. Delta_energ > Delta_energ_s * raq ) .or.
     &             ( Delta_energ > Delta_energ_s * rap ) ) then
              p_self = max( p_self / 2, p_self0 / 8 )
            elseif( i_self > 2
     &         .and. Delta_energ < Delta_energ_s * raq 
     &         .and. Delta_energ_s < Delta_energ_t * rar ) then
              p_self = min( 2 * p_self, p_self0 )
            endif
          end if
        endif
      endif

      rho_self(:,:,:) = p_self * rho_self(:,:,:) 
     &                + ( 1 - p_self ) * rho_self_s(:,:,:)
      chargat_self(:,:) = p_self * chargat_self(:,:) 
     &                + ( 1 - p_self ) * chargat_self_s(:,:)

      if( hubbard .and. i_self > 1 ) then
        do iapr = n_atom_0_self,n_atom_ind_self
          do isp = 1,nspin 
            pop = sum( occ_diag(:,isp,iapr) )
            if( pop < eps10 ) cycle 
            pop_s = sum( occ_diag_s(:,isp,iapr) )
            rap = ( p_self * pop + ( 1 - p_self ) * pop_s ) / pop
            occ_diag(:,isp,iapr) = rap * occ_diag(:,isp,iapr)
          end do 
        end do
      endif

! On stoque les valeurs de l'iteration courrante, pour les injecter
! dans la suivante
      if( .not. convergence .and. i_self /= nself ) then
        Delta_energ_t = Delta_energ_s
        Delta_energ_s = Delta_energ
        En_cluster_t = En_cluster_s
        En_cluster_s = En_cluster
        Energ_self_s(:) = Energ_self(:)   
        chargat_self_s(:,:) = chargat_self(:,:)
        rho_self_s(:,:,:) = rho_self(:,:,:)
! la ponderation se fait sur la matrice avant diagonalisation
! (base du cristal)
        if( hubbard ) occ_diag_s(:,:,:) = occ_diag(:,:,:)
      endif

      return
  100 format(/'----- prep_next_iter ', 70('-'))
  110 format(/' The Fermi level was not reached ! ')
  120 format(/11x,'Total Cluster energy =',f14.3,' eV')
  130 format(11x,'Delta_energ =',f11.3,
     &      ' eV < Delta =',f8.3,' eV,  Weight =',f8.5)
  140 format(11x,'Delta_energ =',f11.3,
     &      ' eV > Delta =',f8.3,' eV,  Weight =',f8.5)
      end
