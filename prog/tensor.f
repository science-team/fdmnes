! FDMNES subroutines

! Calculation of the cartesian tensors

      subroutine tenseur_car(coef_g,Atom_comp,Core_resolved,E1E1,
     &            E1E2,E1E3,E1M1,E2E2,Final_tddft,Green_int,
     &            Hub_nondiag_abs,icheck,ip_max,ip0,is_g,lh_abs,lmoins1,
     &            lplus1,lseuil,m_g,m_hubb,M1M1,mpinodes,mpirank,
     &            nbseuil,ndim1,ndim2,ninit1,ninitl,ninitlr,
     &            ninitls,nlmam,nspin,nspin_t,nspino_t,rof,
     &            rot_atom_abs,rot_hubb_abs,secdd,secdd_m,secdo,secdo_m,
     &            secdq,secdq_m,secmd,secmd_m,secmm,secmm_m,secqq,
     &            secqq_m,Singul,Solsing,Solsing_only,Spinorbite,
     &            taull)

      use declarations  
      implicit real(kind=db) (a-h,o-z)

      parameter( lomax = 3, lmomax = ( lomax + 1 )**2 )

      complex(kind=db), dimension(ninitlr):: Te, Ten, Ten_m, Tens,
     &                                       Tens_m
      complex(kind=db), dimension(3,3) :: mat2
      complex(kind=db), dimension(3,3,3) :: mat3
      complex(kind=db), dimension(3,3,3,3) :: mat4
      complex(kind=db), dimension(3,3,ninitlr,0:mpinodes-1)::
     &                   secdd, secdd_m, secmd, secmd_m, secmm, secmm_m
      complex(kind=db), dimension(3,3,3,ninitlr,0:mpinodes-1):: secdq,
     &                                                          secdq_m
      complex(kind=db), dimension(3,3,3,3,ninitlr,0:mpinodes-1):: secdo, 
     &                                           secdo_m, secqq, secqq_m
      complex(kind=db), dimension(lmomax,lmomax,ninitlr):: Tens_lm,
     &                                                     Tens_lm_m
      complex(kind=db), dimension(nlmam,nspin_t,nspino_t,0:ip_max,
     &                                              ninitls):: rof
      complex(kind=db), dimension(nlmam*nspino_t,nlmam*nspino_t,2,2,
     &                                       ndim1,ndim2,ndim2):: taull
      complex(kind=db), dimension(-m_hubb:m_hubb,-m_hubb:m_hubb,nspin):: 
     &                                                     rot_hubb_abs
      complex(kind=db), dimension(nlmam,nspin,nlmam,nspin,ip0:ip_max,
     &                            nbseuil):: Singul

      integer, dimension(ninitl,2):: m_g
      integer, dimension(ninitl):: is_g

      logical Atom_comp, Base_spin, Core_resolved, E1E1, E1E2, E1E3,      
     &        E1M1, E2E2, Final_tddft, Green_int, Hub_nondiag_abs, 
     &        lmoins1, lplus1, M1M1, Solsing, Solsing_only, Spinorbite 

      real(kind=db), dimension(ninitlr):: Tensi, Tensr
      real(kind=db), dimension(0:lomax,3,3,3,lmomax):: clm
      real(kind=db), dimension(3,3):: rot_atom_abs, rot_tem
      real(kind=db), dimension(ninitl,2):: coef_g

      common/Base_spin/ Base_spin
      common/ldip/ ldip(3), loct(3,3,3), lqua(3,3)
      common/msym/ msymdd(3,3), msymddi(3,3), msymdq(3,3,3), 
     &             msymdqi(3,3,3), msymdo(3,3,3,3), msymdoi(3,3,3,3),
     &             msymqq(3,3,3,3), msymqqi(3,3,3,3)
      common/rot_int/ rot_int(3,3)

      if( icheck > 1 ) write(3,100)

      if( E1E3 ) then
        nrang = 3
      elseif( E1E2 .or. E2E2 ) then
        nrang = 2
      else
        nrang = 1
      endif
      if( E1M1 .or. M1M1 ) then
        irang1 = 0
      else
        irang1 = 1
      endif

      if( nrang > lomax .and. mpirank == 0 ) then
        call write_error
        do ipr = 6,9,3
          write(ipr,'(/A)') ' nrang > lomax in coabs.f !'
        end do
        stop
      endif

! Calcul des composantes de la transf base-cartesienne - base-spherique:
  
      do irang = irang1,nrang

        clm(irang,:,:,:,:) = 0._db

        select case(irang)

          case(0)
! Magnetic dipole
            c = sqrt( 4 * pi )
! Dans ce cas, correspond a l=0,m=0 mais sert a definir x, y ou z.
            clm(irang,1,:,:,4) = c
            clm(irang,2,:,:,2) = c
            clm(irang,3,:,:,3) = c

          case(1)
! Dipole
            c = sqrt( 4 * pi / 3 )
            clm(irang,1,:,:,4) = - c
            clm(irang,2,:,:,2) = - c
            clm(irang,3,:,:,3) = c

          case(2)
! Quadrupole
            c0 = sqrt( 4 * pi ) / 3
            c = sqrt( 4 * pi / 15 )
            c3 = c / sqrt( 3._db )

            clm(irang,1,1,:,1) = c0
            clm(irang,1,1,:,7) = - c3;   clm(irang,1,1,:,9) = c
            clm(irang,1,2,:,5) = c
            clm(irang,1,3,:,db) = - c
            clm(irang,2,2,:,1) = c0
            clm(irang,2,2,:,7) = - c3;   clm(irang,2,2,:,9) = - c
            clm(irang,2,3,:,6) = - c
            clm(irang,3,3,:,1) = c0
            clm(irang,3,3,:,7) = 2 * c3

            do i = 1,3
              do j = i+1,3
                clm(irang,j,i,:,:) = clm(irang,i,j,:,:)
              end do
            end do

          case(3)
! Octupole
            c1 = sqrt( 4 * pi / 75 )

            c = sqrt( 4 * pi / 35 )
            c5 = c / sqrt( 5._db )
            c8 = c / sqrt( 8._db )
            c12 = c / sqrt( 12._db )
            c120 = c / sqrt( 120._db )

            clm(irang,1,1,1,4) = - 3 * c1
            clm(irang,1,1,1,14) = 3 * c120;  clm(irang,1,1,1,16) = - c8
            clm(irang,2,2,2,2) = - 3 * c1
            clm(irang,2,2,2,12) = 3 * c120;  clm(irang,2,2,2,10) = c8
            clm(irang,3,3,3,3) = 3 * c1
            clm(irang,3,3,3,12) = 2 * c5
            clm(irang,1,1,2,2) = - c1
            clm(irang,1,1,2,10) = - c8;      clm(irang,1,1,2,12) = c120
            clm(irang,1,2,2,4) = - c1
            clm(irang,1,2,2,16) = c8;        clm(irang,1,2,2,14) = c120
            clm(irang,1,1,3,3) = c1
            clm(irang,1,1,3,13) = - c5;      clm(irang,1,1,3,15) = c12
            clm(irang,2,2,3,3) = c1
            clm(irang,2,2,3,13) = - c5;      clm(irang,2,2,3,15) = - c12
            clm(irang,2,3,3,2) = - c1
            clm(irang,2,3,3,12) = - 4 * c120
            clm(irang,1,3,3,4) = - c1
            clm(irang,1,3,3,14) = - 4 * c120
            clm(irang,1,2,3,11) = c12

            do i = 1,3
              do j = i,3
                do k = j,3
                  if( i == j .and. i == k ) cycle
                  clm(irang,i,k,j,:) = clm(irang,i,j,k,:)
                  clm(irang,j,i,k,:) = clm(irang,i,j,k,:)
                  clm(irang,k,i,j,:) = clm(irang,i,j,k,:)
                  clm(irang,j,k,i,:) = clm(irang,i,j,k,:)
                  clm(irang,k,j,i,:) = clm(irang,i,j,k,:)
                end do
              end do
            end do

        end select

      end do

      secdd(:,:,:,mpirank) = (0._db,0._db)
      secmm(:,:,:,mpirank) = (0._db,0._db) 
      secmd(:,:,:,mpirank) = (0._db,0._db)
      secdq(:,:,:,:,mpirank) = (0._db,0._db) 
      secqq(:,:,:,:,:,mpirank) = (0._db,0._db) 
      secdo(:,:,:,:,:,mpirank) = (0._db,0._db)

      if( Green_int ) then
        secdd_m(:,:,:,mpirank) = (0._db,0._db)
        secmm_m(:,:,:,mpirank) = (0._db,0._db) 
        secmd_m(:,:,:,mpirank) = (0._db,0._db)
        secdq_m(:,:,:,:,mpirank) = (0._db,0._db) 
        secqq_m(:,:,:,:,:,mpirank) = (0._db,0._db) 
        secdo_m(:,:,:,:,:,mpirank) = (0._db,0._db)
      endif 
! Boucles sur le rang des tenseurs

      do irang = irang1,nrang
        do jrang = irang,nrang
          Tens_lm(:,:,:) = (0._db,0._db)
          Tens_lm_m(:,:,:) = (0._db,0._db)
          if( .not. E1E1 .and. irang == 1 .and. jrang == 1 ) cycle
          if( .not. E1E2 .and. irang == 1 .and. jrang == 2 ) cycle
          if( .not. E2E2 .and. irang == 2 .and. jrang == 2 ) cycle
          if( .not. E1E3 .and. irang == 1 .and. jrang == 3 ) cycle
          if( .not. M1M1 .and. irang == 0 .and. jrang == 0 ) cycle
          if( .not. E1M1 .and. irang == 0 .and. jrang == 1 ) cycle
          if( irang == 0 .and. jrang > 1 ) cycle
          if( jrang == 3 .and. irang > 1 ) cycle

! Boucles sur les indices des tenseurs

          do ke = 1,3

            if( irang == 1 .and. ldip(ke) /= 1 ) cycle

            if( irang <= 1 ) then
              nje = ke
            else
              nje = 3
            endif

            do je = ke,nje

              if( irang == 2 .and. lqua(ke,je) /= 1 ) cycle

              if( irang == 3 ) then
                nj2e = 3
              else
                nj2e = je
              endif
              do j2e = je,nj2e

                if( irang == 3 .and. loct(ke,je,j2e) /= 1 ) cycle

                do ks = 1,3
 
                  if( jrang == 1 .and. ldip(ks) /= 1 ) cycle

                  if( jrang <= 1 ) then
                    njs = ks
                  else
                    njs = 3
                  endif

                  do js = ks,njs

                    if( jrang == 2 .and. lqua(ks,js) /= 1 ) cycle

                    if( jrang == 3 ) then
                      nj2s = 3
                    else
                      nj2s = js
                    endif
                    do j2s = js,nj2s

                      if( jrang == 3 .and. loct(ks,js,j2s) /= 1 ) cycle

                      if( irang == 0 .and. jrang == 0 ) then 
                        if( ks < ke ) cycle
                      elseif( irang == 1 .and. jrang == 1 ) then 
                        if( ks < ke ) cycle
                        if( msymdd(ke,ks) == 0 
     &                                .and. msymddi(ke,ks) == 0 ) cycle
                      elseif( irang == 1 .and. jrang == 2 ) then 
                        if( msymdq(ke,ks,js) == 0 
     &                             .and. msymdqi(ke,ks,js) == 0 ) cycle
                      elseif( irang == 2 .and. jrang == 2 ) then 
                        if( msymqq(ke,je,ks,js) == 0 
     &                          .and. msymqqi(ke,je,ks,js) == 0 ) cycle
                      elseif( irang == 1 .and. jrang == 3 ) then 
                        if( msymdo(ke,ks,js,j2s) == 0
     &                         .and. msymdoi(ke,ks,js,j2s) == 0 ) cycle
                      endif
                      
                      if( irang == 2 .and. jrang == 2 .and.
     &                  sum( abs( secqq(ke,je,ks,js,:,mpirank) ) ) 
     &                                              > 1.e-15_db ) cycle

                      Tens(:) = (0._db,0._db)
                      Tens_m(:) = (0._db,0._db)

! Boucles sur les composantes spheriques des tenseurs
                      lme = 0
                      do le = 0,max(irang,1)
                        do me = -le,le
                          lme = lme + 1

                          clme = clm(irang,ke,je,j2e,lme)
                          if( abs(clme) < eps10 ) cycle

                          lms = 0
                          do ls = 0,max(jrang,1)
                            do ms = -ls,ls
                              lms = lms + 1
 
                              clms = clm(jrang,ks,js,j2s,lms)
                              if( abs(clms) < eps10 ) cycle

! Calcul de la composante du tenseur
                              if( sum( abs( Tens_lm(lme,lms,:) ) )
     &                                              < 1.e-15_db ) then
                                call tens_ab(Atom_comp,coef_g,
     &                            Core_resolved,Final_tddft,Green_int,
     &                            Hub_nondiag_abs,icheck,ip_max,
     &                            ip0,irang,is_g,jrang,le,me,ls,m_g,
     &                            m_hubb,ms,lh_abs,lmoins1,lplus1,
     &                            lseuil,nbseuil,ndim1,ndim2,ninit1,
     &                            ninitl,ninitls,ninitlr,nlmam,
     &                            nspin,nspin_t,nspino_t,rof,
     &                            rot_hubb_abs,Singul,Solsing,
     &                            Solsing_only,Spinorbite,taull,Ten,
     &                            Ten_m)
                                Tens_lm(lme,lms,:) = Ten(:)
                                Tens_lm_m(lme,lms,:) = Ten_m(:)
                              endif

                              Tens(:) = Tens(:) + clme * clms
     &                                  * Tens_lm(lme,lms,:)
                              Tens_m(:) = Tens_m(:) + clme * clms
     &                                  * Tens_lm_m(lme,lms,:)
                            end do       
                          end do       
                        end do       
                      end do ! fin boucle le       

! Remplissage de la valeur calculee dans tous les elements du tenseur
! equivalent par symetrie.

                      Tensr(:) = real( Tens(:),db )  
                      Tensi(:) = aimag( Tens(:) )  

! M1-M1 (dipole magnetique - dipole magnetique)
                      if( irang == 0 .and. jrang == 0 ) then
                        secmm(ke,ks,:,mpirank) = Tens(:)
                        if( Green_int ) then
                          secmm(ks,ke,:,mpirank) = Tens(:)
                          secmm_m(ke,ks,:,mpirank) = Tens_m(:)
                          secmm_m(ks,ke,:,mpirank) = - Tens_m(:)
                        else
                          secmm(ks,ke,:,mpirank) = conjg( Tens(:) )
                        endif
                      endif

! M1-E1 (dipole magnetique - dipole electrique)
                      if( irang == 0 .and. jrang == 1 ) then
                        secmd(ke,ks,:,mpirank) = Tens(:)
                        if( Green_int )
     &                    secmd_m(ke,ks,:,mpirank) = Tens_m(:)
                      endif

! E1-E1 (Dipole-dipole)
                      if( irang == 1 .and. jrang == 1 ) then
                        do kke = 1,3
                          do kks = kke,3
                            if( abs(msymdd(kke,kks)) 
     &                         /= abs(msymdd(ke,ks)) .or.
     &                            abs(msymddi(kke,kks)) 
     &                         /= abs(msymddi(ke,ks)) ) cycle
                            if( msymdd(kke,kks) /= 0 ) then
                              is = msymdd(kke,kks) / msymdd(ke,ks)
                            else
                              is = 0
                            endif
                            if( msymddi(kke,kks) /= 0 ) then
                              isi = msymddi(kke,kks) / msymddi(ke,ks)
                            else
                              isi = 0
                            endif
                            if( Green_int ) then
                              secdd(kke,kks,:,mpirank) = is * Tens(:) 
                              secdd(kks,kke,:,mpirank) = is * Tens(:) 
                              secdd_m(kke,kks,:,mpirank) = isi*Tens_m(:) 
                              secdd_m(kks,kke,:,mpirank) =-isi*Tens_m(:) 
                            else
                      ! tenseur hermitique
                              Te(:) = cmplx(is*Tensr(:),isi*Tensi(:),db)
                              secdd(kke,kks,:,mpirank) = Te(:) 
                              secdd(kks,kke,:,mpirank) = conjg( Te(:) )
                            endif
                          end do
                        end do

! Dipole-quadrupole
                      elseif( irang == 1 .and. jrang == 2 ) then
                        do kke = 1,3
                          do kks = 1,3
                            do jjs = kks,3
                              if( abs(msymdq(kke,kks,jjs)) 
     &                           /= abs(msymdq(ke,ks,js)) .or.
     &                              abs(msymdqi(kke,kks,jjs))
     &                           /= abs(msymdqi(ke,ks,js)) ) cycle
                              if( msymdq(kke,kks,jjs) /= 0 ) then
                                is = msymdq(kke,kks,jjs)
     &                             / msymdq(ke,ks,js)
                              else
                                is = 0
                              endif
                              if( msymdqi(kke,kks,jjs) /= 0 ) then
                                isi = msymdqi(kke,kks,jjs)
     &                              / msymdqi(ke,ks,js)
                              else
                                isi = 0
                              endif
                              if( Green_int ) then
                                secdq(kke,kks,jjs,:,mpirank) =is*Tens(:)
                                secdq(kke,jjs,kks,:,mpirank) =is*Tens(:)
                                secdq_m(kke,kks,jjs,:,mpirank)
     &                                                   = isi*Tens_m(:)
                                secdq_m(kke,jjs,kks,:,mpirank)
     &                                                   = isi*Tens_m(:)
                              else
                                Te(:)=cmplx(is*Tensr(:),isi*Tensi(:),db)
                                secdq(kke,kks,jjs,:,mpirank) = Te(:)
                                secdq(kke,jjs,kks,:,mpirank) = Te(:)
                              endif
                            end do
                          end do
                        end do

! Dipole-octupole
                      elseif( irang == 1 .and. jrang == 3 ) then
                        do kke = 1,3
                          do kks = 1,3
                            do jjs = 1,3
                              do jj2s = jjs,3
                                if( abs(msymdo(kke,kks,jjs,jj2s))
     &                             /= abs(msymdo(ke,ks,js,j2s)) 
     &                            .or. abs(msymdoi(kke,kks,jjs,jj2s)) 
     &                             /= abs(msymdoi(ke,ks,js,j2s))) cycle
                                if( msymdo(kke,kks,jjs,jj2s)/=0 ) then
                                  is = msymdo(kke,kks,jjs,jj2s) 
     &                               / msymdo(ke,ks,js,j2s)
                                else
                                  is = 0
                                endif
                                if( msymdoi(kke,kks,jjs,jj2s)/=0) then
                                  isi = msymdoi(kke,kks,jjs,jj2s)
     &                                / msymdoi(ke,ks,js,j2s)
                                else
                                  isi = 0
                                endif
                                if( Green_int ) then
                                  secdo(kke,kks,jjs,jj2s,:,mpirank)
     &                                          = is * Tens(:)
                                  secdo(kke,kks,jj2s,jjs,:,mpirank)
     &                                          = is * Tens(:)
                                  secdo(kke,jjs,kks,jj2s,:,mpirank)
     &                                          = is * Tens(:)
                                  secdo(kke,jj2s,kks,jjs,:,mpirank)
     &                                          = is * Tens(:)
                                  secdo(kke,jjs,jj2s,kks,:,mpirank)
     &                                          = is * Tens(:)
                                  secdo(kke,jj2s,jjs,kks,:,mpirank)
     &                                          = is * Tens(:)
                                  secdo_m(kke,kks,jjs,jj2s,:,mpirank)
     &                                          = isi * Tens_m(:)
                                  secdo_m(kke,kks,jj2s,jjs,:,mpirank)
     &                                          = isi * Tens_m(:)
                                  secdo_m(kke,jjs,kks,jj2s,:,mpirank)
     &                                          = isi * Tens_m(:)
                                  secdo_m(kke,jj2s,kks,jjs,:,mpirank)
     &                                          = isi * Tens_m(:)
                                  secdo_m(kke,jjs,jj2s,kks,:,mpirank)
     &                                          = isi * Tens_m(:)
                                  secdo_m(kke,jj2s,jjs,kks,:,mpirank)
     &                                          = isi * Tens_m(:)
                                else
                                Te(:)=cmplx(is*Tensr(:),isi*Tensi(:),db)
                                 secdo(kke,kks,jjs,jj2s,:,mpirank)=Te(:)
                                 secdo(kke,kks,jj2s,jjs,:,mpirank)=Te(:)
                                 secdo(kke,jjs,kks,jj2s,:,mpirank)=Te(:)
                                 secdo(kke,jj2s,kks,jjs,:,mpirank)=Te(:)
                                 secdo(kke,jjs,jj2s,kks,:,mpirank)=Te(:)
                                 secdo(kke,jj2s,jjs,kks,:,mpirank)=Te(:)
                                endif
                              end do
                            end do
                          end do
                        end do

! Quadrupole-quadrupole
                      elseif( irang == 2 .and. jrang == 2 ) then
                        do kke = 1,3
                          do jje = kke,3
                            do kks = 1,3
                              do jjs = kks,3
                                if( sum( abs(
     &                            secqq(kke,jje,kks,jjs,:,mpirank) ) )
     &                                               > 1.e-15_db ) cycle
                                if( abs(msymqq(kke,jje,kks,jjs))
     &                             /= abs(msymqq(ke,je,ks,js))
     &                           .or. abs(msymqqi(kke,jje,kks,jjs)) 
     &                             /= abs(msymqqi(ke,je,ks,js)) ) cycle
                                if( msymqq(kke,jje,kks,jjs)/=0 ) then
                                  is = msymqq(kke,jje,kks,jjs) 
     &                               / msymqq(ke,je,ks,js)
                                else
                                  is = 0
                                endif  
                                if( msymqqi(kke,jje,kks,jjs)/=0 ) then
                                  isi = msymqqi(kke,jje,kks,jjs) 
     &                                / msymqqi(ke,je,ks,js)
                                else
                                  isi = 0
                                endif
                                if( Green_int ) then
                                  Te(:) = is * Tens(:)
                                  secqq(kke,jje,kks,jjs,:,mpirank)=Te(:)
                                  secqq(jje,kke,kks,jjs,:,mpirank)=Te(:)
                                  secqq(kke,jje,jjs,kks,:,mpirank)=Te(:)
                                  secqq(jje,kke,jjs,kks,:,mpirank)=Te(:)
                                  secqq(kks,jjs,kke,jje,:,mpirank)=Te(:)
                                  secqq(jjs,kks,kke,jje,:,mpirank)=Te(:)
                                  secqq(kks,jjs,jje,kke,:,mpirank)=Te(:)
                                  secqq(jjs,kks,jje,kke,:,mpirank)=Te(:)
                                  Te(:) = isi * Tens_m(:)
                                  secqq_m(kke,jje,kks,jjs,:,mpirank)
     &                                                           = Te(:)
                                  secqq_m(jje,kke,kks,jjs,:,mpirank)
     &                                                           = Te(:)
                                  secqq_m(kke,jje,jjs,kks,:,mpirank)
     &                                                           = Te(:)
                                  secqq_m(jje,kke,jjs,kks,:,mpirank)
     &                                                           = Te(:)
                                  secqq_m(kks,jjs,kke,jje,:,mpirank)
     &                                                         = - Te(:)
                                  secqq_m(jjs,kks,kke,jje,:,mpirank)
     &                                                         = - Te(:)
                                  secqq_m(kks,jjs,jje,kke,:,mpirank)
     &                                                         = - Te(:)
                                  secqq_m(jjs,kks,jje,kke,:,mpirank)
     &                                                         = - Te(:)
                                else
                                  Te(:)= cmplx( is*Tensr(:),
     &                                          isi*Tensi(:),db)
                                  secqq(kke,jje,kks,jjs,:,mpirank)=Te(:)
                                  secqq(jje,kke,kks,jjs,:,mpirank)=Te(:)
                                  secqq(kke,jje,jjs,kks,:,mpirank)=Te(:)
                                  secqq(jje,kke,jjs,kks,:,mpirank)=Te(:)
                                  Te(:) = Conjg( Te(:) )
                                  secqq(kks,jjs,kke,jje,:,mpirank)=Te(:)
                                  secqq(jjs,kks,kke,jje,:,mpirank)=Te(:)
                                  secqq(kks,jjs,jje,kke,:,mpirank)=Te(:)
                                  secqq(jjs,kks,jje,kke,:,mpirank)=Te(:)
                                endif
                              end do
                            end do
                          end do
                        end do
                      endif

                    end do       
                  end do       
                end do       
              end do       
            end do       
          end do       

          if( icheck < 2 ) cycle

          if( .not. E1E2 .and. irang == 1 .and. jrang == 2 ) cycle
          if( jrang == 3 .and. irang > 1 ) cycle
          if( irang == 0 .and. jrang > 1 ) cycle
          write(3,110)
          lme = 0
          do le = 0,max(irang,1)
            do me = -le,le
             lme = lme + 1
             lms = 0
             do ls = 0,max(jrang,1)
                do ms = -ls,ls
                  lms = lms + 1
                  if( sum( abs(Tens_lm(lme,lms,:)) ) > 1.e-15_db )
     &              write(3,120) irang, jrang, le, me, ls, ms,
     &                 (Tens_lm(lme,lms,initlr),initlr = 1,ninitlr) 
                  if( .not. Green_int ) cycle
                  if( sum( abs(Tens_lm_m(lme,lms,:)) ) > 1.e-15_db )
     &              write(3,125) irang, jrang, le, me, ls, ms,
     &                 (Tens_lm_m(lme,lms,initlr),initlr = 1,ninitlr) 
                end do       
              end do       
            end do       
          end do       

        end do       
      end do       

! Rotation pour avoir les tenseurs dans la base R1

      if( .not. Base_spin ) then

        rot_tem = matmul( rot_int, transpose(rot_atom_abs) )

        do initlr = 1,ninitlr

          if( E1E1 ) then
            mat2(:,:) = secdd(:,:,initlr,mpirank) 
            call rot_tensor_2( mat2, rot_tem )
            secdd(:,:,initlr,mpirank) = mat2(:,:) 
          endif

          if( E1E2 ) then
            mat3(:,:,:) = secdq(:,:,:,initlr,mpirank) 
            call rot_tensor_3( mat3, rot_tem )
            secdq(:,:,:,initlr,mpirank) = mat3(:,:,:) 
          endif

          if( E2E2 ) then
            mat4(:,:,:,:) = secqq(:,:,:,:,initlr,mpirank) 
            call rot_tensor_4( mat4, rot_tem )
            secqq(:,:,:,:,initlr,mpirank) = mat4(:,:,:,:) 
          endif

          if( E1E3 ) then
            mat4(:,:,:,:) = secdo(:,:,:,:,initlr,mpirank) 
            call rot_tensor_4( mat4, rot_tem )
            secdo(:,:,:,:,initlr,mpirank) = mat4(:,:,:,:) 
          endif

          if( E1M1 ) then
            mat2(:,:) = secmd(:,:,initlr,mpirank) 
            call rot_tensor_2( mat2, rot_tem )
            secmd(:,:,initlr,mpirank) = mat2(:,:) 
          endif

          if( M1M1 ) then
            mat2(:,:) = secmm(:,:,initlr,mpirank) 
            call rot_tensor_2( mat2, rot_tem )
            secmm(:,:,initlr,mpirank) = mat2(:,:) 
          endif

          if( .not. Green_int ) Cycle

          if( E1E1 ) then
            mat2(:,:) = secdd_m(:,:,initlr,mpirank) 
            call rot_tensor_2( mat2, rot_tem )
            secdd_m(:,:,initlr,mpirank) = mat2(:,:) 
          endif

          if( E1E2 ) then
            mat3(:,:,:) = secdq_m(:,:,:,initlr,mpirank) 
            call rot_tensor_3( mat3, rot_tem )
            secdq_m(:,:,:,initlr,mpirank) = mat3(:,:,:) 
          endif

          if( E2E2 ) then
            mat4(:,:,:,:) = secqq_m(:,:,:,:,initlr,mpirank) 
            call rot_tensor_4( mat4, rot_tem )
            secqq_m(:,:,:,:,initlr,mpirank) = mat4(:,:,:,:) 
          endif

          if( E1E3 ) then
            mat4(:,:,:,:) = secdo_m(:,:,:,:,initlr,mpirank) 
            call rot_tensor_4( mat4, rot_tem )
            secdo_m(:,:,:,:,initlr,mpirank) = mat4(:,:,:,:) 
          endif

          if( E1M1 ) then
            mat2(:,:) = secmd_m(:,:,initlr,mpirank) 
            call rot_tensor_2( mat2, rot_tem )
            secmd_m(:,:,initlr,mpirank) = mat2(:,:) 
          endif

          if( M1M1 ) then
            mat2(:,:) = secmm_m(:,:,initlr,mpirank) 
            call rot_tensor_2( mat2, rot_tem )
            secmm_m(:,:,initlr,mpirank) = mat2(:,:) 
          endif

        end do

      endif

      return
  100 format(/' ---- Tens_ab --------',100('-'))
  110 format(/' Tensor by harmonics (basis R4) :'/,
     &' ir jr  le me  ls ms      Tens(Ylm,i=1,ninitlr)')
  120 format(2i3,2(i4,i3),1p,20e13.5)
  125 format('i',i2,i3,2(i4,i3),1p,20e13.5)

      end

!***********************************************************************

      subroutine tens_ab(Atom_comp,coef_g,
     &                            Core_resolved,Final_tddft,Green_int,
     &                            Hub_nondiag_abs,icheck,ip_max,
     &                            ip0,irang,is_g,jrang,le,me,ls,m_g,
     &                            m_hubb,ms,lh_abs,lmoins1,lplus1,
     &                            lseuil,nbseuil,ndim1,ndim2,ninit1,
     &                            ninitl,ninitls,ninitlr,nlmam,
     &                            nspin,nspin_t,nspino_t,rof,
     &                            rot_hubb_abs,Singul,Solsing,
     &                            Solsing_only,Spinorbite,taull,Ten,
     &                            Ten_m)

      use declarations
      implicit none

      integer, intent(in):: icheck, ip_max, ip0, irang, jrang, le, me,
     &    ls, m_hubb, ms, lh_abs, lseuil, nbseuil, ndim1, ndim2, ninit1,
     &    ninitl, ninitls, ninitlr, nlmam, nspin, nspin_t, nspino_t
      integer, dimension(ninitl,2), intent(in):: m_g
      integer, dimension(ninitl), intent(in):: is_g

      integer:: i_g_1, i_g_2, initlr, iseuil, iso1, iso2, ispinf1,
     &    ispinf2, isping1, isping2, l1, l2, lf1, lf2, lfe1, lfe2, li,
     &    lm0, lm1, lm2, lmp0, lms1, lms2, m1, m2, mi1, mi2, mv1, mv2

      complex(kind=db) :: Cg

      complex(kind=db):: Gaunte, Gauntm, Gauntmag, Gaunts
      complex(kind=db), dimension(ninitlr):: Ten, Ten_m
      complex(kind=db), dimension(nlmam*nspino_t,nlmam*nspino_t,2,2)::
     &                                              Tau_rad, tau_rad_i
      complex(kind=db), dimension(-m_hubb:m_hubb,-m_hubb:m_hubb,nspin):: 
     &                                                rot_hubb_abs
      complex(kind=db), dimension(nlmam,nspin_t,nspino_t,0:ip_max,
     &                                                  ninitls):: rof
      complex(kind=db), dimension(nlmam*nspino_t,nlmam*nspino_t,2,2,
     &                                       ndim1,ndim2,ndim2):: taull
      complex(kind=db), dimension(nlmam,nspin,nlmam,nspin,ip0:ip_max,
     &                            nbseuil):: Singul

      logical Atom_comp, Core_resolved, Final_tddft, Green_int,  
     &        Hub_nondiag_abs, lmoins1, lplus1, Solsing, Solsing_only,
     &        Spinorbite, Titre, Ylm_comp 

      real(kind=db):: Ci_1, Ci_2, Ci2, J_initl1, J_initl2, Jz1, Jz2
      real(kind=db), dimension(ninitl,2):: coef_g

      Ylm_comp = Spinorbite .or. Atom_comp
      li = lseuil

      Titre = .true.

      Ten(:) = (0._db,0._db)
      Ten_m(:) = (0._db,0._db)

      if( icheck > 2 ) write(3,110) le, me, ls, ms, li

! Boucle sur les etats initiaux
      do i_g_1 = 1,ninitl

        J_initl1 = li + 0.5_db * is_g(i_g_1)
        if( i_g_1 <= ninit1 ) then
          Jz1 = - J_initl1 + i_g_1 - 1
        else
          Jz1 = - J_initl1 + i_g_1 - ninit1 - 1
        endif

        if( i_g_1 <= ninit1 ) then
          iseuil = 1
        else
          iseuil = 2
        endif

        if( Final_tddft ) then
          initlr = 1
        elseif( Core_resolved ) then
          initlr = i_g_1
        else
          initlr = iseuil
        endif

! Boucle sur le spin
        do isping1 = 1,2  ! Spin de l'etat initial 

          mi1 = m_g(i_g_1,isping1)
          Ci_1 = Coef_g(i_g_1,isping1)

          if( abs( Ci_1 ) < eps6 ) cycle

          do i_g_2 = 1,ninitl

            if( .not. Final_tddft .and. i_g_1 /= i_g_2 ) cycle 

            J_initl2 = li + 0.5_db * is_g(i_g_2)
            if( i_g_2 <= ninit1 ) then
              Jz2 = - J_initl2 + i_g_2 - 1
            else
              Jz2 = - J_initl2 + i_g_2 - ninit1 - 1
            endif

            do isping2 = 1,2  ! Spin de l'etat initial 
!        if( isping1 /= isping2 ) cycle
              mi2 = m_g(i_g_2,isping2)
              Ci_2 = Coef_g(i_g_2,isping2)

              if( abs( Ci_2 ) < eps6 ) cycle

              call Mat_rad(Core_resolved,Final_tddft,Green_int,
     &             Hub_nondiag_abs,icheck,i_g_1,i_g_2,ip_max,ip0,irang,
     &             isping1,isping2,jrang,lh_abs,lseuil,m_hubb,nbseuil,
     &             ndim1,ndim2,ninit1,ninitls,nspin,nspin_t,
     &             nspino_t,nlmam,rof,rot_hubb_abs,singul,Solsing,
     &             Solsing_only,Spinorbite,taull,Tau_rad,Tau_rad_i)

              Ci2 = Ci_1 * Ci_2

              if( irang == 0 ) then ! Cas du dipole magnetique
                lfe1 = li
                lfe2 = li
              else
                lfe1 = abs(li - le)
                lfe2 = li + le
              endif
              if( lplus1 ) lfe1 = lfe2
              if( lmoins1 ) lfe2 = lfe1

! Boucle sur les harmoniques de l'etat final
              do l1 = lfe1,lfe2,2
                lm0 = l1**2 + l1 + 1 
                do m1 = -l1,l1
                  lm1 = lm0 + m1
                  if( lm1 > nlmam ) cycle

                  do ispinf1 = 1,2  ! spin de l'etat final en entree

! Il n'y a que pour le dipole magnetique qu'on peut avoir du spin-flip             
                    if( ispinf1 /= isping1 .and. irang /= 0 ) cycle
 
! Boucle sur les 2 solutions
                    do iso1 = 1,nspino_t

                      if( Spinorbite ) then
                        mv1 = m1 + ispinf1 - iso1
                        if( mv1 > l1 .or. mv1 < -l1 ) cycle
                      else
                        mv1 = m1
                      endif
                      lms1 = nlmam * ( iso1 - 1 ) + lm1

                      if( irang == 0 ) then
                        Gaunte = Gauntmag(isping1,ispinf1,me,l1,mv1,li,
     &                                    mi1,Ylm_comp)
                      else
                        Gaunte = Gauntm(l1,mv1,le,me,li,mi1,Ylm_comp)
                      endif

                      if( abs(Gaunte) < eps10 ) cycle

                      if( jrang == 0 ) then
                        lf1 = li
                        lf2 = li
                      else
                        lf1 = abs(li - ls)
                        lf2 = li + ls
                      endif
                      if( lplus1 ) lf1 = lf2
                      if( lmoins1 ) lf2 = lf1

! Boucle sur les harmoniques de l'etat final
                      do l2 = lf1,lf2,2
                        lmp0 = l2**2 + l2 + 1 
                        do m2 = -l2,l2
                          lm2 = lmp0 + m2
                          if( lm2 > nlmam ) cycle

                          do ispinf2 = 1,2  ! spin etat final en sortie
             
                            if( ispinf2 /= isping2 .and. jrang /= 0 )
     &                                                           cycle
                            if( .not. Spinorbite .and.
     &                              ispinf2 /= ispinf1 ) cycle

                            do iso2 = 1,nspino_t

                              if( Spinorbite ) then
                                mv2 = m2 + ispinf2 - iso2
                                if( mv2 > l2 .or. mv2 < -l2 ) cycle
                              else
                                mv2 = m2
                              endif
                              lms2 = nlmam * ( iso2 - 1 ) + lm2

                              if( jrang == 0 ) then
                                Gaunts = Gauntmag(isping2,ispinf2,ms,
     &                                        l2,mv2,li,mi2,Ylm_comp)
                              else
                                Gaunts = Gauntm(l2,mv2,ls,ms,li,mi2,
     &                                          Ylm_comp)
                              endif

                              if( abs(Gaunts) < eps10 ) cycle

                              Cg = - Ci2 * conjg( Gaunte ) * Gaunts 

! Comme on ne divise pas par pi, le resultat apparait comme multiplie
! par pi, si on calcule ensuite le facteur de structure. La
! normalisation par pi n'est donc pas a faire dans coabs sur l'amplitude
! dafs.

                              if( Green_int ) then
                                Ten(initlr) = Ten(initlr) + Real(Cg,db)
     &                            * tau_rad(lms2,lms1,ispinf2,ispinf1)
     &                                      + img * aimag(Cg)
     &                            * tau_rad_i(lms2,lms1,ispinf2,ispinf1)
                                Ten_m(initlr)
     &                          = Ten_m(initlr) + Real(Cg,db)
     &                            * tau_rad_i(lms2,lms1,ispinf2,ispinf1)
     &                                      + img * aimag(Cg)
     &                            * tau_rad(lms2,lms1,ispinf2,ispinf1)
                              else
                                Ten(initlr) = Ten(initlr)
     &                         + Cg * tau_rad(lms2,lms1,ispinf2,ispinf1)
                              endif


                              if( icheck > 2 .and. 
     &                          abs( tau_rad(lms2,lms1,ispinf2,ispinf1))
     &                                  > eps10 ) then
                                if( Titre ) write(3,120)
                                Titre = .false. 
                                write(3,130) i_g_1, isping1, Jz1, mi1,
     &                            Ci_1, i_g_2, isping2, Jz2, 
     &                            mi2, Ci_2, l1, m1, iso1, ispinf1, l2, 
     &                            m2, iso2,  ispinf2, Ten(initlr),
     &                            Cg,tau_rad(lms2,lms1,ispinf2,ispinf1),
     &                            Gaunts, Gaunte
                              endif

                            end do ! fin boucle sur les solutions en sortie
                          end do ! fin boucle sur le spin etat final en sortie
                        end do
                      end do ! fin boucle sur les harmoniques en sortie

                    end do ! fin boucle sur les solutions en entree
                  end do  ! fin boucle sur spin d'etat final en entree
                end do
              end do  ! fin boucle sur les harmoniques en entree

            end do    ! fin boucle sur le spin d'etat initial 2
          end do    ! fin boucle sur les etats initiaux 2 
        end do    ! fin boucle sur le spin d'etat initial 1
      end do   ! fin boucle sur les etats initiaux 1

! On multiplie par -i pour que ce soit la partie reelle du tenseur qui
! soit l'absorption.
      Ten(:) = - img * Ten(:)
      Ten_m(:) = - img * Ten_m(:)

      return
  110 format(/' le, me =',2i3,',  ls, ms =',2i3,', li =',i2)
  120 format(/' ini1 isg1 Jz1 mi1  Ci1  ini2 isg2 Jz2 mi2  Ci2',
     & '   l1  m1 iso1 isf1  l2  m2 iso2 isf2',12x,'Ten',11x,
     & '-Ci1*Ci2*conjg(Gaunte)*Gaunts',9x,'Tau_rad',20x,
     & 'Gaunts',20x,'Gaunte') 
  130 format(2(2i4,f6.1,i3,f7.3),2(4i4,2x),1p,2e13.5,
     &       2(1x,2e13.5),1x,4e13.5) 
      end

!***********************************************************************

      subroutine Mat_rad(Core_resolved,Final_tddft,Green_int,
     &             Hub_nondiag_abs,icheck,i_g_1,i_g_2,ip_max,ip0,irang,
     &             isping1,isping2,jrang,lh_abs,lseuil,m_hubb,nbseuil,
     &             ndim1,ndim2,ninit1,ninitls,nspin,nspin_t,
     &             nspino_t,nlmam,rof,rot_hubb_abs,singul,Solsing,
     &             Solsing_only,Spinorbite,taull,Tau_rad,Tau_rad_i)

      use declarations
      implicit none

      integer:: icheck, i_g_1 ,i_g_2, ijrang, initl1, initl2,
     &  ip_max, ip0, irang, is_r1, is_r2, iseuil1, iseuil2, iso1, iso2,
     &  ispf1, ispf2, ispinf1, ispinf2, isping1, isping12, isping2,
     &  jrang, l1, l2, le, lfe1, lfe2, lfs1, lfs2, lh_abs, li, ls, 
     &  lm0, lm1, lm2, lmp0, lms1, lms2, lmv1, lmv2, lseuil, m_hubb, m1,
     &  m2, mv1, mv2, nbseuil, ndim1, ndim2, ninit1, ninitls,
     &  nlmam, nspin, nspin_t, nspino_t   

      complex(kind=db):: cfe, cfs
      complex(kind=db), dimension(nlmam,nspin_t,nspino_t,0:ip_max,
     &                                                  ninitls):: rof
      complex(kind=db), dimension(nlmam*nspino_t,nlmam*nspino_t,2,2,
     &                                       ndim1,ndim2,ndim2):: taull
      complex(kind=db), dimension(nlmam*nspino_t,nlmam*nspino_t,2,2)::
     &                                               Tau_rad, Tau_rad_i
      complex(kind=db), dimension(-m_hubb:m_hubb,-m_hubb:m_hubb,nspin):: 
     &                                                rot_hubb_abs
      complex(kind=db), dimension(nlmam,nspin,nlmam,nspin,ip0:ip_max,
     &                            nbseuil):: Singul

      logical Core_resolved, Final_tddft, Green_int, Hub_nondiag_abs,
     &        Solsing, Solsing_only, Solsing_rang, Spinorbite 

      real(kind=db):: cfi, cfr

      if( icheck > 2 ) then
        if( Green_int ) then
          write(3,102) irang, jrang
        else
          write(3,104) irang, jrang
        endif
      endif

      Tau_rad(:,:,:,:) = (0._db, 0._db)
      if( Green_int ) Tau_rad_i(:,:,:,:) = (0._db, 0._db)

      ijrang = irang + jrang
      solsing_rang = Solsing .and. ijrang /= 3 .and. ijrang /= 5                                
     &                       .and. ijrang /= 6 .and. ijrang /= 1

      li = lseuil

      if( irang == 0 ) then ! Cas du dipole magnetique
        lfe1 = li
        lfe2 = li
      else
        le = max(irang,1)
        lfe1 = abs(li - le)
        lfe2 = li + le
      endif

      if( jrang == 0 ) then ! Cas du dipole magnetique
        lfs1 = li
        lfs2 = li
      else
        ls = max(jrang,1)
        lfs1 = abs(li - ls)
        lfs2 = li + ls
      endif

      if( Final_tddft) then
        initl1 = i_g_1
        initl2 = i_g_2
      else
        initl1 = 1
        initl2 = 1
      endif

      if( i_g_1 <= ninit1 ) then
        iseuil1 = 1
      else
        iseuil1 = 2
      endif
      if( i_g_2 <= ninit1 ) then
        iseuil2 = 1
      else
        iseuil2 = 2
      endif

      if( Core_resolved .and. Final_tddft) then
        is_r1 = i_g_1
        is_r2 = i_g_2
      else
        is_r1 = iseuil1
        is_r2 = iseuil2
      endif

      if( Final_tddft .and. ( irang == 0 .or. jrang == 0 ) ) then
        isping12 = 2*(isping1 - 1) + isping2
      else
        isping12 = 1 
      end if

! Boucle sur les harmoniques de l'etat final
      do l1 = lfe1,lfe2

        lm0 = l1**2 + l1 + 1 
        do m1 = -l1,l1
          lm1 = lm0 + m1
          if( lm1 > nlmam ) exit

          do ispinf1 = 1,2  ! spin de l'etat final en entree

! Il n'y a que pour le dipole magnetique qu'on peut avoir du spin-flip             
            if( ispinf1 /= isping1 .and. irang /= 0 ) cycle
 
            ispf1 = min( ispinf1, nspin_t )

! Boucle sur les 2 solutions
            do iso1 = 1,nspino_t

              if( Spinorbite ) then
                mv1 = m1 + ispinf1 - iso1
                if( mv1 > l1 .or. mv1 < -l1 ) cycle
                lmv1 = lm0 + mv1
              else
                mv1 = m1
                lmv1 = lm1
              endif
              lms1 = nlmam * ( iso1 - 1 ) + lm1

! Boucle sur les harmoniques de l'etat final
              do l2 = lfs1,lfs2

                lmp0 = l2**2 + l2 + 1 
                do m2 = -l2,l2
                  lm2 = lmp0 + m2
                  if( lm2 > nlmam ) exit

                  do ispinf2 = 1,2  ! spin etat final en sortie
             
                    if( ispinf2 /= isping2 .and. jrang /= 0 )
     &                                                   cycle
                    if( .not. Spinorbite .and.
     &                      ispinf2 /= ispinf1 ) cycle

                    ispf2 = min( ispinf2, nspin_t )

                    do iso2 = 1,nspino_t

                      if( Spinorbite ) then
                        mv2 = m2 + ispinf2 - iso2
                        if( mv2 > l2 .or. mv2 < -l2 ) cycle
                        lmv2 = lmp0 + mv2
                      else
                        mv2 = m2
                        lmv2 = lm2
                      endif

                      lms2 = nlmam * ( iso2 - 1 ) + lm2
! Comme on ne divise pas par pi, le resultat apparait comme multiplie
! par pi, si on calcule ensuite le facteur de structure. La
! normalisation par pi n'est donc pas a faire dans coabs sur l'amplitude
! dafs.
                      if( Solsing_only ) then
                        cfe = (0._db, 0._db)
                        cfs = (0._db, 0._db)
                      else
                        cfe = rof(lmv1,ispf1,iso1,irang,is_r1)
     &                      * rof(lmv2,ispf2,iso2,jrang,is_r2)
     &                      * taull(lms2,lms1,ispinf2,ispinf1,
     &                              isping12,initl2,initl1)
                        cfs = rof(lmv2,ispf2,iso2,jrang,is_r2)
     &                      * rof(lmv1,ispf1,iso1,irang,is_r1)
     &                      * taull(lms1,lms2,ispinf1,ispinf2,
     &                              isping12,initl1,initl2)
                      endif
! Soustraction de la solution singuliere
! Dans cas octupole-octupole, la solution singuliere n'est pas calculee
! (voir routine sphere).
! Dans le cas du dipole magnetique, je ne sais pas.
                      if( Solsing_rang .and. m1 == m2 .and.
     &                   ( ( Spinorbite .and. iso1 == ispf1
     &                                  .and. iso2 == ispf2 ) .or.
     &                   (.not. spinorbite .and. ispf1 == ispf2 ) ) )
     &                    then
                        cfe = cfe + singul(lmv2,ispf2,lmv1,ispf1,irang,
     &                                   iseuil1)
                        cfs = cfs + singul(lmv1,ispf1,lmv2,ispf2,irang,
     &                                   iseuil1)
                      endif
                      if( Green_int ) then
                        Tau_rad(lms2,lms1,ispinf2,ispinf1)
     &                              = 0.5_db * ( cfe + cfs ) 
                        Tau_rad_i(lms2,lms1,ispinf2,ispinf1) 
     &                              = 0.5_db * ( cfe - cfs ) 
                      else
                        cfr = real( cfe - cfs, db )
                        cfi = aimag( cfe + cfs )
                        Tau_rad(lms2,lms1,ispinf2,ispinf1)
     &                              = 0.5_db * cmplx(cfr,cfi,db)
                      endif

                      if( icheck > 2 .and. 
     &                    abs(tau_rad(lms2,lms1,ispinf2,ispinf1))>eps10) 
     &                        then
                        if( Green_int ) then
                          write(3,130) is_r1, l1, m1, iso1, 
     &                      isping1, ispf1, is_r2, l2, m2, iso2,
     &                      isping2, ispf2, 
     &                      taull(lms2,lms1,ispinf2,ispinf1,
     &                              isping12,initl2,initl1),
     &                      rof(lmv1,ispf1,iso1,irang,is_r1),
     &                      rof(lmv2,ispf2,iso2,jrang,is_r2),
     &                      tau_rad(lms2,lms1,ispinf2,ispinf1),
     &                      tau_rad_i(lms2,lms1,ispinf2,ispinf1)
                        else 
                          write(3,130) is_r1, l1, m1, iso1, 
     &                      isping1, ispf1, is_r2, l2, m2, iso2,
     &                      isping2, ispf2, 
     &                      taull(lms2,lms1,ispinf2,ispinf1,
     &                              isping12,initl2,initl1),
     &                      rof(lmv1,ispf1,iso1,irang,is_r1),
     &                      rof(lmv2,ispf2,iso2,jrang,is_r2),
     &                      tau_rad(lms2,lms1,ispinf2,ispinf1)
                        endif
                      endif
!                      if( solsing_rang .and. lmv1 == lmv2 
!     &                  .and. iso2 == iso1 
!     &                  .and. ( (Spinorbite .and. iso1 == ispf1)
!     &                  .or. (.not. Spinorbite .and. ispf1 == ispf2 ) ))
!     &                  Tau_rad(lms2,lms1,ispinf2,ispinf1)
!     &                    = Tau_rad(lms2,lms1,ispinf2,ispinf1)
!     &                    - img * singul(lmv2,ispf2,lmv1,ispf1,irang,
!     &                                   iseuil1)
           
                    end do ! fin boucle sur les solutions en sortie
                  end do ! fin boucle sur le spin etat final en sortie
                end do
              end do ! fin boucle sur les harmoniques en sortie

            end do ! fin boucle sur les solutions en entree
          end do  ! fin boucle sur spin d'etat final en entree
        end do
      end do  ! fin boucle sur les harmoniques en entree

! Verfier si compatible avec Final_tddft ..........
      if( Hub_nondiag_abs .and. (lh_abs+1)**2 <= nlmam ) then
         call rot_tau_rad(icheck,irang,jrang,lh_abs,m_hubb,
     &         nlmam,nspin,nspino_t,
     &         rot_hubb_abs,Spinorbite,Tau_rad)
         if( Green_int ) 
     &     call rot_tau_rad(icheck,irang,jrang,lh_abs,m_hubb,
     &         nlmam,nspin,nspino_t,
     &         rot_hubb_abs,Spinorbite,Tau_rad_i)
      endif

      return
  102 format(/' irang = ',i2,', jrang =',i2,//
     &' isn1 l1 m1 iso1 isg1 isf1  isn2 l2 m2 iso2 isg2 isf2',13x,
     &'Taull',17x,
     &' rof(1,irang,is1)          rof(2,jrang,is2)',14x,'tau_rad',19x,
     &'tau_rad_i')
  104 format(/' irang = ',i2,', jrang =',i2,//
     &' isn1 l1 m1 iso1 isg1 isf1  isn2 l2 m2 iso2 isg2 isf2',13x,
     &'Taull',17x,
     &' rof(1,irang,is1)          rof(2,jrang,is2)',14x,'tau_rad')
  130 format(2(2i4,i3,i4,2i5,2x),1x,1p,2e13.5,1x,4e13.5,2(1x,2e13.5)) 
      end

!***********************************************************************

! Rotation des tau_rad de la base locale, vers la base cluster.
! On neglige les etats hybrides l - l'.

      subroutine rot_tau_rad(icheck,irang,jrang,lh_abs,m_hubb,
     &             nlmam,nspin,nspino,
     &             rot_hubb_abs,spinorbite,Tau_rad)

      use declarations  
      implicit none

      integer:: irang, icheck, iso1,
     &  iso2, isp1, isp2, ispin1, ispin2, jrang, lh, lh_abs, lm0, lm1,
     &  lm2, lms1, lms2, m1, m2, m_hubb, nlmam, nspin, nspino

      logical Spinorbite

      complex(kind=db), dimension(-m_hubb:m_hubb,-m_hubb:m_hubb,nspin):: 
     &                                                rot_hubb_abs
      complex(kind=db), dimension(nlmam*nspino,nlmam*nspino,2,2)::
     &                                                          Tau_rad

      complex(kind=db), dimension(:,:), allocatable:: mat, rotex, rotex2

      lh = lh_abs

      allocate( rotex(-lh:lh,-lh:lh) )
      if( spinorbite ) allocate( rotex2(-lh:lh,-lh:lh) )
      allocate( mat(-lh:lh,-lh:lh) )

      lm0 = lh**2 + lh + 1

      do ispin1 = 1,2
        isp1 = min(ispin1,nspin)
        do ispin2 = 1,2
          isp2 = min(ispin2,nspin)

          if( ispin1 /= ispin2 .and. .not. spinorbite ) cycle

! Recopies pour la matrice de rotation 
          do m1 = -lh,lh
            do m2 = -lh,lh
              rotex(m1,m2) = rot_hubb_abs(m1,m2,isp1)
            end do
          end do

          if( isp1 /= isp2 ) then
            do m1 = -lh,lh
              do m2 = -lh,lh
                rotex2(m1,m2) = rot_hubb_abs(m1,m2,isp2)
              end do
            end do
          endif

          if( icheck > 3 ) then
            write(3,110) ispin1, ispin2, irang, jrang 
            do m1 = -lh,lh
              if( isp1 /= isp2 ) then
                write(3,120)  rotex(m1,:), rotex2(m1,:)
              else
                write(3,120)  rotex(m1,:)
              endif
            end do
          end if

          do iso1 = 1,nspino
            do iso2 = 1,nspino

! Recopies pour tau_ato 
              do m1 = -lh,lh
                lm1 = lm0 + m1
                lms1 = ( iso1 - 1 ) * nlmam + lm1
                do m2 = -lh,lh
                  lm2 = lm0 + m2
                  lms2 = ( iso2 - 1 ) * nlmam + lm2
                  mat(m1,m2) = Tau_rad(lms1,lms2,ispin1,ispin2)
                end do
              end do

              if( icheck > 2 ) then
                write(3,130)
                do m1 = -lh,lh
                  write(3,120) mat(m1,:)
                end do
              end if

              if( isp1 /= isp2 ) then
                mat = matmul( rotex,
     &                        matmul( mat, transpose(conjg(rotex2)) ) )
              else
                mat = matmul( rotex,
     &                        matmul( mat, transpose(conjg(rotex)) ) )
              endif
        
              if( icheck > 2 ) then
                write(3,140)
                do m1 = -lh,lh
                  write(3,120) mat(m1,:)
                end do
              end if

              do m1 = -lh,lh
                lm1 = lm0 + m1
                lms1 = (iso1-1) * nlmam + lm1
                do m2 = -lh,lh
                  lm2 = lm0 + m2
                  lms2 = (iso2-1) * nlmam + lm2
                  Tau_rad(lms1,lms2,ispin1,ispin2) = mat(m1,m2)
                end do
              end do

            end do
          end do

        end do
      end do

      return
  110 format(/' Hubbard Rotation Matrix, ispin1 = ',i2,', ispin2 =',i2,
     &  ', irang =',i2,', jrang =',i2)
  120 format(1p,14(1x,2e13.5))
  130 format(/' Tau_rad local basis')
  140 format(/' Tau_rad cluster basis')
      end

!***********************************************************************

! Calcule le coefficient de Gaunt avec Y(li,mi) complexe, Y(lo,mo) reel
! et Y(l,m) complexe ou reel.
 
      function Gauntm(l,m,lo,mo,li,mi,Ylmcomp)

      use declarations
      implicit real(kind=db) (a-h,o-z)
      complex(kind=db) Gauntm 

      logical Ylmcomp

      if( Ylmcomp ) then

        if( mo == 0 ) then  
          gr = gauntcp(l,m,lo,mo,li,mi)
          gi = 0._db
        elseif( mo > 0 ) then
          gr = (   gauntcp(l,m,lo,mo,li,mi)
     &         + (-1)**mo * gauntcp(l,m,lo,-mo,li,mi) ) / sqrt( 2._db ) 
          gi = 0._db
        else
          gi = ( (-1)**mo * gauntcp(l,m,lo,mo,li,mi)
     &                    - gauntcp(l,m,lo,-mo,li,mi) ) / sqrt( 2._db )
          gr = 0._db
        endif

      else

        if( mi == 0 ) then  
          gr = gauntc(l,m,lo,mo,li,mi)
          gi = 0._db
        elseif( mi > 0 ) then
          gr = gauntc(l,m,lo,mo,li,mi) / sqrt( 2._db )
          gi = gauntc(l,m,lo,mo,li,-mi) / sqrt( 2._db )
        else
          gr = (-1)**mi * gauntc(l,m,lo,mo,li,-mi) / sqrt( 2._db )
          gi = - (-1)**mi * gauntc(l,m,lo,mo,li,mi) / sqrt( 2._db )
        endif

      endif 

      Gauntm = cmplx( gr, gi,db )

      return
      end

!***********************************************************************

! Calcule le coefficient de Gaunt modifie pour la transition dipole
! magnetique
 
      function Gauntmag(isping,ispinf,me,l,mv,li,mi,Spinorbite)

      use declarations
      implicit real(kind=db) (a-h,o-z)
      
      complex(kind=db):: Gaunt, Gauntm, Gauntmag

      logical:: Spinorbite

      select case(me)

        case(-1)     ! Ly + 2*Sy
          if( isping == ispinf ) then
            Gaunt = - 0.5_db * img * ( sqrt( li*(li+1._db) - mi*(mi+1) )
     &                    * Gauntm(l,mv,0,0,li,mi+1,Spinorbite)
     &            - sqrt( li*(li+1._db) - mi*(mi-1) )
     &                    * Gauntm(l,mv,0,0,li,mi-1,Spinorbite) )
          else
            if( isping == 1 ) then
              Gaunt = img * Gauntm(l,mv,0,0,li,mi,Spinorbite)
            else
              Gaunt = - img * Gauntm(l,mv,0,0,li,mi,Spinorbite)
            endif
          endif

        case(0)     ! Lz + 2*Sz
          if( isping /= ispinf ) then
            Gaunt = (0._db,0._db)
          else
            if( isping == 1 ) then
              Gaunt = ( mi + 1 ) * Gauntm(l,mv,0,0,li,mi,Spinorbite) 
            else
              Gaunt = ( mi - 1 ) * Gauntm(l,mv,0,0,li,mi,Spinorbite) 
            endif 
          endif 

        case(1)     ! Lx + 2*Sx
          if( isping == ispinf ) then
            Gaunt = 0.5_db * ( sqrt( li*(li+1._db) - mi*(mi+1) )
     &                    * Gauntm(l,mv,0,0,li,mi+1,Spinorbite)
     &            + sqrt( li*(li+1._db) - mi*(mi-1) )
     &                    * Gauntm(l,mv,0,0,li,mi-1,Spinorbite) )
          else
            Gaunt = Gauntm(l,mv,0,0,li,mi,Spinorbite)
          endif

      end select

      Gauntmag = Gaunt

      return
      end

!***********************************************************************

      subroutine rot_tensor_2( mat2, rot_int )

      use declarations
      complex(kind=db):: cmat
      complex(kind=db), dimension(3,3):: mat, mat2

      real(kind=db), dimension(3,3):: rot_int

      mat(:,:) = (0._db, 0._db) 
      do i = 1,3
        do j = 1,3
          do k = 1,3
            cmat = sum( rot_int(j,:) * mat2(k,:) ) 
            mat(i,j) = mat(i,j) + rot_int(i,k) * cmat  
          end do
        end do
      end do
      mat2(:,:) = mat(:,:)

      return
      end 

!***********************************************************************

      subroutine rot_tensor_3( mat3, rot_int )

      use declarations
      complex(kind=db):: cmas, cmat
      complex(kind=db), dimension(3,3,3):: mat, mat3

      real(kind=db), dimension(3,3):: rot_int

      mat(:,:,:) = (0._db, 0._db) 
      do i = 1,3
        do j = 1,3
          do k = 1,3
            do l = 1,3
              cmas = (0._db, 0._db) 
              do m = 1,3
                cmat = sum( rot_int(k,:) * mat3(l,m,:) ) 
                cmas = cmas + rot_int(j,m) * cmat  
              end do
              mat(i,j,k) = mat(i,j,k) + rot_int(i,l) * cmas  
            end do
          end do
        end do
      end do
      mat3(:,:,:) = mat(:,:,:) 

      return
      end 

!***********************************************************************

      subroutine rot_tensor_4( mat4, rot_int )
     
      use declarations
      complex(kind=db):: cmar, cmas, cmat
      complex(kind=db), dimension(3,3,3,3):: mat, mat4

      real(kind=db), dimension(3,3):: rot_int

      mat(:,:,:,:) = (0._db, 0._db) 

      do i = 1,3
        do j = 1,3
          do k = 1,3
            do l = 1,3
              do m = 1,3
                cmar = (0._db, 0._db) 
                do n = 1,3
                  cmas = (0._db, 0._db) 
                  do n1 = 1,3
                    cmat = sum( rot_int(l,:) * mat4(m,n,n1,:) ) 
                    cmas = cmas + rot_int(k,n1) * cmat  
                  end do
                  cmar = cmar + rot_int(j,n) * cmas  
                end do
                mat(i,j,k,l) = mat(i,j,k,l) + rot_int(i,m) * cmar  
              end do
            end do
          end do
        end do
      end do

      mat4(:,:,:,:) = mat(:,:,:,:) 

      return
      end 

!***********************************************************************

      subroutine extract_coabs(Core_resolved,E1E1,E1E2,E1E3,E1M1,E2E2,
     &            Green_int,icheck,ie,isymext,M1M1,multi_run,nenerg,
     &            ninit1,ninitlr,
     &            nom_fich_extract,secdd,secdd_m,secdo,secdo_m,secdq,
     &            secdq_m,secmd,secmd_m,secmm,secmm_m,secqq,secqq_m,
     &            Tddft)
 
      use declarations
      implicit real(kind=db) (a-h,o-z)

      character(len=132) mot, nom_fich_extract

      complex(kind=db), dimension(3,3,ninitlr,0:0):: secdd, secdd_m,
     &                                secmd, secmd_m, secmm, secmm_m
      complex(kind=db), dimension(3,3,3,ninitlr,0:0):: secdq, secdq_m
      complex(kind=db), dimension(3,3,3,3,ninitlr,0:0):: secdo, secdo_m,
     &                                                   secqq, secqq_m
      complex(kind=db), dimension(3,3):: secdd_t, secmd_t, secmm_t
      complex(kind=db), dimension(3,3,3):: secdq_t
      complex(kind=db), dimension(3,3,3,3):: secdo_t, secqq_t

      logical Comp, Core_resolved, E1E1, E1E2, E1E3, E1M1, E2E2, 
     &        Green_int, M1M1, Tddft, Tensor_rot

      real(kind=db), dimension(3,3):: rot_tem

      common/ang_rotsup/ ang_rotsup(3)
      common/tensor_rot/ tensor_rot
      common/rotsup/ rotsup(3,3)

      if( ie == 1 ) then
        open(1, file = nom_fich_extract, status='old', iostat=istat) 
        if( istat /= 0 ) call write_open_error(nom_fich_extract,istat,1)

! Si les tenseurs importes doivent subir une rotation
        tensor_rot = .false.
        rotsup = 0._db
        do i = 1,3
          rotsup(i,i) = 1._db
        end do
        do i = 1,3
          if( abs( ang_rotsup(i) ) < eps6 ) cycle
          tensor_rot = .true. 
          ang_rotsup(i) = ang_rotsup(i) * pi / 180
          cosa = cos( ang_rotsup(i) )
          sina = sin( ang_rotsup(i) )
          j = mod(i+2,3) + 1
          k = mod(i,3) + 1
          l = mod(i+1,3) + 1
          rot_tem(j,j) = cosa; rot_tem(j,k) = sina; rot_tem(j,l) = 0._db 
          rot_tem(k,j) = -sina; rot_tem(k,k) = cosa; 
          rot_tem(k,l) = 0._db; rot_tem(l,j) = 0._db  
          rot_tem(l,k) = 0._db; rot_tem(l,l) = 1._db 

          rotsup = matmul( rot_tem, rotsup )
        end do

        if( tensor_rot .and. icheck > 0 ) then
          write(3,110)
          write(3,120) ( rotsup(i,1:3), i = 1,3 )
        endif

        i = 0
        do l = 1,1000000
          read(1,'(A)' ) mot
          if( mot(2:15) /= 'Absorbing atom' ) cycle
          i = i + 1
          if( i == multi_run ) exit
        end do

        if( tddft ) then
          do l = 1,1000000
            read(1,'(A)',err=1000,end=1000) mot
            if( mot(2:12) == 'Cycle TDDFT' ) exit
          end do
 1000     continue
        endif

      endif

      do l = 1,100000
        read(1,'(A)') mot
        if( mot(7:11) == 'Coabs' ) exit
      end do

      call extract_tens(Comp,Core_resolved,E1E1,E1E2,E1E3,E1M1,
     &        E2E2,Green_int,isymext,M1M1,ninit1,ninitlr,secdd,secdd_m,
     &        secdo,secdo_m,secdq,secdq_m,secmd,secmd_m,secmm,secmm_m,
     &        secqq,secqq_m)
      
      if( tensor_rot ) then

        do i_g = 1,ninitlr

          if( E1E1 ) then
            secdd_t(:,:) = secdd(:,:,i_g,0)
            call rot_tensor_2( secdd_t, rotsup )
            secdd(:,:,i_g,0) = secdd_t(:,:)
          endif

          if( E1E2 ) then
            secdq_t(:,:,:) = secdq(:,:,:,i_g,0)
            call rot_tensor_3( secdq_t, rotsup )
            secdq(:,:,:,i_g,0) = secdq_t(:,:,:)
          endif

          if( E2E2 ) then
            secqq_t(:,:,:,:) = secqq(:,:,:,:,i_g,0)
            call rot_tensor_4( secqq_t, rotsup )
            secqq(:,:,:,:,i_g,0) = secqq_t(:,:,:,:)
          endif

          if( E1E3 ) then
            secdo_t(:,:,:,:) = secdo(:,:,:,:,i_g,0)
            call rot_tensor_4( secdo_t, rotsup )
            secdo(:,:,:,:,i_g,0) = secdo_t(:,:,:,:)
          endif

          if( E1M1 ) then
            secmd_t(:,:) = secmd(:,:,i_g,0)
            call rot_tensor_2( secmd_t, rotsup )
            secmd(:,:,i_g,0) = secmd_t(:,:)
          endif

          if( M1M1 ) then
            secmm_t(:,:) = secmm(:,:,i_g,0)
            call rot_tensor_2( secmm_t, rotsup )
            secmm(:,:,i_g,0) = secmm_t(:,:)
          endif

          if( .not. ( Comp .and. Green_int ) ) cycle

          if( E1E1 ) then
            secdd_t(:,:) = secdd_m(:,:,i_g,0)
            call rot_tensor_2( secdd_t, rotsup )
            secdd_m(:,:,i_g,0) = secdd_t(:,:)
          endif

          if( E1E2 ) then
            secdq_t(:,:,:) = secdq_m(:,:,:,i_g,0)
            call rot_tensor_3( secdq_t, rotsup )
            secdq_m(:,:,:,i_g,0) = secdq_t(:,:,:)
          endif

          if( E2E2 ) then
            secqq_t(:,:,:,:) = secqq_m(:,:,:,:,i_g,0)
            call rot_tensor_4( secqq_t, rotsup )
            secqq_m(:,:,:,:,i_g,0) = secqq_t(:,:,:,:)
          endif

          if( E1E3 ) then
            secdo_t(:,:,:,:) = secdo_m(:,:,:,:,i_g,0)
            call rot_tensor_4( secdo_t, rotsup )
            secdo_m(:,:,:,:,i_g,0) = secdo_t(:,:,:,:)
          endif

          if( E1M1 ) then
            secmd_t(:,:) = secmd_m(:,:,i_g,0)
            call rot_tensor_2( secmd_t, rotsup )
            secmd_m(:,:,i_g,0) = secmd_t(:,:)
          endif

          if( M1M1 ) then
            secmm_t(:,:) = secmm_m(:,:,i_g,0)
            call rot_tensor_2( secmm_t, rotsup )
            secmm_m(:,:,i_g,0) = secmm_t(:,:)
          endif
 
        end do

      endif

      if( ie == nenerg ) Close(1)

      return
  110 format(/' Matrix rotation for the extracted tensors, rot_sup :')
  120 format(3x,3f9.5)
      end

!***********************************************************************

      subroutine extract_tens(Comp,Core_resolved,E1E1,E1E2,E1E3,E1M1,
     &        E2E2,Green_int,isymext,M1M1,ninit1,ninitlr,secdd,secdd_m,
     &        secdo,secdo_m,secdq,secdq_m,secmd,secmd_m,secmm,secmm_m,
     &        secqq,secqq_m)

      use declarations
      implicit real(kind=db) (a-h,o-z)

      character(len=132) mot

      logical comp, Core_resolved_e, Core_resolved, E1E1, E1E2, E1E3,
     &        E1M1, E2E2, Green_int, M1M1

      real(kind=db), dimension(3):: vi, vr, wi, wr
      real(kind=db), dimension(3,3):: matopsym

      complex(kind=db), dimension(3,3):: mat2
      complex(kind=db), dimension(3,3,3):: mat3
      complex(kind=db), dimension(3,3,3,3):: mat4
      complex(kind=db), dimension(3,3,ninitlr):: secdd, secdd_m,
     &                         secmd, secmd_m, secmm, secmm_m
      complex(kind=db), dimension(3,3,3,ninitlr):: secdq, secdq_m
      complex(kind=db), dimension(3,3,3,3,ninitlr):: secdo, secdo_m,
     &                         secqq, secqq_m

      if( E1E1 ) secdd(:,:,:) = (0._db,0._db)
      if( E1E2 ) secdq(:,:,:,:) = (0._db,0._db)
      if( E2E2 ) secqq(:,:,:,:,:) = (0._db,0._db)
      if( E1E3 ) secdo(:,:,:,:,:) = (0._db,0._db)
      if( E1M1 ) secmd(:,:,:) = (0._db,0._db)
      if( M1M1 ) secmm(:,:,:) = (0._db,0._db)
      if( E1E1 ) secdd_m(:,:,:) = (0._db,0._db)
      if( E1E2 ) secdq_m(:,:,:,:) = (0._db,0._db)
      if( E2E2 ) secqq_m(:,:,:,:,:) = (0._db,0._db)
      if( E1E3 ) secdo_m(:,:,:,:,:) = (0._db,0._db)
      if( E1M1 ) secmd_m(:,:,:) = (0._db,0._db)
      if( M1M1 ) secmm_m(:,:,:) = (0._db,0._db)

      do l = 1,100000
        read(1,'(A)') mot
        if( mot(2:4) == 'sec' .or. mot(2:7) == 'Tensor' ) exit
      end do

      n_g = 1
      Core_resolved_e = .false.

      backspace(1)
      backspace(1)
      backspace(1)
      read(1,'(A)') mot
      if( mot(13:16) == 'tate' .or. mot(26:29) == 'Edge') then
        if( mot(13:16) == 'tate' ) Core_resolved_e = .true.
        backspace(1)
        read(1,'(24x,i3)') n_g
      else
        n_g = ninitlr
      endif

      do j_g = 1,n_g

        if( ( Core_resolved_e .and. Core_resolved ) .or. 
     &      ( .not. Core_resolved_e .and. .not. Core_resolved ) ) then
          i_g = j_g
        elseif( j_g <= ninit1 ) then
          i_g = 1
        else
          i_g = 2
        endif

        Boucle_Multipole: do Multipole = 1,6

          if( Multipole == 1 .and. .not. E1E1 ) cycle
          if( Multipole == 2 .and. .not. E1E2 ) cycle
          if( Multipole == 3 .and. .not. E2E2 ) cycle
          if( Multipole == 4 .and. .not. E1E3 ) cycle
          if( Multipole == 5 .and. .not. E1M1 ) cycle
          if( Multipole == 6 .and. .not. M1M1 ) cycle

          do l = 1,100000

            read(1,'(A)',iostat=istat) mot

            if( istat /= 0 ) then
              call write_error
              do ipr = 6,9,3
                select case(Multipole)
                  Case(1)
                    write(ipr,110) ' E1-E1 '
                  Case(2)
                    write(ipr,110) ' E1-E2 '
                  Case(3)
                    write(ipr,110) ' E2-E2 '
                  Case(4)
                    write(ipr,110) ' E1-E3 '
                  Case(5)
                    write(ipr,110) ' E1-M1 '
                  Case(6)
                    write(ipr,110) ' M1-M1 '
                end select
              end do
              stop
            endif

            select case(Multipole)
              Case(1)
                if( mot(2:10) == 'Tensor_dd' ) exit
              Case(2)
                if( mot(2:10) == 'Tensor_dq' ) exit
                if( ( mot(2:7) == 'Tensor' .and. mot(9:10) /= 'dd' )
     &                   .or. mot(2:4) == '---' ) then
                   backspace(1)
                   Cycle Boucle_Multipole
                endif
              Case(3)
                if( mot(2:10) == 'Tensor_qq' ) exit
              Case(4)
                if( mot(2:10) == 'Tensor_do' ) exit
              Case(5)
                if( mot(2:10) == 'Tensor_md' ) exit
              Case(6)
                if( mot(2:10) == 'Tensor_mm' ) exit
            end select

          end do

          if( mot(43:56) == 'Green integral' ) Green_int = .true.

          n = nnombre(1,1320)
          if( n == 12 .or. ( n == 6 .and. .not. Green_int ) ) then
            Comp = .true.
          else
            Comp = .false.
            wi(:) = 0._db
          endif

          select case(Multipole)
            Case(1,5,6)

              do ke = 1,3

                if( Green_int .and. comp ) then
                  read(1,*) ( wr(k), wi(k), k = 1,3 ),
     &                      ( vr(k), vi(k), k = 1,3 )
                elseif( comp ) then
                  read(1,*) ( wr(k), wi(k), k = 1,3 )
                else
                  read(1,*) wr(1:3)
                endif

                if( Multipole == 1 ) then
                  if( Green_int .and. comp ) secdd_m(ke,:,i_g)
     &                                   = cmplx( vr(:), vi(:),db )
                  secdd(ke,:,i_g) = cmplx( wr(:), wi(:),db )
                elseif( Multipole == 5 ) then
                  if( Green_int .and. comp ) secmd_m(ke,:,i_g)
     &                                   = cmplx( vr(:), vi(:),db )
                  secmd(ke,:,i_g) = cmplx( wr(:), wi(:),db )
                elseif( Multipole == 6 ) then
                  if( Green_int .and. comp ) secmm_m(ke,:,i_g)
     &                                   = cmplx( vr(:), vi(:),db )
                  secmm(ke,:,i_g) = cmplx( wr(:), wi(:),db )
                endif

              end do

            Case(2)

              do ke = 1,3
                do ks = 1,3
                  if( Green_int .and. comp ) then
                    read(1,*) ( wr(k), wi(k), k = 1,3 ),
     &                        ( vr(k), vi(k), k = 1,3 )
                  elseif( comp ) then
                    read(1,*) ( wr(k), wi(k), k = 1,3 )
                  else
                    read(1,*) wr(1:3)
                  endif
                  if( Green_int .and. comp ) secdq_m(ke,ks,:,i_g)
     &                                       = cmplx( vr(:), vi(:),db )
                  secdq(ke,ks,:,i_g) = cmplx( wr(:), wi(:),db )
                end do
                read(1,*)
                read(1,*)
              end do
              Backspace(1)

            Case(3)

              do js = 1,3
                do ks = 1,3
                  do ke = 1,3
                    if( Green_int .and. comp ) then
                      read(1,*) ( wr(k), wi(k), k = 1,3 ),
     &                          ( vr(k), vi(k), k = 1,3 )
                    elseif( comp ) then
                      read(1,*) ( wr(k), wi(k), k = 1,3 )
                    else
                      read(1,*) wr(1:3)
                    endif
                    if( Green_int .and. comp ) secqq_m(ke,:,ks,js,i_g)
     &                                       = cmplx( vr(:), vi(:),db )
                    secqq(ke,:,ks,js,i_g) = cmplx( wr(:), wi(:),db )
                  end do
                  read(1,*)
                  read(1,*)
                end do
              end do
              Backspace(1)

            Case(4)

              do ke = 1,3
                do ks = 1,3
                  do j1 = 1,3
                    if( Green_int .and. comp ) then
                      read(1,*) ( wr(k), wi(k), k = 1,3 ),
     &                          ( vr(k), vi(k), k = 1,3 )
                    elseif( comp ) then
                      read(1,*) ( wr(k), wi(k), k = 1,3 )
                    else
                      read(1,*) wr(1:3)
                    endif
                    if( Green_int .and. comp ) secdo_m(ke,ks,j1,:,i_g)
     &                                       = cmplx( vr(:), vi(:),db )
                    secdo(ke,ks,j1,:,i_g) = cmplx( wr(:), wi(:),db )
                  end do
                  read(1,*)
                  read(1,*)
                end do
              end do
              Backspace(1)

           end select

        end do Boucle_Multipole

      end do

      do i_g = 1,ninitlr

        if( isymext == 1 ) cycle
        isym = abs( isymext )
        call opsym(isym,matopsym)

        if( E1E1 ) then
          mat2(:,:) = secdd(:,:,i_g)
          call rot_tensor_2( mat2, matopsym )
          if( isymext < 0 ) mat2(:,:) = conjg( mat2(:,:) )
          secdd(:,:,i_g) = mat2(:,:)
        endif

        if( E1E2 ) then
          mat3(:,:,:) = secdq(:,:,:,i_g)
          call rot_tensor_3( mat3, matopsym )
          if( isymext < 0 ) mat3(:,:,:) = conjg( mat3(:,:,:) )
          secdq(:,:,:,i_g) = mat3(:,:,:)
        endif

        if( E2E2 ) then
          mat4(:,:,:,:) = secqq(:,:,:,:,i_g)
          call rot_tensor_4( mat4, matopsym )
          if( isymext < 0 ) mat4(:,:,:,:) = conjg( mat4(:,:,:,:) )
          secqq(:,:,:,:,i_g) = mat4(:,:,:,:)
        endif

        if( E1E3 ) then
          mat4(:,:,:,:) = secdo(:,:,:,:,i_g)
          call rot_tensor_4( mat4, matopsym )
          if( isymext < 0 ) mat4(:,:,:,:) = conjg( mat4(:,:,:,:) )
          secdo(:,:,:,:,i_g) = mat4(:,:,:,:)
        endif

        if( E1M1 ) then
          mat2(:,:) = secmd(:,:,i_g)
          call rot_tensor_2( mat2, matopsym )
          if( isymext < 0 ) mat2(:,:) = conjg( mat2(:,:) )
          secmd(:,:,i_g) = mat2(:,:)
        endif

        if( M1M1 ) then
          mat2(:,:) = secmm(:,:,i_g)
          call rot_tensor_2( mat2, matopsym )
          if( isymext < 0 ) mat2(:,:) = conjg( mat2(:,:) )
          secmm(:,:,i_g) = mat2(:,:)
        endif

        if( .not. ( Green_int .and. Comp ) ) cycle

        if( E1E1 ) then
          mat2(:,:) = secdd_m(:,:,i_g)
          call rot_tensor_2( mat2, matopsym )
          if( isymext < 0 ) mat2(:,:) = - mat2(:,:)
          secdd_m(:,:,i_g) = mat2(:,:)
        endif

        if( E1E2 ) then
          mat3(:,:,:) = secdq_m(:,:,:,i_g)
          call rot_tensor_3( mat3, matopsym )
          if( isymext < 0 ) mat3(:,:,:) = - mat3(:,:,:)
          secdq_m(:,:,:,i_g) = mat3(:,:,:)
        endif

        if( E2E2 ) then
          mat4(:,:,:,:) = secqq_m(:,:,:,:,i_g)
          call rot_tensor_4( mat4, matopsym )
          if( isymext < 0 ) mat4(:,:,:,:) = - mat4(:,:,:,:)
          secqq_m(:,:,:,:,i_g) = mat4(:,:,:,:)
        endif

        if( E1E3 ) then
          mat4(:,:,:,:) = secdo_m(:,:,:,:,i_g)
          call rot_tensor_4( mat4, matopsym )
          if( isymext < 0 ) mat4(:,:,:,:) = - mat4(:,:,:,:)
          secdo_m(:,:,:,:,i_g) = mat4(:,:,:,:)
        endif

        if( E1M1 ) then
          mat2(:,:) = secmd_m(:,:,i_g)
          call rot_tensor_2( mat2, matopsym )
          if( isymext < 0 ) mat2(:,:) = - mat2(:,:)
          secmd_m(:,:,i_g) = mat2(:,:)
        endif

        if( M1M1 ) then
          mat2(:,:) = secmm_m(:,:,i_g)
          call rot_tensor_2( mat2, matopsym )
          if( isymext < 0 ) mat2(:,:) = - mat2(:,:)
          secmm_m(:,:,i_g) = mat2(:,:)
        endif

      end do

      return
  110 format(//A,' not found in the extract file !'//)
      end
