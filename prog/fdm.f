! Subroutine of the FDMNES Package
! Contain the main of the FDM-MST part of the calculation

      subroutine fdm(comt,Convolution_cal,Delta_edge,
     &      E_cut_imp,E_Fermi_man,Ecent,Elarg,Estart,Fit_cal,Gamma_hole,
     &      Gamma_hole_imp,Gamma_max,Gamma_tddft,Green_plus,
     &      icheck,ifile_notskip,indice_par,iscratch,
     &      itape1,itape4,Length_word,mpinodes,mpirank,
     &      n_atom_proto_p,ngamh,ngroup_par,nnotskip,nnotskipm,nomfich,
     &      nomfichbav,npar,nparm,param,Scan_a,Space_file,typepar)

      use declarations
      implicit real(kind=db) (a-h,o-z)
      include 'mpif.h'

      parameter( n_tens_dd=9, n_tens_dq=15, n_tens_qq=25,
     &         n_tens_t = n_tens_dd + n_tens_dq + n_tens_qq,
     &         n_tens_max = 8 + 2 * n_tens_t + 2 * n_tens_dq ) 

      character(len=3) seuil
      character(len=5):: struct
      character(len=8) PointGroup
      character(len=9):: keyword
      character(len=132):: comt, identmot, mot, nomfich, 
     &   nom_fich_extract, nomfichbav, nomfich_cal_convt, nomfich_s,
     &   nomfich_tddft_data, nomstruct,
     &   nomvcoul, nomr2v, nomr2vdn, nompsii, Space_file
      character(len=6), dimension(10):: nomspr
      character(len=9), dimension(ngroup_par,nparm):: typepar
      character(len=13), dimension(:), allocatable:: ltypcal
      character(len=35), dimension(:), allocatable:: com
      character(len=Length_word), dimension(:), allocatable:: nomabs
      character(len=132), dimension(:), allocatable::   
     &   nomfile_atom, nomclm, nomfich_cal_conv, nomfich_cal_tddft_conv

      complex(kind=db), dimension(:), allocatable:: konde 
      complex(kind=db), dimension(:,:), allocatable:: karact, phdafs,   
     &      phdf0t, phdt, pol, poldafsem, poldafssm 
      complex(kind=db), dimension(:,:,:), allocatable:: poldafse,   
     &       poldafss, rot_hubb_abs
      complex(kind=db), dimension(:,:,:,:), allocatable:: occ_hubb, 
     &         rot_hubb, secdd, secdd_m, secmd, secmd_m, secmm, secmm_m  
      complex(kind=db), dimension(:,:,:,:,:), allocatable:: rof, rof0,   
     &     roff, rofsd, secdq, secdq_m, statedens_hd, tau_ato, taull
      complex(kind=db), dimension(:,:,:,:,:,:), allocatable:: Chi_0,
     &      rofsd_hd, rhov_self, secdo, secdo_m, secqq, secqq_m,
     &      sing, singul, taull_stk           
      complex(kind=db), dimension(:,:,:,:,:,:,:), allocatable:: Chi,
     &     taull_abs 

      integer:: extract_nenerg, Z, Z_nospinorbite
      integer, dimension(2):: mix_repr     
      integer, dimension(30):: icheck
      integer, dimension(ngroup_par) :: npar
      integer, dimension(nnotskipm) :: ifile_notskip
      integer, dimension(ngroup_par,nparm) :: indice_par
      integer, dimension(nrepm,2):: irep_util(nrepm,2)
     
      integer, dimension(:), allocatable :: ia_eq_inv, ia_eq_inv_self,     
     &   iabsm,  iabsorig, iapot, iaproto, iaprotoi, icom, igroup,  
     &   igroupi, imoy, imoy_out, is_g, ispin_maj, isrt, isymeq, itdil,
     &   its_lapw, itype, itypei, itypep, itypepr, ldil, lmaxa, lmaxat,  
     &   lqnexc, lval, nb_eq, nbord, nbordf, ngreq, ngreqm, nlat, nlmsa,   
     &   nlmso0, norbv, nqnexc, nphi_dafs, nposextract, nrato, 
     &   nrato_lapw, nrmtg, nsymextract, numat, numia

      integer, dimension(:,:), allocatable :: hkl_dafs, ia_eq,  
     &   ibord, igreq, indice, iopsym_atom, is_eq, isbord,
     &   isigpi, isymqa, isvois, ivois, iso, lcoeur, lso, lvval,
     &   m_g, mso, nb_rpr, ncoeur, nlmsa0, nvval

      integer, dimension(:,:,:), allocatable ::
     &   ia_rep, iato, lato, mato, mpres, nb_rep_t

      logical Absauto, Absorbeur, Allsite, Atom_nonsph, Atom_occ_mat,
     &   Atomic_scr, Basereel, Base_hexa, Base_ortho, BSE, Clementi,
     &   Convergence, 
     &   Core_resolved, Cal_xanes, Convolution_cal, Coupelapw, Dafs,    
     &   Density, Dipmag, Dyn_eg, Dyn_g, E_comp, E1E1, E1E2, E1E2e,
     &   E1E3, E1M1, E1M2, E2E2, E_Fermi_man, Eneg, Eneg_i,
     &   Eneg_n_i, Energphot, Etatlie, Extract, Extract_Green,  
     &   Fermi, Final_tddft, Fit_cal, Flapw, Flapw_new, Force_ecr, 
     &   Full_atom, Full_atom_e, Full_self_abs, Gamma_hole_imp,
     &   Gamma_tddft, Green, Green_i, Green_int, Green_plus, Green_s,
     &   Green_self, Hub_nondiag,    
     &   Hub_nondiag_abs, Hubbat, Hubbard, key_calc, korigimp, 
     &   Level_val_abs, Level_val_exc, lmaxfree, lmoins1, lplus1, M1M1,       
     &   M1M2, M2M2, Magnetic, Matper, Memory_save, Moy_cluster,
     &   Moy_loc, Moyenne,  
     &   Muffintin, Noncentre, Nonexc_g, Nonexc, Normaltau, Octupole, 
     &   One_run, Overad, Proto_all, Quadmag, Quadrupole, Readfast,
     &   Recop, Recup_tddft_data, Relativiste, RPALF, 
     &   Rydberg, Save_tddft_data, Scan_a, Scf_elecabs, SCF_mag_fix,
     &   Second_run, Self_abs, Self_cons, Self_nonexc,        
     &   Solsing, Solsing_bess, Solsing_s, Solsing_only, State_all, 
     &   State_all_out, 
     &   Spino, Spinorbite, Spinorbite_t, Supermuf, Sym_4, Sym_cubic,   
     &   Symauto, Tau_nondiag, Taux, Tddft, Tddft_mix, Tddft_so,
     &   Tddft_xanes,   
     &   Trace_format_wien, Xan_atom, Ylm_complex, Ylmcomp, Ylmcompa

      logical, dimension(:), allocatable:: Atom_axe, Atom_comp,
     &    Atom_mag_gr,
     &    Atom_nsph, Hubb, Repres_comp, Run_done, Skip_run, Sgn
     
      real(kind=db):: Kern_fac
      real(kind=db), dimension(2):: chg_open_val, pop_open_val
      real(kind=db), dimension(3):: angxyz, axyz
      real(kind=db), dimension(6):: tp
      real(kind=db), dimension(10) :: Gamma_hole, tpt 
      real(kind=db), dimension(3,3):: Cubmat, rot_atom_abs
      real(kind=db), dimension(ngroup_par,nparm):: param 
      real(kind=db), dimension(:), allocatable:: cdil, ch_coeur,  
     &   chargat, chg_cluster, cgrad, clapl, Decal_initl, dista, distai,    
     &   dv0bdcF, dvc_ex_nex, dv_ex_nex, Ecinetic_edge, Ecinetic_out,  
     &   ecrantage, eeient, Energ, Ef, egamme, Eimag, 
     &   eimag_coh, eimag_s, eimagent, energ_coh, Energ_s, Energ_self,   
     &   Energ_self_s, En_coeur, En_coeur_s, Epsii, Eseuil,    
     &   poidso, poidsov, poidsov_out,  
     &   popatc, r0_lapw, rato_abs, rato_e, rchimp, rhons, rlapw, 
     &   rmt, rmtimp, rmtg, rmtg0, rmtsd, rs, rsato_abs, rvol, sec_atom,  
     &   Taux_eq, Taux_ipr, Taux_oc, 
     &   V_hubbard, Vh, Vhns, V0bd_e, v0bd_out, v0bdcFimp
      real(kind=db), dimension(:,:), allocatable :: coef_g, 
     &   angpoldafs, Axe_atom_clu, Axe_Atom_Clui, Axe_atom_gr, 
     &   chargat_init, chargat_self, chargat_self_s,
     &   drho_ex_nex, Ecinetic, Efato, excato, Int_tens, occ_diag_t, 
     &   occ_mat_gr, pdp, pdpolar, poidsa, polar, pop_nonsph,
     &   pop_orb_val, popatv,   
     &   popexc, pos, poseq, posi, posi_self, posn, psi_open_val, psii,       
     &   rato, rho, rho_coeur, rho_cor, rho_self_t, rhoato_abs,   
     &   rhoit, rsato, V_abs_i, V0bd, Vcato, vcato_init, vecdafsem,
     &   vecdafssm, veconde, vr, vrato_e, vxc, vec, xyz, ylmso
      real(kind=db), dimension(:,:,:), allocatable:: fxc,  
     &   gradvr, hybrid, Int_statedens, occ_diag, occ_diag_s, popatm, 
     &   popats, popval, posq, psi_coeur, psival, rho_chg,  
     &   rho_self, rho_self_s, rhoato_init, rot_atom, rot_atom_gr, 
     &   rotloc_lapw, singulsd, vecdafse, vecdafss,
     &   vrato, vxcato, Ylmato

      real(kind=db), dimension(:,:,:,:), allocatable:: drho_self, 
     &       occ_hubb_i, occ_hubb_r, sing_self, statedens
      real(kind=db), dimension(:,:,:,:,:), allocatable:: imag_taull, zet
      real(kind=db), dimension(:,:,:,:,:,:), allocatable:: phiato
      real(kind=db), dimension(:,:,:,:,:,:,:), allocatable::  Kern

      real(kind=sg) time
      
      common/Atom_nonsph/ Atom_nonsph
      common/Base_hexa/ Base_hexa
      common/Base_ortho/ Base_ortho
      common/iopsymc/ iopsymc(nopsm)
      common/iopsymr/ iopsymr(nopsm)
      common/orthmat/ orthmat(3,3), orthmati(3,3)

      data nomspr/'Lectur','Reseau','Potent','Ylm   ','Potex ',
     &   'Sphere','Mat   ','Tensor','Coabs ','Total '/

      if( mpirank == 0 ) then
        call CPU_TIME(time)
        tp1 = real(time,db)
      endif

      E_Fermi = -5._db / Rydb
      E_cut = E_Fermi
      E_Open_val = E_Fermi
      E_Open_val_exc = E_Fermi
      Moy_cluster = .true.

      call lectdim(Absauto,Atom_occ_mat,Extract,Flapw,
     &      Full_self_abs,Hubbard,itape4,Magnetic,Memory_save,mpinodes,
     &      mpirank,n_multi_run_e,ncolm,neimagent,nenerg_s,ngamme
     &      ,ngroup,ngroup_neq,nhybm,nklapw,nlatm,nlmlapwm,nmatsym,
     &      norbdil,npldafs,nple,nplrm,nspin,nspino,
     &      ntype,Readfast,Self_abs,Space_file,Taux,Xan_atom)

      if( Atom_nonsph ) then
        ngroup_nonsph = ngroup
      else
        ngroup_nonsph = 1
      endif
      if( Flapw ) then
        ngroup_lapw = ngroup_neq
      else
        ngroup_lapw = 1
      endif

      allocate( egamme(ngamme) )
      allocate( eeient(neimagent) )
      allocate( eimagent(neimagent) )
      allocate( angpoldafs(3,npldafs) )
      allocate( Atom_nsph(ngroup) )    
      allocate( Axe_atom_gr(3,ngroup) )
      allocate( com(0:ntype) )
      allocate( cdil(norbdil) )
      allocate( ecrantage(nspin) )
      allocate( hkl_dafs(3,npldafs) )
      allocate( hybrid(nhybm,16,ngroup_nonsph) ) 
      allocate( iabsm(n_multi_run_e) )
      allocate( iabsorig(n_multi_run_e) )
      allocate( icom(0:ntype) )
      allocate( isigpi(npldafs,2) )
      allocate( itdil(norbdil) )
      allocate( its_lapw(ngroup_lapw) )
      allocate( itype(ngroup) )
      allocate( ldil(norbdil) )
      allocate( lvval(0:ntype,nlatm) )
      allocate( nlat(0:ntype) )
      allocate( nomclm(2*nspin-1) ) 
      allocate( nomfile_atom(0:ntype) )
      allocate( norbv(0:ngroup_nonsph) )
      allocate( nphi_dafs(npldafs) )
      allocate( nrato(0:ntype) )
      allocate( nrato_lapw(0:ntype) )
      allocate( nposextract(n_multi_run_e) )
      allocate( nsymextract(n_multi_run_e) )
      allocate( numat(0:ntype) )
      allocate( nvval(0:ntype,nlatm) )
      allocate( occ_mat_gr(14,ngroup) )
      allocate( pdpolar(nple,2) )
      allocate( polar(3,nple) )
      allocate( poldafsem(3,npldafs) )
      allocate( poldafssm(3,npldafs) )
      allocate( pop_nonsph(nhybm,ngroup_nonsph) ) 
      allocate( popatc(0:ntype) )
      allocate( popats(ngroup,nlatm,nspin) )
      allocate( popatv(0:ntype,nlatm) )
      allocate( popval(0:ntype,nlatm,nspin) )
      allocate( posn(3,ngroup) ) 
      allocate( r0_lapw(0:ntype) )
      allocate( rchimp(0:ntype) )
      allocate( rlapw(0:ntype) )
      allocate( rmt(0:ntype) )
      allocate( rmtimp(0:ntype) )
      allocate( rotloc_lapw(3,3,ngroup_lapw) )
      allocate( Rot_Atom_gr(3,3,ngroup) )
      allocate( Taux_oc(ngroup) ) 
      allocate( V_hubbard(0:ntype) )
      allocate( Hubb(0:ntype) )
      allocate( v0bdcFimp(nspin) )
      allocate( vecdafsem(3,npldafs) )
      allocate( vecdafssm(3,npldafs) )
      allocate( veconde(3,nple) )

      if( mpinodes > 1 ) call MPI_BARRIER(MPI_COMM_WORLD,mpierr) 

      call lectur(alfpot,Allsite,angpoldafs,angxyz,Atom_occ_mat,
     &  Atom_nsph,atomic_scr,Axe_atom_gr,axyz,Basereel,BSE,
     &  Clementi,com,comt,Core_resolved,Coupelapw,Cubmat,Dafs,Density,
     &  Dipmag,Dyn_eg,Dyn_g,Eclie,ecrantage,eeient,egamme,eimagent,
     &  Delta_En_conv,Delta_Epsii,E1E1,E1E2e,E1E3,E1M1,E1M2,E2E2,Eneg_i,
     &  Eneg_n_i,Energphot,Etatlie,Extract,Fit_cal,Flapw,Flapw_new,
     &  Force_ecr,Full_atom_e,Full_self_abs,Gamma_hole,Gamma_hole_imp,
     &  Gamma_max,Gamma_tddft,Green_int,
     &  Green_s,Green_self,hkl_dafs,Hubb,Hubbard,hybrid,iabsm,iabsorig,
     &  icheck,icom,indice_par,iscratch,isigpi,itdil,its_lapw,iord,
     &  itape4,itype,jseuil,Kern_fac,korigimp,l_selec_max,lamstdens,
     &  ldil,lecrantage,lin_gam,lmaxfree,lmaxso0,lmaxat0,lmoins1,lplus1,
     &  lseuil,lvval,M1M1,M1M2,M2M2,Magnetic,Matper,mix_repr,mpinodes,
     &  mpirank,
     &  Muffintin,multrmax,n_atom_proto,n_multi_run_e,nbseuil,nchemin,
     &  necrantage,neimagent,nenerg_s,ngamh,ngamme,ngroup,ngroup_lapw,
     &  ngroup_neq,ngroup_nonsph,ngroup_par,nhybm,nlat,nlatm,nnlm,
     &  nom_fich_extract,nomfich,nomfich_tddft_data,nomfichbav,nomclm,
     &  nomfile_atom,nompsii,nomr2v,nomr2vdn,nomstruct,nomvcoul,
     &  Noncentre,Nonexc,norbdil,norbv,Normaltau,npar,nparm,nphi_dafs,
     &  nphim,npldafs,nple,nposextract,nrato,nrato_dirac,nrato_lapw,nrm,
     &  nself,nseuil,nspin,nsymextract,ntype,numat,numat_abs,nvval,
     &  occ_mat_gr,Octupole,One_run,Overad,overlap,p_self0,param,
     &  pdpolar,PointGroup,polar,poldafsem,poldafssm,
     &  pop_nonsph,popatc,popats,popatv,popval,posn,Quadmag,Quadrupole,
     &  r0_lapw,rchimp,Readfast,Recup_tddft_data,Relativiste,r_self,
     &  rlapw,rmt,rmtimp,Rot_Atom_gr,rotloc_lapw,roverad,RPALF,rpotmax,
     &  Rydberg,rsorte_s,Save_tddft_data,Scf_elecabs,SCF_mag_free,
     &  Self_abs,
     &  Self_cons,Self_nonexc,seuil,Solsing_s,Solsing_only,Space_file,
     &  Spinorbite,State_all,State_all_out,Struct,Supermuf,Symauto,Taux,
     &  Taux_oc,Tddft,
     &  Tddft_mix,Tddft_so,temp,Test_dist_min,Trace_format_wien,typepar,
     &  V_hubbard,vecdafsem,vecdafssm,veconde,v0bdcFimp,Ylm_complex,
     &  Z_nospinorbite)

      if( icheck(1) > 0 .and. mpirank == 0 ) then
        if( mpinodes > 1 ) then
          write(3,110) mpinodes
        else
          write(3,120) 
        endif  
      endif

      if( mpirank /= 0 .and. Extract ) goto 1040

      if( Extract ) then
        mpinodee = 1
      else
        mpinodee = mpinodes
      endif

      Moy_loc = Green .and. .not. Moy_cluster

      if( nphim > 1 ) Scan_a = .true.

      allocate( Eseuil(nbseuil) )
      allocate( lcoeur(2,0:ntype) )
      allocate( ncoeur(2,0:ntype) )
      allocate( popexc(nnlm,nspin) )
      allocate( psi_coeur(0:nrm,2,0:ntype) )
      allocate( psii(nrm,nbseuil) )
      allocate( psival(0:nrm,nlatm,0:ntype) )
      allocate( psi_open_val(nrm,2) )
      allocate( rato(0:nrm,0:ntype) )
      allocate( rhoit(0:nrm,0:ntype) )
      allocate( rho_coeur(0:nrm,0:ntype) )
      allocate( rho_cor(0:nrm,0:ntype) )
    
      call atom(Clementi,com,icheck(2),icom,itype,jseuil,lcoeur,
     &      lseuil,lvval,mpinodee,mpirank,nbseuil,ncoeur,ngroup,nlat,
     &      nlatm,nnlm,nomfile_atom,Nonexc,nrato,nrato_dirac,nrato_lapw,
     &      nrm,nseuil,nspin,ntype,numat,nvval,popatc,popats,
     &      popatv,popexc,popval,psi_coeur,psii,psival,
     &      r0_lapw,rato,Relativiste,rho_coeur,rhoit,rlapw,rmt)
        
      allocate( Atom_mag_gr(0:ngroup) )

      if( Symauto ) then
        if( mpirank == 0 )
     &    call symsite(absauto,angxyz,Atom_mag_gr,Atom_nsph,
     &      Axe_atom_gr,axyz,Base_ortho,Cubmat,Flapw,iabsm,icheck(3),
     &      iscratch,itype,Magnetic,
     &      Matper,Memory_save,n_atom_proto,n_multi_run_e,ngroup,
     &      nlat,nlatm,nspin,ntype,numat,numat_abs,popats,posn,Struct)
        if( mpinodee > 1 ) then
          call MPI_BARRIER(MPI_COMM_WORLD,mpierr)
          call MPI_Bcast(n_atom_proto,1,MPI_INTEGER,0,MPI_COMM_WORLD,
     &             mpierr)
          call MPI_Bcast(Atom_mag_gr,ngroup+1,MPI_LOGICAL,0,
     &             MPI_COMM_WORLD,mpierr)
          call MPI_Bcast(Base_ortho,1,MPI_LOGICAL,0,MPI_COMM_WORLD,
     &             mpierr)
        endif
      endif

      Volume_maille = Cal_Volume_maille(axyz,angxyz)

      call esdata(Eseuil,icheck(4),jseuil,nbseuil,nseuil,numat_abs,
     &            workf,mpirank)
      if( Flapw ) workf = 0._db

      call dim_init(jseuil,lseuil,nbseuil,ninit1,ninitl)
      allocate( coef_g(ninitl,2) )
      allocate( m_g(ninitl,2) )
      allocate( is_g(ninitl) )
      call coef_init(coef_g,is_g,ninitl,jseuil,lseuil,m_g,nbseuil)
      if( Core_resolved ) then
        ninitlu = ninitl
      else
        ninitlu = nbseuil
      endif

      allocate( igreq(0:n_atom_proto,ngroup) )
      allocate( isymqa(0:n_atom_proto,ngroup) )
      allocate( ngreq(0:n_atom_proto) )
      allocate( posq(3,n_atom_proto,ngroup) )  

      if( mpirank == 0 ) then
        Rewind(iscratch)
        do ipr = 1,n_atom_proto
          read(iscratch,*) ngreq(ipr)
          do i = 1,ngreq(ipr)
            read(iscratch,*) igreq(ipr,i), posq(:,ipr,i), isymqa(ipr,i)
          end do
        end do
        Close(iscratch)
      endif

      if( mpinodee > 1 ) then
        nn = n_atom_proto + 1 
        ndim = nn * ngroup
        n = 3 * n_atom_proto * ngroup
        call MPI_BARRIER(MPI_COMM_WORLD,mpierr)
        call MPI_Bcast(igreq,ndim,MPI_INTEGER,0,MPI_COMM_WORLD,mpierr)
        call MPI_Bcast(ngreq,nn,MPI_INTEGER,0,MPI_COMM_WORLD,mpierr)
        call MPI_Bcast(isymqa,ndim,MPI_INTEGER,0,MPI_COMM_WORLD,mpierr)
        call MPI_Bcast(posq,n,MPI_REAL8,0,MPI_COMM_WORLD,mpierr)
      endif

      if( absauto ) then
        multi_run = 0
        do ipr = 1,n_atom_proto
          it = abs( itype(igreq(ipr,1)) ) 
          if( numat( it ) /= numat_abs ) cycle
          multi_run = multi_run + 1
        end do 
        n_multi_run = multi_run
      else
        n_multi_run = n_multi_run_e 
      endif
 
      allocate( chargat(0:n_atom_proto) )
      allocate( ngreqm(n_multi_run) )
      allocate( popatm(0:n_atom_proto,nlatm,nspin) )
      allocate( Run_done(n_multi_run) )
      Run_done(:) = .false.
      allocate( Skip_run(n_multi_run) )
      Skip_run(:) = .false.

      if( absauto ) then
        deallocate( iabsm )
        deallocate( iabsorig )
        deallocate( nsymextract )
        deallocate( nposextract )
        allocate( iabsm(n_multi_run) )
        allocate( iabsorig(n_multi_run) )
        allocate( nsymextract(n_multi_run) )
        allocate( nposextract(n_multi_run) )
	  nsymextract(:) = 1
        do multi_run = 1,n_multi_run
          nposextract(multi_run) = multi_run
        end do
        multi_run = 0
        do ipr = 1,n_atom_proto
          it = abs( itype(igreq(ipr,1)) ) 
          if( numat( it ) /= numat_abs ) cycle
          multi_run = multi_run + 1
          iabsm(multi_run) = igreq(ipr,1) 
          iabsorig(multi_run) = ipr 
        end do 
      endif

      call pop_proto(chargat,chargm,Flapw,iabsm,icheck(4),igreq,
     &      itype,mpirank,n_atom_proto,n_multi_run,ngreq,ngreqm,
     &      ngroup,nlat,nlatm,nspin,ntype,numat,popatc,popatm,
     &      popats,Run_done)

      if( nnotskip > 0 .and. Extract .and.
     &                 n_atom_proto_p == n_atom_proto ) then
        do multi_run = 1,n_multi_run
          Skip_run(multi_run) = .true.
          do i = 1,nnotskip
            if( ifile_notskip(i) == iabsorig(multi_run) ) 
     &                      Skip_run(multi_run) = .false.
          end do
        end do
      endif

      n_atom_proto_p = n_atom_proto

      if( Extract ) then
        Green_s = Extract_Green(nom_fich_extract) 
      else
        if( Recup_tddft_data )
     &    call Recup_nenerg(Energ_max,mpirank,nenerg_s,
     &                      nomfich_tddft_data)
        allocate( Energ_s(nenerg_s) )
        allocate( Eimag_s(nenerg_s) )
        if( Recup_tddft_data ) then
          Energ_s(nenerg_s) = Energ_max
        else
          call grille_xanes(eeient,eimag_s,eimagent,egamme,Energ_s,
     &                      icheck(4),lin_gam,ngamme,neimagent,nenerg_s)
        endif
      endif

      deallocate( egamme )
      deallocate( eeient )
      deallocate( eimagent )

      if( mpirank == 0 ) then
        call CPU_TIME(time)
        tp2 = real(time,db)
        tpt(1) = tp2 - tp1
      endif

      allocate( Decal_initl(ninitlu) )
      allocate( nomfich_cal_conv(n_multi_run) )
      if( Tddft ) allocate( nomfich_cal_tddft_conv(n_multi_run) )

! Boucle sur tous les absorbeurs nonequivalents selon le groupe d'espace 
      if( mpirank == 0 ) write(6,130) n_multi_run

      do multi_run = 1,n_multi_run

        if( mpirank == 0 ) then
          tpt(2:9) = 0._db
          tpt1 = 0._db
          tpt2 = 0._db
          call CPU_TIME(time)
          tp(1) = real(time,db)
          tp_init = tp(1)
        endif

        Second_run = multi_run > 1 .and. One_run
        if( Second_run ) Noncentre = .true.

        multi_imp = nposextract(multi_run)

! ninitlr devient 1 pour le final Tddft
        ninitlr = ninitlu

        if( multi_run > 1 )  deallocate( Epsii )
        allocate( Epsii(ninitlu) )

        if( Run_done(multi_run) .or. Skip_run(multi_run) ) cycle

        if( Extract ) then
          if( Tddft ) then
            nbbseuil = 1
          else
            nbbseuil = nbseuil
          endif
        endif 

        if( Spinorbite ) then
          Tau_nondiag = .true.
        else
          Tau_nondiag = .false.
        endif
        iabsorbeur = iabsm(multi_run)
        itabs_nonexc = abs(itype(iabsorbeur)) 
        numat_abs = numat( itabs_nonexc )

        nomfich_s = nomfich
        Final_tddft = .false.
        m = min( multi_run, n_multi_run_e )

        allocate( lqnexc(nnlm) ); allocate( nqnexc(nnlm) ) 

        call init_run(com,ecrantage,
     &    Force_ecr,Hubb,iabsm(multi_run),iabsorig(multi_run),icheck(4),
     &    icom,
     &    itabs,itype,jseuil,lcoeur,lecrantage,lqnexc,lseuil,lvval,
     &    mpinodee,mpirank,n_multi_run,n_orbexc,nbseuil,ncoeur,
     &    necrantage,ngroup,nlat,nlatm,nnlm,
     &    nomfich_s,nompsii,Nonexc,nqnexc,
     &    nrato,nrato_dirac,nrm,nseuil,nspin,ntype,numat,nvval,
     &    pop_open_val,popatc,popats,popatv,popexc,popval,
     &    psi_coeur,psii,psi_open_val,psival,rato,rchimp,
     &    Relativiste,rho_coeur,rhoit,rmt,rmtimp,V_hubbard)
    
        boucle_1: do iprabs = 1,n_atom_proto
          do i = 1,ngreq(iprabs)
            if( abs(igreq(iprabs,i)) == iabsorbeur ) exit boucle_1
          end do
        end do boucle_1

        if( Nonexc ) then
          iprabs_reel = iprabs
        else
          iprabs_reel = 0
        endif 

        natomsym = ngreq(iprabs)
        allocate( poseq(3,natomsym) )
        allocate( isymeq(natomsym) )
        allocate( Taux_eq(natomsym) )

        poseq(:,1:natomsym) = posq(:,iprabs,1:natomsym)
        isymeq(1:natomsym) = isymqa(iprabs,1:natomsym)
        Taux_eq(1:natomsym) = Taux_oc( abs( igreq(iprabs,1:natomsym) ) )

        if( Extract ) then
          E_cut = extract_E_cut(multi_imp,nom_fich_extract)
          v0bdcF = extract_v0bdcF()
          v0muf = workF + v0bdcF
          call extract_Epsii(Core_resolved,Delta_Epsii,Delta_Eseuil,
     &               Epsii,Epsii_moy,icheck(14),nbseuil,
     &               ninit1,ninitl,ninitlu)
          nenerg_s = extract_nenerg(multi_imp,nom_fich_extract,Tddft)
          allocate( Energ_s(nenerg_s) )
          allocate( eimag_s(nenerg_s) )
          call extract_energ(Energ_s,Eseuil,multi_imp,nbbseuil,nbseuil,
     &                       nenerg_s,nom_fich_extract,Tddft)
        endif

        allocate( iapot( 0:n_atom_proto ) )
        allocate( itypepr( 0:n_atom_proto ) )

        call Pop_mod(chargat,chargm,Flapw,iabsorbeur,icheck(4),igreq,
     &      isymqa,itabs,iprabs,itype,itypepr,lqnexc,lvval,n_atom_proto,
     &      n_orbexc,ngreq,ngroup,nlat,nlatm,nnlm,nqnexc,nspin,ntype,
     &      numat_abs,nvval,popatm,popexc)

        deallocate( lqnexc ); deallocate( nqnexc ) 

        d_ecrant = 1 - sum( ecrantage(:) )         
        call natomp_cal(angxyz,axyz,chargat,d_ecrant,Flapw,
     &         iabsorbeur,icheck(5),igreq,itabs,itype,Matper,mpirank,
     &         multrmax,
     &         n_atom_proto,natomeq_s,natomeq_coh,natomp,ngreq,ngroup,
     &         Noncentre,posn,Proto_all,r_self,rsorte_s,rmax,rpotmax,
     &         Self_cons,Test_dist_min)

        allocate( Axe_atom_clu(3,natomp) )
        allocate( iaproto(natomp) )
        allocate( igroup(natomp) )
        allocate( itypep(natomp) )
        allocate( dista(natomp) )
        allocate( pos(3,natomp) )
      
! Preambule coherence: 
        if( Self_cons .and. .not. Second_run ) then
          Convergence = .false.
        else
          Convergence = .true. 
        end if 

! On augmente lla_state afin de calculer les electrons des harmoniques
! superieures
        if( .not. Extract ) then
          lla_state = 4               
          lla2_state = ( lla_state + 1 )**2
        end if
        if( Hubbard ) then
          m_hubb = 0
          do it = 0, ntype
            lh = l_hubbard(numat(it))
            if( Hubb(it) )  m_hubb = max( m_hubb, lh)
          end do
        else
          m_hubb = 0
        endif

! Boucle coherence
        boucle_coh: do i_self = 1,nself+1

          if( Convergence .and. i_self < nself + 1 ) cycle

          if( i_self == nself + 1 ) then
            Cal_xanes = .true.
          else
            Cal_xanes = Convergence
            Fermi = .false.
            E_Fermi = -5._db / Rydb
            E_cut = -5._db / Rydb
          end if   

          Level_val_abs = .false.
          Level_val_exc = .false.

! Par defaut, le calcul auto-coherent se fait en diffusion multiple
! Un calcul non auto-coherent est mene en mode donne en entree        
          if( Cal_xanes ) then
            natomeq = natomeq_s
            Green = Green_s
            rsorte = rsorte_s
           else 
            natomeq = natomeq_coh 
            Green = Green_self
            rsorte = r_self
          endif
          if( Cal_xanes .and. Tddft ) then
            Eneg = .false.
          elseif( Green ) then
            Eneg = .not. Eneg_n_i
          else
            Eneg = Eneg_i
          endif 

          if( icheck(4) > 0 ) then 
            if( Cal_xanes ) then
              write(3,140) 
            else
              write(3,150) i_self
            endif
          endif

          if( Cal_xanes .and. icheck(27) > 0 ) write(3,160) E_cut * Rydb

          if( i_self > 1 .and. Cal_xanes .and. .not. Second_run )
     &                                            deallocate( karact )
          if( i_self == 1 .or. Cal_xanes ) then
            allocate( karact(nopsm,nrepm) )
            if( Cal_xanes ) then
              Nonexc_g = Nonexc
              l_val_max = l_selec_max
            else
              Nonexc_g = Self_nonexc
              l_val_max = 4
            endif
            if( Nonexc_g ) then
              ipr1 = 1
              itab = itabs_nonexc
            else
              ipr1 = 0
              itab = itabs
            endif
            if( Cal_xanes ) then
              ich = icheck(5)
            elseif( icheck(27) > 1 ) then
              ich = max( icheck(5), icheck(27) )
            else
              ich = 0
            endif
              
            call agregat(angxyz,Atom_mag_gr,Atom_nsph,Axe_atom_clu,
     &      Axe_atom_gr,axyz,chargat,chargm,Cubmat,dista,Flapw,Green_s,
     &      Hubbard,iaabs,iabsorbeur,iaproto,iapot,ich,igreq,igroup,
     &      igrpt_nomag,itab,itype,itypep,karact,l_val_max,
     &      Magnetic,Matper,mpirank,n_atom_proto,
     &      natomp,nb_rep,nb_sym_op,ngreq,ngroup,nlat,nlatm,Noncentre,
     &      Nonexc_g,nspin,ntype,numat,PointGroup,popats,pos,posn,rmax,
     &      Self_nonexc,Spinorbite,Rot_Atom_gr,Sym_4,Struct,Sym_cubic)

          endif
                    
          if( Cal_xanes ) then  

! Evaluation de la forme des tenseurs cartesiens
            call Tensor_shape(Atom_mag_gr,Atom_nsph,Axe_atom_clu,
     &             Dipmag,E1E1,E1E2,E1E2e,E1E3,Green,iaabs,
     &             icheck(6),igroup,itype,itypep,lseuil,Magnetic,natomp,
     &             ngroup,nlat,nlatm,nspin,ntype,numat,Octupole,popats,
     &             pos,Quadrupole,rot_atom_abs,Spinorbite,State_all)

            allocate( poldafse(3,npldafs,nphim) )
            allocate( poldafss(3,npldafs,nphim) )
            allocate( phdafs(natomsym,npldafs) )
            allocate( phdf0t(npldafs,nphim) )
            allocate( phdt(npldafs,nphim) )
            allocate( vecdafse(3,npldafs,nphim) )
            allocate( vecdafss(3,npldafs,nphim) )
        
            ngrm = 0
            do ipr = 1,n_atom_proto
              ngrm = max( ngrm, ngreq(ipr) )
            end do

            allocate( ltypcal(nplrm) )
            allocate( nomabs(ncolm) )
            allocate( pol(3,nplrm) )
            allocate( vec(3,nplrm) )
            allocate( pdp(nplrm,2) )

! Evaluation des polarisations et vecteurs d'onde.
            call polond(Dipmag,Green_plus,icheck(6),Length_word,ltypcal,
     &        Moyenne,mpirank,ncolm,ncolr,nomabs,nple,
     &        nplr,nplrm,nxanout,Octupole,pdp,pdpolar,pol,polar,
     &        Quadrupole,veconde,vec,Xan_atom)

            ncolt = ncolr

            allocate( Taux_ipr(n_atom_proto) ) 
            do ipr = 1,n_atom_proto
              Taux_ipr(ipr)
     &             = sum( Taux_oc(abs( igreq(ipr,1:ngreq(ipr)) )) )
     &             / ngreq(ipr) 
            end do
            fpp_avantseuil = fpp_cal(Eseuil,Full_self_abs,icheck(6),
     &         itypepr,n_atom_proto,nbseuil,ngreq,ntype,numat,
     &         Self_abs,Taux_ipr,volume_maille)
            deallocate( Taux_ipr )

            if( Dafs ) then
              call prepdafs(angpoldafs,angxyz,Axe_atom_gr,axyz,
     &          Base_spin,Eseuil,hkl_dafs,icheck(6),igreq,iprabs,isigpi,
     &          itabs,itype,itypepr,lvval,Magnetic,mpirank,n_atom_proto,
     &          natomsym,nbseuil,
     &          ngreq,ngrm,ngroup,nlat,nlatm,nphi_dafs,nphim,npldafs,
     &          nrato,nrm,nspin,ntype,numat,phdafs,phdf0t,
     &          phdt,poldafse,poldafsem,poldafss,poldafssm,
     &          popatm,posn,psival,rato,Taux_oc,temp,
     &          vecdafse,vecdafsem,vecdafss,vecdafsm)
        
              call col_dafs_name(angpoldafs,Full_self_abs,hkl_dafs,
     &         isigpi,Length_word,mpirank,ncolm,ncolr,ncolt,nomabs,
     &         npldafs,Self_abs)
            endif

            if( mpirank == 0 ) allocate(Int_tens(n_tens_max,0:natomsym)) 
                    
            if( Extract ) goto 1030 ! --------> extraction

          endif

! Calcul du nombre d'atomes du petit agregat symmetrise
          natome = natome_cal(igrpt_nomag,mpirank,natomeq,natomp,pos)

! Le nombre d'atomes decrivant le potentiel est defini soit par les 
! atomes prototypiques, soit par les atomes a l'interieur du petit
! agregat 
          if( i_self == 1 .or. Second_run ) Full_atom = ( Full_atom_e
     &     .or. ( Self_cons .and. .not. (Self_nonexc .and. Proto_all) )
     &     .or. ( natome <= n_atom_proto + 1 )
     &     .or. Hubbard )
     &     .and. .not. Flapw

          if( Full_atom ) then
            n_atom_0 = 1
            n_atom_ind = natome
          else
            if( Nonexc ) then
              n_atom_0 = 1
            else
              n_atom_0 = 0
            endif
            n_atom_ind = n_atom_proto
          endif
 
          if( mpirank == 0 ) then
            allocate( Int_statedens(lla2_state,nspin,
     &                                         n_atom_0:n_atom_ind) ) 
            Int_statedens(:,:,:) = 0._db
          endif  

          if( Self_cons .and. .not. Second_run ) then
            nrm_self = nrm
          else
            nrm_self = 0
          endif

          if( i_self == 1 .or. Second_run ) then
            if( Self_nonexc .or. Full_atom ) then
              n_atom_0_self = 1
            else
              n_atom_0_self = 0
            endif
            n_atom_ind_self = n_atom_ind
            natome_self = natome
            natomeq_self = natomeq
            allocate( chargat_init(n_atom_0_self:n_atom_ind_self,nspin))
            allocate( chargat_self(n_atom_0_self:n_atom_ind_self,nspin))
            allocate( chargat_self_s(n_atom_0_self:n_atom_ind_self,
     &                               nspin) )
            allocate( drho_ex_nex(0:nrm,nspin) )
            allocate( dvc_ex_nex(0:nrm) )
            allocate( dv_ex_nex(0:nrm) )
            allocate( Energ_self(n_atom_0_self:n_atom_ind_self) )
            allocate( Energ_self_s(n_atom_0_self:n_atom_ind_self) )
            allocate( En_coeur(n_atom_0_self:n_atom_ind_self) )
            allocate( En_coeur_s(n_atom_0_self:n_atom_ind_self) )
            allocate( pop_orb_val(n_atom_0_self:n_atom_ind_self,nspin) )
            allocate( rho_chg(0:nrm_self,nspin,
     &                                  n_atom_0_self:n_atom_ind_self) )
            allocate( rho_self(0:nrm_self,nspin,
     &                                  n_atom_0_self:n_atom_ind_self) )
            allocate( rho_self_t(0:nrm_self,
     &                                  n_atom_0_self:n_atom_ind_self) )
            allocate( rho_self_s(0:nrm_self,nspin,
     &                                  n_atom_0_self:n_atom_ind_self) )
            allocate( rhoato_init(0:nrm_self,nspin,
     &                                  n_atom_0_self:n_atom_ind_self) )
            allocate( vcato_init(0:nrm_self,
     &                                  n_atom_0_self:n_atom_ind_self) )
            allocate( occ_hubb(-m_hubb:m_hubb,-m_hubb:m_hubb,nspin,
     &                                  n_atom_0_self:n_atom_ind_self) ) 
            allocate( occ_diag(-m_hubb:m_hubb,
     &                            nspin,n_atom_0_self:n_atom_ind_self) ) 
            allocate( occ_diag_s(-m_hubb:m_hubb,
     &                            nspin,n_atom_0_self:n_atom_ind_self) ) 
            if(.not. Atom_occ_mat .and. Hubbard ) then
              population = 1._db / nspin 
              do m = -m_hubb, m_hubb 
                occ_diag(m,:,:) = population
              end do
            endif
            allocate( rot_hubb(-m_hubb:m_hubb,-m_hubb:m_hubb,nspin,
     &                                  n_atom_0_self:n_atom_ind_self) ) 
            allocate( rot_hubb_abs(-m_hubb:m_hubb,-m_hubb:m_hubb,nspin)) 
          endif

          if( Hubbard .and. .not. Cal_xanes )
     &                      occ_hubb(:,:,:,:) = (0._db, 0._db)

          allocate( dv0bdcF(nspin) )

          if( i_self > 1 .and. Cal_xanes .and. .not. Second_run ) then
            deallocate( Atom_axe )
            deallocate( Atom_comp )
            deallocate( Axe_Atom_Clui )
            deallocate( distai )
            deallocate( ia_eq )
            deallocate( ia_eq_inv )
            deallocate( ia_rep )
            deallocate( iaprotoi )
            deallocate( igroupi )
            deallocate( iopsym_atom )
            deallocate( is_eq )
            deallocate( itypei )
            deallocate( nb_eq )
            deallocate( nb_rpr )
            deallocate( nb_rep_t )
            posi_self(:,:) = posi(:,:)
            deallocate( posi )
            deallocate( rot_atom )
          endif 

          if( i_self == 1 .or. Second_run ) 
     &                             allocate( posi_self(3,natome) )

          if( i_self == 1 .or. Cal_xanes ) then

! L'indice 0, correspond a l'agregat en entier
            allocate( Atom_axe(natome) )
            allocate( Atom_comp(0:natome) )
            allocate( Axe_Atom_Clui(3,natome) )
            allocate( distai(natome) )
            allocate( ia_eq(nb_sym_op,natome) )
            allocate( ia_eq_inv(natomeq) )
            allocate( ia_rep(nb_sym_op,natome,natome) )
            allocate( iaprotoi(natome) )
            allocate( igroupi(natome) )
            allocate( iopsym_atom(nopsm,natome) )
            allocate( is_eq(nb_sym_op,natome) )
            allocate( itypei(natome) )
            allocate( nb_eq(natome) )
            allocate( nb_rpr(natome,natome) )
            allocate( nb_rep_t(nb_sym_op,natome,natome) )
            allocate( posi(3,natome) )
            allocate( rot_atom(3,3,natome) )
            Energ_self_s(:) = 0._db

            if( Cal_xanes ) then
              ich = icheck(7)
            elseif( icheck(27) > 1 ) then
              ich = max( icheck(7), icheck(27) )
            else
              ich = 0
            endif
              
            call Atom_selec(Atom_axe,Atom_comp,Atom_mag_gr,Atom_nsph,
     &         Atom_occ_mat,Axe_atom_clu,Axe_Atom_clui,
     &         dista,distai,Full_atom,Green,Hubbard,ia_eq,ia_eq_inv,
     &         ia_rep,iaabs,iaabsi,iaproto,iaprotoi,ich,igreq,
     &         igroup,igroupi,igrpt_nomag,iopsym_atom,iord,is_eq,
     &         itype,itypei,itypep,itypepr,Magnetic,m_hubb,
     &         mpirank,natome,n_atom_0_self,
     &         n_atom_ind_self,n_atom_proto,natomeq,natomp,
     &         nb_eq,nb_rpr,nb_rep_t,nb_sym_op,ngroup,nlat,nlatm,
     &         nspin,ntype,numat,nx,occ_diag,occ_mat_gr,
     &         Overad,popats,pos,posi,rmt,rot_atom,roverad,
     &         rsort,rsorte,Spinorbite,Ylm_complex)

          end if

          if( i_self == 1 .or. Second_run ) then
            allocate( ia_eq_inv_self(natomeq_self) )
            ia_eq_inv_self(:) = ia_eq_inv(:)
          endif 

          Tddft_xanes = Tddft .and. Cal_xanes
  
! Calcul des representations utiles
          Recop = .false.
          if( Cal_xanes ) then
            call etafin(Atom_comp(iaabsi),icheck(8),irep_util,jseuil,
     &            karact,lmoins1,lplus1,lseuil,mpirank,nb_rep,nbseuil,
     &            ngrph,nspino,Spinorbite,State_all,Sym_cubic)
            if( ( Sym_cubic .or. Sym_4 )  .and. .not. State_all )
     &                                                   Recop = .true.           
          elseif( i_self == 1 ) then
! Pour le calcul auto-coherent, il faut calculer toutes les representations
            if( icheck(27) > 1 ) then
              ich = max( icheck(8), icheck(27) )
            else
              ich = 0
            endif
            call irep_util_all(ich,irep_util,karact,nb_rep,ngrph,nspino,
     &                         Spinorbite)
          endif
          if( i_self > 1 .and. Cal_xanes .and. .not. Second_run ) then
             deallocate( Repres_comp )
          end if
          if( i_self == 1 .or. Cal_xanes ) then
             allocate( Repres_comp(ngrph) )
             call irep_comp(Atom_comp(0),Green,iopsymr,
     &              irep_util,karact,ngrph,nspino,Repres_comp)
          end if
          if( ( i_self == 1 .or. Cal_xanes )
     &                         .and. .not. second_run ) then

            nbm = 0;     nbtm = 0
            npoint = 0;  npr = 0
            nsort = 0
            nsm = 0;     nstm = 0

! Calcul des dimensions de tableaux pour le maillage

            call nbpoint(Moy_loc,iaabs,igrpt_nomag,iord,mpirank,
     &                   natomp,npoint,npso,nx,pos,rsort )   
        
            nim = npoint
            npsom = npso

            if( Base_hexa ) then
              nvois = 4 * iord
            else
              nvois = 3 * iord
            endif

            if( Cal_xanes .and. i_self > 1 ) then
              deallocate( clapl )
              deallocate( cgrad )
              deallocate( ivois )
              deallocate( isvois )
              deallocate( numia )
              deallocate( rvol )
              deallocate( xyz )
            endif
            allocate( clapl(0:nvois) )
            allocate( cgrad(nvois) )
            allocate( ivois(npsom,nvois) )
            allocate( isvois(npsom,nvois) )
            allocate( numia(npsom) )
            allocate( rvol(nim) )
            allocate( xyz(4,npsom) )

! Elaboration du maillage

            allocate( indice(npsom,3) )
            allocate( mpres(-nx:nx,-nx:nx,-nx:nx) )

! Meme en Green, on definit des points afin de calculer le potentiel moyen
            call reseau(Moy_loc,iaabs,icheck(9),igrpt_nomag,indice,iord,
     &               itypei,mpirank,mpres,natome,natomp,nim,npoint,
     &               npr,npso,npsom,ntype,numia,nx,pos,
     &               posi,rmt,rsort,rvol,xyz)

            if( .not. Green ) then
              call laplac(cgrad,clapl,icheck(9),igrpt_nomag,indice,iord,
     &                  ivois,isvois,mpirank,mpres,npso,npsom,nvois,nx)
              call bordure(Green,icheck(9),iord,iscratch,ivois,
     &                     mpirank,natome,nbm,nbtm,nim,
     &                     npoint,npso,npsom,nsm,nstm,numia,
     &                     nvois,posi,rvol,xyz)
            endif
            deallocate( indice )
            deallocate( mpres )

          endif

! On calcule les rayons seulement aux premiere et derniere iterations 

          if( i_self == 1 .or. Second_run )
     &                              allocate( V_abs_i(0:nrm,nspin) )

          if( .not. Second_run ) then
            if( i_self == 1 ) then
              allocate( lmaxat(0:n_atom_proto) )
              allocate( nrmtg(0:n_atom_proto) ) 
              allocate( rmtg(0:n_atom_proto) ) 
              allocate( rmtg0(0:n_atom_proto) )
              allocate( rmtsd(0:n_atom_proto) )
            end if      
            allocate( Ef(npoint) )
            allocate( Efato(0:nrm,n_atom_0:n_atom_ind) )
            allocate( rs(npoint) )
            allocate( rsato(0:nrm,n_atom_0:n_atom_ind) )
            allocate( Vcato(0:nrm,n_atom_0:n_atom_ind) )
            allocate( Vh(npoint) )
            allocate( Vhns(npoint) )
            allocate( Vxc(npoint,nspin) )
            allocate( Vxcato(0:nrm,nspin,n_atom_0:n_atom_ind) )
          endif
           
          allocate( excato(nrm,n_atom_0:n_atom_ind) )
          allocate( ibord(nbtm,natome) )
          allocate( isbord(nbtm,natome) )
          allocate( imoy(npoint) );  allocate( imoy_out(npoint) )
          allocate( isrt(nstm) )
          allocate( nbord(natome) )
          allocate( nbordf(natome) )
          allocate( poidsa(nbm,natome) )
          allocate( poidso(nsm) )
          allocate( poidsov(npoint) ); allocate( poidsov_out(npoint) )
          allocate( rho(npoint,nspin) )
          allocate( rhons(npoint) )
          allocate( Vr(npoint,nspin) )
          allocate( Vrato(0:nrm,nspin,n_atom_0:n_atom_ind) )
          allocate( rhoato_abs(0:nrm,nspin) )

          if( .not. Green ) call recup_bordure(ibord,isbord,iscratch,
     &          isrt,mpinodes,mpirank,natome,nbord,nbordf,nbm,nbtm,nsm,
     &          nsort,nsortf,nstm,poidsa,poidso)

          if( mpirank == 0 ) then
            call CPU_TIME(time)
            tp(2) = real(time,db)
          endif

          if( .not. Second_run ) then

! Calcul du potentiel
            if( Flapw ) then

              call potlapw(axyz,chargat,Coupelapw,Efato,Flapw_new,
     &        Full_atom,iapot,iaproto,iaprotoi,icheck(13),igroup,iprabs,
     &        ipr1,itabs,
     &        its_lapw,itypei,itypep,itypepr,Magnetic,mpinodes,
     &        mpirank,n_atom_0,n_atom_ind,n_atom_proto,natome,natomeq,
     &        natomp,ngreq,ngroup,ngroup_lapw,
     &        nklapw,nlmlapwm,nmatsym,nomclm,nomr2v,nomr2vdn,nomvcoul,
     &        npoint,npsom,nrato,nrato_lapw,nrm,nrmtg,
     &        nspin,ntype,numat,overlap,pos,rato,rchimp,rho,
     &        rlapw,rmtg,rmtg0,rmtimp,rmtsd,
     &        rotloc_lapw,rs,rsato,rsort,Trace_format_wien,V_abs_i,
     &        V0bdcFimp(1),Vcato,Vh,Vxc,Vxcato,xyz)     

            else

              call potsup(alfpot,Axe_atom_clu,Cal_xanes,cdil,
     &        chargat,chargat_init,chargat_self,drho_ex_nex,dv_ex_nex,
     &        dvc_ex_nex,Efato,excato,Full_atom,hybrid,i_self,
     &        ia_eq_inv,ia_eq_inv_self,iaabs,iaproto,iaprotoi,iapot,
     &        icheck,igreq,igroup,iprabs,iprabs_reel,ipr1,itab,itdil,
     &        itypei,itypep,itypepr,ldil,lvval,Magnetic,mpirank,
     &        n_atom_0,n_atom_0_self,n_atom_ind,n_atom_ind_self,
     &        n_atom_proto,natome,natome_self,natomeq,
     &        natomeq_self,natomp,ngreq,ngroup,
     &        ngroup_nonsph,nhybm,nlat,nlatm,Nonexc,norbdil,norbv,
     &        npoint,npsom,nrato,nrm,nrm_self,nrmtg,nspin,ntype,
     &        numat,overlap,pop_nonsph,popatm,popatv,pos,posi,posi_self,
     &        psival,r_self,rato,rchimp,rho,rho_chg,rho_self,
     &        rhoato_abs,rhoato_init,
     &        rhoit,rhons,rmtg,rmtimp,rmtg0,rmtsd,Rot_Atom_gr,rs,
     &        RPALF,rsato,rsort,Self_nonexc,Tddft_xanes,V_abs_i,
     &        Vcato,Vcato_init,Vh,Vhns,Vxc,Vxcato,v0bdcFimp(1),
     &        xyz)

! Initialisation de rho_self, une fois avoir calcule le nouveau
! potentiel en potsup. Au dela du rayon rmtsd on initialise a zero.
              if( .not. Cal_xanes ) then

                if( i_self == 1 ) then
                  allocate( ch_coeur(n_atom_0_self:n_atom_ind_self) )  
                  allocate( chg_cluster(nspin) )  
                  allocate( ispin_maj(n_atom_0_self:n_atom_ind_self) )  
                  rho_self_s(:,:,:) = rhoato_init(:,:,:) 

! Calcul de l'energie de depart du comptage d'electrons; on la calcule
! une seule fois, bien que les Vcato et Vxcato changent a chaque
! iteration
                  allocate( Sgn(n_atom_0_self:n_atom_ind_self) )

                  call En_dep(E_start,Full_atom,Green,iaprotoi,
     &               icheck(27),itypepr,lcoeur,n_atom_0,n_atom_0_self,
     &               n_atom_ind,n_atom_ind_self,n_atom_proto,natome,
     &               ncoeur,nenerg_coh,nrato,nrm,numat,nspin,ntype,
     &               Pas_SCF,psi_coeur,Relativiste,rato,Sgn,
     &               Vcato,Vxcato,workf)  
        
! Calcul de la charge de reference en cas de calcul auto_coherent
                  call chg_agr(chargat,chargat_init,ch_coeur,
     &               chg_cluster,chg_open_val,Full_atom,iaprotoi,iprabs,
     &               ispin_maj,itabs,icheck(27),itypepr,lcoeur,mpirank,
     &               natome,n_atom_0_self,n_atom_ind_self,
     &               n_atom_proto,nb_eq,ncoeur,ngreq,nrato,nrm,
     &               nrm_self,nspin,ntype,numat,pop_open_val,psi_coeur,
     &               psi_open_val,rato,rho_chg,rho_coeur,rho_cor,
     &               rhoato_init,rmtsd,SCF_mag_fix,SCF_mag_free,Sgn)

                  chargat_self_s(:,:) = chargat_init(:,:)
                  deallocate( Sgn )
! Mise en place de la grille en energie pour l'autocoherence    
                  allocate( energ_coh(nenerg_coh) )
                  allocate( eimag_coh(nenerg_coh) )
                  call grille_coh(eimag_coh,energ_coh,E_start,Green,
     &                          icheck(27),nenerg_coh,Pas_SCF)

                endif

                do iapr = n_atom_0_self,n_atom_ind_self
                  if( Full_atom ) then
                    ipr = iaprotoi(iapr)
                  else
                    ipr = iapr
                  endif
                  it = itypepr(ipr)
                  nr = nrato(it)
                  do n = 1,nr
                    if( rato(n,it) > rmtsd(ipr) ) exit
                  end do
                  do ispin = 1,nspin
                    do ir = 0,n             
                    rho_self(ir,ispin,iapr) = rho_chg(ir,ispin,iapr) 
     &                  + ( rho_coeur(ir,it) / nspin ) - rho_cor(ir,it)       
                    end do
                    rho_self(n+1:nr,:,iapr) = rhoato_init(n+1:nr,:,iapr)
                  end do
                end do
           
              endif

            endif
          endif

! On stocke le potentiel non excite de l'absorbeur (sert au calcul de
! l'energie du niveau initial )
          if( Second_run ) then
            if( Full_atom ) then
              iaprabs = iaabsi 
            else
              iaprabs = iprabs
            endif
            do ispin = 1,nspin
              V_abs_i(:,ispin) = Vcato(:,iaprabs)
     &                         + Vxcato(:,ispin,iaprabs)
            end do
          endif

          if( ipr1 == 1 ) rmtg(0) = rmtg(iprabs)

          if( Cal_xanes ) then
            nenerg = nenerg_s 
            allocate( Energ(nenerg) )
            allocate( Eimag(nenerg) )
            Energ(:) = Energ_s(:)   
            Eimag(:) = eimag_s(:)  
          else
            nenerg = nenerg_coh 
            allocate( Energ(nenerg) )
            allocate( Eimag(nenerg) )
            Energ(:) = energ_coh(:) 
            Eimag(:) = eimag_coh(:)
          end if   

! Operations complementaires sur le potentiel
          call potential_comp(Cal_xanes,distai(natome),dv0bdcF,
     &        ecineticmax,ecineticmax_out,
     &        Eclie,Ef,Eneg,Energ(nenerg),Etatlie,Green,iaabs,iaproto,
     &        icheck(13),imoy,imoy_out,isrt,korigimp,Magnetic,Moy_loc,
     &        mpirank,n_atom_proto,natomp,nim,
     &        npoint,npsom,nptmoy,nptmoy_out,nsortf,nspin,nstm,
     &        poidsov,poidsov_out,pos,rmtg0,rs,rsort,rvol,
     &        V0bdcFimp,V0muf,Vh,vr,vxc,xyz,workf)

! Calcul de l'energie du niveau de coeur initial.
          if( Cal_xanes ) then
! indice du type avant excitation  (itabs est le type excite)
            itexc = abs( itype(iabsorbeur) )
            V0bdF = V0muf - Workf
            call enrgseuil(Core_resolved,Delta_Epsii,Delta_Eseuil,Epsii,
     &         Epsii_moy,Eseuil,Flapw,icheck(14),is_g,itabs,itexc,
     &         lseuil,m_g,mpirank,nbseuil,ninit1,ninitl,ninitlu,nrato,
     &         nrm,nseuil,nspin,ntype,numat_abs,psii,rato,V_abs_i,V0bdF)

            Decal_initl(:) = Epsii(:) - Epsii_moy

          endif

! Determination du lmax
          if( .not. Green ) then
            call clmax(ecineticmax_out,rsort,lmaxso0,lmaxso_max,2,
     &                 .true.)
          else
            lmaxso_max = 0
          endif
          lmaxmax = 0
          if( Octupole ) then
            lmax_probe = lseuil + 3
          elseif( Quadrupole ) then
            lmax_probe = lseuil + 2
          else
            lmax_probe = lseuil + 1
          endif
          nlm_probe = (lmax_probe + 1)**2

          lmaxabs = 0
          do ipr = 0,n_atom_proto
            Z = numat( itypepr(ipr) )
            call clmax(ecineticmax,rmtg(ipr),lmaxat0,lmaxat(ipr),Z,
     &               lmaxfree)
            if( Z == numat_abs ) then
              lmaxat(ipr) = max( lmax_probe, lmaxat(ipr) )
              lmaxabs = max( lmaxat(ipr), lmaxabs )
            endif  
            lmaxmax = max(lmaxmax,lmaxat(ipr))
          end do
          do ipr = 0,n_atom_proto
            Z = numat( itypepr(ipr) )
            if( Z == numat_abs ) lmaxat(ipr) = lmaxabs
          end do

          nlmmax = (lmaxmax + 1 )**2
          nlmamax = (lmaxabs + 1 )**2
          if( Tddft ) then
! nlmamax ne sert que pour la Tddft, on la limite pour espace memoire:
            lmaxabs_t = min( lmaxabs, 4)
            nlmamax = (lmaxabs_t + 1 )**2
            if( iopsymc(25) == 0 .or. Quadrupole .or. Dipmag ) then
              imparite = 2
            elseif( mod(lseuil,2) == 0 ) then
              imparite = 1
            else
              imparite = 0
            endif
            nlmamax_u = 0
            do l = 0,lmaxabs_t
              if( (imparite /= mod(l,2)) .and. (imparite /= 2) ) cycle
              nlmamax_u = nlmamax_u + 2 * l + 1
            end do
          else
            lmaxabs_t = 0
            nlmamax = 0
            nlmamax_u = 0
            imparite = 2
          endif

          nlmsam = nspin * nlmmax
          nlmomax = ( lmaxso_max + 1 )**2
          nso1 = nspino * nlmomax     

          if( mpirank == 0 ) then
            call CPU_TIME(time)
            tp(3) = real(time,db)
          endif
        
          allocate( iato(nlmsam,natome,ngrph) )
          allocate( lato(nlmsam,natome,ngrph) )
          allocate( iso(nso1,ngrph) )
          allocate( lso(nso1,ngrph) )
          allocate( mato(nlmsam,natome,ngrph) )
          allocate( mso(nso1,ngrph) )
          allocate( nlmso0(ngrph) )
          allocate( nlmsa0(natome,ngrph) )

! Determination des (l,m) de chaque representation
          call lmrep(Green,iaprotoi,iato,icheck(15),iopsym_atom,
     &           irep_util,iso,itypei,karact,lato,
     &           lmaxat,lmaxso_max,lso,mato,mpirank,mso,n_atom_proto,
     &           natome,nbordf,ngrph,nlmsa0,nlmsam,nlmso0,
     &           nso1,nsortf,nspino,ntype,numat,posi,rot_atom)

! Calcul des Ylm sur les points du maillage
          if( .not. Green )  then
            allocate( Ylmato(nbtm,nlmmax,natome) )
            allocate( Ylmso(nsort,nlmomax) )
            call ylmpt(iaprotoi,ibord,icheck(15),isrt,lmaxat,lmaxso_max,
     &           n_atom_proto,natome,nbord,nbtm,nlmmax,nlmomax,
     &           nsort,nstm,npsom,posi,rot_atom,rot_atom_abs,xyz,
     &           Ylmato,Ylmso)
          endif

          if( nspin == 1 .or. Spinorbite ) then
            nspinorb = 1
          else
            nspinorb = 2
          endif

          allocate( Ecinetic_edge(nspin) )
          allocate( Ecinetic_out(nspin) )
          allocate( konde(nspin) )
          allocate( lmaxa(natome) )
          allocate( nlmsa(natome) )
          allocate( V0bd_out(nspin) ) 
          if( .not. Green ) then
            if( Spinorbite .or. Relativiste ) then
              nicm = nim
            else
              nicm = 1
            endif
            allocate( gradvr(nicm,3,nspin))
          endif

          allocate( drho_self(0:nrm_self,nspin,
     &                     n_atom_0_self:n_atom_ind_self,0:mpinodes-1) )
          allocate( statedens(lla2_state,nspin,n_atom_0:n_atom_ind,
     &                        0:mpinodes-1) )
          allocate( statedens_hd(-m_hubb:m_hubb,-m_hubb:m_hubb,nspin, 
     &                     n_atom_0_self:n_atom_ind_self,0:mpinodes-1) )

          allocate( sec_atom(ninitlr) )

          if( mpirank == 0 ) then
            call CPU_TIME(time)
            tp(4) = real(time,db)
            do i = 2,4
              tpt(i) = tp(i) - tp(i-1)
            end do
          endif
        
! Initialisation de l'energie de l'agregat
          enragr = 0._db
          Energ_self(:) = 0._db

          if( Tddft_xanes ) then
            allocate( rof0(nenerg,nlmamax,nspin,nspino,nbseuil) )
            rof0(:,:,:,:,:) = (0._db,0._db)

            if( Dipmag ) then
              ns_dipmag = 4
            else
              ns_dipmag = 1
            endif  
            allocate(imag_taull(nenerg,nlmamax_u,nspin,nlmamax_u,nspin))
            imag_taull(:,:,:,:,:) = 0._db

          end if

! Point de retour pour le calcul final en Tddft
 1000     continue
 
          if( Final_tddft ) then
            nr = nrato(itabs)
            Xan_atom = .false.
! Calcul du noyau xc
            allocate( fxc(0:nr,2,2) )
            allocate( rato_abs(0:nr) )
            allocate( rsato_abs(0:nr) )
            if( Full_atom ) then
              iapr0 = iaabsi
            else
              iapr0 = iprabs_reel
            end if
            rato_abs(0:nr) = rato(0:nr,itabs)
            rsato_abs(0:nr) = rsato(0:nr,iapr0)
            if( .not. RPALF ) then
              call fxcorr(alfpot,fxc,icheck(23),Magnetic,nr,nrm,nspin,
     &                                    rato_abs,rhoato_abs,rsato_abs)
            end if
            deallocate( rato_abs )
            deallocate( rsato_abs )

            deallocate( Ecinetic )
            deallocate( Epsii )
            deallocate( secdd, secmd, secmm, secdq, secdo, secqq )
            deallocate( secdd_m, secmd_m, secmm_m, secdq_m, secdo_m,
     &                  secqq_m )
            deallocate( V0bd )    
            deallocate( Vrato )

            n_vr_0 = 1
            n_vr_ind = ninitlu
            ninitlt = ninitlu
            nbbseuil = 1
            ninitlr = 1
            nrm_tddft = nrm

            allocate( Epsii(ninitlr) )
            Epsii(1) = Epsii_moy
            allocate( Vrato(0:nrm,nspin,n_vr_0:n_vr_ind) )

          else

            nbbseuil = nbseuil
            nrm_tddft = 1
            ninitlt = 1
            n_vr_0 = n_atom_0
            n_vr_ind = n_atom_ind

          endif

          allocate( secdd(3,3,ninitlr,0:mpinodes-1) )
          allocate( secmd(3,3,ninitlr,0:mpinodes-1) )
          allocate( secmm(3,3,ninitlr,0:mpinodes-1) )
          allocate( secdq(3,3,3,ninitlr,0:mpinodes-1) )
          allocate( secdo(3,3,3,3,ninitlr,0:mpinodes-1) )
          allocate( secqq(3,3,3,3,ninitlr,0:mpinodes-1) )
          allocate( secdd_m(3,3,ninitlr,0:mpinodes-1) )
          allocate( secmd_m(3,3,ninitlr,0:mpinodes-1) )
          allocate( secmm_m(3,3,ninitlr,0:mpinodes-1) )
          allocate( secdq_m(3,3,3,ninitlr,0:mpinodes-1) )
          allocate( secdo_m(3,3,3,3,ninitlr,0:mpinodes-1) )
          allocate( secqq_m(3,3,3,3,ninitlr,0:mpinodes-1) )

          if( Final_tddft .and. Tddft_so ) then
            nspino_t = 2
            nspin_t = 2
            Spinorbite_t = .true.
          else
            nspin_t = nspin
            nspino_t = nspino
            Spinorbite_t = Spinorbite
          endif

          allocate( V0bd(nspin,ninitlt) )    
          allocate( Ecinetic(nspin,ninitlt) )
          if( Final_tddft .and. Core_resolved ) then
            ninitls = ninitl
          else
            ninitls = nbseuil
          endif

          if( Final_tddft .and. Gamma_tddft ) then
            Green_i = .true.
          else
            Green_i = Green_int
          endif

          Hub_nondiag = Hubbard .and. Tau_nondiag .and. i_self > 1
          Hub_nondiag_abs = .false.

          if( Hub_nondiag .and. .not. Final_tddft )
     &      call sel_rot_hub_abs(Full_atom,Hub_nondiag_abs,Hubb,
     &            iaabsi,iaprotoi,iprabs,itabs,lh_abs,m_hubb,
     &            n_atom_0_self,n_atom_ind_self,natome,nspin,ntype,
     &            numat,rot_hubb,rot_hubb_abs)

! Boucle sur l'energie
          nge = ( nenerg - 1 ) / mpinodes + 1

          if( One_run .and. mpinodes > 1 .and. multi_run == 1 
     &                     .and. Cal_xanes .and. .not. Final_tddft )
     &      allocate( taull_stk(nlm_probe,nspin,nlm_probe,nspin,
     &                          2:n_multi_run,nge) )
          index_e = 0

          boucle_energ: do je = 1,nge

            if( Recup_tddft_data .and. Cal_xanes
     &                           .and. .not. Final_tddft ) exit       

            if( mpirank == 0 ) then
              call CPU_TIME(time)
              tp(1) = real(time,db)
            endif

            ie = ( je - 1 ) * mpinodes + mpirank + 1

            if( ie > nenerg ) goto 1010
            index_e = index_e + 1
            Enervide = Energ(ie) - Workf 
        
! Calcul du potentiel dans l'etat excite
            call potex(axyz,alfpot,Cal_xanes,Decal_initl,dv0bdcF,Ef,
     &        Efato,Energ(ie),Enervide,Final_tddft,Full_atom,iaabsi,
     &        iapot,iaprotoi,icheck(16),imoy,imoy_out,iprabs,
     &        iprabs_reel,ispin,
     &        itab,itypepr,Magnetic,mpinodes,mpirank,n_atom_0,
     &        n_atom_ind,n_atom_proto,n_vr_0,n_vr_ind,natome,ninitlt,
     &        ninitlu,Nonexc_g,npoint,npsom,nptmoy,
     &        nptmoy_out,nrato,nrm,nrmtg,nspin,ntype,poidsov,
     &        poidsov_out,rato,rho,rhons,rmtg,rs,rsato,Vcato,Vh,Vhns,
     &        Vr,Vxc,Vrato,Vxcato,V0bd,V0bd_out,xyz)

            do ispin = 1,nspin

              if( .not. Final_tddft ) v0bd_out(ispin) = V0bd(ispin,1) 

              if( Muffintin .and. .not. Final_tddft ) then
                call mdfmuf(Axe_Atom_Clu,axyz,Full_atom,iaproto,
     &             iaprotoi,icheck(16),ispin,itypep,n_atom_0,n_atom_ind,
     &             n_atom_proto,natome,natomp,npoint,npsom,nrato,nrm,
     &             nspin,ntype,pos,rato,rho,rhons,rmtg,Vhns,
     &             V0bd(ispin,1),Vr,Vrato,xyz)
              endif

              if( Supermuf  .and. .not. Final_tddft )
     &          call modmuf(Full_atom,iaprotoi,itypepr,icheck(16),
     &            ispin,n_atom_0,n_atom_ind,n_atom_proto,natome,
     &            nrato,nrm,nspin,ntype,rato,rmtg,V0bd(ispin,1),Vrato)

              if( .not. ( Green .or. Final_tddft .or. Second_run )
     &           .and. ( Spinorbite .or. Relativiste ) )
     &          call gradpot(icheck(16),npoint,ispin,npsom,nspin,ivois,
     &                 iord,nvois,cgrad,nicm,gradvr,nspin,Vr)

              Ecinetic_out(ispin) = Enervide - V0bd_out(ispin)
              if( Eneg ) then
! Problemes numeriques autour de 0
                em = 0.01 / rydb   
                ei0 = 0.01 / rydb
                if( Ecinetic_out(ispin) < 0._db ) then
                  Eimag(ie) = max( Eimag(ie), ei0 )
                elseif( ecinetic_out(ispin) < em ) then 
                  eii = ei0 * ( 1 - ecinetic_out(ispin) / em ) 
                  Eimag(ie) = max( Eimag(ie), eii )
                endif
              elseif( Etatlie ) then
                Ecinetic_out(ispin) = max( Ecinetic_out(ispin), Eclie )
              endif

              do initl = 1,ninitlt

                Ecinetic(ispin,initl) = Enervide - V0bd(ispin,initl)

                if( Final_tddft ) then
                  Ecinetic(ispin,initl) = Ecinetic(ispin,initl)
     &                                  - Decal_initl(initl)
! L'energie cinetique minimale est prise egale a celle du niveau de
! Fermi. 
                  Ec_min = E_cut_tddft - workf - V0bd(ispin,initl) 
                  Ecinetic(ispin,initl)
     &                   = max( Ecinetic(ispin,initl), Ec_min )
                endif
       
                if( .not. Eneg .and. Etatlie ) Ecinetic(ispin,initl) 
     &                            = max( Ecinetic(ispin,initl), Eclie )

                if( ( Ecinetic(ispin,initl) < eps10 .or.  
     &              Ecinetic_out(ispin) < eps10 ).and. .not. Eneg ) then
                  if( mpirank == 0 ) then
                    call write_error
                    do ipr = 6,9,3
                      write(ipr,170) Ecinetic(ispin,initl) * rydb
                      if( nptmoy_out > 0 )
     &                    write(ipr,180) Ecinetic_out(ispin) * rydb
                    end do
                  endif
                  stop
                endif

               end do

            end do  ! fin boucle sur spin

            if( abs( Eimag(ie) ) > eps10 .or. Ecinetic_out(1) < eps10
     &         .or. Ecinetic_out(nspin) < eps10 ) then
              E_comp = .true.
            else
              E_comp = .false.
            endif

            if( E_comp ) then
              Solsing = .true.
            else
              Solsing = Solsing_s
            endif

            Ecmax = 0._db
            Ecmax_out = 0._db
            do ispin = 1,nspin 
              Ecmax_out = max( Ecmax_out, Ecinetic_out(ispin) )
              do initl = 1,ninitlt
                Ecmax = max( Ecmax, Ecinetic(ispin,initl) )
              end do
            end do    

            lmaxg = 0
            lmaxabsa = 0
            do ipr = 0,n_atom_proto
              Z = numat( itypepr(ipr) )
              call clmax(Ecmax,rmtg(ipr),lmaxat0,lmaxat(ipr),Z,lmaxfree)
! En Tddft sur 2 seuils, le premier seuil a plus haute energie peut
! avoir un lmax plus grand (et inutile).
              lmaxat(ipr) = min(lmaxat(ipr), lmaxmax)
              if( Z == numat_abs ) then 
                 lmaxat(ipr) = max( lmax_probe, lmaxat(ipr) ) 
                 lmaxabsa = max( lmaxabsa, lmaxat(ipr) ) 
              end if
              lmaxg = max( lmaxat(ipr), lmaxg)
            end do 
            do ipr = 0,n_atom_proto
              Z = numat( itypepr(ipr) )
              if( Z == numat_abs ) lmaxat(ipr) = lmaxabsa 
            end do 
            nlmagm = ( lmaxg + 1 )**2
            if( Tddft ) then
              nlmam = ( lmaxabsa + 1 )**2
              nlmam_t = min(nlmamax, nlmam)
              nlmam_u = 0
              lmax_t = min(lmaxabsa,lmaxabs_t)
              do l = 0,lmax_t
                if( (imparite /= mod(l,2)) .and. (imparite /= 2) ) cycle
                nlmam_u = nlmam_u + 2 * l + 1
              end do
            else
              nlmam = nlm_probe
              nlmam_t = nlm_probe
            endif
            if( Dipmag ) then
              ip0 = 0
            else
              ip0 = 1
            endif
            if( Octupole ) then
              ip_max = 3
            else
              ip_max = 2
            endif

            allocate( singul(nlmam_t,nspin,nlmam_t,nspin,ip0:ip_max,
     &                       nbseuil) )
            allocate( tau_ato(nlmagm,nspin,nlmagm,nspin,
     &                               n_atom_0:n_atom_ind) )
            allocate( rof(nlmam_t,nspin_t,nspino_t,0:ip_max,ninitls) )
            allocate( rofsd(nlmagm,nspin,nspino,nspino,
     &                                     n_atom_0:n_atom_ind) )
            allocate( singulsd(nlmagm,nspin,n_atom_0:n_atom_ind) )
            if( Spinorbite .or. Hubbard ) then
              nllm0 = 1;  nllmm = nlmagm
            else
              nllm0 = 0;  nllmm = lmaxg
            endif
            allocate( rhov_self(0:nrm_self,nllm0:nllmm,nspin,
     &                nspino,nspino,n_atom_0_self:n_atom_ind_self) )
            allocate( sing_self(0:nrm_self,nllm0:nllmm,nspin,
     &                    n_atom_0_self:n_atom_ind_self) )
            allocate( rofsd_hd(-m_hubb:m_hubb,nspino,-m_hubb:m_hubb, 
     &        nspino,nspin,n_atom_0_self:n_atom_ind_self) )
            if( Green ) then
              nphiato1 = 1; nphiato7 = 1
            else
              nphiato1 = nbtm
              if( abs(Eimag(ie)) < eps10 ) then
                nphiato7 = 1
              else
                nphiato7 = 2
              endif
            endif
            allocate( phiato(nphiato1,nllm0:nllmm,nspin,nspino,natome,
     &                       nphiato7) )
            allocate( zet(nrm_tddft,nlmam_t,nspino_t,nspin_t,ninitls) )  

            rof(:,:,:,:,:) = (0._db,0._db)
            
            if( .not. Final_tddft ) then
              allocate( taull(nlmagm,nspin,nlmagm,nspin,natome) )
              phiato(:,:,:,:,:,:) = 0._db
              rhov_self(:,:,:,:,:,:) = (0._db,0._db)
              rofsd(:,:,:,:,:) = (0._db,0._db)
              rofsd_hd(:,:,:,:,:,:) = (0._db,0._db)
              sing_self(:,:,:,:) = 0._db
              singul(:,:,:,:,:,:) = (0._db,0._db)
              singulsd(:,:,:) = 0._db
              tau_ato(:,:,:,:,:) = (0._db,0._db)
              taull(:,:,:,:,:) = (0._db,0._db)  
            endif

            if( .not. ( Green .or. Final_tddft ) ) then
              Z = 2
              call clmax(Ecmax_out,rsort,lmaxso0,lmaxso,Z,.true.)
              if( Cal_xanes .and. icheck(18) > 0 )
     &                           write(3,'(/a9,i4)') ' lmaxso =', lmaxso
              lmaxso = min( lmaxso, lmaxso_max )
            endif

            if( mpirank == 0 ) then
              call CPU_TIME(time)
              tp(2) = real(time,db)
            endif

            allocate( occ_diag_t(-m_hubb:m_hubb,nspin) )

            if( Final_tddft ) then
              icheck_s = max( icheck(18), icheck(22) )
            elseif( Cal_xanes ) then
              if( Hubbard ) then
                icheck_s = max( icheck(18), icheck(26) )
              else
                icheck_s = icheck(18)
              endif
            else
              if( Hubbard ) then
                icheck_s = max(icheck(18), icheck(26), icheck(27)) - 1
              else
                icheck_s = max( icheck(18), icheck(27) ) - 1
              endif
            endif
            if( icheck_s > 0 ) write(3,190)

! Boucle sur les atomes nonequivalents dans la maille
            if( Cal_xanes ) then
              n1 = n_atom_0
            else
              n1 = n_atom_0_self
            endif 

            do iapr = n1,n_atom_ind

              if( Full_atom ) then
                ipr = iaprotoi( iapr )
              else
                ipr = iapr
                if( Cal_xanes .and. natome_self > natome ) then
                  do ia = 1,natome
                    if( iaprotoi(ia) == iapr ) exit 
                  end do
                  if( ia == natome+1 ) cycle
                endif
              endif
              Z = numat( itypepr(ipr) )
              if( Z == 0 .and. .not. Green ) cycle
              if( Z == Z_nospinorbite .and. .not. Final_tddft ) then
                Spino = .false.
              else
                Spino = Spinorbite_t
              endif
              it = itypepr(ipr)
              if( Full_atom ) then
                Absorbeur = iapr == iaabsi .and. Cal_xanes
              else
                Absorbeur = iapr == iprabs_reel .and. Cal_xanes
              endif

              if( Final_tddft ) then
                if( .not. Absorbeur ) cycle 
                zet(:,:,:,:,:) = 0._db
              endif 

              if( Hubbard .and. .not. ( Hubb(it) .or. Final_tddft ))then
                if( Cal_xanes ) then
                  icheck_t = max( icheck(18), icheck(26) )
                else
                  icheck_t = max(icheck(18), icheck(26), icheck(27)) - 1
                endif
              else
                icheck_t = icheck_s
              endif

              Hubbat = .false.
              if( Hubb(it) ) then
                if( iapr == 0 .and. Self_nonexc ) then
                  iapr1 = iprabs
                else
                  iapr1 = iapr
                end if
                if( iapr <= n_atom_ind_self ) then
                  occ_diag_t(:,:) = occ_diag(:,:,iapr1)
                  Hubbat = .true.
                else
                  Hubbat = .false.
                end if
              endif

              allocate( rato_e(0:nrm) )
              allocate( Vrato_e(0:nrm,nspin) )
              allocate( V0bd_e(nspin) )
              rato_e(:) = rato(:,it)

              lmax = lmaxat(ipr)
              if( Final_tddft ) lmax = min( lmax, lmaxabs_t )

              do is_t = 1,ninitlt

                konde(:) = sqrt( cmplx(Ecinetic(:,is_t),Eimag(ie),db) )

                Ecinetic_edge(:) = Ecinetic(:,is_t)
                Eimag_edge = Eimag(ie)
                V0bd_e(:) = V0bd(:,is_t)

                if( Final_tddft ) then
                  Energ_t = Energ(ie) - Decal_initl(is_t)
                  Enervide_t = Enervide - Decal_initl(is_t)
                  Enervide_t = max( Enervide_t, E_cut_tddft-workf ) 
                  Vrato_e(:,:) = Vrato(:,:,is_t)
                else
                  Energ_t = Energ(ie)
                  Enervide_t = Enervide
                  Vrato_e(:,:) = Vrato(:,:,iapr)
                endif

                call sphere(Absorbeur,Axe_Atom_Clui,Cal_xanes,Density,
     &            Dipmag,Ecinetic_edge,eimag_edge,Energ_t,
     &            enervide_t,Eseuil,Final_tddft,Full_atom,Green,
     &            Green_plus,Hubbat,
     &            Hubbard,iaabsi,iapr,iaprotoi,ibord,icheck_t,ie,
     &            ip_max,ip0,is_t,ipr,konde,lmax,lmaxabs_t,
     &            lmax_probe,m_hubb,n_atom_0,n_atom_0_self,
     &            n_atom_ind,n_atom_ind_self,natome,nbord,nbseuil,nbtm,
     &            ninitls,nllm0,nllmm,nlmam,nlmam_t,nlmagm,
     &            nphiato1,nphiato7,npoint,npsom,nrato(it),
     &            nrm,nrm_self,nrm_tddft,nspin,nspin_t,nspino,nspino_t,      
     &            numat(it),occ_diag_t,Octupole,phiato,posi,psii,
     &            rato_e,Relativiste,rhov_self,rmtg(ipr),rmtsd(ipr),rof,
     &            rofsd,rofsd_hd,sing_self,singul,singulsd,
     &            Solsing,Solsing_bess,Spino,State_all,
     &            tau_ato,Tddft,V_hubbard(it),Vr,Vrato_e,V0bd_e,xyz,zet)

              end do

              if( Absorbeur .and. Tddft_xanes .and. .not. Final_tddft )
     &               rof0(ie,1:nlmam_t,:,:,:) = rof(1:nlmam_t,:,:,0,:)   

              deallocate( rato_e )
              deallocate( vrato_e )
              deallocate( V0bd_e )

            end do

            deallocate( occ_diag_t )

            if( Hub_nondiag .and. .not. Final_tddft )
     &        call rotation_hubb(Cal_xanes,Full_atom,hubb,
     &          iaprotoi,icheck(26),iprabs,itypei,green,  
     &          m_hubb,n_atom_0,n_atom_0_self,n_atom_ind,
     &          n_atom_ind_self,natome,natome_self,nbord,nbtm, 
     &          nlmagm,nlmmax,nspin,nspino,ntype,numat,
     &          rot_hubb,spinorbite,tau_ato,Ylmato,.true.)

            if( mpirank == 0 ) then
              call CPU_TIME(time)
              tp(3) = real(time,db)
            endif

            if( Final_tddft ) then

              deallocate( phiato )
              deallocate( rhov_self )
              deallocate( rofsd_hd )
              deallocate( rofsd )
              deallocate( sing_self )
              deallocate( singulsd )

              nr = nrato(itabs)

              if( je == 1 ) then
                allocate( Chi(nlm_probe*nspino_t,nlm_probe*nspino_t,2,2,
     &                        ns_dipmag,ninitl,ninitl) )

                Ylmcomp = Spinorbite .or. Atom_comp(iaabsi)
                Ylmcompa = .true.  ! pour routine tenseur
                Rmtsd_abs = Rmtsd(iprabs_reel)
              endif

              allocate( Kern(nlmam_u*nspino_t,nlmam_u*nspino_t,2,2,
     &                       ns_dipmag,ninitl,ninitl) )

! Calcul du noyau
              call kernel(atomic_scr,BSE,coef_g,Core_resolved,Dipmag,
     &              dv_ex_nex,
     &              Dyn_eg,Dyn_g,fxc,Kern,Kern_fac,icheck(23),ie,
     &              imparite,itabs,lmax_t,lseuil,m_g,nbseuil,ninit1,
     &              ninitl,ninitls,nlmam_t,nlmam_u,nr,nrm,nrm_tddft,
     &              ns_dipmag,nspin_t,nspino_t,ntype,
     &              psii,rato,Rmtsd_abs,RPALF,Spinorbite_t,zet)

              deallocate( zet )

              call Cal_Chi(Chi,Chi_0,coef_g,Core_resolved,Dipmag,
     &            Energ(ie),iaabsi,icheck(25),ie,imparite,Kern,
     &            lato,lmax_probe,
     &            lmax_t,mato,mix_repr,natome,nenerg,ninit1,ninitl,
     &            ninitlu,ngrph,nlm_probe,nlmam_u,nlmamax_u,nlmsa0,
     &            nlmsam,nomfich,ns_dipmag,nspin,nspino,nspino_t,
     &            Repres_comp,Spinorbite,Tddft_mix,Tddft_so,Ylmcomp)

              deallocate( Kern )

            else

              deallocate( zet )

              Ylmcompa = Atom_comp(iaabsi)

! nspinorb = 2 si magnetique sans Spinorbite
! nspinorb = 1 si nonmagnetique ou Spinorbite
! nspino = 2 si Spinorbite

! igrph: boucle sur les representations
  
              lmaxg = 0
              do ia = 1,natome
                lmaxa(ia) = lmaxat( iaprotoi(ia) )
                lmaxg = max(lmaxg,lmaxa(ia))
              end do

              if( multi_run == 1 .or. .not. One_run ) then

                do ispin = 1,nspinorb
                  do igrph = 1,ngrph
          
                    nlmsamax = 0
                    nlmsa(:) = 0
                    do ia = 1,natome
                      Z = numat(itypei(ia)) 
                      if( .not. Green .and. Z == 0 ) cycle
                      lmaxa(ia) = lmaxat( iaprotoi(ia) )
                      n = nlmsa0(ia,igrph)
                      allocate( lval(n) )
                      lval(1:n) = lato(1:n,ia,igrph)
                      call cnlmmax(lmaxa(ia),lval,n,nlmsa(ia))
                      nlmsamax = max(nlmsamax,nlmsa(ia))
                      deallocate( lval )
                    end do
                    if( nlmsamax == 0 .and. igrph /= ngrph ) cycle

                    if( Green ) then
                      call msm(Atom_comp,Axe_Atom_Clui,Cal_xanes,
     &                  Ecinetic_edge,Eimag(ie),Full_atom,
     &                  ia_eq,ia_rep,iaabsi,iaprotoi,iato,
     &                  icheck(19),igrph,irep_util,is_eq,ispin,karact,
     &                  konde,lato,lmaxa,lmaxg,mato,n_atom_0,n_atom_ind,
     &                  natome,natomp,nb_eq,nb_rpr,nb_rep_t,
     &                  nb_sym_op,nchemin,ngrph,nlmagm,nlmsa,nlmsam,
     &                  nlmsamax,Normaltau,nspin,nspino,pos,posi,Recop,
     &                  Repres_comp(igrph),rot_atom,Solsing,
     &                  Solsing_bess,Spinorbite,State_all,Sym_cubic,
     &                  tau_ato,Tau_nondiag,taull,tpt1,tpt2)
                    else
                      n = nlmso0(igrph)
                      allocate( lval(n) )
                      lval(1:n) = lso(1:n,igrph)
                      call cnlmmax(lmaxso,lval,n,nlmso)
                      deallocate( lval )
                      call mat(Atom_axe,Atom_comp,Axe_Atom_Clui,
     &                  Basereel,Cal_xanes,cgrad,clapl,distai,E_comp,
     &                  Ecinetic_out,Eclie,Eimag(ie),Eneg,Enervide,
     &                  Etatlie,Full_atom,gradvr,Hubbard,iaabsi,
     &                  iaprotoi,iato,ibord,icheck(19),igrph,
     &                  irep_util,isbord,iso,ispin,isrt,ivois,isvois,
     &                  karact,lato,lmaxa,lmaxso,lso,mato,mpirank,mso,
     &                  natome,n_atom_0,n_atom_ind,nbm,
     &                  nbord,nbordf,nbtm,ngrph,nim,nicm,
     &                  nllm0,nllmm,nlmagm,nlmmax,nlmomax,nlmsa,nlmsam,
     &                  nlmso,nphiato1,nphiato7,npoint,npr,npsom,nsm,
     &                  nsort,nsortf,nso1,nspin,nspino,nstm,numia,nvois,
     &                  phiato,poidsa,poidso,Recop,Relativiste,
     &                  Repres_comp(igrph),rsort,rvol,Rydberg,Solsing,
     &                  Spinorbite,State_all,Sym_cubic,tau_ato,
     &                  taull,tpt1,tpt2,V0bd_out,Vr,xyz,Ylmato,Ylmso)
                    endif

                  end do  ! fin de la boucle sur les representations
                end do  ! fin de la boucle sur le spin

              endif

              if( One_run .and. Cal_xanes )
     &          call Data_one_run(Atom_comp,iabsm,iaprotoi,icheck(19),
     &              igreq,index_e,igroupi,lmax_probe,lmaxa,
     &              mpinodes,multi_run,n_atom_proto,n_multi_run,natome,
     &              nge,ngreq,ngroup,nlm_probe,nlmagm,nspin,Rot_atom,
     &              Spinorbite,taull,taull_stk)

! On passe en base locale (ou on retourne en Cluster si FDM)
              if( Hub_nondiag ) then
                if( Green ) then 
                  call rot_taull(Cal_xanes,Full_atom,Hubb,iaabsi,
     &              iaprotoi,icheck(26),iprabs,itypei,m_hubb,n_atom_0,
     &              n_atom_0_self,n_atom_ind,n_atom_ind_self,natome,
     &              natome_self,nlmagm,nspin,ntype,numat,rot_hubb,
     &              Spinorbite,State_all,tau_ato,taull)
                else
                  call rotation_hubb(Cal_xanes,Full_atom,Hubb,
     &              iaprotoi,icheck(26),iprabs,itypei,green,  
     &              m_hubb,n_atom_0,n_atom_0_self,n_atom_ind,
     &              n_atom_ind_self,natome,natome_self,nbord,nbtm, 
     &              nlmagm,nlmmax,nspin,nspino,ntype,numat,
     &              rot_hubb,spinorbite,tau_ato,Ylmato,.false.)
                endif
              endif

              if( Tddft_xanes ) then
                lm1 = 0
                do l1 = 0,lmax_t
                  if( mod(l1,2) /= imparite .and. imparite /= 2 ) cycle
                  do m1 = -l1,l1
                    lm1 = lm1 + 1
                    lmv1 = l1**2 + l1 + 1 + m1
                    lm2 = 0
                    do l2 = 0,lmax_t
                      if(mod(l2,2)/= imparite .and. imparite /= 2) cycle
                      do m2 = -l2,l2
                        lm2 = lm2 + 1
                        lmv2 = l2**2 + l2 + 1 + m2
                        imag_taull(ie,lm1,1:nspin,lm2,1:nspin)
     &                    = - aimag( taull(lmv1,1:nspin,lmv2,
     &                                          1:nspin,iaabsi) )
                      end do
                    end do
                  end do
                end do
              endif

            endif
             
            if( mpirank == 0 ) then
              call CPU_TIME(time)
              tp(4) = real(time,db)
            endif

! Calcul des tenseurs cartesiens
            if( Cal_xanes ) then

              if( Final_tddft ) then
                nlmam = nlm_probe
                allocate( roff(nlmam,nspin_t,nspino_t,0:ip_max,ninitls))
                do lm = 1,nlmam
                  roff(lm,:,:,:,:) = rof(lm,:,:,:,:)   
                end do
                deallocate( rof )
                allocate( rof(nlmam,nspin_t,nspino_t,0:ip_max,ninitls) )
                rof(:,:,:,:,:) = roff(:,:,:,:,:)  
                deallocate( roff )
                allocate( sing(nlmam,nspin,nlmam,nspin,ip0:ip_max,
     &                         nbseuil) )
                do lm1 = 1,nlmam
                  do lm2 = 1,nlmam
                    sing(lm1,:,lm2,:,:,:) = singul(lm1,:,lm2,:,:,:)   
                  end do
                end do
                deallocate( singul )
                allocate( singul(nlmam,nspin,nlmam,nspin,ip0:ip_max,
     &                           nbseuil) )
                singul(:,:,:,:,:,:) = sing(:,:,:,:,:,:)  
                deallocate( sing )
              else
                nlmam = nlmam_t
              endif

              if( Final_tddft ) then
                ndim1 = ns_dipmag
                ndim2 = ninitl
              else
                ndim1 = 1
                ndim2 = 1
              endif        
              allocate( taull_abs(nlmam*nspino_t,nlmam*nspino_t,2,2,
     &                            ndim1,ndim2,ndim2) )

              do i = 1,2

                if( i == 1 .and. ( .not. Xan_atom .or. Final_tddft ) )
     &                                                         cycle
                
                if( Final_tddft ) then
                  taull_abs(:,:,:,:,:,:,:) = Chi(:,:,:,:,:,:,:)
                else
                  taull_abs(:,:,:,:,:,:,:) = (0._db,0._db)
                  if( i == 1 ) then
                    if( Full_atom ) then
                      iapr = iaabsi
                    elseif( Nonexc ) then
                      iapr = iprabs
                    else
                      iapr = 0
                    endif
                  endif

                  do lm1 = 1,nlmam
! Pour la partie atomique, tous le signal est dans la solution
! singuliere
                    if( i == 1 .and. Solsing .and. .not. Solsing_bess )
     &                                                           exit
                    do iso1 = 1,nspino
                      lm1g = nlmam*(iso1-1) + lm1
                      do lm2 = 1,nlmam
                        do iso2 = 1,nspino
                          do isp = 1,2
                            if( Spinorbite ) then
                              iss1 = iso1; iss2 = iso2
                            else
                              iss1 = min(isp,nspin)
                              iss2 = min(isp,nspin)
                            endif
                            lm2g = nlmam*(iso2-1) + lm2

                            if( i == 1 ) then
                              taull_abs(lm1g,lm2g,isp,isp,1,1,1)
     &                            = tau_ato(lm1,iss1,lm2,iss2,iapr)
                            else                          
                              taull_abs(lm1g,lm2g,isp,isp,1,1,1)
     &                            = taull(lm1,iss1,lm2,iss2,iaabsi)
                            endif
                          end do
                        end do
                      end do
                    end do
                  end do
                endif

                call tenseur_car(coef_g,Ylmcompa,Core_resolved,E1E1,
     &            E1E2,E1E3,E1M1,E2E2,Final_tddft,Green_i,
     &            Hub_nondiag_abs,icheck(20),ip_max,ip0,is_g,lh_abs,
     &            lmoins1,lplus1,lseuil,m_g,m_hubb,M1M1,mpinodes,
     &            mpirank,nbseuil,ndim1,ndim2,ninit1,ninitl,ninitlr,
     &            ninitls,nlmam,nspin,nspin_t,nspino_t,rof,
     &            rot_atom_abs,
     &            rot_hubb_abs,secdd,secdd_m,secdo,secdo_m,secdq,
     &            secdq_m,secmd,secmd_m,secmm,secmm_m,secqq,secqq_m,
     &            Singul,Solsing,Solsing_only,Spinorbite_t,
     &            taull_abs)

                if( i == 1 ) then
          
                  sec_atom(:) = 0._db
                  do initl = 1,ninitlr
                    do j = 1,3
                      sec_atom(initl) = sec_atom(initl)
     &                            + Real(secdd(j,j,initl,mpirank), db)
                    end do
                  end do
                  sec_atom(:) = sec_atom(:) / 3

                endif

              end do

              deallocate( taull_abs )

            endif
        
            deallocate( tau_ato )
            deallocate( singul )
            deallocate( rof )

            if( .not. Final_tddft ) then

              if( Density .or. .not. Cal_xanes ) 
     &          call cal_data(Cal_xanes,drho_self,Full_atom,Hubbard,
     &            iaabsi,iaprotoi,itypei,itypepr,
     &            lla2_state,lmaxat,mpinodes,mpirank,n_atom_0,
     &            n_atom_0_self,n_atom_ind,n_atom_ind_self,n_atom_proto,
     &            natome,nllm0,nllmm,nlmagm,nrato,nrm,nrm_self,
     &            nspin,nspino,ntype,numat,rato,rhov_self,rmtsd,
     &            rofsd,sing_self,singulsd,Solsing,Solsing_only,
     &            Spinorbite,State_all_out,statedens,taull)

              icheck_t = max( icheck(27), icheck(26) - 1 ) 
              if( Hubbard .and. .not. Cal_xanes ) 
     &          call cal_data_hub(Full_atom,Hubb,iaprotoi,icheck_t,
     &            iprabs,itypei,m_hubb,mpinodes,mpirank,n_atom_0,
     &            n_atom_0_self,n_atom_ind,n_atom_ind_self,natome,
     &            natome_self,nlmagm,nspin,nspino,ntype,
     &            numat,rofsd_hd,Self_nonexc,singulsd,Solsing,
     &            statedens_hd,Spinorbite,taull)

              deallocate( phiato )
              deallocate( rhov_self )
              deallocate( rofsd_hd )
              deallocate( rofsd )
              deallocate( sing_self )
              deallocate( singulsd )
              deallocate( taull )

            endif

 1010       continue

            if( mpirank == 0 ) then        
              call CPU_TIME(time)
              tp(5) = real(time,db)
            endif

            if( mpinodes > 1 ) then

              if( Cal_xanes )
     &          call MPI_RECV_all(E1E1,E1E2,E1E3,E1M1,E2E2,M1M1,
     &                 mpinodes,mpirank,ninitlr,secdd,secdo,   
     &                 secdq,secmd,secmm,secqq)      
              if( Cal_xanes .and. Green_i )
     &          call MPI_RECV_all(E1E1,E1E2,E1E3,E1M1,E2E2,M1M1,
     &                 mpinodes,mpirank,ninitlr,secdd_m,secdo_m,   
     &                 secdq_m,secmd_m,secmm_m,secqq_m)      

              if( .not. Cal_xanes .or. Density ) then
                call MPI_BARRIER(MPI_COMM_WORLD,mpierr)
                call MPI_RECV_statedens(lla2_state,lmaxat,mpinodes,
     &                      mpirank,n_atom_0,n_atom_ind,
     &                      n_atom_proto,nspin,statedens)
              endif

              if( .not. Cal_xanes .and. Hubbard ) then
                call MPI_BARRIER(MPI_COMM_WORLD,mpierr)
                call MPI_RECV_statedens_hd(m_hubb,mpinodes,mpirank,
     &                n_atom_0_self,n_atom_ind_self,nspin,statedens_hd)
              endif

              if( .not. Cal_xanes ) then
                call MPI_BARRIER(MPI_COMM_WORLD,mpierr)
                call MPI_RECV_self(drho_self,mpinodes,mpirank,
     &                    n_atom_0_self,n_atom_ind_self,nrm_self,nspin)
              endif

              if( Tddft_xanes .and. .not. Final_tddft ) then
                call MPI_BARRIER(MPI_COMM_WORLD,mpierr)
                call MPI_RECV_tddft(imag_taull,je,mpinodes,mpirank,
     &                    nbseuil,nenerg,nlmamax,nlmamax_u,nspin,nspino,
     &                    rof0)
              endif

            endif

            do ie_computer = 0,mpinodes-1

              ie = ( je - 1 ) * mpinodes + ie_computer + 1

              if( ie > nenerg ) exit      

              if( mpirank == 0 ) then

                if( Cal_xanes ) then 
                  call write_coabs(Allsite,angxyz,axyz,Core_resolved,
     &          Dafs,E_cut,E1E1,E1E2,E1E3,E1M1,E2E2,Energ,Energphot,
     &          Extract,Epsii,Eseuil,Final_tddft,fpp_avantseuil,
     &          Full_self_abs,Green_i,Green_plus,hkl_dafs,
     &          iabsorig(multi_run),icheck(21),ie,ie_computer,
     &          Int_tens,isigpi,isymeq,jseuil,length_word,ltypcal,M1M1,
     &          Moyenne,mpinodee,n_multi_run,natomsym,nbseuil,ncolm,
     &          ncolr,ncolt,nenerg,ninit1,ninitlr,nomabs,
     &          nomfich,nomfich_cal_convt,nomfich_s,npldafs,nphi_dafs,
     &          nphim,nplr,
     &          nplrm,nseuil,nspin,numat_abs,nxanout,pdp,phdafs,
     &          phdf0t,phdt,pol,poldafse,
     &          poldafss,sec_atom,secdd,secdd_m,secdq,
     &          secdq_m,secdo,secdo_m,secmd,secmd_m,secmm,
     &          secmm_m,secqq,secqq_m,Self_abs,Spinorbite_t,Taux_eq,
     &          v0muf,vecdafse,vecdafss,vec,Volume_maille,
     &          Xan_atom)

                  if( ie == 1 ) then
                    if( Final_tddft ) then
                      nomfich_cal_tddft_conv(multi_run)
     &                                    = nomfich_cal_convt
                    else  
                      nomfich_cal_conv(multi_run) = nomfich_cal_convt
                    endif
                  endif
                endif
              
                if( ( Density .and. Cal_xanes .and.
     &                .not. Final_tddft ) .or. 
     &               (.not. Convergence .and. .not. Final_tddft) ) then
                  if( .not. Convergence ) then
                    ich = icheck(27)
                  else
                    ich = icheck(28)
                  endif
                  call cal_state(chg_cluster,
     &              chg_open_val,Cal_xanes,chargat_self,Density,
     &              drho_self,E_cut,E_Open_val,
     &              E_Open_val_exc,Energ,E_Fermi,enragr,Energ_self,
     &              Fermi,Full_atom,Hubb,iaabsi,iaprotoi,
     &              ich,ie,ie_computer,occ_hubb,Int_statedens,
     &              i_self,ispin_maj,itypei,itypepr,lamstdens,
     &              Level_val_abs,Level_val_exc,lla_state,lla2_state,
     &              lmaxat,m_hubb,mpinodes,n_atom_0,n_atom_0_self,
     &              n_atom_ind,n_atom_ind_self,n_atom_proto,natome,
     &              nb_eq,nenerg,ngreq,nomfich_s,Nonexc_g,nrato,
     &              nrm,nrm_self,nspin,ntype,numat,pop_orb_val,rato,       
     &              rho_self,rho_self_t,rmtsd,SCF_elecabs,SCF_mag_fix,
     &              Self_nonexc,State_all_out,statedens,statedens_hd)
                endif

              endif
 
              if( .not. Cal_xanes ) then
                call MPI_BARRIER(MPI_COMM_WORLD,mpierr)
                call MPI_Bcast(Fermi,1,MPI_LOGICAL,0,MPI_COMM_WORLD,
     &                 mpierr)
                call MPI_Bcast(E_cut,1,MPI_REAL8,0,MPI_COMM_WORLD,
     &                 mpierr)
 ! Si on a atteint le niveau de Fermi on sort de la boucle
                if( Fermi ) then
                   exit boucle_energ
                end if
              endif 

            end do
                
            if( mpirank == 0 ) then        
              call CPU_TIME(time)
              tp(6) = real(time,db)
              do i = 5,9
                tpt(i) = tpt(i) + tp(i-3) - tp(i-4)
              end do
            endif

          end do boucle_energ   ! Fin de la boucle sur l'energie.

          if( One_run .and. mpinodes > 1 .and. multi_run == n_multi_run 
     &                     .and. Cal_xanes .and. .not. Final_tddft )
     &                                  deallocate( taull_stk )

          if( Save_tddft_data .and. mpirank == 0 .and. Cal_xanes
     &                        .and. .not. Final_tddft )
     &      call Write_tddft_data(E_cut,imag_taull,Energ_s,multi_run,
     &                  n_multi_run,nbseuil,nenerg,nlmamax,nlmamax_u,
     &                  nomfich,nspin,nspino,rof0)
 
          if( Tddft_xanes .and. .not. Final_tddft ) then

            if( icheck(1) > 0 ) write(3,200)
 
            Final_tddft = .true.

            if( Recup_tddft_data ) call Read_tddft_data(E_cut,Energ_s,
     &                 imag_taull,mpirank,multi_run,
     &                 n_multi_run,nbseuil,nenerg,nlmamax,nlmamax_u,
     &                 nomfich_tddft_data,nspin,nspino,rof0)

! Elaboration de la grille etendue pour la Tddft
            deallocate( Energ )
            deallocate( Eimag )

            call dim_grille_tddft(Energ_s,Delta_Eseuil,Estart,nbseuil,
     &                            nenerg,nenerg_s)

            allocate( Energ(nenerg) )
            allocate( Eimag(nenerg) )

            call grille_tddft(Energ,Energ_s,Delta_Eseuil,Estart,
     &                        icheck(22),nbseuil,nenerg,nenerg_s)
            Eimag(:) = 0._db              ! for simplicity

! Calcul de Chi_0
            allocate( Chi_0(nenerg,nlmamax_u*nspino,nlmamax_u*nspino,
     &                                     nspin,nspin,ninitlr) ) 

            if( E_Fermi_man ) then
              E_cut_tddft = E_cut_imp
            else
              E_cut_tddft = E_cut
            endif

            call Chi_0_int(Chi_0,Core_resolved,Decal_initl,Delta_edge,
     &            E_cut_tddft,Ecent,EFermi_min,Elarg,Energ,Energ_s,
     &            Eseuil(nbseuil),Gamma_hole,Gamma_hole_imp,Gamma_max,
     &            Gamma_tddft,
     &            icheck(24),imag_taull,imparite,jseuil,lmaxabs_t,
     &            lseuil,mpinodes,mpirank,nbseuil,nenerg,nenerg_s,
     &            ngamh,ninit1,ninitlu,nlmamax,nlmamax_u,
     &            nspin,nseuil,nspino,numat_abs,rof0,Spinorbite)

! Valeur eventuellement decalee vers le bas pour ne rien couper a la
! convolution.
            E_cut = EFermi_min

            deallocate( imag_taull )
            deallocate( rof0 )

            goto 1000   ! On remonte au dessus de la boucle en energie

          endif

          if( Final_tddft ) then
            deallocate( Chi )    
            deallocate( Chi_0 )
          end if

          if( .not. Cal_xanes .and. Hubbard ) then                

            if( mpinodes > 1 ) then
              ndim = m * nspin * ( 2 * m_hubb + 1 )**2
              allocate( occ_hubb_r(-m_hubb:m_hubb,-m_hubb:m_hubb,nspin,
     &                                  n_atom_0_self:n_atom_ind_self) ) 
              allocate( occ_hubb_i(-m_hubb:m_hubb,-m_hubb:m_hubb,nspin,
     &                                  n_atom_0_self:n_atom_ind_self) ) 
              if( mpirank == 0 ) then
                occ_hubb_r(:,:,:,:) = real( occ_hubb(:,:,:,:), db )
                occ_hubb_i(:,:,:,:) = aimag( occ_hubb(:,:,:,:) )
              endif
              call MPI_Bcast(occ_hubb_r,ndim,MPI_REAL8,0,
     &                     MPI_COMM_WORLD,mpierr)
              call MPI_Bcast(occ_hubb_i,ndim,MPI_REAL8,0,
     &                     MPI_COMM_WORLD,mpierr)
              if( mpirank /= 0 ) occ_hubb(:,:,:,:) =
     &            cmplx( occ_hubb_r(:,:,:,:), occ_hubb_i(:,:,:,:), db )
              deallocate( occ_hubb_r ) 
              deallocate( occ_hubb_i )
            endif

            call diag(Atom_comp,Full_atom,Hubb,i_self,iaprotoi,
     &              icheck(26),iprabs,itypei,itypepr,m_hubb,
     &              n_atom_0_self,n_atom_ind_self,n_atom_proto,
     &              natome,natome_self,nspin,ntype,numat,
     &              occ_diag,occ_hubb,rot_hubb,Self_nonexc,
     &              Tau_nondiag)

          endif

! Calcul de l'energie de l agregat

          if( .not. Cal_xanes .and. mpirank == 0 ) then

            call eps_coeur(ch_coeur,En_coeur,Full_atom,iaprotoi,
     &            itypepr,lcoeur,n_atom_0,n_atom_0_self,
     &            n_atom_ind,n_atom_ind_self,n_atom_proto,natome,
     &            nrato,nrm,nspin,ntype,psi_coeur,rato,Relativiste,
     &            Vcato,Vxcato)
     

            if( i_self == 1 ) then
              En_coeur_s(:) = En_coeur(:)
              En_coeur(:) = 0._db
            else
              En_coeur(:) = En_coeur(:) - En_coeur_s(:)    
            end if  

            call En_DFT(En_cluster,Energ_self,En_coeur,excato,
     &           Full_atom,Hubb,iaprotoi,icheck(27),itypepr,m_hubb,
     &           n_atom_0,n_atom_0_self,n_atom_ind,n_atom_ind_self,
     &           n_atom_proto,natome,nb_eq,ngreq,nrm,nrm_self,nrato,
     &           nspin,ntype,numat,occ_diag,rato,rho_self,rmtsd,
     &           V_hubbard,Vcato,Vxcato)
          
            if( i_self == 1 ) then
              En_coeur(:) = 0._db
            else
              En_coeur(:) = En_coeur(:) - En_coeur_s(:)    
            end if  

          end if

! Echange des grandeurs liees a l'auto-coherence
          if( .not. Cal_xanes .and. mpinodes > 1 ) then
            m = n_atom_ind_self - n_atom_0_self + 1
            m2 = nspin * m
            n = ( 1 + nrm_self ) * nspin * m
            call MPI_BARRIER(MPI_COMM_WORLD,mpierr)
            call MPI_Bcast(rho_self,n,MPI_REAL8,0,MPI_COMM_WORLD,mpierr)
            call MPI_Bcast(chargat_self,m2,MPI_REAL8,0,
     &                                            MPI_COMM_WORLD,mpierr)
            call MPI_Bcast(Energ_self,m,MPI_REAL8,0,
     &                                            MPI_COMM_WORLD,mpierr)
            call MPI_Bcast(enragr,1,MPI_REAL8,0,MPI_COMM_WORLD,mpierr)
          endif

          if( .not. Cal_xanes ) then 

! Preparation de l'iteration suivante pour la self-consistence
            call prep_next_iter(chargat_self,chargat_self_s,
     &             Convergence,Delta_En_conv,Delta_energ,Delta_energ_s,
     &             Delta_energ_t,En_cluster,En_cluster_s,En_cluster_t,
     &             Energ_self,Energ_self_s,Fermi,Full_atom,Hubbard,
     &             i_self,icheck(27),m_hubb,
     &             mpirank,n_atom_0_self,n_atom_ind_self,n_atom_proto,
     &             natome,natomeq,nb_eq,ngreq,nrm_self,nself,nspin,
     &             occ_diag,occ_diag_s,p_self,p_self_s,
     &             p_self_t,p_self0,rho_self,rho_self_s)

          else

            deallocate( rot_hubb )
            deallocate( rot_hubb_abs )

          endif

          deallocate( drho_self )
          deallocate( statedens )
          deallocate( statedens_hd )
          deallocate( secdd, secmd, secmm, secdq, secdo, secqq )
          deallocate( secdd_m, secmd_m, secmm_m, secdq_m, secdo_m,
     &                  secqq_m )
          deallocate( sec_atom )
          deallocate( Energ )
          deallocate( Eimag )
          deallocate( konde )
          deallocate( Ecinetic );  deallocate( Ecinetic_out )
          deallocate( Ecinetic_edge ); 
          deallocate( excato )
          deallocate( ibord )
          deallocate( isbord );
          deallocate( imoy );      deallocate( imoy_out )
          deallocate( isrt )
          deallocate( iato )
          deallocate( lato );      deallocate( lmaxa )
          deallocate( iso );       deallocate( lso )
          deallocate( mato );      deallocate( mso )
          deallocate( nbord )
          deallocate( nbordf )
          deallocate( nlmsa );     deallocate( nlmsa0 )
          deallocate( nlmso0 )
          deallocate( poidsa )
          deallocate( poidso );    
          deallocate( poidsov );   deallocate( poidsov_out )
          deallocate( rho )
          deallocate( rhoato_abs )
          deallocate( rhons )
          deallocate( V0bd )
          deallocate( V0bd_out )
          deallocate( Vr )
          deallocate( Vrato )

          if( .not. One_run .or. .not. Cal_xanes  .or. 
     &                                 multi_run == n_multi_run ) then
            deallocate( Ef );   deallocate( Efato )
            deallocate( rs );   deallocate( rsato )
            deallocate( Vh );   deallocate( Vhns )
            deallocate( Vcato )    
            deallocate( Vxc );  deallocate( Vxcato )
          endif
     
          if( Final_tddft ) deallocate( fxc )

          if( .not. Green ) then
            deallocate( gradvr )
            deallocate( ylmato );  deallocate( Ylmso )
          endif
      
          deallocate( dv0bdcF )
          if( mpirank == 0 ) deallocate( Int_statedens ) 

        end do boucle_coh  ! fin de la boucle coherente
      
        if( Self_cons .and. nself > 0 .and. .not. Second_run ) then
          deallocate( ch_coeur )
          deallocate( chg_cluster )
          deallocate( energ_coh )
          deallocate( eimag_coh )
          deallocate( ispin_maj )
        end if

        deallocate( Atom_axe )
        deallocate( Atom_comp )
        deallocate( Axe_Atom_Clui )
        deallocate( chargat_init )
        deallocate( chargat_self ); deallocate( chargat_self_s )
        deallocate( distai )
        deallocate( drho_ex_nex )
        deallocate( dvc_ex_nex )
        deallocate( dv_ex_nex )
        deallocate( Energ_self ); deallocate( Energ_self_s )
        deallocate( En_coeur ); deallocate( En_coeur_s )
        deallocate( ia_eq )
        deallocate( ia_eq_inv )
        deallocate( ia_eq_inv_self )
        deallocate( ia_rep )
        deallocate( iaprotoi )
        deallocate( igroupi )
        deallocate( occ_diag )
        deallocate( occ_diag_s )
        deallocate( occ_hubb )
        deallocate( iopsym_atom )
        deallocate( is_eq )
        deallocate( itypei )
        deallocate( nb_eq )
        deallocate( nb_rpr )
        deallocate( nb_rep_t )
        deallocate( pop_orb_val )
        deallocate( posi )
        deallocate( posi_self )
        deallocate( Repres_comp )
        deallocate( rho_chg )
        deallocate( rho_self ); deallocate( rho_self_s )
	  deallocate( rho_self_t ); 
        deallocate( rhoato_init )
        deallocate( rot_atom )
        deallocate( V_abs_i )
        deallocate( vcato_init )

        if( .not. One_run .or. multi_run == n_multi_run ) then
          deallocate( cgrad )
          deallocate( clapl )
          deallocate( ivois );     deallocate( isvois )
          deallocate( lmaxat )
          deallocate( nrmtg )
          deallocate( numia )
          deallocate( rmtg )
          deallocate( rmtg0 )
          deallocate( rmtsd )
          deallocate( rvol )
          deallocate( xyz )
        endif
 
 1030   continue   ! Point d'arrivee en cas d'Extract

        deallocate( karact )

        if( Extract .and. mpirank == 0 ) then

          if( Tddft ) then
            Final_tddft = .true.
            deallocate( Epsii )
            ninitlr = 1
            allocate( Epsii(ninitlr) )
            Epsii(1) = Epsii_moy
          elseif( Core_resolved ) then
            ninitlr = ninitl
          else
            ninitlr = nbseuil
          endif
          call extract_write_coabs(Allsite,angxyz,axyz,Core_resolved,
     &      Dafs,E1E1,E1E2,E1E3,E1M1,E2E2,E_cut,Energ_s,Energphot,Epsii,
     &      Eseuil,Final_tddft,fpp_avantseuil,Full_self_abs,Green_i,
     &      Green_plus,hkl_dafs,iabsorig(multi_run),icheck(21),Int_tens,
     &      isigpi,isymeq,
     &      nsymextract(multi_run),jseuil,length_word,ltypcal,M1M1,
     &      Moyenne,mpinodee,multi_imp,n_multi_run,n_tens_max,natomsym,
     &      nbseuil,ncolm,ncolr,ncolt,nenerg_s,ninit1,ninitlr,nomabs,
     &      nomfich,nomfich_cal_conv(multi_run),nomfich_cal_convt,
     &      nom_fich_extract,nomfich_s,nphi_dafs,nphim,npldafs,nplr,
     &      nplrm,nseuil,nspin,numat_abs,nxanout,pdp,phdafs,phdf0t,
     &      phdt,pol,poldafse,poldafss,
     &      Self_abs,Spinorbite,Taux_eq,Tddft,V0muf,vecdafse,
     &      vecdafss,vec,Volume_maille,Xan_atom)

          deallocate( Energ_s )
          deallocate( eimag_s )

        endif

        deallocate( Axe_atom_clu )
        deallocate( dista )
        deallocate( iapot )
        deallocate( iaproto )
        deallocate( igroup )
        deallocate( isymeq )
        deallocate( itypep )
        deallocate( itypepr )
        deallocate( pos )
        deallocate( poseq )
        deallocate( Taux_eq )
      
        if( Cal_xanes ) then
          if( mpirank == 0 ) deallocate( Int_tens )    
          deallocate( poldafse )
          deallocate( poldafss )
          deallocate( phdafs )
          deallocate( phdf0t )    
          deallocate( phdt )
          deallocate( vecdafse )
          deallocate( vecdafss )
          deallocate( ltypcal )  
          deallocate( nomabs )         
          deallocate( pdp )    
          deallocate( pol )    
          deallocate( vec )    
        end if  

        if( .not. Extract .and. icheck(1) > 0 .and. mpirank == 0 ) then
          call CPU_TIME(time)
          tp1 = real(time,db)
          tpt(10) = tp1 - tp_init + tpt(1)
          tptt = sum( tpt(1:9) )
          do i = 1,10000
            if( tpt(10) >= tptt - 0.1_db ) exit
            tpt(10) = tpt(10) + 86400.
          end do
          itph = int( tpt(10) / 3600 )
          rtph = tpt(10) - itph * 3600
          itpm = int( rtph / 60 )
          itps = nint( rtph - itpm * 60 )
          write(3,210) multi_run
          write(3,220) (nomspr(i), tpt(i), i = 1,9)
          write(3,230) tpt1, tpt2
          if( itph > 0 .or. itpm > 0 ) then
            write(3,240) nomspr(10), tpt(10), itph, itpm, itps
          else
            write(3,220) nomspr(10), tpt(10)
          endif
        endif

      end do ! Fin boucle sur sites non equivalents

      if( Recup_tddft_data ) Convolution_cal = .false. 

      if( Convolution_cal .and. mpirank == 0 ) then

        Rewind(itape1)
        key_calc = .false.
        do l = 1,10000
          read(itape1,'(A)',iostat=istat) mot
          if( istat /= 0 ) then
            backspace(itape1)
            exit
          endif
          keyword = identmot(mot,9)
          if( keyword == 'calculati' ) key_calc = .true.
          if( keyword == 'Run_done' ) then
            backspace(itape1)
            exit
          endif
        end do

        if( key_calc ) then
          write(itape1,'(A)') ' Run_done '
          do i = 1,n_multi_run
            if( Skip_run(i) ) then
              is = 0
            else
              is = 1
            endif
            if( Run_done(i) ) then
              ir = 0
            else
              ir = 1
            endif
            write(itape1,'(2i2)') ir, is
          end do
        else
          write(itape1,'(A)') ' calculation'
          do multi_run = 1,n_multi_run
            if( Run_done(multi_run) ) cycle
            write(itape1,'(A)') nomfich_cal_conv(multi_run)
          end do
          if( Tddft .and. .not. Extract .and. ( .not. Gamma_tddft 
     &                          .or. Dafs  .or. n_multi_run > 1 ) ) then
          write(itape1,'(A)') ' cal_tddft'
          do multi_run = 1,n_multi_run
            if( Run_done(multi_run) ) cycle
            write(itape1,'(A)') nomfich_cal_tddft_conv(multi_run)
          end do
          endif
        endif
      endif

! desallocation des tableaux attribues avant la boucle multi_run

      deallocate( Decal_initl )
      deallocate( Eseuil )
      deallocate( lcoeur )
      deallocate( ncoeur )
      deallocate( popexc )
      deallocate( psi_coeur )
      deallocate( psii )
      deallocate( psival )
      deallocate( psi_open_val )
      deallocate( rato )
      deallocate( rhoit )
      deallocate( rho_coeur )
      deallocate( rho_cor )
      deallocate( Atom_mag_gr )
      deallocate( coef_g )
      deallocate( m_g )
      deallocate( is_g )
      deallocate( igreq )
      deallocate( isymqa )
      deallocate( ngreq )
      deallocate( posq )
      deallocate( chargat )
      deallocate( ngreqm )
      deallocate( popatm )
      deallocate( Run_done )
      deallocate( Skip_run )                    
      if( .not. Extract ) then
        deallocate( Energ_s )
        deallocate( eimag_s )
      endif
      deallocate( nomfich_cal_conv )
      if( Tddft ) deallocate( nomfich_cal_tddft_conv )
      deallocate( Epsii )

 1040 continue  ! Point d'arrivee en cas de mpirank /= 0 avec Extract

      if( mpirank /= 0 .and. Extract ) then
        deallocate( egamme )
        deallocate( eeient )
        deallocate( eimagent )
      endif
         
! Desallocation des tableaux alloues avant la sousroutine lectur
      deallocate( angpoldafs )
      deallocate( Atom_nsph )    
      deallocate( Axe_atom_gr )
      deallocate( com )
      deallocate( cdil )
      deallocate( ecrantage )
      deallocate( hkl_dafs )
      deallocate( hybrid )
      deallocate( iabsm )
      deallocate( iabsorig )
      deallocate( icom )
      deallocate( isigpi )
      deallocate( itdil )
      deallocate( itype )
      deallocate( its_lapw )
      deallocate( ldil )
      deallocate( lvval )
      deallocate( nlat )
      deallocate( nomclm )
      deallocate( nomfile_atom )
      deallocate( norbv )
      deallocate( nphi_dafs )
      deallocate( nposextract )
      deallocate( nrato )
      deallocate( nrato_lapw )
      deallocate( nsymextract )
      deallocate( numat )
      deallocate( nvval )
      deallocate( occ_mat_gr )
      deallocate( pdpolar )
      deallocate( polar )
      deallocate( poldafsem )
      deallocate( poldafssm )
      deallocate( pop_nonsph ) 
      deallocate( popatc )
      deallocate( popats )
      deallocate( popatv )
      deallocate( popval )
      deallocate( posn )
      deallocate( r0_lapw )
      deallocate( rchimp )
      deallocate( rlapw )
      deallocate( rmt )
      deallocate( rmtimp )
      deallocate( Rot_Atom_gr )
      deallocate( rotloc_lapw )
      deallocate( Taux_oc )
      deallocate( Hubb )
      deallocate( V_hubbard )
      deallocate( v0bdcFimp )
      deallocate( vecdafsem )
      deallocate( vecdafssm )
      deallocate( veconde )

      return
  110 format(/' Parallel computation on',i3,' processors')
  120 format(/' Sequential calculation')
  130 format(/' Number of calculated non equivalent absorbing atom =',
     &          i3)
  140 format(/1x,120('-')//,' Last cycle, XANES calculation')
  150 format(/1x,120('-')//,' Begining of cycle',i3)
  160 format(/' E_cut =',f11.5,' eV')
  170 format(///' E_kinetic =',f7.3,' eV < 0.',/
     &' Start the calculation at higher energy !'///)
  180 format(///' or E_kinetic_ext =',f7.3,' eV < 0.',/
     &' Start the calculation at higher energy !'///)
  190 format(/' ---- Sphere -------',100('-'))
  200 format(/1x,120('-')//,' Cycle Tddft')
  210 format(/,1x,120('-')//' Subroutine times for absorbing atom',i3,
     &        ' (sCPU)')
  220 format(3(4x,a6,' =',f11.3))
  230 format(4x,'Rempli =',f11.3,4x,'Triang =',f11.3)
  240 format(4x,a6,' =',f11.3,' =',i4,' h,',i3,' min,',i3,' sCPU')
      end

!***********************************************************************

      subroutine extract_write_coabs(Allsite,angxyz,axyz,Core_resolved,
     &     Dafs,E1E1,E1E2,E1E3,E1M1,E2E2,E_cut,Energ_s,Energphot,Epsii,
     &     Eseuil,Final_tddft,fpp_avantseuil,Full_self_abs,Green_int,
     &     Green_plus,hkl_dafs,iabsorig,icheck,Int_tens,isigpi,isymeq,
     &     isymext,jseuil,length_word,ltypcal,M1M1,
     &     Moyenne,mpinodee,multi_run,n_multi_run,n_tens_max,natomsym,
     &     nbseuil,ncolm,ncolr,ncolt,nenerg_s,ninit1,ninitlr,nomabs,
     &     nomfich,nomfich_cal_conv,nomfich_cal_convt,
     &     nom_fich_extract,nomfich_s,nphi_dafs,nphim,npldafs,nplr,
     &     nplrm,nseuil,nspin,numat_abs,nxanout,pdp,phdafs,phdf0t,
     &     phdt,pol,poldafse,poldafss,
     &     Self_abs,Spinorbite,Taux_eq,Tddft,V0muf,vecdafse,
     &     vecdafss,vec,Volume_maille,Xan_atom)

      use declarations
      implicit none

      integer:: iabsorig, icheck, ie, ie_computer, isymext, jseuil, 
     &   length_word, mpinodee, multi_run, n_multi_run, n_tens_max,
     &   natomsym, nbseuil, ncolm, 
     &   ncolr, ncolt, ninit1, ninitlr, nenerg_s, nseuil, nspin,  
     &   nphim, npldafs, nplr, nplrm, numat_abs, nxanout 
      integer, dimension(3,npldafs):: hkl_dafs
      integer, dimension(npldafs,2):: isigpi
      integer, dimension(npldafs):: nphi_dafs
      integer, dimension(natomsym):: isymeq

      character(len=132):: nomfich, nomfich_cal_conv, nomfich_cal_convt,
     &                     nom_fich_extract, nomfich_s
      character(len=13), dimension(nplrm):: ltypcal
      character(len=Length_word), dimension(ncolm):: nomabs
 
      complex(kind=db), dimension(npldafs,nphim):: phdf0t, phdt
      complex(kind=db), dimension(natomsym,npldafs):: phdafs
      complex(kind=db), dimension(3,nplrm):: pol
      complex(kind=db), dimension(3,npldafs,nphim):: poldafse, poldafss
      complex(kind=db), dimension(3,3,ninitlr,0:0):: secdd, secdd_m,
     &                               secmd, secmd_m, secmm, secmm_m
      complex(kind=db), dimension(3,3,3,ninitlr,0:0):: secdq, secdq_m
      complex(kind=db), dimension(3,3,3,3,ninitlr,0:0):: secdo, secdo_m,
     &                                                   secqq, secqq_m

      logical:: Allsite, Core_resolved, Dafs, E1E1, E1E2, E1E3, E1M1,
     &   E2E2, Energphot, Final_tddft, Full_self_abs, Green_int,
     &   Green_plus, M1M1, Moyenne, Self_abs, Spinorbite, Tddft,
     &   Xan_atom

      real(kind=db):: E_cut, fpp_avantseuil, V0muf, Volume_maille
      real(kind=db), dimension(3):: angxyz, axyz
      real(kind=db), dimension(nbseuil):: Eseuil
      real(kind=db), dimension(ninitlr):: Epsii, Sec_atom
      real(kind=db), dimension(nenerg_s):: Energ_s
      real(kind=db), dimension(n_tens_max,0:natomsym):: Int_tens
      real(kind=db), dimension(nplrm,2):: pdp
      real(kind=db), dimension(natomsym):: Taux_eq
      real(kind=db), dimension(3,nplrm):: vec
      real(kind=db), dimension(3,npldafs,nphim):: vecdafse, vecdafss

! Sec_atom n'est pas extrait du fichier bav.
      Sec_atom(:) = 0._db

      ie_computer = 0 
      do ie = 1,nenerg_s
        call extract_coabs(Core_resolved,E1E1,E1E2,E1E3,E1M1,E2E2,
     &            Green_int,icheck,ie,isymext,M1M1,multi_run,nenerg_s,
     &            ninit1,ninitlr,
     &            nom_fich_extract,secdd,secdd_m,secdo,secdo_m,secdq,
     &            secdq_m,secmd,secmd_m,secmm,secmm_m,secqq,secqq_m,
     &            Tddft)
        call write_coabs(Allsite,angxyz,axyz,Core_resolved,
     &          Dafs,E_cut,E1E1,E1E2,E1E3,E1M1,E2E2,Energ_s,Energphot,
     &          .true.,Epsii,Eseuil,Final_tddft,fpp_avantseuil,
     &          Full_self_abs,Green_int,Green_plus,hkl_dafs,
     &          iabsorig,icheck,ie,ie_computer,
     &          Int_tens,isigpi,isymeq,jseuil,length_word,ltypcal,M1M1,
     &          Moyenne,mpinodee,n_multi_run,natomsym,nbseuil,ncolm,
     &          ncolr,ncolt,nenerg_s,ninit1,ninitlr,nomabs,
     &          nomfich,nomfich_cal_convt,nomfich_s,npldafs,nphi_dafs,
     &          nphim,nplr,
     &          nplrm,nseuil,nspin,numat_abs,nxanout,pdp,phdafs,
     &          phdf0t,phdt,pol,poldafse,
     &          poldafss,sec_atom,secdd,secdd_m,secdq,
     &          secdq_m,secdo,secdo_m,secmd,secmd_m,secmm,
     &          secmm_m,secqq,secqq_m,Self_abs,Spinorbite,Taux_eq,v0muf,
     &          vecdafse,vecdafss,vec,Volume_maille,Xan_atom)
        if( ie == 1 ) nomfich_cal_conv = nomfich_cal_convt     
      end do

      return
      end

!***********************************************************************

      subroutine MPI_RECV_all(E1E1,E1E2,E1E3,E1M1,E2E2,M1M1,
     &                mpinodes,mpirank,  
     &                ninitlr,secdd,secdo,secdq,secmd,secmm,secqq)

      use declarations
      implicit real(kind=db) (a-h,o-z)
      include 'mpif.h'

      integer:: idim1, idim2, idim3, mpinodes, mpirank, ninitlr, rang

      complex(kind=db), dimension(3,3,ninitlr,0:mpinodes-1)::
     &                                            secdd, secmd, secmm
      complex(kind=db), dimension(3,3,3,ninitlr,0:mpinodes-1):: secdq
      complex(kind=db),dimension(3,3,3,3,ninitlr,0:mpinodes-1)::
     &                                                   secdo, secqq

      logical:: E1E1, E1E2, E1E3, E1M1, E2E2, M1M1

      real(kind=db), dimension(3,3,ninitlr,0:mpinodes-1) :: 
     &      secdd_er, secmd_er, secmm_er, secdd_ei, secmd_ei, secmm_ei
      real(kind=db), dimension(3,3,3,ninitlr,0:mpinodes-1) ::
     &                             secdq_er, secdq_ei
      real(kind=db), dimension(3,3,3,3,ninitlr,0:mpinodes-1) ::
     &                    secdo_er, secqq_er, secdo_ei, secqq_ei
  
      idim1 = 3 * 3 * ninitlr
      idim2 = 3 * idim1
      idim3 = 3 * idim2

! Recopies afin de stoquer les parties reele et imaginaire; l'indice sur
! les processeurs est en dernier

      if( E1E1 )  then
        secdd_er(:,:,:,mpirank) = real( secdd(:,:,:,mpirank),db )
        secdd_ei(:,:,:,mpirank) = aimag( secdd(:,:,:,mpirank) )
      endif

      if( E1M1 )  then
        secmd_er(:,:,:,mpirank) = real( secmd(:,:,:,mpirank),db )
        secmd_ei(:,:,:,mpirank) = aimag( secmd(:,:,:,mpirank) )
      endif

      if( M1M1 )  then
        secmm_er(:,:,:,mpirank) = real( secmm(:,:,:,mpirank),db )
        secmm_ei(:,:,:,mpirank) = aimag( secmm(:,:,:,mpirank) )
      endif

      if( E1E2 )  then
        secdq_er(:,:,:,:,mpirank) = real( secdq(:,:,:,:,mpirank),db)
        secdq_ei(:,:,:,:,mpirank) = aimag( secdq(:,:,:,:,mpirank) )
      endif

      if( E1E3 )  then
        secdo_er(:,:,:,:,:,mpirank) = 
     &                    real( secdo(:,:,:,:,:,mpirank),db )
        secdo_ei(:,:,:,:,:,mpirank) = 
     &                    aimag( secdo(:,:,:,:,:,mpirank) )
      endif

      if( E2E2 )  then
        secqq_er(:,:,:,:,:,mpirank) = 
     &                    real( secqq(:,:,:,:,:,mpirank),db )
        secqq_ei(:,:,:,:,:,mpirank) = 
     &                    aimag( secqq(:,:,:,:,:,mpirank) )
      endif

      call MPI_BARRIER(MPI_COMM_WORLD,ier)

      if( mpirank == 0 ) then 
        if( E1E1 )  then
          call MPI_GATHER(MPI_IN_PLACE,idim1,MPI_REAL8,secdd_er,
     &                idim1,MPI_REAL8,0,MPI_COMM_WORLD,mpierr)
          call MPI_GATHER(MPI_IN_PLACE,idim1,MPI_REAL8,secdd_ei,
     &                idim1,MPI_REAL8,0,MPI_COMM_WORLD,mpierr)
        endif
        if( E1M1 )  then
          call MPI_GATHER(MPI_IN_PLACE,idim1,MPI_REAL8,secmd_er,
     &               idim1,MPI_REAL8,0,MPI_COMM_WORLD,mpierr)
          call MPI_GATHER(MPI_IN_PLACE,idim1,MPI_REAL8,secmd_ei,
     &               idim1,MPI_REAL8,0,MPI_COMM_WORLD,mpierr)
        end if
        if( M1M1 )  then
          call MPI_GATHER(MPI_IN_PLACE,idim1,MPI_REAL8,secmm_er,
     &               idim1,MPI_REAL8,0,MPI_COMM_WORLD,mpierr)
          call MPI_GATHER(MPI_IN_PLACE,idim1,MPI_REAL8,secmm_ei,
     &               idim1,MPI_REAL8,0,MPI_COMM_WORLD,mpierr)
        end if
        if( E1E2 )  then
          call MPI_GATHER(MPI_IN_PLACE,idim2,MPI_REAL8,secdq_er,
     &               idim2,MPI_REAL8,0,MPI_COMM_WORLD,mpierr)
          call MPI_GATHER(MPI_IN_PLACE,idim2,MPI_REAL8,secdq_ei,
     &               idim2,MPI_REAL8,0,MPI_COMM_WORLD,mpierr)
        end if
        if( E2E2 )  then
          call MPI_GATHER(MPI_IN_PLACE,idim3,MPI_REAL8,secqq_er,
     &               idim3,MPI_REAL8,0,MPI_COMM_WORLD,mpierr)
          call MPI_GATHER(MPI_IN_PLACE,idim3,MPI_REAL8,secqq_ei,
     &               idim3,MPI_REAL8,0,MPI_COMM_WORLD,mpierr)
        end if
        if( E1E3 )  then
          call MPI_GATHER(MPI_IN_PLACE,idim3,MPI_REAL8,secdo_er,
     &               idim3,MPI_REAL8,0,MPI_COMM_WORLD,mpierr)
          call MPI_GATHER(MPI_IN_PLACE,idim3,MPI_REAL8,secdo_ei,
     &               idim3,MPI_REAL8,0,MPI_COMM_WORLD,mpierr)
        end if

      else

        if( E1E1 ) then
          call MPI_GATHER(secdd_er(1,1,1,mpirank),idim1,MPI_REAL8,
     &              secdd_er,idim1,MPI_REAL8,
     &              0,MPI_COMM_WORLD,mpierr)
          call MPI_GATHER(secdd_ei(1,1,1,mpirank),idim1,MPI_REAL8,
     &              secdd_ei,idim1,MPI_REAL8,
     &              0,MPI_COMM_WORLD,mpierr)
        endif
        if( E1M1 ) then
          call MPI_GATHER(secmd_er(1,1,1,mpirank),idim1,MPI_REAL8,
     &                secmd_er,idim1,MPI_REAL8,
     &                0,MPI_COMM_WORLD,mpierr)
          call MPI_GATHER(secmd_ei(1,1,1,mpirank),idim1,MPI_REAL8,
     &              secmd_ei,idim1,MPI_REAL8,
     &              0,MPI_COMM_WORLD,mpierr)
        end if

        if( M1M1 ) then
          call MPI_GATHER(secmm_er(1,1,1,mpirank),idim1,MPI_REAL8,
     &              secmm_er,idim1,MPI_REAL8,
     &              0,MPI_COMM_WORLD,mpierr)
          call MPI_GATHER(secmm_ei(1,1,1,mpirank),idim1,MPI_REAL8,
     &              secmm_ei,idim1,MPI_REAL8,
     &              0,MPI_COMM_WORLD,mpierr)
        end if

        if( E1E2 ) then
          call MPI_GATHER(secdq_er(1,1,1,1,mpirank),idim2,
     &              MPI_REAL8,secdq_er,
     &              idim2,MPI_REAL8,0,MPI_COMM_WORLD,mpierr)
          call MPI_GATHER(secdq_ei(1,1,1,1,mpirank),idim2,
     &              MPI_REAL8,secdq_ei,
     &              idim2,MPI_REAL8,0,MPI_COMM_WORLD,mpierr)
        end if

        if( E2E2 ) then
          call MPI_GATHER(secqq_er(1,1,1,1,1,mpirank),idim3,
     &              MPI_REAL8,secqq_er,
     &              idim3,MPI_REAL8,0,MPI_COMM_WORLD,mpierr)
          call MPI_GATHER(secqq_ei(1,1,1,1,1,mpirank),idim3,
     &              MPI_REAL8,secqq_ei,
     &              idim3,MPI_REAL8,0,MPI_COMM_WORLD,mpierr)
        end if

        if( E1E3 ) then
          call MPI_GATHER(secdo_er(1,1,1,1,1,mpirank),idim3,
     &              MPI_REAL8,secdo_er,
     &              idim3,MPI_REAL8,0,MPI_COMM_WORLD,mpierr)
          call MPI_GATHER(secdo_ei(1,1,1,1,1,mpirank),idim3,
     &              MPI_REAL8,secdo_ei,
     &              idim3,MPI_REAL8,0,MPI_COMM_WORLD,mpierr)
        end if

      end if
      
      call MPI_BARRIER(MPI_COMM_WORLD,mpierr) 

! On reconstruit les secXX; maintenant le processus central voit les
! resultats de tous les autres
      if( mpirank == 0 ) then       
        do rang = 1,mpinodes-1
          if( E1E1 ) secdd(:,:,:,rang) = cmplx(  
     &         secdd_er(:,:,:,rang), secdd_ei(:,:,:,rang),db )
          if( E1M1 ) secmd(:,:,:,rang) = cmplx(
     &         secmd_er(:,:,:,rang), secmd_ei(:,:,:,rang),db )
          if( M1M1 ) secmm(:,:,:,rang) = cmplx(
     &         secmm_er(:,:,:,rang), secmm_ei(:,:,:,rang),db )
          if( E1E2 ) secdq(:,:,:,:,rang) = cmplx( 
     &       secdq_er(:,:,:,:,rang), secdq_ei(:,:,:,:,rang),db )
          if( E2E2 ) secqq(:,:,:,:,:,rang) = cmplx( 
     &      secqq_er(:,:,:,:,:,rang), secqq_ei(:,:,:,:,:,rang),db )
          if( E1E3 ) secdo(:,:,:,:,:,rang) = cmplx( 
     &      secdo_er(:,:,:,:,:,rang), secdo_ei(:,:,:,:,:,rang),db )
        end do
      end if

      return
      end

!***********************************************************************

      subroutine MPI_RECV_statedens(lla2_state,lmaxat,mpinodes,
     &                      mpirank,n_atom_0,n_atom_ind,
     &                      n_atom_proto,nspin,statedens)

      use declarations
      implicit real(kind=db) (a-h,o-z)
      include 'mpif.h'

      integer, dimension(0:n_atom_proto):: lmaxat
      integer, dimension(0:n_atom_proto,0:mpinodes-1):: lmaxat_e

      real(kind=db), dimension(lla2_state,nspin,n_atom_0:n_atom_ind,
     &                        0:mpinodes-1):: statedens

      lmaxat_e(:,mpirank) = lmaxat(:) 
      idim = n_atom_proto + 1

      if( mpirank == 0 ) then
        call MPI_GATHER(MPI_IN_PLACE,idim,MPI_INTEGER,
     &                  lmaxat_e,idim,MPI_INTEGER,0,MPI_COMM_WORLD,
     &                  mpierr)
      else
        call MPI_GATHER(lmaxat_e(0,mpirank),idim,MPI_INTEGER,
     &                  lmaxat_e,idim,MPI_INTEGER,0,MPI_COMM_WORLD,
     &                  mpierr)
      end if

! Ceux sont les lmax correspondant a l'energie la plus grande qui sont
! les plus grands.
 
      if( mpirank == 0 ) lmaxat(:) = lmaxat_e(:,mpinodes-1)

! MPI_GATHER: le choix lorsque tous les ordinateurs envoyent le meme nombre 
! d'elements a l'ordinateur central

      idim = lla2_state * nspin * ( n_atom_ind - n_atom_0 + 1 )

      if( mpirank == 0 ) then
        call MPI_GATHER(MPI_IN_PLACE,idim,MPI_REAL8,
     &                  statedens,idim,MPI_REAL8,0,MPI_COMM_WORLD,
     &                  mpierr)
      else
        call MPI_GATHER(statedens(1,1,n_atom_0,mpirank),idim,MPI_REAL8,
     &                  statedens,idim,MPI_REAL8,0,MPI_COMM_WORLD,
     &                  mpierr)
      end if
    
      return
      end

!***********************************************************************

      subroutine MPI_RECV_statedens_hd(m_hubb,mpinodes,mpirank,
     &               n_atom_0_self,n_atom_ind_self,nspin,statedens_hd)

      use declarations
      implicit real(kind=db) (a-h,o-z)
      include 'mpif.h'

      complex(kind=db), dimension(-m_hubb:m_hubb,-m_hubb:m_hubb,nspin, 
     &       n_atom_0_self:n_atom_ind_self,0:mpinodes-1):: statedens_hd
      real(kind=db), dimension(2*m_hubb+1,2*m_hubb+1,nspin, 
     &          n_atom_0_self:n_atom_ind_self,0:mpinodes-1)::
     &                             statedens_hd_er, statedens_hd_ei

! MPI_GATHER: le choix lorsque tous les ordinateurs envoyent le meme nombre 
! d'elements a l'ordinateur central

      idim = ( ( 2 * m_hubb + 1 )**2 ) * nspin * ( n_atom_ind_self -
     &            n_atom_0_self + 1 ) 

      do m1 = -m_hubb,m_hubb 
        n1 = m1 + m_hubb + 1 
        do m2 = -m_hubb,m_hubb
          n2 = m2 + m_hubb + 1 
          statedens_hd_er(n1,n2,:,:,mpirank)
     &               = real( statedens_hd(m1,m2,:,:,mpirank),db )
          statedens_hd_ei(n1,n2,:,:,mpirank)
     &               = aimag( statedens_hd(m1,m2,:,:,mpirank) )
        end do
      end do
  
      call MPI_BARRIER(MPI_COMM_WORLD,mpierr) 
  
      if( mpirank == 0 ) then
        call MPI_GATHER(MPI_IN_PLACE,idim,MPI_REAL8,
     &                  statedens_hd_er,idim,MPI_REAL8,0,MPI_COMM_WORLD,
     &                  mpierr)
      else
        call MPI_GATHER(
     &     statedens_hd_er(1,1,1,n_atom_0_self,mpirank),idim,
     &                  MPI_REAL8,
     &                  statedens_hd_er,idim,MPI_REAL8,0,MPI_COMM_WORLD,
     &                  mpierr)
      end if

      call MPI_BARRIER(MPI_COMM_WORLD,mpierr) 

      if( mpirank == 0 ) then
        call MPI_GATHER(MPI_IN_PLACE,idim,MPI_REAL8,
     &                  statedens_hd_ei,idim,MPI_REAL8,0,MPI_COMM_WORLD,
     &                  mpierr)
      else
        call MPI_GATHER(
     &     statedens_hd_ei(1,1,1,n_atom_0_self,mpirank),idim,
     &                  MPI_REAL8,
     &                  statedens_hd_ei,idim,MPI_REAL8,0,MPI_COMM_WORLD,
     &                  mpierr)
      end if
      call MPI_BARRIER(MPI_COMM_WORLD,mpierr) 

      if( mpirank == 0 ) then       
        do i = 1,mpinodes-1
          do m1 = -m_hubb,m_hubb 
            n1 = m1 + m_hubb + 1 
            do m2 = -m_hubb,m_hubb
              n2 = m2 + m_hubb + 1 
              statedens_hd(m1,m2,:,:,i)
     &           = cmplx( statedens_hd_er(n1,n2,:,:,i), 
     &                    statedens_hd_ei(n1,n2,:,:,i),db )
            end do
          end do
        end do
      endif
    
      return
      end

!***********************************************************************
! IMPORTANT: depending on the operating system, the executable may run out
!     of virtual memory as the dummy arrays (such as drho_self) and the 
!     temporary ones are created in 
!     the stack (i.e. static memory), which is subject to a limited available space. 
!     Whereas a sequential application would normally return an error message,
!     a parallel MPI one would crash without any indication. 
!     In order to avoid such problems it is advisable to force your compiler use
!     dynamic allocation even for temporary arrays, at least for those whose size
!     exceeds a certain limit that you may indicate at compilation time.
!     Should you use this trick, make sure that you compile and run the program 
!     on the very same machine. This operation might have a price to pay in
!     terms of performance.
 
!     Linux Intel compiler for Itanium based applications: -heap-array[:size]

      subroutine MPI_RECV_self(drho_self,mpinodes,mpirank,
     &                   n_atom_0_self,n_atom_ind_self,nrm_self,nspin)
       
      use declarations
      implicit real(kind=db) (a-h,o-z)
      include 'mpif.h'

      integer rang

      real(kind=db), dimension(0:nrm_self,nspin,
     &           n_atom_0_self:n_atom_ind_self,0:mpinodes-1):: drho_self
      real(kind=db), dimension(0:nrm_self,nspin,0:mpinodes-1)::
     &                                   drho_self_e

      idim1 = ( nrm_self + 1 ) * nspin 

      do ia = n_atom_0_self,n_atom_ind_self

        drho_self_e(:,:,mpirank) = drho_self(:,:,ia,mpirank)

! Ici la barriere est tres importante: si on l'ommet on risque d'utiliser le
!    meme buffer rhov_self_eX par deux processus differents aux iapr differents

        call MPI_BARRIER(MPI_COMM_WORLD,mpierr) 
          
        if( mpirank == 0 ) then 
          call MPI_GATHER(MPI_IN_PLACE,idim1,MPI_REAL8,
     &                    drho_self_e,idim1,MPI_REAL8,
     &                    0,MPI_COMM_WORLD,mpierr)
        else
          call MPI_GATHER(drho_self_e(0,1,mpirank),
     &                    idim1,MPI_REAL8,drho_self_e,
     &                    idim1,MPI_REAL8,0,MPI_COMM_WORLD,mpierr)
        end if

        call MPI_BARRIER(MPI_COMM_WORLD,mpierr)

        if( mpirank == 0 ) then
          do rang = 1,mpinodes-1
            drho_self(:,:,ia,rang) = drho_self_e(:,:,rang)
          end do
        end if

! Cette barriere est importante, car drho_self_e est utilise en tant que
! buffer et ne devrait pas etre remplie pour deux ia differents 
        call MPI_BARRIER(MPI_COMM_WORLD,mpierr) 

      end do
               
      return
      end

!***********************************************************************

      subroutine MPI_RECV_tddft(imag_taull,je,mpinodes,mpirank,
     &                    nbseuil,nenerg,nlmamax,nlmamax_u,nspin,nspino,
     &                    rof0)

      use declarations
      implicit real(kind=db) (a-h,o-z)
      include 'mpif.h'

      integer:: ie, ie_computer, je, mpinodes, mpirank, ndim, nbseuil,
     &          nenerg, nlmamax, nlmamax_u, nspin, nspino

      complex(kind=db), dimension(nenerg,nlmamax,nspin,nspino,nbseuil)::
     &                                                    rof0

      real(kind=db), dimension(nenerg,nlmamax_u,nspin,nlmamax_u,nspin)::
     &                                                    imag_taull
      real(kind=db), dimension(nlmamax_u,nspin,nlmamax_u,nspin)::
     &                                                    imag_taull_e
      real(kind=db), dimension(nlmamax,nspin,nspino,nbseuil):: rof0_i,
     &                                                         rof0_r

      ndim = ( nlmamax_u * nspin )**2 

      do ie_computer = 0,mpinodes-1

        ie = ( je - 1 ) * mpinodes + ie_computer + 1
        if( ie > nenerg ) exit      

        if( ie_computer == mpirank ) imag_taull_e(:,:,:,:)
     &                             = imag_taull(ie,:,:,:,:) 

        call MPI_Bcast(imag_taull_e,ndim,MPI_REAL8,ie_computer,
     &                                           MPI_COMM_WORLD,mpierr)

        call MPI_BARRIER(MPI_COMM_WORLD,mpierr)

        if( ie_computer /= mpirank ) imag_taull(ie,:,:,:,:)
     &                             = imag_taull_e(:,:,:,:) 

      end do
 
      ndim = nlmamax * nspin * nspino * nbseuil 

      do ie_computer = 0,mpinodes-1

        ie = ( je - 1 ) * mpinodes + ie_computer + 1
        if( ie > nenerg ) exit      

        if( ie_computer == mpirank ) then
          rof0_r(:,:,:,:) = real( rof0(ie,:,:,:,:),db )
          rof0_i(:,:,:,:) = aimag( rof0(ie,:,:,:,:) )
        endif

        call MPI_Bcast(rof0_r,ndim,MPI_REAL8,ie_computer,
     &                                           MPI_COMM_WORLD,mpierr)
        call MPI_Bcast(rof0_i,ndim,MPI_REAL8,ie_computer,
     &                                           MPI_COMM_WORLD,mpierr)

        call MPI_BARRIER(MPI_COMM_WORLD,mpierr)

        if( ie_computer /= mpirank ) rof0(ie,:,:,:,:)
     &                   = cmplx( rof0_r(:,:,:,:), rof0_i(:,:,:,:),db )

      end do

      return
      end

!***********************************************************************

      subroutine Write_tddft_data(E_cut,imag_taull,Energ,multi_run,
     &                  n_multi_run,nbseuil,nenerg,nlmamax,nlmamax_u,
     &                  nomfich,nspin,nspino,rof0)

      use declarations
      implicit none
      include 'mpif.h'

      character(len=132):: nomfich, nomfich_tddft_data

      integer:: ie, long, nbseuil,multi_run, n_multi_run,
     &          nenerg, nlmamax, nlmamax_u, nspin, nspino

      complex(kind=db), dimension(nenerg,nlmamax,nspin,nspino,nbseuil)::
     &                                                    rof0

      real(kind=db):: E_cut
      real(kind=db), dimension(nenerg):: Energ
      real(kind=db), dimension(nenerg,nlmamax_u,nspin,nlmamax_u,nspin)::
     &                                                    imag_taull

      nomfich_tddft_data = nomfich

      long = len_trim(nomfich_tddft_data)
      nomfich_tddft_data(long+1:long+15) = '_tddft_data.txt'

      if( multi_run == 1 ) open(20, file = nomfich_tddft_data )

      write(20,*) E_cut*Rydb, nenerg, Energ(nenerg)*Rydb

      do ie = 1,nenerg
        write(20,*) Energ(ie)*Rydb 
        write(20,*) imag_taull(ie,:,:,:,:)
      end do 
      do ie = 1,nenerg
        write(20,*) Energ(ie)*Rydb 
        write(20,*) rof0(ie,:,:,:,:)
      end do 

      if( multi_run == n_multi_run ) close(20)

      return
      end

!***********************************************************************

      subroutine Recup_nenerg(Energ_max,mpirank,nenerg_s,
     &                        nomfich_tddft_data)

      use declarations
      implicit real(kind=db) (a-h,o-z)
      include 'mpif.h'

      character(len=132):: nomfich_tddft_data

      integer:: istat, mpirank, nenerg_s

      real(kind=db):: E_cut, Energ_max

      if( mpirank == 0 ) then

        open(20, file = nomfich_tddft_data, status='old', iostat=istat) 
        if(istat/=0) call write_open_error(nomfich_tddft_data,istat,1)

        read(20,*) E_cut, nenerg_s, Energ_max
        Energ_max = Energ_max / Rydb

        Close(20)

      endif
 
      call MPI_Bcast(nenerg_s,1,MPI_INTEGER,0,MPI_COMM_WORLD,mpierr)
      call MPI_Bcast(Energ_max,1,MPI_REAL8,0,MPI_COMM_WORLD,mpierr)

      call MPI_BARRIER(MPI_COMM_WORLD,mpierr)

      return
      end

!***********************************************************************

      subroutine Read_tddft_data(E_cut,Energ,
     &                 imag_taull,mpirank,multi_run,
     &                 n_multi_run,nbseuil,nenerg,nlmamax,nlmamax_u,
     &                 nomfich_tddft_data,nspin,nspino,rof0)

      use declarations
      implicit real(kind=db) (a-h,o-z)
      include 'mpif.h'

      character(len=132):: nomfich_tddft_data

      integer:: ie, istat, mpirank, multi_run, n_multi_run, nbseuil,
     &          ndim, nenerg, nlmamax, nlmamax_u, nspin, nspino

      complex(kind=db), dimension(nenerg,nlmamax,nspin,nspino,nbseuil)::
     &                                                    rof0

      real(kind=db):: E_cut
      real(kind=db), dimension(nenerg):: Energ
      real(kind=db), dimension(nlmamax,nspin,nspino,nbseuil):: rof0_i,
     &                                                         rof0_r
      real(kind=db), dimension(nenerg,nlmamax_u,nspin,nlmamax_u,nspin)::
     &                                                    imag_taull
      real(kind=db), dimension(nlmamax_u,nspin,nlmamax_u,nspin)::
     &                                                    imag_taull_e

      if( mpirank == 0 ) then

        if( multi_run == 1 ) then
          open(20, file =nomfich_tddft_data, status='old', iostat=istat) 
          if(istat/=0) call write_open_error(nomfich_tddft_data,istat,1)
        endif

        read(20,*) E_cut

        do ie = 1,nenerg
          read(20,*) Energ(ie)
          read(20,*) imag_taull(ie,:,:,:,:)
        end do 

        do ie = 1,nenerg
          read(20,*) 
          read(20,*) rof0(ie,:,:,:,:)
        end do

        E_cut = E_cut / Rydb 
        Energ(:) = Energ(:) / Rydb 

        if( multi_run == n_multi_run ) close(20)

      endif

      call MPI_Bcast(E_cut,1,MPI_REAL8,0,MPI_COMM_WORLD,mpierr)

      call MPI_BARRIER(MPI_COMM_WORLD,mpierr)

      call MPI_Bcast(Energ,nenerg,MPI_REAL8,0,MPI_COMM_WORLD,mpierr)

      call MPI_BARRIER(MPI_COMM_WORLD,mpierr)

      ndim = ( nlmamax_u * nspin )**2 

      do ie = 1,nenerg

        if( mpirank == 0 ) imag_taull_e(:,:,:,:)= imag_taull(ie,:,:,:,:) 

        call MPI_Bcast(imag_taull_e,ndim,MPI_REAL8,0,MPI_COMM_WORLD,
     &                                                       mpierr)

        call MPI_BARRIER(MPI_COMM_WORLD,mpierr)

        if( mpirank /= 0) imag_taull(ie,:,:,:,:) = imag_taull_e(:,:,:,:) 

      end do

      ndim = nlmamax * nspin * nspino * nbseuil 

      do ie = 1,nenerg

        if( mpirank == 0 ) then
          rof0_r(:,:,:,:) = real( rof0(ie,:,:,:,:),db )
          rof0_i(:,:,:,:) = aimag( rof0(ie,:,:,:,:) )
        endif

        call MPI_Bcast(rof0_r,ndim,MPI_REAL8,0,MPI_COMM_WORLD,mpierr)
        call MPI_Bcast(rof0_i,ndim,MPI_REAL8,0,MPI_COMM_WORLD,mpierr)

        call MPI_BARRIER(MPI_COMM_WORLD,mpierr)

        if( mpirank /= 0 ) rof0(ie,:,:,:,:)
     &                   = cmplx( rof0_r(:,:,:,:), rof0_i(:,:,:,:),db )

      end do

      return
      end
